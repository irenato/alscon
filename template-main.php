<?php
/*
 *Template Name: Home page template
 */
?>
<?php get_header(); ?>
<?php if (ICL_LANGUAGE_CODE == 'en') {
    $home_page = 1607;
} else $home_page = 1843;
?>
<!--START CONTENT-->
<section class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="services">
                <div class="content scroll" id="scroll-bottom" data-anchor="scroll-bottom">
                    <h2 class="title"><?= get_field('services_title', $home_page) ?></h2>
                    <div class="slider">
                        <div id="servise-title"><h4 class="mini-title"><?= __('Illustrations', 'titles') ?></h4></div>
                        <div class="center">

                            <?= custom_services(); ?>

                        </div>
                        <div class="arrow-slider">
                            <span class="prev-arrow"></span>
                            <span class="next-arrow"></span>
                        </div>
                        <div id="servise-desc">
                            <h5>
                                <strong><?= __('Illustrations', 'titles') ?> </strong><?= __('— We know how to draw seven red lines, all perpendicular, some with green ink, some with transparent.', 'titles') ?>
                            </h5>
                        </div>
                    </div>
                    <a id="service-link" href="<?php if (ICL_LANGUAGE_CODE == 'en') {
                        echo get_permalink(1746);
                    } else echo get_permalink(1845); ?>" class="button"><?= __('DETAILS...', 'buttons') ?></a>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="wrapper">
    <div class="container-fluid">
        <div class="our-work" id="portfolio">
            <h2 class="title "><?= get_field('portfolio_title', $home_page) ?></h2>
        </div>
        <!-- Codrops top bar -->
        <section class="row">
            <?php
            $query = new WP_Query(array(
                'post_type' => 'our-portfolio',
                'posts_per_page' => 12,
                'orderby' => 'rand',
                'order' => 'ASC',
            )); ?>

            <ul id="da-thumbs" class="da-thumbs">
                <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
                    <?php $category = get_the_terms($id, 'portfolio-category');
                    if ($category[0]->slug == 'our_works') {
                        $category_class = $category[1]->slug;
                    } else {
                        $category_class = $category[0]->slug;
                    } ?>
                    <li>
                        <a href="<?php the_permalink(); ?>">
                            <img src="<?php the_post_thumbnail_url(); ?>"/>
                            <div data-bg='<?= get_field("hover-color") ?>'
                                 data-colorInner='<?= get_field("text_color") ?>'>
                                <span><?php the_title(); ?></span>
                                <p><?= get_field('objective') ?></p>
                            </div>
                        </a>
                    </li>
                <?php endwhile; ?>
                <?php endif; ?>
                <?php wp_reset_query(); ?>

            </ul>
        </section>
    </div>
</section>


<section class="wrapper slider-comment" id="reviews">
    <div class="bg-slider">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-sx-12">
                    <h5><?= get_field('reviews_title', $home_page) ?></h5>
                    <div class="comment">

                      <?= custom_reviews(); ?>

                    </div>
                    <div class="arrow-slider">
                        <span class="arrow-prev"></span>
                        <span class="arrow-next"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="wrapper why" id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <div class="why-are-we">
                    <?= get_field('about_title', $home_page) ?>
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 first-reason">
                <span class="fake-reason" style="display: none"><?= get_field('about_fake_1', $home_page) ?></span>
                <span class="real-reason" style="display: none"><?= get_field('about_us_1', $home_page) ?></span>
                <h5><span><?= get_field('count_1', $home_page) ?></span>
                    <div class="reason-hover"><?= get_field('about_us_1', $home_page) ?></div>
                </h5>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 first-reason">
                <span class="fake-reason" style="display: none"><?= get_field('about_fake_2', $home_page) ?></span>
                <span class="real-reason" style="display: none"><?= get_field('about_us_2', $home_page) ?></span>
                <h5><span><?= get_field('count_2', $home_page) ?></span>
                    <div class="reason-hover"><?= get_field('about_us_2', $home_page) ?></div>
                </h5>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 first-reason">
                <span class="fake-reason" style="display: none"><?= get_field('about_fake_3', $home_page) ?></span>
                <span class="real-reason" style="display: none"><?= get_field('about_us_3', $home_page) ?></span>
                <h5><span><?= get_field('count_3', $home_page) ?></span>
                    <div class="reason-hover"><?= get_field('about_us_3', $home_page) ?></div>
                </h5>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 first-reason">
                <span class="fake-reason" style="display: none"><?= get_field('about_fake_4', $home_page) ?></span>
                <span class="real-reason" style="display: none"><?= get_field('about_us_4', $home_page) ?></span>
                <h5><span><?= get_field('count_4', $home_page) ?></span>
                    <div class="reason-hover"><?= get_field('about_us_4', $home_page) ?></div>
                </h5>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 first-reason">
                <span class="fake-reason" style="display: none"><?= get_field('about_fake_5', $home_page) ?></span>
                <span class="real-reason" style="display: none"><?= get_field('about_us_5', $home_page) ?></span>
                <h5><span><?= get_field('count_5', $home_page) ?></span>
                    <div class="reason-hover"><?= get_field('about_us_5', $home_page) ?></div>
                </h5>
            </div>
            <div class="col-lg-4 col-md-4  col-sm-4 first-reason">
                <span class="fake-reason" style="display: none"><?= get_field('about_fake_6', $home_page) ?></span>
                <span class="real-reason" style="display: none"><?= get_field('about_us_6', $home_page) ?></span>
                <h5><span><?= get_field('count_6', $home_page) ?></span>
                    <div class="reason-hover"><?= get_field('about_us_6', $home_page) ?></div>
                </h5>
            </div>
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <a id="contact_us_link" href="#contacts" class="button"><?php _e('Contact us', 'buttons') ?></a>
            </div>

        </div>
    </div>
</section>
<section id="global-map">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <h4><?= get_field('map_title', $home_page) ?></h4>
                    <div id="map">

                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<section class="wrapper bg" id="partners">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <h5><?= get_field('partners_title', $home_page) ?></h5>

                <div class="class-par">

                    <?php masterslider(3); ?>

                </div>
            </div>
        </div>
    </div>
</section>
<section class="wrapper leave-comments" id="contacts">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?= get_field('contacts_slogan', $home_page) ?>
            </div>
            <div class="col-lg-7 col-md-8 col-sm-7 col-xs-12">
                <?php if (ICL_LANGUAGE_CODE == 'en') {
                    echo do_shortcode('[contact-form-7 id="1822" title="Contact form 1"]');
                } else {
                    echo do_shortcode('[contact-form-7 id="1824" title="Contact Form ru"]');
                } ?>
            </div>
            <div class="col-lg-5 col-md-4 col-sm-5 col-xs-12">
                <ul>
                    <li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i> <?= get_option("email_"); ?></a>
                    </li>
                    <li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i> <?= get_option("skype"); ?></a></li>
                    <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i> <?= get_option("phone_"); ?></a>
                    </li>
                </ul>
                <a href="<?= get_option("freelancer_link"); ?>"><img
                        src="<?= get_template_directory_uri() ?>/images/freelancer.com.png" alt=""></a>
                <a href="<?= get_option("upwork_link"); ?>"><img
                        src="<?= get_template_directory_uri() ?>/images/upwork-2.png" alt=""></a>
            </div>
        </div>
    </div>
</section>
<a href="#" class="scroll-top fixed" data-scroll="scroll-top"><i class="fa  fa-chevron-up" aria-hidden="true"></i></a>
<!--END CONTENT-->
<?php get_footer(); ?>
