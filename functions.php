<?php
require_once get_template_directory() . '/included-scripts.php';
require_once get_template_directory() . '/custom-func.php';



remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links_extra', 3);


add_theme_support( 'post-thumbnails' );


//registration menu
if ( ! function_exists( 'own_theme_setup' ) ) :
    function own_theme_setup() {
           register_nav_menus( array(
            'main_menu' => __( 'Main Menu'),
            'sub_menu' => __('Sub menu'),
        ));
        function my_wp_nav_menu_args( $args='' ){
            $args['container'] = '';
            return $args;
        }
        add_filter( 'wp_nav_menu_args', 'my_wp_nav_menu_args' );
    }
endif; // theme_setup
add_action( 'after_setup_theme', 'own_theme_setup' );

// change logo and main video
function themeslug_theme_customizer( $wp_customize ) {
    $wp_customize->add_section( 'themeslug_logo_section' , array(
        'title'       => __( 'Logo', 'themeslug' ),
        'priority'    => 30,
        'description' => 'Upload a logo to replace the default site name and description in the header',
    ));
    $wp_customize->add_setting( 'themeslug_logo' );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'themeslug_logo', array(
        'label'    => __( 'Logo', 'themeslug' ),
        'section'  => 'themeslug_logo_section',
        'settings' => 'themeslug_logo',
    ) ) );
    $wp_customize->add_section( 'themeslug_logo_color_section' , array(
        'title'       => __( 'Logo in color', 'themeslug' ),
        'priority'    => 30,
        'description' => 'Upload a logo to replace the default site name and description in the header',
    ));
    $wp_customize->add_setting( 'themeslug_logo_color' );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'themeslug_logo_color', array(
        'label'    => __( 'Logo in color', 'themeslug' ),
        'section'  => 'themeslug_logo_color_section',
        'settings' => 'themeslug_logo_color',
    ) ) );

    $wp_customize->add_section( 'themeslug_video_section' , array(
        'title'       => __( 'Video', 'themeslug' ),
        'priority'    => 30,
        'description' => 'Upload a video to replace the default background video in the header',
    ));
    $wp_customize->add_setting( 'themeslug_video' );
    $wp_customize->add_control( new WP_Customize_Upload_Control($wp_customize, 'themeslug_video', array(
        'label'    => __( 'Video', 'themeslug' ),
        'section'  => 'themeslug_video_section',
        'settings' => 'themeslug_video',
    ) ) );
    $wp_customize->add_section( 'themeslug_mobile_vert_video' , array(
        'title'       => __( 'Video vertical', 'themeslug' ),
        'priority'    => 30,
        'description' => 'Upload a video to replace the default mobile background video in the header',
    ));
    $wp_customize->add_setting( 'themeslug_mobile_vert' );
    $wp_customize->add_control( new WP_Customize_Upload_Control($wp_customize, 'themeslug_mobile_vert', array(
        'label'    => __( 'Video vertical', 'themeslug' ),
        'section'  => 'themeslug_mobile_vert_video',
        'settings' => 'themeslug_mobile_vert',
    ) ) );

    $wp_customize->add_section( 'themeslug_mobile_goriz_video' , array(
        'title'       => __( 'Video gorizontal', 'themeslug' ),
        'priority'    => 30,
        'description' => 'Upload a video to replace the default mobile background video in the header',
    ));
    $wp_customize->add_setting( 'themeslug_mobile_goriz' );
    $wp_customize->add_control( new WP_Customize_Upload_Control($wp_customize, 'themeslug_mobile_goriz', array(
        'label'    => __( 'Video gorizontal', 'themeslug' ),
        'section'  => 'themeslug_mobile_goriz_video',
        'settings' => 'themeslug_mobile_goriz',
    ) ) );

}
add_action( 'customize_register', 'themeslug_theme_customizer' );


add_action('init', 'sections_register');
function sections_register() {
    register_post_type( 'services' ,
        array(
            'label' => 'Services',
            'singular_label' => 'service',
            'public' => true,
            'show_ui' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'rewrite' => true,
            'menu_position' => 4,
            'menu_icon' =>'dashicons-dashboard',
            'supports' => array('title', 'thumbnail'
            ),
        )
    );
    register_post_type( 'our-portfolio' ,
        array(
            'label' => __( 'Portfolio' ),
            'singular_label' => 'portfolio',
            'public' => true,
            'show_ui' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'rewrite' => true,
            'menu_position' => 4,
            'menu_icon' =>'dashicons-format-image',
            'supports' => array('title', 'thumbnail'
            ),
        )
    );
    register_taxonomy( 'portfolio-category',
        'our-portfolio',
        array( 'hierarchical' => true,
            'label' => __( 'Categories portfolio' ),
            'sort' => true,
            'args' => array( 'orderby' => 'term_order' ),
            'rewrite' => true
        )
    );
    register_post_type( 'our-reviews' ,
        array(
            'label' => 'Our reviews',
            'singular_label' => 'review',
            'public' => true,
            'show_ui' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'rewrite' => true,
            'menu_position' => 4,
            'menu_icon' =>'dashicons-thumbs-up',
            'supports' => array('title', 'thumbnail'
            ),
        )
    );
}


add_filter('upload_dir', 'awesome_wallpaper_dir');

function awesome_wallpaper_dir( $param ){

    $param['path'] = str_replace ('alscon-web.com/wp-content', 'img.alscon-web.com',  $param['path']);
    $param['url'] = str_replace ('http://alscon-web.com/wp-content', 'http://img.alscon-web.com',  $param['url']);
    $param['basedir'] =str_replace ('alscon-web.com/wp-content', 'img.alscon-web.com',  $param['basedir']);
    $param['baseurl'] =str_replace ('http://alscon-web.com/wp-content', 'http://img.alscon-web.com',  $param['baseurl']);

    return $param;

}


function add_async_attribute($tag)
{
    return str_replace('src', 'defer  src', $tag);
}

add_filter('script_loader_tag', 'add_async_attribute', 10, 2);


function remove_style () {
    wp_deregister_style('contact-form-7');
    wp_deregister_style('cff');
    wp_deregister_style('language-selector');
}

add_action ('wp_print_styles','remove_style',100);

function for_slider(){
    wp_deregister_style('masterslider-main');
//    wp_deregister_script('masterslider-core');

}

add_action ('masterslider_enqueue_styles','for_slider',100);




