<?php
/*
 *Template Name: Portfolio page template
 */
?>
<?php get_header(); ?>
    <!--START CONTENT-->
    <section class="wrapper min-h">
        <div class="container-fluid">
            <div class="row">
                <div class="gallery">
                    <div class="our-work">
                        <h2 class="title"><strong><?= __('OUR', 'titles') ?></strong> <?= __('WORKS', 'titles') ?></h2>
                        <ul class="menu-gallery <?php if (ICL_LANGUAGE_CODE == 'ru') echo 'ru-class' ?>">
                            <li class="filter" data-filter="all">
                                <?php _e('All', 'our_works'); ?>
                            </li>

                            <?php $args = array('taxonomy' => 'portfolio-category',
                                'hide_empty' => 0); ?>

                            <?php $categories = get_categories($args);
                            $parent_category = array();?>

                            <?php foreach ($categories as $category) : ?>
                                <?php if($category->category_parent!==0){
                                    $sub_class='sub_class';
                                } else{
                                    $sub_class ='';
                                    $parent_category[]=$category->cat_ID;
                                }?>

                                <?php if ($category->slug == $_GET['class']) {
                                    $class = 'active';
                                } else {
                                    $class = '';
                                }
                                ?>

                                <li class="filter <?= $class . $sub_class; ?>" data-sort="random"
                                    data-filter=".<?= $category->slug; ?>">
                                    <?php echo $category->name; ?>
                                </li>

                            <?php endforeach; ?>
                        </ul>
                        <ul class="menu-gallery-mob">
                            <li class="filter" data-filter="all">
                                <?php _e('All', 'our_works'); ?>
                            </li>
                            <?php $parent_cat_info = get_categories(array('taxonomy' => 'portfolio-category',
                                'hide_empty' => 0, 'include'      => implode(',',$parent_category),));
//                            print_r($parent_cat_info);
                            foreach($parent_cat_info as $parent_info){
                                $sub_category = get_categories(array('taxonomy' => 'portfolio-category',
                                    'hide_empty' => 0, 'child_of' => $parent_info->term_id));
                                if ($parent_info->slug == $_GET['class']) {
                                    $parent_class = 'active';
                                } else {
                                    $parent_class = '';
                                }
                                ?>
                                <li class="filter <?= $parent_class  ?>" data-sort="random"
                                    data-filter=".<?= $parent_info->slug; ?>">
                                    <span class="open-category fa fa-chevron-down"></span>
                                    <?php echo $parent_info->name; ?>
                                    <ul class="sub-menu-gallery-mob">
                                        <?php foreach($sub_category as $sub){
                                            if ($sub->slug == $_GET['class']) {
                                                $sub_cat_class = 'active';
                                            } else {
                                                $sub_cat_class = '';
                                            } ?>
                                            <li class="filter <?= $sub_cat_class ?>" data-sort="random"
                                                data-filter=".<?= $sub->slug; ?>">
                                                <?php echo $sub->name; ?>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </li>

                          <?php  }

                            ?>

                        </ul>

                    </div>

                    <?php
                    $query = new WP_Query(array(
                        'post_type' => 'our-portfolio',
                        'posts_per_page' => -1,
                        'orderby' => 'ID',
                        'order' => 'ASC',
                    ));
                    if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
                        <?php $id = get_the_ID(); ?>
                        <?php $category = get_the_terms($id, 'portfolio-category'); ?>

                        <?php //if($category[0]->slug == 'our_works'){
//                                $category_class = $category[1]->slug;
//                            }else{
//                                $category_class = $category[0]->slug;
//                            } ?>

                        <?php $item_categories = array();

                        foreach ($category as $category_class) {
                            $item_categories[] .= $category_class->slug;
                        };

                        if (in_array($_GET['class'], $item_categories)) : ?>

                            <div
                                class="mix portfolio_item <?php foreach ($item_categories as $item) {
                                    echo $item . ' ';
                                } ?>" style="display : inline-block">
                                    <a href="<?php the_permalink(); ?>">
                                        <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>"/>
                                        <!--                                        <p class='view'></p>-->
                                    </a>
                            </div>

                        <?php else: ?>

                            <div
                                class="mix portfolio_item <?php foreach ($item_categories as $item) {
                                    echo $item . ' ';
                                } ?>" style="display : none">
                                    <a href="<?php the_permalink(); ?>">
                                        <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>"/>
                                    </a>
                            </div>

                        <?php endif; ?>

                    <?php endwhile; ?>

                    <?php endif; ?>

                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <hr class="sline">

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--END CONTENT-->

<?php get_footer(); ?>