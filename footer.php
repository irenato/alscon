<?php wp_footer(); ?>
<!--START FOOTER-->
<footer class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul>
                    <li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i> <?= get_option("email_"); ?></a>
                    </li>
                    <li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i> <?= get_option("skype"); ?></a></li>
                    <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i> <?= get_option("phone_"); ?></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!--END FOOTER-->

</div>

<?php if (is_single()) { ?>
    <div id="zoom" style="display: none"><?= get_field('zoom') ?></div>
    <div id="center" style="display: none"><?= get_field('centre_coordinates') ?></div>
    <div id="position" style="display: none"><?= get_field('coordinates') ?></div>
    <div id="title" style="display: none"><?= get_field('title', $id); ?></div>
    <div id="icon" style="display: none"><?= get_template_directory_uri(); ?></div>
    <div id=""></div>
<?php } ?>

<!--MODAl START-->
<div class="modal">
    <div class="modal-container">
        <i class="fa fa-times close" aria-hidden="true"></i>
        <h4><?= __('Leave the application and', 'titles') ?>
            <strong><?= __(' our ', 'titles') ?></strong><?= __('specialists will contact you', 'titles') ?></h4>
        <?php if (ICL_LANGUAGE_CODE == 'en') {
            echo do_shortcode('[contact-form-7 id="1822" title="Contact form 1"]');
        } else {
            echo do_shortcode('[contact-form-7 id="1824" title="Contact Form ru"]');
        } ?>

    </div>
</div>
<!--MODAl END-->

<?php if (is_page_template('template-portfolio.php')) { ?>
    <div id="sort-categories" style="display:none;"><?php if (isset($_GET['class'])) {
            echo $_GET['class'];
        } else {
            echo 'random';
        } ?></div>
    <div id="sort-filter" style="display:none;"><?php if (isset($_GET['class'])) {
            echo '.' . $_GET['class'];
        } else {
            echo '';
        } ?></div>
<?php } ?>

<!-- Yandex.Metrika counter -->
<script type="text/javascript"> (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter37841450 = new Ya.Metrika({
                    id: 37841450,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true,
                    webvisor: true,
                    trackHash: true
                });
            } catch (e) {
            }
        });
        var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () {
            n.parentNode.insertBefore(s, n);
        };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";
        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks"); 


</script>

<noscript>
    <div><img src="https://mc.yandex.ru/watch/37841450" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript> <!-- /Yandex.Metrika counter -->

</body>
</html>
<script>
    function bw() {
        var link_arr = ["<?php echo plugins_url();?>/contact-form-7/includes/css/styles.css", "<?php echo plugins_url();?>/masterslider/public/assets/css/masterslider.main.css", "https://fonts.googleapis.com/css?family=Raleway:400,800", "<?php echo plugins_url();?>/sitepress-multilingual-cms/res/css/language-selector.css"];
        var count_link = link_arr.length;
        for (var i = 0; i < count_link; i++) {

            var ww = document.createElement("link");
            ww.rel = "stylesheet";
            ww.href = link_arr[i];
            document.getElementsByTagName("html")[0].appendChild(ww);
        }
    }
    window.addEventListener("load", bw);
</script>
