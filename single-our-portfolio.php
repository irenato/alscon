<?php get_header();
global $paged;
global $wp_query;
?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <?php
    $id = get_the_ID();
    $args = array(
        'taxonomy'  => 'portfolio-category',
        'hide_empty'   => 0
    );
        $previous_post = get_adjacent_post( false, '', true, 'portfolio-category');
        $next_post = get_adjacent_post( false, '', false, 'portfolio-category');
        $current_categories = get_the_terms($id, 'portfolio-category');

    ?>

    <section class="wrapper portfolio-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2 class="title"><?= get_field('title_project', $id); ?></h2>
                    <div class="description">
                        <h5><?= __('Project tasks','titles')?>:</h5>
                        <p><?= get_field('objective', $id); ?></p>
                        <h5><?= __("Employer's country", 'titles'); ?>:</h5>
                        <p><?= get_field('city', $id).', '.get_field('country', $id); ?></p>
                        <h5><?= __('Project length','titles') ?>:</h5>
                        <p><?= get_field('completion_date', $id); ?></p>


                    </div>
                    <div class="maps">
                        <div id="map"></div>
                    </div>
                    <div class="tags-catalog">
                        <?php if($current_categories!=''): foreach ($current_categories as $category): ?>
                            <a href="<?php if (ICL_LANGUAGE_CODE == 'en') {
                                echo get_permalink(1746) . '?class=' . $category->slug;
                            } else echo get_permalink(1845) . '&class=' . $category->slug; ?>" class="buttons"><?= $category->name ?></a>
                        <?php endforeach; endif; ?>
                    </div>
                    
                    <div class="portfolio-item">
<!--                        <a href="--><?//= get_field('coordinates')?><!--">--><?//= __('VISIT WEBSITE') ?><!--</a>-->
                        <?php if(!empty(get_field('link'))): ?>
                        <a href="<?= get_field('link')?>" target="_blank" class="buttons" ><?= __('LIVE PREVIEW','buttons')?></a>
                        <?php endif; ?>
                        <img src="<?php echo get_field('full_image', $id); ?>" alt="" class="current-post-img"
                             data-site-link="<?= get_field('link', $id); ?>">
                        <?php if(!empty(get_field('animate_image'))) { ?>
                        <img src="<?php echo get_field('animate_image', $id)['url']; ?>" alt="" class="current-post-img">
                        <?php } ?>
                    </div>
                    <?php if(is_a( $previous_post, 'WP_Post' )) { ?>
                    <a href="<?= $previous_post->guid; ?>" class="prev"></a>
                    <?php } ?>
                    <?php if(is_a( $next_post, 'WP_Post' )) { ?>
                    <a href="<?= $next_post->guid; ?>" class="next"></a>
                    <?php } ?>

                </div>
            </div>
        </div>
    </section>
    <a href="#" class="scroll-top fixed" data-scroll="scroll-top"><i class="fa  fa-chevron-up" aria-hidden="true"></i></a>
<?php endwhile; ?>

<?php endif; ?>

<?php wp_reset_query(); ?>

<?php get_footer(); ?>
