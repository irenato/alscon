Все css файлы  добавлены в  общий файл (кроме  for-admin.css) в порядке их подключения, они лежат в папке css, но сайт работает на стилях из папки new_css  - файл all.css. 

Порядок добавления стилей в  all.css:

1.	animate.min.css
2.	bootstrap.min.css
3.	bootstrap-grid-3.3.1.min.css
4.	bootstrap-theme.min.css
5.	font-awesome.min.css
6.	fonts.css
7.	ico.css
8.	jquery.fancybox.css
9.	main.css
10.	style.css
11.	slick.css
12.	slick-theme.css
13.	media.css