<?php
// current menu
function main_site_menu($location) {
    $options = get_option( 'theme_settings' );
    $menu_list="";
    if (($locations = get_nav_menu_locations()) && isset($locations[$location])) {
        global $post;
        $page_for_posts = get_option( 'page_for_posts' );

        if($page_for_posts){
            if(is_home()) {
                $thePostID= $page_for_posts ;
            } else{
                $thePostID = $post->ID;
            }
        } else{
            $thePostID = $post->ID;
        }

        $menu = wp_get_nav_menu_object($locations[$location]);
        $menu_items = wp_get_nav_menu_items($menu->term_id);
        $menu_list = '<ul class="menu ' . $location . '">';
        $count=0;
        foreach ($menu_items as $menu_item) {
            $title = $menu_item->title;
            $url = $menu_item->url;
            $menu_list .= '<li><a href="' . $url . '">' . $title . '</a></li>';
            $count++;
        }
        $menu_list .= "</ul>";
    }
    return $menu_list;
}
// get_languages
function icl_post_languages(){
    $languages = apply_filters( 'wpml_active_languages', NULL, 'orderby=id&order=desc' );

    if(!empty($languages)){
        echo '<button type="button" data-toggle="dropdown" class="language-button">';
        foreach($languages as $lang){
            if($lang['active']){
                echo '<span data-bind="label">'.ucfirst($lang['code']).'</span><i></i></button>';
            }
            $lang = null;}
        echo '<ul id="language" class="languages" name="language">';
        foreach( $languages as $lang ) {
            if(!$lang['active']) {
                echo '<li><a href="' . $lang['url'] . '"> ' . ucfirst($lang['code']) . '</a></li>';
            }
        }
        echo '</ul>';
    }
}
function custom_reviews()
{
    $i = -1;
    $query = new WP_Query(array(
        'post_type' => 'our-reviews',
        'posts_per_page' => -1,
        'orderby' => 'ID',
        'order' => 'ASC',
    ));
    $reviews = array();
    if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();
        $i++;
        $reviews[$i]['face'] = get_field('face', get_the_ID())['url'];
        $reviews[$i]['name'] = get_field('first_name', get_the_ID());
        $reviews[$i]['review'] = get_field('review', get_the_ID());
    endwhile;
    endif;
    foreach ($reviews as $key => $value) {
        if ($value['face'] == "") {
            unset($reviews[$key]);
            $reviews[] = $value;
        }
    }
    foreach ($reviews as $item) { ?>
        <div>
            <?php if ($item['face'] != "") { ?>
                <img src="<?= $item['face'] ?>" alt="">
            <?php } else { ?>
                <img class="no-user" src="<?= get_template_directory_uri() ?>/images/nouser.png"
                     alt="">
            <?php } ?>
            <h5 class="name"><?= $item['name'] ?></h5>
            <p><?= $item['review'] ?></p>
        </div>
    <?php }


}
function custom_services(){
    $query = new WP_Query(array(
        'post_type' => 'services',
        'posts_per_page' => -1,
        'orderby' => 'ID',
        'order' => 'ASC',
    ));
    if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();
        $id = get_the_ID();
        $title = get_field('title', $id);
        $icon = get_field('icon', $id);
        $description = get_field('description', $id);
        $anchor = get_field('anchor', $id); ?>
        <a class="check-centre" href="<?php if (ICL_LANGUAGE_CODE == 'en') {
            echo get_permalink(1746) . '?class=' . $anchor;
        } else echo get_permalink(1845) . '?class=' . $anchor; ?>">
            <div><i class="fa <?= $icon ?> circle" aria-hidden="true" data-item="<?= $anchor ?>"
                    data-title="<?= $title ?>" data-description="<?= $description ?>"></i></div>
        </a>
    <?php endwhile;
    endif;
}