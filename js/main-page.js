/*!
 * Bootstrap v3.3.6 (http://getbootstrap.com)
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under the MIT license
 */
if ("undefined" == typeof jQuery) throw new Error("Bootstrap's JavaScript requires jQuery"); + function (a) {
	"use strict";
	var b = a.fn.jquery.split(" ")[0].split(".");
	if (b[0] < 2 && b[1] < 9 || 1 == b[0] && 9 == b[1] && b[2] < 1 || b[0] > 2) throw new Error("Bootstrap's JavaScript requires jQuery version 1.9.1 or higher, but lower than version 3")
}(jQuery), + function (a) {
	"use strict";

	function b() {
		var a = document.createElement("bootstrap")
			, b = {
			WebkitTransition: "webkitTransitionEnd"
			, MozTransition: "transitionend"
			, OTransition: "oTransitionEnd otransitionend"
			, transition: "transitionend"
		};
		for (var c in b)
			if (void 0 !== a.style[c]) return {
				end: b[c]
			};
		return !1
	}
	a.fn.emulateTransitionEnd = function (b) {
		var c = !1
			, d = this;
		a(this).one("bsTransitionEnd", function () {
			c = !0
		});
		var e = function () {
			c || a(d).trigger(a.support.transition.end)
		};
		return setTimeout(e, b), this
	}, a(function () {
		a.support.transition = b(), a.support.transition && (a.event.special.bsTransitionEnd = {
			bindType: a.support.transition.end
			, delegateType: a.support.transition.end
			, handle: function (b) {
				return a(b.target).is(this) ? b.handleObj.handler.apply(this, arguments) : void 0
			}
		})
	})
}(jQuery), + function (a) {
	"use strict";

	function b(b) {
		return this.each(function () {
			var c = a(this)
				, e = c.data("bs.alert");
			e || c.data("bs.alert", e = new d(this)), "string" == typeof b && e[b].call(c)
		})
	}
	var c = '[data-dismiss="alert"]'
		, d = function (b) {
		a(b).on("click", c, this.close)
	};
	d.VERSION = "3.3.6", d.TRANSITION_DURATION = 150, d.prototype.close = function (b) {
		function c() {
			g.detach().trigger("closed.bs.alert").remove()
		}
		var e = a(this)
			, f = e.attr("data-target");
		f || (f = e.attr("href"), f = f && f.replace(/.*(?=#[^\s]*$)/, ""));
		var g = a(f);
		b && b.preventDefault(), g.length || (g = e.closest(".alert")), g.trigger(b = a.Event("close.bs.alert")), b.isDefaultPrevented() || (g.removeClass("in"), a.support.transition && g.hasClass("fade") ? g.one("bsTransitionEnd", c).emulateTransitionEnd(d.TRANSITION_DURATION) : c())
	};
	var e = a.fn.alert;
	a.fn.alert = b, a.fn.alert.Constructor = d, a.fn.alert.noConflict = function () {
		return a.fn.alert = e, this
	}, a(document).on("click.bs.alert.data-api", c, d.prototype.close)
}(jQuery), + function (a) {
	"use strict";

	function b(b) {
		return this.each(function () {
			var d = a(this)
				, e = d.data("bs.button")
				, f = "object" == typeof b && b;
			e || d.data("bs.button", e = new c(this, f)), "toggle" == b ? e.toggle() : b && e.setState(b)
		})
	}
	var c = function (b, d) {
		this.$element = a(b), this.options = a.extend({}, c.DEFAULTS, d), this.isLoading = !1
	};
	c.VERSION = "3.3.6", c.DEFAULTS = {
		loadingText: "loading..."
	}, c.prototype.setState = function (b) {
		var c = "disabled"
			, d = this.$element
			, e = d.is("input") ? "val" : "html"
			, f = d.data();
		b += "Text", null == f.resetText && d.data("resetText", d[e]()), setTimeout(a.proxy(function () {
			d[e](null == f[b] ? this.options[b] : f[b]), "loadingText" == b ? (this.isLoading = !0, d.addClass(c).attr(c, c)) : this.isLoading && (this.isLoading = !1, d.removeClass(c).removeAttr(c))
		}, this), 0)
	}, c.prototype.toggle = function () {
		var a = !0
			, b = this.$element.closest('[data-toggle="buttons"]');
		if (b.length) {
			var c = this.$element.find("input");
			"radio" == c.prop("type") ? (c.prop("checked") && (a = !1), b.find(".active").removeClass("active"), this.$element.addClass("active")) : "checkbox" == c.prop("type") && (c.prop("checked") !== this.$element.hasClass("active") && (a = !1), this.$element.toggleClass("active")), c.prop("checked", this.$element.hasClass("active")), a && c.trigger("change")
		} else this.$element.attr("aria-pressed", !this.$element.hasClass("active")), this.$element.toggleClass("active")
	};
	var d = a.fn.button;
	a.fn.button = b, a.fn.button.Constructor = c, a.fn.button.noConflict = function () {
		return a.fn.button = d, this
	}, a(document).on("click.bs.button.data-api", '[data-toggle^="button"]', function (c) {
		var d = a(c.target);
		d.hasClass("btn") || (d = d.closest(".btn")), b.call(d, "toggle"), a(c.target).is('input[type="radio"]') || a(c.target).is('input[type="checkbox"]') || c.preventDefault()
	}).on("focus.bs.button.data-api blur.bs.button.data-api", '[data-toggle^="button"]', function (b) {
		a(b.target).closest(".btn").toggleClass("focus", /^focus(in)?$/.test(b.type))
	})
}(jQuery), + function (a) {
	"use strict";

	function b(b) {
		return this.each(function () {
			var d = a(this)
				, e = d.data("bs.carousel")
				, f = a.extend({}, c.DEFAULTS, d.data(), "object" == typeof b && b)
				, g = "string" == typeof b ? b : f.slide;
			e || d.data("bs.carousel", e = new c(this, f)), "number" == typeof b ? e.to(b) : g ? e[g]() : f.interval && e.pause().cycle()
		})
	}
	var c = function (b, c) {
		this.$element = a(b), this.$indicators = this.$element.find(".carousel-indicators"), this.options = c, this.paused = null, this.sliding = null, this.interval = null, this.$active = null, this.$items = null, this.options.keyboard && this.$element.on("keydown.bs.carousel", a.proxy(this.keydown, this)), "hover" == this.options.pause && !("ontouchstart" in document.documentElement) && this.$element.on("mouseenter.bs.carousel", a.proxy(this.pause, this)).on("mouseleave.bs.carousel", a.proxy(this.cycle, this))
	};
	c.VERSION = "3.3.6", c.TRANSITION_DURATION = 600, c.DEFAULTS = {
		interval: 5e3
		, pause: "hover"
		, wrap: !0
		, keyboard: !0
	}, c.prototype.keydown = function (a) {
		if (!/input|textarea/i.test(a.target.tagName)) {
			switch (a.which) {
				case 37:
					this.prev();
					break;
				case 39:
					this.next();
					break;
				default:
					return
			}
			a.preventDefault()
		}
	}, c.prototype.cycle = function (b) {
		return b || (this.paused = !1), this.interval && clearInterval(this.interval), this.options.interval && !this.paused && (this.interval = setInterval(a.proxy(this.next, this), this.options.interval)), this
	}, c.prototype.getItemIndex = function (a) {
		return this.$items = a.parent().children(".item"), this.$items.index(a || this.$active)
	}, c.prototype.getItemForDirection = function (a, b) {
		var c = this.getItemIndex(b)
			, d = "prev" == a && 0 === c || "next" == a && c == this.$items.length - 1;
		if (d && !this.options.wrap) return b;
		var e = "prev" == a ? -1 : 1
			, f = (c + e) % this.$items.length;
		return this.$items.eq(f)
	}, c.prototype.to = function (a) {
		var b = this
			, c = this.getItemIndex(this.$active = this.$element.find(".item.active"));
		return a > this.$items.length - 1 || 0 > a ? void 0 : this.sliding ? this.$element.one("slid.bs.carousel", function () {
			b.to(a)
		}) : c == a ? this.pause().cycle() : this.slide(a > c ? "next" : "prev", this.$items.eq(a))
	}, c.prototype.pause = function (b) {
		return b || (this.paused = !0), this.$element.find(".next, .prev").length && a.support.transition && (this.$element.trigger(a.support.transition.end), this.cycle(!0)), this.interval = clearInterval(this.interval), this
	}, c.prototype.next = function () {
		return this.sliding ? void 0 : this.slide("next")
	}, c.prototype.prev = function () {
		return this.sliding ? void 0 : this.slide("prev")
	}, c.prototype.slide = function (b, d) {
		var e = this.$element.find(".item.active")
			, f = d || this.getItemForDirection(b, e)
			, g = this.interval
			, h = "next" == b ? "left" : "right"
			, i = this;
		if (f.hasClass("active")) return this.sliding = !1;
		var j = f[0]
			, k = a.Event("slide.bs.carousel", {
			relatedTarget: j
			, direction: h
		});
		if (this.$element.trigger(k), !k.isDefaultPrevented()) {
			if (this.sliding = !0, g && this.pause(), this.$indicators.length) {
				this.$indicators.find(".active").removeClass("active");
				var l = a(this.$indicators.children()[this.getItemIndex(f)]);
				l && l.addClass("active")
			}
			var m = a.Event("slid.bs.carousel", {
				relatedTarget: j
				, direction: h
			});
			return a.support.transition && this.$element.hasClass("slide") ? (f.addClass(b), f[0].offsetWidth, e.addClass(h), f.addClass(h), e.one("bsTransitionEnd", function () {
				f.removeClass([b, h].join(" ")).addClass("active"), e.removeClass(["active", h].join(" ")), i.sliding = !1, setTimeout(function () {
					i.$element.trigger(m)
				}, 0)
			}).emulateTransitionEnd(c.TRANSITION_DURATION)) : (e.removeClass("active"), f.addClass("active"), this.sliding = !1, this.$element.trigger(m)), g && this.cycle(), this
		}
	};
	var d = a.fn.carousel;
	a.fn.carousel = b, a.fn.carousel.Constructor = c, a.fn.carousel.noConflict = function () {
		return a.fn.carousel = d, this
	};
	var e = function (c) {
		var d, e = a(this)
			, f = a(e.attr("data-target") || (d = e.attr("href")) && d.replace(/.*(?=#[^\s]+$)/, ""));
		if (f.hasClass("carousel")) {
			var g = a.extend({}, f.data(), e.data())
				, h = e.attr("data-slide-to");
			h && (g.interval = !1), b.call(f, g), h && f.data("bs.carousel").to(h), c.preventDefault()
		}
	};
	a(document).on("click.bs.carousel.data-api", "[data-slide]", e).on("click.bs.carousel.data-api", "[data-slide-to]", e), a(window).on("load", function () {
		a('[data-ride="carousel"]').each(function () {
			var c = a(this);
			b.call(c, c.data())
		})
	})
}(jQuery), + function (a) {
	"use strict";

	function b(b) {
		var c, d = b.attr("data-target") || (c = b.attr("href")) && c.replace(/.*(?=#[^\s]+$)/, "");
		return a(d)
	}

	function c(b) {
		return this.each(function () {
			var c = a(this)
				, e = c.data("bs.collapse")
				, f = a.extend({}, d.DEFAULTS, c.data(), "object" == typeof b && b);
			!e && f.toggle && /show|hide/.test(b) && (f.toggle = !1), e || c.data("bs.collapse", e = new d(this, f)), "string" == typeof b && e[b]()
		})
	}
	var d = function (b, c) {
		this.$element = a(b), this.options = a.extend({}, d.DEFAULTS, c), this.$trigger = a('[data-toggle="collapse"][href="#' + b.id + '"],[data-toggle="collapse"][data-target="#' + b.id + '"]'), this.transitioning = null, this.options.parent ? this.$parent = this.getParent() : this.addAriaAndCollapsedClass(this.$element, this.$trigger), this.options.toggle && this.toggle()
	};
	d.VERSION = "3.3.6", d.TRANSITION_DURATION = 350, d.DEFAULTS = {
		toggle: !0
	}, d.prototype.dimension = function () {
		var a = this.$element.hasClass("width");
		return a ? "width" : "height"
	}, d.prototype.show = function () {
		if (!this.transitioning && !this.$element.hasClass("in")) {
			var b, e = this.$parent && this.$parent.children(".panel").children(".in, .collapsing");
			if (!(e && e.length && (b = e.data("bs.collapse"), b && b.transitioning))) {
				var f = a.Event("show.bs.collapse");
				if (this.$element.trigger(f), !f.isDefaultPrevented()) {
					e && e.length && (c.call(e, "hide"), b || e.data("bs.collapse", null));
					var g = this.dimension();
					this.$element.removeClass("collapse").addClass("collapsing")[g](0).attr("aria-expanded", !0), this.$trigger.removeClass("collapsed").attr("aria-expanded", !0), this.transitioning = 1;
					var h = function () {
						this.$element.removeClass("collapsing").addClass("collapse in")[g](""), this.transitioning = 0, this.$element.trigger("shown.bs.collapse")
					};
					if (!a.support.transition) return h.call(this);
					var i = a.camelCase(["scroll", g].join("-"));
					this.$element.one("bsTransitionEnd", a.proxy(h, this)).emulateTransitionEnd(d.TRANSITION_DURATION)[g](this.$element[0][i])
				}
			}
		}
	}, d.prototype.hide = function () {
		if (!this.transitioning && this.$element.hasClass("in")) {
			var b = a.Event("hide.bs.collapse");
			if (this.$element.trigger(b), !b.isDefaultPrevented()) {
				var c = this.dimension();
				this.$element[c](this.$element[c]())[0].offsetHeight, this.$element.addClass("collapsing").removeClass("collapse in").attr("aria-expanded", !1), this.$trigger.addClass("collapsed").attr("aria-expanded", !1), this.transitioning = 1;
				var e = function () {
					this.transitioning = 0, this.$element.removeClass("collapsing").addClass("collapse").trigger("hidden.bs.collapse")
				};
				return a.support.transition ? void this.$element[c](0).one("bsTransitionEnd", a.proxy(e, this)).emulateTransitionEnd(d.TRANSITION_DURATION) : e.call(this)
			}
		}
	}, d.prototype.toggle = function () {
		this[this.$element.hasClass("in") ? "hide" : "show"]()
	}, d.prototype.getParent = function () {
		return a(this.options.parent).find('[data-toggle="collapse"][data-parent="' + this.options.parent + '"]').each(a.proxy(function (c, d) {
			var e = a(d);
			this.addAriaAndCollapsedClass(b(e), e)
		}, this)).end()
	}, d.prototype.addAriaAndCollapsedClass = function (a, b) {
		var c = a.hasClass("in");
		a.attr("aria-expanded", c), b.toggleClass("collapsed", !c).attr("aria-expanded", c)
	};
	var e = a.fn.collapse;
	a.fn.collapse = c, a.fn.collapse.Constructor = d, a.fn.collapse.noConflict = function () {
		return a.fn.collapse = e, this
	}, a(document).on("click.bs.collapse.data-api", '[data-toggle="collapse"]', function (d) {
		var e = a(this);
		e.attr("data-target") || d.preventDefault();
		var f = b(e)
			, g = f.data("bs.collapse")
			, h = g ? "toggle" : e.data();
		c.call(f, h)
	})
}(jQuery), + function (a) {
	"use strict";

	function b(b) {
		var c = b.attr("data-target");
		c || (c = b.attr("href"), c = c && /#[A-Za-z]/.test(c) && c.replace(/.*(?=#[^\s]*$)/, ""));
		var d = c && a(c);
		return d && d.length ? d : b.parent()
	}

	function c(c) {
		c && 3 === c.which || (a(e).remove(), a(f).each(function () {
			var d = a(this)
				, e = b(d)
				, f = {
				relatedTarget: this
			};
			e.hasClass("open") && (c && "click" == c.type && /input|textarea/i.test(c.target.tagName) && a.contains(e[0], c.target) || (e.trigger(c = a.Event("hide.bs.dropdown", f)), c.isDefaultPrevented() || (d.attr("aria-expanded", "false"), e.removeClass("open").trigger(a.Event("hidden.bs.dropdown", f)))))
		}))
	}

	function d(b) {
		return this.each(function () {
			var c = a(this)
				, d = c.data("bs.dropdown");
			d || c.data("bs.dropdown", d = new g(this)), "string" == typeof b && d[b].call(c)
		})
	}
	var e = ".dropdown-backdrop"
		, f = '[data-toggle="dropdown"]'
		, g = function (b) {
		a(b).on("click.bs.dropdown", this.toggle)
	};
	g.VERSION = "3.3.6", g.prototype.toggle = function (d) {
		var e = a(this);
		if (!e.is(".disabled, :disabled")) {
			var f = b(e)
				, g = f.hasClass("open");
			if (c(), !g) {
				"ontouchstart" in document.documentElement && !f.closest(".navbar-nav").length && a(document.createElement("div")).addClass("dropdown-backdrop").insertAfter(a(this)).on("click", c);
				var h = {
					relatedTarget: this
				};
				if (f.trigger(d = a.Event("show.bs.dropdown", h)), d.isDefaultPrevented()) return;
				e.trigger("focus").attr("aria-expanded", "true"), f.toggleClass("open").trigger(a.Event("shown.bs.dropdown", h))
			}
			return !1
		}
	}, g.prototype.keydown = function (c) {
		if (/(38|40|27|32)/.test(c.which) && !/input|textarea/i.test(c.target.tagName)) {
			var d = a(this);
			if (c.preventDefault(), c.stopPropagation(), !d.is(".disabled, :disabled")) {
				var e = b(d)
					, g = e.hasClass("open");
				if (!g && 27 != c.which || g && 27 == c.which) return 27 == c.which && e.find(f).trigger("focus"), d.trigger("click");
				var h = " li:not(.disabled):visible a"
					, i = e.find(".dropdown-menu" + h);
				if (i.length) {
					var j = i.index(c.target);
					38 == c.which && j > 0 && j--, 40 == c.which && j < i.length - 1 && j++, ~j || (j = 0), i.eq(j).trigger("focus")
				}
			}
		}
	};
	var h = a.fn.dropdown;
	a.fn.dropdown = d, a.fn.dropdown.Constructor = g, a.fn.dropdown.noConflict = function () {
		return a.fn.dropdown = h, this
	}, a(document).on("click.bs.dropdown.data-api", c).on("click.bs.dropdown.data-api", ".dropdown form", function (a) {
		a.stopPropagation()
	}).on("click.bs.dropdown.data-api", f, g.prototype.toggle).on("keydown.bs.dropdown.data-api", f, g.prototype.keydown).on("keydown.bs.dropdown.data-api", ".dropdown-menu", g.prototype.keydown)
}(jQuery), + function (a) {
	"use strict";

	function b(b, d) {
		return this.each(function () {
			var e = a(this)
				, f = e.data("bs.modal")
				, g = a.extend({}, c.DEFAULTS, e.data(), "object" == typeof b && b);
			f || e.data("bs.modal", f = new c(this, g)), "string" == typeof b ? f[b](d) : g.show && f.show(d)
		})
	}
	var c = function (b, c) {
		this.options = c, this.$body = a(document.body), this.$element = a(b), this.$dialog = this.$element.find(".modal-dialog"), this.$backdrop = null, this.isShown = null, this.originalBodyPad = null, this.scrollbarWidth = 0, this.ignoreBackdropClick = !1, this.options.remote && this.$element.find(".modal-content").load(this.options.remote, a.proxy(function () {
			this.$element.trigger("loaded.bs.modal")
		}, this))
	};
	c.VERSION = "3.3.6", c.TRANSITION_DURATION = 300, c.BACKDROP_TRANSITION_DURATION = 150, c.DEFAULTS = {
		backdrop: !0
		, keyboard: !0
		, show: !0
	}, c.prototype.toggle = function (a) {
		return this.isShown ? this.hide() : this.show(a)
	}, c.prototype.show = function (b) {
		var d = this
			, e = a.Event("show.bs.modal", {
			relatedTarget: b
		});
		this.$element.trigger(e), this.isShown || e.isDefaultPrevented() || (this.isShown = !0, this.checkScrollbar(), this.setScrollbar(), this.$body.addClass("modal-open"), this.escape(), this.resize(), this.$element.on("click.dismiss.bs.modal", '[data-dismiss="modal"]', a.proxy(this.hide, this)), this.$dialog.on("mousedown.dismiss.bs.modal", function () {
			d.$element.one("mouseup.dismiss.bs.modal", function (b) {
				a(b.target).is(d.$element) && (d.ignoreBackdropClick = !0)
			})
		}), this.backdrop(function () {
			var e = a.support.transition && d.$element.hasClass("fade");
			d.$element.parent().length || d.$element.appendTo(d.$body), d.$element.show().scrollTop(0), d.adjustDialog(), e && d.$element[0].offsetWidth, d.$element.addClass("in"), d.enforceFocus();
			var f = a.Event("shown.bs.modal", {
				relatedTarget: b
			});
			e ? d.$dialog.one("bsTransitionEnd", function () {
				d.$element.trigger("focus").trigger(f)
			}).emulateTransitionEnd(c.TRANSITION_DURATION) : d.$element.trigger("focus").trigger(f)
		}))
	}, c.prototype.hide = function (b) {
		b && b.preventDefault(), b = a.Event("hide.bs.modal"), this.$element.trigger(b), this.isShown && !b.isDefaultPrevented() && (this.isShown = !1, this.escape(), this.resize(), a(document).off("focusin.bs.modal"), this.$element.removeClass("in").off("click.dismiss.bs.modal").off("mouseup.dismiss.bs.modal"), this.$dialog.off("mousedown.dismiss.bs.modal"), a.support.transition && this.$element.hasClass("fade") ? this.$element.one("bsTransitionEnd", a.proxy(this.hideModal, this)).emulateTransitionEnd(c.TRANSITION_DURATION) : this.hideModal())
	}, c.prototype.enforceFocus = function () {
		a(document).off("focusin.bs.modal").on("focusin.bs.modal", a.proxy(function (a) {
			document === event.target || this.$element[0] === a.target || this.$element.has(a.target).length || this.$element.trigger("focus")
		}, this))
	}, c.prototype.escape = function () {
		this.isShown && this.options.keyboard ? this.$element.on("keydown.dismiss.bs.modal", a.proxy(function (a) {
			27 == a.which && this.hide()
		}, this)) : this.isShown || this.$element.off("keydown.dismiss.bs.modal")
	}, c.prototype.resize = function () {
		this.isShown ? a(window).on("resize.bs.modal", a.proxy(this.handleUpdate, this)) : a(window).off("resize.bs.modal")
	}, c.prototype.hideModal = function () {
		var a = this;
		this.$element.hide(), this.backdrop(function () {
			a.$body.removeClass("modal-open"), a.resetAdjustments(), a.resetScrollbar(), a.$element.trigger("hidden.bs.modal")
		})
	}, c.prototype.removeBackdrop = function () {
		this.$backdrop && this.$backdrop.remove(), this.$backdrop = null
	}, c.prototype.backdrop = function (b) {
		var d = this
			, e = this.$element.hasClass("fade") ? "fade" : "";
		if (this.isShown && this.options.backdrop) {
			var f = a.support.transition && e;
			if (this.$backdrop = a(document.createElement("div")).addClass("modal-backdrop " + e).appendTo(this.$body), this.$element.on("click.dismiss.bs.modal", a.proxy(function (a) {
					return this.ignoreBackdropClick ? void(this.ignoreBackdropClick = !1) : void(a.target === a.currentTarget && ("static" == this.options.backdrop ? this.$element[0].focus() : this.hide()))
				}, this)), f && this.$backdrop[0].offsetWidth, this.$backdrop.addClass("in"), !b) return;
			f ? this.$backdrop.one("bsTransitionEnd", b).emulateTransitionEnd(c.BACKDROP_TRANSITION_DURATION) : b()
		} else if (!this.isShown && this.$backdrop) {
			this.$backdrop.removeClass("in");
			var g = function () {
				d.removeBackdrop(), b && b()
			};
			a.support.transition && this.$element.hasClass("fade") ? this.$backdrop.one("bsTransitionEnd", g).emulateTransitionEnd(c.BACKDROP_TRANSITION_DURATION) : g()
		} else b && b()
	}, c.prototype.handleUpdate = function () {
		this.adjustDialog()
	}, c.prototype.adjustDialog = function () {
		var a = this.$element[0].scrollHeight > document.documentElement.clientHeight;
		this.$element.css({
			paddingLeft: !this.bodyIsOverflowing && a ? this.scrollbarWidth : ""
			, paddingRight: this.bodyIsOverflowing && !a ? this.scrollbarWidth : ""
		})
	}, c.prototype.resetAdjustments = function () {
		this.$element.css({
			paddingLeft: ""
			, paddingRight: ""
		})
	}, c.prototype.checkScrollbar = function () {
		var a = window.innerWidth;
		if (!a) {
			var b = document.documentElement.getBoundingClientRect();
			a = b.right - Math.abs(b.left)
		}
		this.bodyIsOverflowing = document.body.clientWidth < a, this.scrollbarWidth = this.measureScrollbar()
	}, c.prototype.setScrollbar = function () {
		var a = parseInt(this.$body.css("padding-right") || 0, 10);
		this.originalBodyPad = document.body.style.paddingRight || "", this.bodyIsOverflowing && this.$body.css("padding-right", a + this.scrollbarWidth)
	}, c.prototype.resetScrollbar = function () {
		this.$body.css("padding-right", this.originalBodyPad)
	}, c.prototype.measureScrollbar = function () {
		var a = document.createElement("div");
		a.className = "modal-scrollbar-measure", this.$body.append(a);
		var b = a.offsetWidth - a.clientWidth;
		return this.$body[0].removeChild(a), b
	};
	var d = a.fn.modal;
	a.fn.modal = b, a.fn.modal.Constructor = c, a.fn.modal.noConflict = function () {
		return a.fn.modal = d, this
	}, a(document).on("click.bs.modal.data-api", '[data-toggle="modal"]', function (c) {
		var d = a(this)
			, e = d.attr("href")
			, f = a(d.attr("data-target") || e && e.replace(/.*(?=#[^\s]+$)/, ""))
			, g = f.data("bs.modal") ? "toggle" : a.extend({
			remote: !/#/.test(e) && e
		}, f.data(), d.data());
		d.is("a") && c.preventDefault(), f.one("show.bs.modal", function (a) {
			a.isDefaultPrevented() || f.one("hidden.bs.modal", function () {
				d.is(":visible") && d.trigger("focus")
			})
		}), b.call(f, g, this)
	})
}(jQuery), + function (a) {
	"use strict";

	function b(b) {
		return this.each(function () {
			var d = a(this)
				, e = d.data("bs.tooltip")
				, f = "object" == typeof b && b;
			(e || !/destroy|hide/.test(b)) && (e || d.data("bs.tooltip", e = new c(this, f)), "string" == typeof b && e[b]())
		})
	}
	var c = function (a, b) {
		this.type = null, this.options = null, this.enabled = null, this.timeout = null, this.hoverState = null, this.$element = null, this.inState = null, this.init("tooltip", a, b)
	};
	c.VERSION = "3.3.6", c.TRANSITION_DURATION = 150, c.DEFAULTS = {
		animation: !0
		, placement: "top"
		, selector: !1
		, template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
		, trigger: "hover focus"
		, title: ""
		, delay: 0
		, html: !1
		, container: !1
		, viewport: {
			selector: "body"
			, padding: 0
		}
	}, c.prototype.init = function (b, c, d) {
		if (this.enabled = !0, this.type = b, this.$element = a(c), this.options = this.getOptions(d), this.$viewport = this.options.viewport && a(a.isFunction(this.options.viewport) ? this.options.viewport.call(this, this.$element) : this.options.viewport.selector || this.options.viewport), this.inState = {
				click: !1
				, hover: !1
				, focus: !1
			}, this.$element[0] instanceof document.constructor && !this.options.selector) throw new Error("`selector` option must be specified when initializing " + this.type + " on the window.document object!");
		for (var e = this.options.trigger.split(" "), f = e.length; f--;) {
			var g = e[f];
			if ("click" == g) this.$element.on("click." + this.type, this.options.selector, a.proxy(this.toggle, this));
			else if ("manual" != g) {
				var h = "hover" == g ? "mouseenter" : "focusin"
					, i = "hover" == g ? "mouseleave" : "focusout";
				this.$element.on(h + "." + this.type, this.options.selector, a.proxy(this.enter, this)), this.$element.on(i + "." + this.type, this.options.selector, a.proxy(this.leave, this))
			}
		}
		this.options.selector ? this._options = a.extend({}, this.options, {
			trigger: "manual"
			, selector: ""
		}) : this.fixTitle()
	}, c.prototype.getDefaults = function () {
		return c.DEFAULTS
	}, c.prototype.getOptions = function (b) {
		return b = a.extend({}, this.getDefaults(), this.$element.data(), b), b.delay && "number" == typeof b.delay && (b.delay = {
			show: b.delay
			, hide: b.delay
		}), b
	}, c.prototype.getDelegateOptions = function () {
		var b = {}
			, c = this.getDefaults();
		return this._options && a.each(this._options, function (a, d) {
			c[a] != d && (b[a] = d)
		}), b
	}, c.prototype.enter = function (b) {
		var c = b instanceof this.constructor ? b : a(b.currentTarget).data("bs." + this.type);
		return c || (c = new this.constructor(b.currentTarget, this.getDelegateOptions()), a(b.currentTarget).data("bs." + this.type, c)), b instanceof a.Event && (c.inState["focusin" == b.type ? "focus" : "hover"] = !0), c.tip().hasClass("in") || "in" == c.hoverState ? void(c.hoverState = "in") : (clearTimeout(c.timeout), c.hoverState = "in", c.options.delay && c.options.delay.show ? void(c.timeout = setTimeout(function () {
			"in" == c.hoverState && c.show()
		}, c.options.delay.show)) : c.show())
	}, c.prototype.isInStateTrue = function () {
		for (var a in this.inState)
			if (this.inState[a]) return !0;
		return !1
	}, c.prototype.leave = function (b) {
		var c = b instanceof this.constructor ? b : a(b.currentTarget).data("bs." + this.type);
		return c || (c = new this.constructor(b.currentTarget, this.getDelegateOptions()), a(b.currentTarget).data("bs." + this.type, c)), b instanceof a.Event && (c.inState["focusout" == b.type ? "focus" : "hover"] = !1), c.isInStateTrue() ? void 0 : (clearTimeout(c.timeout), c.hoverState = "out", c.options.delay && c.options.delay.hide ? void(c.timeout = setTimeout(function () {
			"out" == c.hoverState && c.hide()
		}, c.options.delay.hide)) : c.hide())
	}, c.prototype.show = function () {
		var b = a.Event("show.bs." + this.type);
		if (this.hasContent() && this.enabled) {
			this.$element.trigger(b);
			var d = a.contains(this.$element[0].ownerDocument.documentElement, this.$element[0]);
			if (b.isDefaultPrevented() || !d) return;
			var e = this
				, f = this.tip()
				, g = this.getUID(this.type);
			this.setContent(), f.attr("id", g), this.$element.attr("aria-describedby", g), this.options.animation && f.addClass("fade");
			var h = "function" == typeof this.options.placement ? this.options.placement.call(this, f[0], this.$element[0]) : this.options.placement
				, i = /\s?auto?\s?/i
				, j = i.test(h);
			j && (h = h.replace(i, "") || "top"), f.detach().css({
				top: 0
				, left: 0
				, display: "block"
			}).addClass(h).data("bs." + this.type, this), this.options.container ? f.appendTo(this.options.container) : f.insertAfter(this.$element), this.$element.trigger("inserted.bs." + this.type);
			var k = this.getPosition()
				, l = f[0].offsetWidth
				, m = f[0].offsetHeight;
			if (j) {
				var n = h
					, o = this.getPosition(this.$viewport);
				h = "bottom" == h && k.bottom + m > o.bottom ? "top" : "top" == h && k.top - m < o.top ? "bottom" : "right" == h && k.right + l > o.width ? "left" : "left" == h && k.left - l < o.left ? "right" : h, f.removeClass(n).addClass(h)
			}
			var p = this.getCalculatedOffset(h, k, l, m);
			this.applyPlacement(p, h);
			var q = function () {
				var a = e.hoverState;
				e.$element.trigger("shown.bs." + e.type), e.hoverState = null, "out" == a && e.leave(e)
			};
			a.support.transition && this.$tip.hasClass("fade") ? f.one("bsTransitionEnd", q).emulateTransitionEnd(c.TRANSITION_DURATION) : q()
		}
	}, c.prototype.applyPlacement = function (b, c) {
		var d = this.tip()
			, e = d[0].offsetWidth
			, f = d[0].offsetHeight
			, g = parseInt(d.css("margin-top"), 10)
			, h = parseInt(d.css("margin-left"), 10);
		isNaN(g) && (g = 0), isNaN(h) && (h = 0), b.top += g, b.left += h, a.offset.setOffset(d[0], a.extend({
			using: function (a) {
				d.css({
					top: Math.round(a.top)
					, left: Math.round(a.left)
				})
			}
		}, b), 0), d.addClass("in");
		var i = d[0].offsetWidth
			, j = d[0].offsetHeight;
		"top" == c && j != f && (b.top = b.top + f - j);
		var k = this.getViewportAdjustedDelta(c, b, i, j);
		k.left ? b.left += k.left : b.top += k.top;
		var l = /top|bottom/.test(c)
			, m = l ? 2 * k.left - e + i : 2 * k.top - f + j
			, n = l ? "offsetWidth" : "offsetHeight";
		d.offset(b), this.replaceArrow(m, d[0][n], l)
	}, c.prototype.replaceArrow = function (a, b, c) {
		this.arrow().css(c ? "left" : "top", 50 * (1 - a / b) + "%").css(c ? "top" : "left", "")
	}, c.prototype.setContent = function () {
		var a = this.tip()
			, b = this.getTitle();
		a.find(".tooltip-inner")[this.options.html ? "html" : "text"](b), a.removeClass("fade in top bottom left right")
	}, c.prototype.hide = function (b) {
		function d() {
			"in" != e.hoverState && f.detach(), e.$element.removeAttr("aria-describedby").trigger("hidden.bs." + e.type), b && b()
		}
		var e = this
			, f = a(this.$tip)
			, g = a.Event("hide.bs." + this.type);
		return this.$element.trigger(g), g.isDefaultPrevented() ? void 0 : (f.removeClass("in"), a.support.transition && f.hasClass("fade") ? f.one("bsTransitionEnd", d).emulateTransitionEnd(c.TRANSITION_DURATION) : d(), this.hoverState = null, this)
	}, c.prototype.fixTitle = function () {
		var a = this.$element;
		(a.attr("title") || "string" != typeof a.attr("data-original-title")) && a.attr("data-original-title", a.attr("title") || "").attr("title", "")
	}, c.prototype.hasContent = function () {
		return this.getTitle()
	}, c.prototype.getPosition = function (b) {
		b = b || this.$element;
		var c = b[0]
			, d = "BODY" == c.tagName
			, e = c.getBoundingClientRect();
		null == e.width && (e = a.extend({}, e, {
			width: e.right - e.left
			, height: e.bottom - e.top
		}));
		var f = d ? {
			top: 0
			, left: 0
		} : b.offset()
			, g = {
			scroll: d ? document.documentElement.scrollTop || document.body.scrollTop : b.scrollTop()
		}
			, h = d ? {
			width: a(window).width()
			, height: a(window).height()
		} : null;
		return a.extend({}, e, g, h, f)
	}, c.prototype.getCalculatedOffset = function (a, b, c, d) {
		return "bottom" == a ? {
			top: b.top + b.height
			, left: b.left + b.width / 2 - c / 2
		} : "top" == a ? {
			top: b.top - d
			, left: b.left + b.width / 2 - c / 2
		} : "left" == a ? {
			top: b.top + b.height / 2 - d / 2
			, left: b.left - c
		} : {
			top: b.top + b.height / 2 - d / 2
			, left: b.left + b.width
		}
	}, c.prototype.getViewportAdjustedDelta = function (a, b, c, d) {
		var e = {
			top: 0
			, left: 0
		};
		if (!this.$viewport) return e;
		var f = this.options.viewport && this.options.viewport.padding || 0
			, g = this.getPosition(this.$viewport);
		if (/right|left/.test(a)) {
			var h = b.top - f - g.scroll
				, i = b.top + f - g.scroll + d;
			h < g.top ? e.top = g.top - h : i > g.top + g.height && (e.top = g.top + g.height - i)
		} else {
			var j = b.left - f
				, k = b.left + f + c;
			j < g.left ? e.left = g.left - j : k > g.right && (e.left = g.left + g.width - k)
		}
		return e
	}, c.prototype.getTitle = function () {
		var a, b = this.$element
			, c = this.options;
		return a = b.attr("data-original-title") || ("function" == typeof c.title ? c.title.call(b[0]) : c.title)
	}, c.prototype.getUID = function (a) {
		do a += ~~(1e6 * Math.random()); while (document.getElementById(a));
		return a
	}, c.prototype.tip = function () {
		if (!this.$tip && (this.$tip = a(this.options.template), 1 != this.$tip.length)) throw new Error(this.type + " `template` option must consist of exactly 1 top-level element!");
		return this.$tip
	}, c.prototype.arrow = function () {
		return this.$arrow = this.$arrow || this.tip().find(".tooltip-arrow")
	}, c.prototype.enable = function () {
		this.enabled = !0
	}, c.prototype.disable = function () {
		this.enabled = !1
	}, c.prototype.toggleEnabled = function () {
		this.enabled = !this.enabled
	}, c.prototype.toggle = function (b) {
		var c = this;
		b && (c = a(b.currentTarget).data("bs." + this.type), c || (c = new this.constructor(b.currentTarget, this.getDelegateOptions()), a(b.currentTarget).data("bs." + this.type, c))), b ? (c.inState.click = !c.inState.click, c.isInStateTrue() ? c.enter(c) : c.leave(c)) : c.tip().hasClass("in") ? c.leave(c) : c.enter(c)
	}, c.prototype.destroy = function () {
		var a = this;
		clearTimeout(this.timeout), this.hide(function () {
			a.$element.off("." + a.type).removeData("bs." + a.type), a.$tip && a.$tip.detach(), a.$tip = null, a.$arrow = null, a.$viewport = null
		})
	};
	var d = a.fn.tooltip;
	a.fn.tooltip = b, a.fn.tooltip.Constructor = c, a.fn.tooltip.noConflict = function () {
		return a.fn.tooltip = d, this
	}
}(jQuery), + function (a) {
	"use strict";

	function b(b) {
		return this.each(function () {
			var d = a(this)
				, e = d.data("bs.popover")
				, f = "object" == typeof b && b;
			(e || !/destroy|hide/.test(b)) && (e || d.data("bs.popover", e = new c(this, f)), "string" == typeof b && e[b]())
		})
	}
	var c = function (a, b) {
		this.init("popover", a, b)
	};
	if (!a.fn.tooltip) throw new Error("Popover requires tooltip.js");
	c.VERSION = "3.3.6", c.DEFAULTS = a.extend({}, a.fn.tooltip.Constructor.DEFAULTS, {
		placement: "right"
		, trigger: "click"
		, content: ""
		, template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
	}), c.prototype = a.extend({}, a.fn.tooltip.Constructor.prototype), c.prototype.constructor = c, c.prototype.getDefaults = function () {
		return c.DEFAULTS
	}, c.prototype.setContent = function () {
		var a = this.tip()
			, b = this.getTitle()
			, c = this.getContent();
		a.find(".popover-title")[this.options.html ? "html" : "text"](b), a.find(".popover-content").children().detach().end()[this.options.html ? "string" == typeof c ? "html" : "append" : "text"](c), a.removeClass("fade top bottom left right in"), a.find(".popover-title").html() || a.find(".popover-title").hide()
	}, c.prototype.hasContent = function () {
		return this.getTitle() || this.getContent()
	}, c.prototype.getContent = function () {
		var a = this.$element
			, b = this.options;
		return a.attr("data-content") || ("function" == typeof b.content ? b.content.call(a[0]) : b.content)
	}, c.prototype.arrow = function () {
		return this.$arrow = this.$arrow || this.tip().find(".arrow")
	};
	var d = a.fn.popover;
	a.fn.popover = b, a.fn.popover.Constructor = c, a.fn.popover.noConflict = function () {
		return a.fn.popover = d, this
	}
}(jQuery), + function (a) {
	"use strict";

	function b(c, d) {
		this.$body = a(document.body), this.$scrollElement = a(a(c).is(document.body) ? window : c), this.options = a.extend({}, b.DEFAULTS, d), this.selector = (this.options.target || "") + " .nav li > a", this.offsets = [], this.targets = [], this.activeTarget = null, this.scrollHeight = 0, this.$scrollElement.on("scroll.bs.scrollspy", a.proxy(this.process, this)), this.refresh(), this.process()
	}

	function c(c) {
		return this.each(function () {
			var d = a(this)
				, e = d.data("bs.scrollspy")
				, f = "object" == typeof c && c;
			e || d.data("bs.scrollspy", e = new b(this, f)), "string" == typeof c && e[c]()
		})
	}
	b.VERSION = "3.3.6", b.DEFAULTS = {
		offset: 10
	}, b.prototype.getScrollHeight = function () {
		return this.$scrollElement[0].scrollHeight || Math.max(this.$body[0].scrollHeight, document.documentElement.scrollHeight)
	}, b.prototype.refresh = function () {
		var b = this
			, c = "offset"
			, d = 0;
		this.offsets = [], this.targets = [], this.scrollHeight = this.getScrollHeight(), a.isWindow(this.$scrollElement[0]) || (c = "position", d = this.$scrollElement.scrollTop()), this.$body.find(this.selector).map(function () {
			var b = a(this)
				, e = b.data("target") || b.attr("href")
				, f = /^#./.test(e) && a(e);
			return f && f.length && f.is(":visible") && [[f[c]().top + d, e]] || null
		}).sort(function (a, b) {
			return a[0] - b[0]
		}).each(function () {
			b.offsets.push(this[0]), b.targets.push(this[1])
		})
	}, b.prototype.process = function () {
		var a, b = this.$scrollElement.scrollTop() + this.options.offset
			, c = this.getScrollHeight()
			, d = this.options.offset + c - this.$scrollElement.height()
			, e = this.offsets
			, f = this.targets
			, g = this.activeTarget;
		if (this.scrollHeight != c && this.refresh(), b >= d) return g != (a = f[f.length - 1]) && this.activate(a);
		if (g && b < e[0]) return this.activeTarget = null, this.clear();
		for (a = e.length; a--;) g != f[a] && b >= e[a] && (void 0 === e[a + 1] || b < e[a + 1]) && this.activate(f[a])
	}, b.prototype.activate = function (b) {
		this.activeTarget = b, this.clear();
		var c = this.selector + '[data-target="' + b + '"],' + this.selector + '[href="' + b + '"]'
			, d = a(c).parents("li").addClass("active");
		d.parent(".dropdown-menu").length && (d = d.closest("li.dropdown").addClass("active")), d.trigger("activate.bs.scrollspy")
	}, b.prototype.clear = function () {
		a(this.selector).parentsUntil(this.options.target, ".active").removeClass("active")
	};
	var d = a.fn.scrollspy;
	a.fn.scrollspy = c, a.fn.scrollspy.Constructor = b, a.fn.scrollspy.noConflict = function () {
		return a.fn.scrollspy = d, this
	}, a(window).on("load.bs.scrollspy.data-api", function () {
		a('[data-spy="scroll"]').each(function () {
			var b = a(this);
			c.call(b, b.data())
		})
	})
}(jQuery), + function (a) {
	"use strict";

	function b(b) {
		return this.each(function () {
			var d = a(this)
				, e = d.data("bs.tab");
			e || d.data("bs.tab", e = new c(this)), "string" == typeof b && e[b]()
		})
	}
	var c = function (b) {
		this.element = a(b)
	};
	c.VERSION = "3.3.6", c.TRANSITION_DURATION = 150, c.prototype.show = function () {
		var b = this.element
			, c = b.closest("ul:not(.dropdown-menu)")
			, d = b.data("target");
		if (d || (d = b.attr("href"), d = d && d.replace(/.*(?=#[^\s]*$)/, "")), !b.parent("li").hasClass("active")) {
			var e = c.find(".active:last a")
				, f = a.Event("hide.bs.tab", {
				relatedTarget: b[0]
			})
				, g = a.Event("show.bs.tab", {
				relatedTarget: e[0]
			});
			if (e.trigger(f), b.trigger(g), !g.isDefaultPrevented() && !f.isDefaultPrevented()) {
				var h = a(d);
				this.activate(b.closest("li"), c), this.activate(h, h.parent(), function () {
					e.trigger({
						type: "hidden.bs.tab"
						, relatedTarget: b[0]
					}), b.trigger({
						type: "shown.bs.tab"
						, relatedTarget: e[0]
					})
				})
			}
		}
	}, c.prototype.activate = function (b, d, e) {
		function f() {
			g.removeClass("active").find("> .dropdown-menu > .active").removeClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded", !1), b.addClass("active").find('[data-toggle="tab"]').attr("aria-expanded", !0), h ? (b[0].offsetWidth, b.addClass("in")) : b.removeClass("fade"), b.parent(".dropdown-menu").length && b.closest("li.dropdown").addClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded", !0), e && e()
		}
		var g = d.find("> .active")
			, h = e && a.support.transition && (g.length && g.hasClass("fade") || !!d.find("> .fade").length);
		g.length && h ? g.one("bsTransitionEnd", f).emulateTransitionEnd(c.TRANSITION_DURATION) : f(), g.removeClass("in")
	};
	var d = a.fn.tab;
	a.fn.tab = b, a.fn.tab.Constructor = c, a.fn.tab.noConflict = function () {
		return a.fn.tab = d, this
	};
	var e = function (c) {
		c.preventDefault(), b.call(a(this), "show")
	};
	a(document).on("click.bs.tab.data-api", '[data-toggle="tab"]', e).on("click.bs.tab.data-api", '[data-toggle="pill"]', e)
}(jQuery), + function (a) {
	"use strict";

	function b(b) {
		return this.each(function () {
			var d = a(this)
				, e = d.data("bs.affix")
				, f = "object" == typeof b && b;
			e || d.data("bs.affix", e = new c(this, f)), "string" == typeof b && e[b]()
		})
	}
	var c = function (b, d) {
		this.options = a.extend({}, c.DEFAULTS, d), this.$target = a(this.options.target).on("scroll.bs.affix.data-api", a.proxy(this.checkPosition, this)).on("click.bs.affix.data-api", a.proxy(this.checkPositionWithEventLoop, this)), this.$element = a(b), this.affixed = null, this.unpin = null, this.pinnedOffset = null, this.checkPosition()
	};
	c.VERSION = "3.3.6", c.RESET = "affix affix-top affix-bottom", c.DEFAULTS = {
		offset: 0
		, target: window
	}, c.prototype.getState = function (a, b, c, d) {
		var e = this.$target.scrollTop()
			, f = this.$element.offset()
			, g = this.$target.height();
		if (null != c && "top" == this.affixed) return c > e ? "top" : !1;
		if ("bottom" == this.affixed) return null != c ? e + this.unpin <= f.top ? !1 : "bottom" : a - d >= e + g ? !1 : "bottom";
		var h = null == this.affixed
			, i = h ? e : f.top
			, j = h ? g : b;
		return null != c && c >= e ? "top" : null != d && i + j >= a - d ? "bottom" : !1
	}, c.prototype.getPinnedOffset = function () {
		if (this.pinnedOffset) return this.pinnedOffset;
		this.$element.removeClass(c.RESET).addClass("affix");
		var a = this.$target.scrollTop()
			, b = this.$element.offset();
		return this.pinnedOffset = b.top - a
	}, c.prototype.checkPositionWithEventLoop = function () {
		setTimeout(a.proxy(this.checkPosition, this), 1)
	}, c.prototype.checkPosition = function () {
		if (this.$element.is(":visible")) {
			var b = this.$element.height()
				, d = this.options.offset
				, e = d.top
				, f = d.bottom
				, g = Math.max(a(document).height(), a(document.body).height());
			"object" != typeof d && (f = e = d), "function" == typeof e && (e = d.top(this.$element)), "function" == typeof f && (f = d.bottom(this.$element));
			var h = this.getState(g, b, e, f);
			if (this.affixed != h) {
				null != this.unpin && this.$element.css("top", "");
				var i = "affix" + (h ? "-" + h : "")
					, j = a.Event(i + ".bs.affix");
				if (this.$element.trigger(j), j.isDefaultPrevented()) return;
				this.affixed = h, this.unpin = "bottom" == h ? this.getPinnedOffset() : null, this.$element.removeClass(c.RESET).addClass(i).trigger(i.replace("affix", "affixed") + ".bs.affix")
			}
			"bottom" == h && this.$element.offset({
				top: g - b - f
			})
		}
	};
	var d = a.fn.affix;
	a.fn.affix = b, a.fn.affix.Constructor = c, a.fn.affix.noConflict = function () {
		return a.fn.affix = d, this
	}, a(window).on("load", function () {
		a('[data-spy="affix"]').each(function () {
			var c = a(this)
				, d = c.data();
			d.offset = d.offset || {}, null != d.offsetBottom && (d.offset.bottom = d.offsetBottom), null != d.offsetTop && (d.offset.top = d.offsetTop), b.call(c, d)
		})
	})
}(jQuery);

/* common.js*/
$(document).ready(function () {
	var $root = $('html, body');

	$('.menu a').click(function () {
		var href = $.attr(this, 'href');

		$root.animate({
			scrollTop: $(href).offset().top - $('.menu').height()
		}, 500, function () {
			window.location.hash = href;
		});
		return false;
	});
	$('#contact_us_link').click(function () {
		var href = $.attr(this, 'href');

		$root.animate({
			scrollTop: $(href).offset().top - $('.menu').height()
		}, 500, function () {
			window.location.hash = href;
		});
		return false;
	});

	$('.check-centre').click(function (e) {
		if (!$(this).hasClass('slick-center')) {
			e.preventDefault();
			var curent_url = $(this).attr('href');
			var curent_title = $(this).find('i.circle');
			$('#service-link').attr('href', curent_url);
			$('#servise-title').html('<h4 class="mini-title">' + curent_title.attr('data-title') + '</h4>');
			$('#servise-desc').html('<h5>' + curent_title.attr('data-description') + '</h5>');
		}
	});

	var link = $('#service-link').attr('href');

	if (link.search('ru') != -1) {
		$('#service-link').attr('href', $('#service-link').attr('href') + '?class=' + 'web-design-ru');
	} else {
		$('#service-link').attr('href', $('#service-link').attr('href') + '?class=' + 'web-design');
	}
	$('span.next-arrow').click(function () {
		var title = $('a.slick-center').next().find('i.circle');
		$('#servise-title').html('<h4 class="mini-title">' + title.attr('data-title') + '</h4>');
		$('#servise-desc').html('<h5>' + title.attr('data-description') + '</h5>');
		if (link.search('ru') != -1) {
			if (title.attr('data-item') != '') {
				$('#service-link').attr('href', link + '?class=' + title.attr('data-item')+'-ru');
			} else {
				$('#service-link').attr('href', link);
			}
		} else {
			if (title.attr('data-item') != '') {
				$('#service-link').attr('href', link + '?class=' + title.attr('data-item'));
			} else {
				$('#service-link').attr('href', link);
			}
		}
	});
	$('span.prev-arrow').click(function () {
		var title = $('a.slick-center').prev().find('i.circle');
		$('#servise-title').html('<h4 class="mini-title">' + title.attr('data-title') + '</h4>');
		$('#servise-desc').html('<h5>' + title.attr('data-description') + '</h5>');
		if (link.search('ru') != -1) {
			if (title.attr('data-item') != '') {
				$('#service-link').attr('href', link + '?class=' + title.attr('data-item')+'-ru');
			} else {
				$('#service-link').attr('href', link);
			}
		} else {
			if (title.attr('data-item') != '') {
				$('#service-link').attr('href', link + '?class=' + title.attr('data-item'));
			} else {
				$('#service-link').attr('href', link);
			}
		}
	});

	//--------------------------- about us hover change text
	$('.first-reason').hover(function () {
		var new_text = $(this).find('.fake-reason').text();

		$(this).find('.reason-hover').text(new_text);

	}, function () {
		var new_text = $(this).find('.real-reason').text();
		$(this).find('.reason-hover').text(new_text);

	});

});

/*
 _ _      _       _
 ___| (_) ___| | __  (_)___
 / __| | |/ __| |/ /  | / __|
 \__ \ | | (__|   < _ | \__ \
 |___/_|_|\___|_|\_(_)/ |___/
 |__/

 Version: 1.6.0
 Author: Ken Wheeler
 Website: http://kenwheeler.github.io
 Docs: http://kenwheeler.github.io/slick
 Repo: http://github.com/kenwheeler/slick
 Issues: http://github.com/kenwheeler/slick/issues

 */
! function (a) {
	"use strict";
	"function" == typeof define && define.amd ? define(["jquery"], a) : "undefined" != typeof exports ? module.exports = a(require("jquery")) : a(jQuery)
}(function (a) {
	"use strict";
	var b = window.Slick || {};
	b = function () {
		function c(c, d) {
			var f, e = this;
			e.defaults = {
				accessibility: !0
				, adaptiveHeight: !1
				, appendArrows: a(c)
				, appendDots: a(c)
				, arrows: !0
				, asNavFor: null
				, prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">Previous</button>'
				, nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button">Next</button>'
				, autoplay: !1
				, autoplaySpeed: 3e3
				, centerMode: !1
				, centerPadding: "50px"
				, cssEase: "ease"
				, customPaging: function (b, c) {
					return a('<button type="button" data-role="none" role="button" tabindex="0" />').text(c + 1)
				}
				, dots: !1
				, dotsClass: "slick-dots"
				, draggable: !0
				, easing: "linear"
				, edgeFriction: .35
				, fade: !1
				, focusOnSelect: !1
				, infinite: !0
				, initialSlide: 0
				, lazyLoad: "ondemand"
				, mobileFirst: !1
				, pauseOnHover: !0
				, pauseOnFocus: !0
				, pauseOnDotsHover: !1
				, respondTo: "window"
				, responsive: null
				, rows: 1
				, rtl: !1
				, slide: ""
				, slidesPerRow: 1
				, slidesToShow: 1
				, slidesToScroll: 1
				, speed: 500
				, swipe: !0
				, swipeToSlide: !1
				, touchMove: !0
				, touchThreshold: 5
				, useCSS: !0
				, useTransform: !0
				, variableWidth: !1
				, vertical: !1
				, verticalSwiping: !1
				, waitForAnimate: !0
				, zIndex: 1e3
			}, e.initials = {
				animating: !1
				, dragging: !1
				, autoPlayTimer: null
				, currentDirection: 0
				, currentLeft: null
				, currentSlide: 0
				, direction: 1
				, $dots: null
				, listWidth: null
				, listHeight: null
				, loadIndex: 0
				, $nextArrow: null
				, $prevArrow: null
				, slideCount: null
				, slideWidth: null
				, $slideTrack: null
				, $slides: null
				, sliding: !1
				, slideOffset: 0
				, swipeLeft: null
				, $list: null
				, touchObject: {}
				, transformsEnabled: !1
				, unslicked: !1
			}, a.extend(e, e.initials), e.activeBreakpoint = null, e.animType = null, e.animProp = null, e.breakpoints = [], e.breakpointSettings = [], e.cssTransitions = !1, e.focussed = !1, e.interrupted = !1, e.hidden = "hidden", e.paused = !0, e.positionProp = null, e.respondTo = null, e.rowCount = 1, e.shouldClick = !0, e.$slider = a(c), e.$slidesCache = null, e.transformType = null, e.transitionType = null, e.visibilityChange = "visibilitychange", e.windowWidth = 0, e.windowTimer = null, f = a(c).data("slick") || {}, e.options = a.extend({}, e.defaults, d, f), e.currentSlide = e.options.initialSlide, e.originalSettings = e.options, "undefined" != typeof document.mozHidden ? (e.hidden = "mozHidden", e.visibilityChange = "mozvisibilitychange") : "undefined" != typeof document.webkitHidden && (e.hidden = "webkitHidden", e.visibilityChange = "webkitvisibilitychange"), e.autoPlay = a.proxy(e.autoPlay, e), e.autoPlayClear = a.proxy(e.autoPlayClear, e), e.autoPlayIterator = a.proxy(e.autoPlayIterator, e), e.changeSlide = a.proxy(e.changeSlide, e), e.clickHandler = a.proxy(e.clickHandler, e), e.selectHandler = a.proxy(e.selectHandler, e), e.setPosition = a.proxy(e.setPosition, e), e.swipeHandler = a.proxy(e.swipeHandler, e), e.dragHandler = a.proxy(e.dragHandler, e), e.keyHandler = a.proxy(e.keyHandler, e), e.instanceUid = b++, e.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/, e.registerBreakpoints(), e.init(!0)
		}
		var b = 0;
		return c
	}(), b.prototype.activateADA = function () {
		var a = this;
		a.$slideTrack.find(".slick-active").attr({
			"aria-hidden": "false"
		}).find("a, input, button, select").attr({
			tabindex: "0"
		})
	}, b.prototype.addSlide = b.prototype.slickAdd = function (b, c, d) {
		var e = this;
		if ("boolean" == typeof c) d = c, c = null;
		else if (0 > c || c >= e.slideCount) return !1;
		e.unload(), "number" == typeof c ? 0 === c && 0 === e.$slides.length ? a(b).appendTo(e.$slideTrack) : d ? a(b).insertBefore(e.$slides.eq(c)) : a(b).insertAfter(e.$slides.eq(c)) : d === !0 ? a(b).prependTo(e.$slideTrack) : a(b).appendTo(e.$slideTrack), e.$slides = e.$slideTrack.children(this.options.slide), e.$slideTrack.children(this.options.slide).detach(), e.$slideTrack.append(e.$slides), e.$slides.each(function (b, c) {
			a(c).attr("data-slick-index", b)
		}), e.$slidesCache = e.$slides, e.reinit()
	}, b.prototype.animateHeight = function () {
		var a = this;
		if (1 === a.options.slidesToShow && a.options.adaptiveHeight === !0 && a.options.vertical === !1) {
			var b = a.$slides.eq(a.currentSlide).outerHeight(!0);
			a.$list.animate({
				height: b
			}, a.options.speed)
		}
	}, b.prototype.animateSlide = function (b, c) {
		var d = {}
			, e = this;
		e.animateHeight(), e.options.rtl === !0 && e.options.vertical === !1 && (b = -b), e.transformsEnabled === !1 ? e.options.vertical === !1 ? e.$slideTrack.animate({
			left: b
		}, e.options.speed, e.options.easing, c) : e.$slideTrack.animate({
			top: b
		}, e.options.speed, e.options.easing, c) : e.cssTransitions === !1 ? (e.options.rtl === !0 && (e.currentLeft = -e.currentLeft), a({
			animStart: e.currentLeft
		}).animate({
			animStart: b
		}, {
			duration: e.options.speed
			, easing: e.options.easing
			, step: function (a) {
				a = Math.ceil(a), e.options.vertical === !1 ? (d[e.animType] = "translate(" + a + "px, 0px)", e.$slideTrack.css(d)) : (d[e.animType] = "translate(0px," + a + "px)", e.$slideTrack.css(d))
			}
			, complete: function () {
				c && c.call()
			}
		})) : (e.applyTransition(), b = Math.ceil(b), e.options.vertical === !1 ? d[e.animType] = "translate3d(" + b + "px, 0px, 0px)" : d[e.animType] = "translate3d(0px," + b + "px, 0px)", e.$slideTrack.css(d), c && setTimeout(function () {
			e.disableTransition(), c.call()
		}, e.options.speed))
	}, b.prototype.getNavTarget = function () {
		var b = this
			, c = b.options.asNavFor;
		return c && null !== c && (c = a(c).not(b.$slider)), c
	}, b.prototype.asNavFor = function (b) {
		var c = this
			, d = c.getNavTarget();
		null !== d && "object" == typeof d && d.each(function () {
			var c = a(this).slick("getSlick");
			c.unslicked || c.slideHandler(b, !0)
		})
	}, b.prototype.applyTransition = function (a) {
		var b = this
			, c = {};
		b.options.fade === !1 ? c[b.transitionType] = b.transformType + " " + b.options.speed + "ms " + b.options.cssEase : c[b.transitionType] = "opacity " + b.options.speed + "ms " + b.options.cssEase, b.options.fade === !1 ? b.$slideTrack.css(c) : b.$slides.eq(a).css(c)
	}, b.prototype.autoPlay = function () {
		var a = this;
		a.autoPlayClear(), a.slideCount > a.options.slidesToShow && (a.autoPlayTimer = setInterval(a.autoPlayIterator, a.options.autoplaySpeed))
	}, b.prototype.autoPlayClear = function () {
		var a = this;
		a.autoPlayTimer && clearInterval(a.autoPlayTimer)
	}, b.prototype.autoPlayIterator = function () {
		var a = this
			, b = a.currentSlide + a.options.slidesToScroll;
		a.paused || a.interrupted || a.focussed || (a.options.infinite === !1 && (1 === a.direction && a.currentSlide + 1 === a.slideCount - 1 ? a.direction = 0 : 0 === a.direction && (b = a.currentSlide - a.options.slidesToScroll, a.currentSlide - 1 === 0 && (a.direction = 1))), a.slideHandler(b))
	}, b.prototype.buildArrows = function () {
		var b = this;
		b.options.arrows === !0 && (b.$prevArrow = a(b.options.prevArrow).addClass("slick-arrow"), b.$nextArrow = a(b.options.nextArrow).addClass("slick-arrow"), b.slideCount > b.options.slidesToShow ? (b.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), b.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), b.htmlExpr.test(b.options.prevArrow) && b.$prevArrow.prependTo(b.options.appendArrows), b.htmlExpr.test(b.options.nextArrow) && b.$nextArrow.appendTo(b.options.appendArrows), b.options.infinite !== !0 && b.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true")) : b.$prevArrow.add(b.$nextArrow).addClass("slick-hidden").attr({
			"aria-disabled": "true"
			, tabindex: "-1"
		}))
	}, b.prototype.buildDots = function () {
		var c, d, b = this;
		if (b.options.dots === !0 && b.slideCount > b.options.slidesToShow) {
			for (b.$slider.addClass("slick-dotted"), d = a("<ul />").addClass(b.options.dotsClass), c = 0; c <= b.getDotCount(); c += 1) d.append(a("<li />").append(b.options.customPaging.call(this, b, c)));
			b.$dots = d.appendTo(b.options.appendDots), b.$dots.find("li").first().addClass("slick-active").attr("aria-hidden", "false")
		}
	}, b.prototype.buildOut = function () {
		var b = this;
		b.$slides = b.$slider.children(b.options.slide + ":not(.slick-cloned)").addClass("slick-slide"), b.slideCount = b.$slides.length, b.$slides.each(function (b, c) {
			a(c).attr("data-slick-index", b).data("originalStyling", a(c).attr("style") || "")
		}), b.$slider.addClass("slick-slider"), b.$slideTrack = 0 === b.slideCount ? a('<div class="slick-track"/>').appendTo(b.$slider) : b.$slides.wrapAll('<div class="slick-track"/>').parent(), b.$list = b.$slideTrack.wrap('<div aria-live="polite" class="slick-list"/>').parent(), b.$slideTrack.css("opacity", 0), (b.options.centerMode === !0 || b.options.swipeToSlide === !0) && (b.options.slidesToScroll = 1), a("img[data-lazy]", b.$slider).not("[src]").addClass("slick-loading"), b.setupInfinite(), b.buildArrows(), b.buildDots(), b.updateDots(), b.setSlideClasses("number" == typeof b.currentSlide ? b.currentSlide : 0), b.options.draggable === !0 && b.$list.addClass("draggable")
	}, b.prototype.buildRows = function () {
		var b, c, d, e, f, g, h, a = this;
		if (e = document.createDocumentFragment(), g = a.$slider.children(), a.options.rows > 1) {
			for (h = a.options.slidesPerRow * a.options.rows, f = Math.ceil(g.length / h), b = 0; f > b; b++) {
				var i = document.createElement("div");
				for (c = 0; c < a.options.rows; c++) {
					var j = document.createElement("div");
					for (d = 0; d < a.options.slidesPerRow; d++) {
						var k = b * h + (c * a.options.slidesPerRow + d);
						g.get(k) && j.appendChild(g.get(k))
					}
					i.appendChild(j)
				}
				e.appendChild(i)
			}
			a.$slider.empty().append(e), a.$slider.children().children().children().css({
				width: 100 / a.options.slidesPerRow + "%"
				, display: "inline-block"
			})
		}
	}, b.prototype.checkResponsive = function (b, c) {
		var e, f, g, d = this
			, h = !1
			, i = d.$slider.width()
			, j = window.innerWidth || a(window).width();
		if ("window" === d.respondTo ? g = j : "slider" === d.respondTo ? g = i : "min" === d.respondTo && (g = Math.min(j, i)), d.options.responsive && d.options.responsive.length && null !== d.options.responsive) {
			f = null;
			for (e in d.breakpoints) d.breakpoints.hasOwnProperty(e) && (d.originalSettings.mobileFirst === !1 ? g < d.breakpoints[e] && (f = d.breakpoints[e]) : g > d.breakpoints[e] && (f = d.breakpoints[e]));
			null !== f ? null !== d.activeBreakpoint ? (f !== d.activeBreakpoint || c) && (d.activeBreakpoint = f, "unslick" === d.breakpointSettings[f] ? d.unslick(f) : (d.options = a.extend({}, d.originalSettings, d.breakpointSettings[f]), b === !0 && (d.currentSlide = d.options.initialSlide), d.refresh(b)), h = f) : (d.activeBreakpoint = f, "unslick" === d.breakpointSettings[f] ? d.unslick(f) : (d.options = a.extend({}, d.originalSettings, d.breakpointSettings[f]), b === !0 && (d.currentSlide = d.options.initialSlide), d.refresh(b)), h = f) : null !== d.activeBreakpoint && (d.activeBreakpoint = null, d.options = d.originalSettings, b === !0 && (d.currentSlide = d.options.initialSlide), d.refresh(b), h = f), b || h === !1 || d.$slider.trigger("breakpoint", [d, h])
		}
	}, b.prototype.changeSlide = function (b, c) {
		var f, g, h, d = this
			, e = a(b.currentTarget);
		switch (e.is("a") && b.preventDefault(), e.is("li") || (e = e.closest("li")), h = d.slideCount % d.options.slidesToScroll !== 0, f = h ? 0 : (d.slideCount - d.currentSlide) % d.options.slidesToScroll, b.data.message) {
			case "previous":
				g = 0 === f ? d.options.slidesToScroll : d.options.slidesToShow - f, d.slideCount > d.options.slidesToShow && d.slideHandler(d.currentSlide - g, !1, c);
				break;
			case "next":
				g = 0 === f ? d.options.slidesToScroll : f, d.slideCount > d.options.slidesToShow && d.slideHandler(d.currentSlide + g, !1, c);
				break;
			case "index":
				var i = 0 === b.data.index ? 0 : b.data.index || e.index() * d.options.slidesToScroll;
				d.slideHandler(d.checkNavigable(i), !1, c), e.children().trigger("focus");
				break;
			default:
				return
		}
	}, b.prototype.checkNavigable = function (a) {
		var c, d, b = this;
		if (c = b.getNavigableIndexes(), d = 0, a > c[c.length - 1]) a = c[c.length - 1];
		else
			for (var e in c) {
				if (a < c[e]) {
					a = d;
					break
				}
				d = c[e]
			}
		return a
	}, b.prototype.cleanUpEvents = function () {
		var b = this;
		b.options.dots && null !== b.$dots && a("li", b.$dots).off("click.slick", b.changeSlide).off("mouseenter.slick", a.proxy(b.interrupt, b, !0)).off("mouseleave.slick", a.proxy(b.interrupt, b, !1)), b.$slider.off("focus.slick blur.slick"), b.options.arrows === !0 && b.slideCount > b.options.slidesToShow && (b.$prevArrow && b.$prevArrow.off("click.slick", b.changeSlide), b.$nextArrow && b.$nextArrow.off("click.slick", b.changeSlide)), b.$list.off("touchstart.slick mousedown.slick", b.swipeHandler), b.$list.off("touchmove.slick mousemove.slick", b.swipeHandler), b.$list.off("touchend.slick mouseup.slick", b.swipeHandler), b.$list.off("touchcancel.slick mouseleave.slick", b.swipeHandler), b.$list.off("click.slick", b.clickHandler), a(document).off(b.visibilityChange, b.visibility), b.cleanUpSlideEvents(), b.options.accessibility === !0 && b.$list.off("keydown.slick", b.keyHandler), b.options.focusOnSelect === !0 && a(b.$slideTrack).children().off("click.slick", b.selectHandler), a(window).off("orientationchange.slick.slick-" + b.instanceUid, b.orientationChange), a(window).off("resize.slick.slick-" + b.instanceUid, b.resize), a("[draggable!=true]", b.$slideTrack).off("dragstart", b.preventDefault), a(window).off("load.slick.slick-" + b.instanceUid, b.setPosition), a(document).off("ready.slick.slick-" + b.instanceUid, b.setPosition)
	}, b.prototype.cleanUpSlideEvents = function () {
		var b = this;
		b.$list.off("mouseenter.slick", a.proxy(b.interrupt, b, !0)), b.$list.off("mouseleave.slick", a.proxy(b.interrupt, b, !1))
	}, b.prototype.cleanUpRows = function () {
		var b, a = this;
		a.options.rows > 1 && (b = a.$slides.children().children(), b.removeAttr("style"), a.$slider.empty().append(b))
	}, b.prototype.clickHandler = function (a) {
		var b = this;
		b.shouldClick === !1 && (a.stopImmediatePropagation(), a.stopPropagation(), a.preventDefault())
	}, b.prototype.destroy = function (b) {
		var c = this;
		c.autoPlayClear(), c.touchObject = {}, c.cleanUpEvents(), a(".slick-cloned", c.$slider).detach(), c.$dots && c.$dots.remove(), c.$prevArrow && c.$prevArrow.length && (c.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), c.htmlExpr.test(c.options.prevArrow) && c.$prevArrow.remove()), c.$nextArrow && c.$nextArrow.length && (c.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), c.htmlExpr.test(c.options.nextArrow) && c.$nextArrow.remove()), c.$slides && (c.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function () {
			a(this).attr("style", a(this).data("originalStyling"))
		}), c.$slideTrack.children(this.options.slide).detach(), c.$slideTrack.detach(), c.$list.detach(), c.$slider.append(c.$slides)), c.cleanUpRows(), c.$slider.removeClass("slick-slider"), c.$slider.removeClass("slick-initialized"), c.$slider.removeClass("slick-dotted"), c.unslicked = !0, b || c.$slider.trigger("destroy", [c])
	}, b.prototype.disableTransition = function (a) {
		var b = this
			, c = {};
		c[b.transitionType] = "", b.options.fade === !1 ? b.$slideTrack.css(c) : b.$slides.eq(a).css(c)
	}, b.prototype.fadeSlide = function (a, b) {
		var c = this;
		c.cssTransitions === !1 ? (c.$slides.eq(a).css({
			zIndex: c.options.zIndex
		}), c.$slides.eq(a).animate({
			opacity: 1
		}, c.options.speed, c.options.easing, b)) : (c.applyTransition(a), c.$slides.eq(a).css({
			opacity: 1
			, zIndex: c.options.zIndex
		}), b && setTimeout(function () {
			c.disableTransition(a), b.call()
		}, c.options.speed))
	}, b.prototype.fadeSlideOut = function (a) {
		var b = this;
		b.cssTransitions === !1 ? b.$slides.eq(a).animate({
			opacity: 0
			, zIndex: b.options.zIndex - 2
		}, b.options.speed, b.options.easing) : (b.applyTransition(a), b.$slides.eq(a).css({
			opacity: 0
			, zIndex: b.options.zIndex - 2
		}))
	}, b.prototype.filterSlides = b.prototype.slickFilter = function (a) {
		var b = this;
		null !== a && (b.$slidesCache = b.$slides, b.unload(), b.$slideTrack.children(this.options.slide).detach(), b.$slidesCache.filter(a).appendTo(b.$slideTrack), b.reinit())
	}, b.prototype.focusHandler = function () {
		var b = this;
		b.$slider.off("focus.slick blur.slick").on("focus.slick blur.slick", "*:not(.slick-arrow)", function (c) {
			c.stopImmediatePropagation();
			var d = a(this);
			setTimeout(function () {
				b.options.pauseOnFocus && (b.focussed = d.is(":focus"), b.autoPlay())
			}, 0)
		})
	}, b.prototype.getCurrent = b.prototype.slickCurrentSlide = function () {
		var a = this;
		return a.currentSlide
	}, b.prototype.getDotCount = function () {
		var a = this
			, b = 0
			, c = 0
			, d = 0;
		if (a.options.infinite === !0)
			for (; b < a.slideCount;) ++d, b = c + a.options.slidesToScroll, c += a.options.slidesToScroll <= a.options.slidesToShow ? a.options.slidesToScroll : a.options.slidesToShow;
		else if (a.options.centerMode === !0) d = a.slideCount;
		else if (a.options.asNavFor)
			for (; b < a.slideCount;) ++d, b = c + a.options.slidesToScroll, c += a.options.slidesToScroll <= a.options.slidesToShow ? a.options.slidesToScroll : a.options.slidesToShow;
		else d = 1 + Math.ceil((a.slideCount - a.options.slidesToShow) / a.options.slidesToScroll);
		return d - 1
	}, b.prototype.getLeft = function (a) {
		var c, d, f, b = this
			, e = 0;
		return b.slideOffset = 0, d = b.$slides.first().outerHeight(!0), b.options.infinite === !0 ? (b.slideCount > b.options.slidesToShow && (b.slideOffset = b.slideWidth * b.options.slidesToShow * -1, e = d * b.options.slidesToShow * -1), b.slideCount % b.options.slidesToScroll !== 0 && a + b.options.slidesToScroll > b.slideCount && b.slideCount > b.options.slidesToShow && (a > b.slideCount ? (b.slideOffset = (b.options.slidesToShow - (a - b.slideCount)) * b.slideWidth * -1, e = (b.options.slidesToShow - (a - b.slideCount)) * d * -1) : (b.slideOffset = b.slideCount % b.options.slidesToScroll * b.slideWidth * -1, e = b.slideCount % b.options.slidesToScroll * d * -1))) : a + b.options.slidesToShow > b.slideCount && (b.slideOffset = (a + b.options.slidesToShow - b.slideCount) * b.slideWidth, e = (a + b.options.slidesToShow - b.slideCount) * d), b.slideCount <= b.options.slidesToShow && (b.slideOffset = 0, e = 0), b.options.centerMode === !0 && b.options.infinite === !0 ? b.slideOffset += b.slideWidth * Math.floor(b.options.slidesToShow / 2) - b.slideWidth : b.options.centerMode === !0 && (b.slideOffset = 0, b.slideOffset += b.slideWidth * Math.floor(b.options.slidesToShow / 2)), c = b.options.vertical === !1 ? a * b.slideWidth * -1 + b.slideOffset : a * d * -1 + e, b.options.variableWidth === !0 && (f = b.slideCount <= b.options.slidesToShow || b.options.infinite === !1 ? b.$slideTrack.children(".slick-slide").eq(a) : b.$slideTrack.children(".slick-slide").eq(a + b.options.slidesToShow), c = b.options.rtl === !0 ? f[0] ? -1 * (b.$slideTrack.width() - f[0].offsetLeft - f.width()) : 0 : f[0] ? -1 * f[0].offsetLeft : 0, b.options.centerMode === !0 && (f = b.slideCount <= b.options.slidesToShow || b.options.infinite === !1 ? b.$slideTrack.children(".slick-slide").eq(a) : b.$slideTrack.children(".slick-slide").eq(a + b.options.slidesToShow + 1), c = b.options.rtl === !0 ? f[0] ? -1 * (b.$slideTrack.width() - f[0].offsetLeft - f.width()) : 0 : f[0] ? -1 * f[0].offsetLeft : 0, c += (b.$list.width() - f.outerWidth()) / 2)), c
	}, b.prototype.getOption = b.prototype.slickGetOption = function (a) {
		var b = this;
		return b.options[a]
	}, b.prototype.getNavigableIndexes = function () {
		var e, a = this
			, b = 0
			, c = 0
			, d = [];
		for (a.options.infinite === !1 ? e = a.slideCount : (b = -1 * a.options.slidesToScroll, c = -1 * a.options.slidesToScroll, e = 2 * a.slideCount); e > b;) d.push(b), b = c + a.options.slidesToScroll, c += a.options.slidesToScroll <= a.options.slidesToShow ? a.options.slidesToScroll : a.options.slidesToShow;
		return d
	}, b.prototype.getSlick = function () {
		return this
	}, b.prototype.getSlideCount = function () {
		var c, d, e, b = this;
		return e = b.options.centerMode === !0 ? b.slideWidth * Math.floor(b.options.slidesToShow / 2) : 0, b.options.swipeToSlide === !0 ? (b.$slideTrack.find(".slick-slide").each(function (c, f) {
			return f.offsetLeft - e + a(f).outerWidth() / 2 > -1 * b.swipeLeft ? (d = f, !1) : void 0
		}), c = Math.abs(a(d).attr("data-slick-index") - b.currentSlide) || 1) : b.options.slidesToScroll
	}, b.prototype.goTo = b.prototype.slickGoTo = function (a, b) {
		var c = this;
		c.changeSlide({
			data: {
				message: "index"
				, index: parseInt(a)
			}
		}, b)
	}, b.prototype.init = function (b) {
		var c = this;
		a(c.$slider).hasClass("slick-initialized") || (a(c.$slider).addClass("slick-initialized"), c.buildRows(), c.buildOut(), c.setProps(), c.startLoad(), c.loadSlider(), c.initializeEvents(), c.updateArrows(), c.updateDots(), c.checkResponsive(!0), c.focusHandler()), b && c.$slider.trigger("init", [c]), c.options.accessibility === !0 && c.initADA(), c.options.autoplay && (c.paused = !1, c.autoPlay())
	}, b.prototype.initADA = function () {
		var b = this;
		b.$slides.add(b.$slideTrack.find(".slick-cloned")).attr({
			"aria-hidden": "true"
			, tabindex: "-1"
		}).find("a, input, button, select").attr({
			tabindex: "-1"
		}), b.$slideTrack.attr("role", "listbox"), b.$slides.not(b.$slideTrack.find(".slick-cloned")).each(function (c) {
			a(this).attr({
				role: "option"
				, "aria-describedby": "slick-slide" + b.instanceUid + c
			})
		}), null !== b.$dots && b.$dots.attr("role", "tablist").find("li").each(function (c) {
			a(this).attr({
				role: "presentation"
				, "aria-selected": "false"
				, "aria-controls": "navigation" + b.instanceUid + c
				, id: "slick-slide" + b.instanceUid + c
			})
		}).first().attr("aria-selected", "true").end().find("button").attr("role", "button").end().closest("div").attr("role", "toolbar"), b.activateADA()
	}, b.prototype.initArrowEvents = function () {
		var a = this;
		a.options.arrows === !0 && a.slideCount > a.options.slidesToShow && (a.$prevArrow.off("click.slick").on("click.slick", {
			message: "previous"
		}, a.changeSlide), a.$nextArrow.off("click.slick").on("click.slick", {
			message: "next"
		}, a.changeSlide))
	}, b.prototype.initDotEvents = function () {
		var b = this;
		b.options.dots === !0 && b.slideCount > b.options.slidesToShow && a("li", b.$dots).on("click.slick", {
			message: "index"
		}, b.changeSlide), b.options.dots === !0 && b.options.pauseOnDotsHover === !0 && a("li", b.$dots).on("mouseenter.slick", a.proxy(b.interrupt, b, !0)).on("mouseleave.slick", a.proxy(b.interrupt, b, !1))
	}, b.prototype.initSlideEvents = function () {
		var b = this;
		b.options.pauseOnHover && (b.$list.on("mouseenter.slick", a.proxy(b.interrupt, b, !0)), b.$list.on("mouseleave.slick", a.proxy(b.interrupt, b, !1)))
	}, b.prototype.initializeEvents = function () {
		var b = this;
		b.initArrowEvents(), b.initDotEvents(), b.initSlideEvents(), b.$list.on("touchstart.slick mousedown.slick", {
			action: "start"
		}, b.swipeHandler), b.$list.on("touchmove.slick mousemove.slick", {
			action: "move"
		}, b.swipeHandler), b.$list.on("touchend.slick mouseup.slick", {
			action: "end"
		}, b.swipeHandler), b.$list.on("touchcancel.slick mouseleave.slick", {
			action: "end"
		}, b.swipeHandler), b.$list.on("click.slick", b.clickHandler), a(document).on(b.visibilityChange, a.proxy(b.visibility, b)), b.options.accessibility === !0 && b.$list.on("keydown.slick", b.keyHandler), b.options.focusOnSelect === !0 && a(b.$slideTrack).children().on("click.slick", b.selectHandler), a(window).on("orientationchange.slick.slick-" + b.instanceUid, a.proxy(b.orientationChange, b)), a(window).on("resize.slick.slick-" + b.instanceUid, a.proxy(b.resize, b)), a("[draggable!=true]", b.$slideTrack).on("dragstart", b.preventDefault), a(window).on("load.slick.slick-" + b.instanceUid, b.setPosition), a(document).on("ready.slick.slick-" + b.instanceUid, b.setPosition)
	}, b.prototype.initUI = function () {
		var a = this;
		a.options.arrows === !0 && a.slideCount > a.options.slidesToShow && (a.$prevArrow.show(), a.$nextArrow.show()), a.options.dots === !0 && a.slideCount > a.options.slidesToShow && a.$dots.show()
	}, b.prototype.keyHandler = function (a) {
		var b = this;
		a.target.tagName.match("TEXTAREA|INPUT|SELECT") || (37 === a.keyCode && b.options.accessibility === !0 ? b.changeSlide({
			data: {
				message: b.options.rtl === !0 ? "next" : "previous"
			}
		}) : 39 === a.keyCode && b.options.accessibility === !0 && b.changeSlide({
			data: {
				message: b.options.rtl === !0 ? "previous" : "next"
			}
		}))
	}, b.prototype.lazyLoad = function () {
		function g(c) {
			a("img[data-lazy]", c).each(function () {
				var c = a(this)
					, d = a(this).attr("data-lazy")
					, e = document.createElement("img");
				e.onload = function () {
					c.animate({
						opacity: 0
					}, 100, function () {
						c.attr("src", d).animate({
							opacity: 1
						}, 200, function () {
							c.removeAttr("data-lazy").removeClass("slick-loading")
						}), b.$slider.trigger("lazyLoaded", [b, c, d])
					})
				}, e.onerror = function () {
					c.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), b.$slider.trigger("lazyLoadError", [b, c, d])
				}, e.src = d
			})
		}
		var c, d, e, f, b = this;
		b.options.centerMode === !0 ? b.options.infinite === !0 ? (e = b.currentSlide + (b.options.slidesToShow / 2 + 1), f = e + b.options.slidesToShow + 2) : (e = Math.max(0, b.currentSlide - (b.options.slidesToShow / 2 + 1)), f = 2 + (b.options.slidesToShow / 2 + 1) + b.currentSlide) : (e = b.options.infinite ? b.options.slidesToShow + b.currentSlide : b.currentSlide, f = Math.ceil(e + b.options.slidesToShow), b.options.fade === !0 && (e > 0 && e--, f <= b.slideCount && f++)), c = b.$slider.find(".slick-slide").slice(e, f), g(c), b.slideCount <= b.options.slidesToShow ? (d = b.$slider.find(".slick-slide"), g(d)) : b.currentSlide >= b.slideCount - b.options.slidesToShow ? (d = b.$slider.find(".slick-cloned").slice(0, b.options.slidesToShow), g(d)) : 0 === b.currentSlide && (d = b.$slider.find(".slick-cloned").slice(-1 * b.options.slidesToShow), g(d))
	}, b.prototype.loadSlider = function () {
		var a = this;
		a.setPosition(), a.$slideTrack.css({
			opacity: 1
		}), a.$slider.removeClass("slick-loading"), a.initUI(), "progressive" === a.options.lazyLoad && a.progressiveLazyLoad()
	}, b.prototype.next = b.prototype.slickNext = function () {
		var a = this;
		a.changeSlide({
			data: {
				message: "next"
			}
		})
	}, b.prototype.orientationChange = function () {
		var a = this;
		a.checkResponsive(), a.setPosition()
	}, b.prototype.pause = b.prototype.slickPause = function () {
		var a = this;
		a.autoPlayClear(), a.paused = !0
	}, b.prototype.play = b.prototype.slickPlay = function () {
		var a = this;
		a.autoPlay(), a.options.autoplay = !0, a.paused = !1, a.focussed = !1, a.interrupted = !1
	}, b.prototype.postSlide = function (a) {
		var b = this;
		b.unslicked || (b.$slider.trigger("afterChange", [b, a]), b.animating = !1, b.setPosition(), b.swipeLeft = null, b.options.autoplay && b.autoPlay(), b.options.accessibility === !0 && b.initADA())
	}, b.prototype.prev = b.prototype.slickPrev = function () {
		var a = this;
		a.changeSlide({
			data: {
				message: "previous"
			}
		})
	}, b.prototype.preventDefault = function (a) {
		a.preventDefault()
	}, b.prototype.progressiveLazyLoad = function (b) {
		b = b || 1;
		var e, f, g, c = this
			, d = a("img[data-lazy]", c.$slider);
		d.length ? (e = d.first(), f = e.attr("data-lazy"), g = document.createElement("img"), g.onload = function () {
			e.attr("src", f).removeAttr("data-lazy").removeClass("slick-loading"), c.options.adaptiveHeight === !0 && c.setPosition(), c.$slider.trigger("lazyLoaded", [c, e, f]), c.progressiveLazyLoad()
		}, g.onerror = function () {
			3 > b ? setTimeout(function () {
				c.progressiveLazyLoad(b + 1)
			}, 500) : (e.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), c.$slider.trigger("lazyLoadError", [c, e, f]), c.progressiveLazyLoad())
		}, g.src = f) : c.$slider.trigger("allImagesLoaded", [c])
	}, b.prototype.refresh = function (b) {
		var d, e, c = this;
		e = c.slideCount - c.options.slidesToShow, !c.options.infinite && c.currentSlide > e && (c.currentSlide = e), c.slideCount <= c.options.slidesToShow && (c.currentSlide = 0), d = c.currentSlide, c.destroy(!0), a.extend(c, c.initials, {
			currentSlide: d
		}), c.init(), b || c.changeSlide({
			data: {
				message: "index"
				, index: d
			}
		}, !1)
	}, b.prototype.registerBreakpoints = function () {
		var c, d, e, b = this
			, f = b.options.responsive || null;
		if ("array" === a.type(f) && f.length) {
			b.respondTo = b.options.respondTo || "window";
			for (c in f)
				if (e = b.breakpoints.length - 1, d = f[c].breakpoint, f.hasOwnProperty(c)) {
					for (; e >= 0;) b.breakpoints[e] && b.breakpoints[e] === d && b.breakpoints.splice(e, 1), e--;
					b.breakpoints.push(d), b.breakpointSettings[d] = f[c].settings
				}
			b.breakpoints.sort(function (a, c) {
				return b.options.mobileFirst ? a - c : c - a
			})
		}
	}, b.prototype.reinit = function () {
		var b = this;
		b.$slides = b.$slideTrack.children(b.options.slide).addClass("slick-slide"), b.slideCount = b.$slides.length, b.currentSlide >= b.slideCount && 0 !== b.currentSlide && (b.currentSlide = b.currentSlide - b.options.slidesToScroll), b.slideCount <= b.options.slidesToShow && (b.currentSlide = 0), b.registerBreakpoints(), b.setProps(), b.setupInfinite(), b.buildArrows(), b.updateArrows(), b.initArrowEvents(), b.buildDots(), b.updateDots(), b.initDotEvents(), b.cleanUpSlideEvents(), b.initSlideEvents(), b.checkResponsive(!1, !0), b.options.focusOnSelect === !0 && a(b.$slideTrack).children().on("click.slick", b.selectHandler), b.setSlideClasses("number" == typeof b.currentSlide ? b.currentSlide : 0), b.setPosition(), b.focusHandler(), b.paused = !b.options.autoplay, b.autoPlay(), b.$slider.trigger("reInit", [b])
	}, b.prototype.resize = function () {
		var b = this;
		a(window).width() !== b.windowWidth && (clearTimeout(b.windowDelay), b.windowDelay = window.setTimeout(function () {
			b.windowWidth = a(window).width(), b.checkResponsive(), b.unslicked || b.setPosition()
		}, 50))
	}, b.prototype.removeSlide = b.prototype.slickRemove = function (a, b, c) {
		var d = this;
		return "boolean" == typeof a ? (b = a, a = b === !0 ? 0 : d.slideCount - 1) : a = b === !0 ? --a : a, d.slideCount < 1 || 0 > a || a > d.slideCount - 1 ? !1 : (d.unload(), c === !0 ? d.$slideTrack.children().remove() : d.$slideTrack.children(this.options.slide).eq(a).remove(), d.$slides = d.$slideTrack.children(this.options.slide), d.$slideTrack.children(this.options.slide).detach(), d.$slideTrack.append(d.$slides), d.$slidesCache = d.$slides, void d.reinit())
	}, b.prototype.setCSS = function (a) {
		var d, e, b = this
			, c = {};
		b.options.rtl === !0 && (a = -a), d = "left" == b.positionProp ? Math.ceil(a) + "px" : "0px", e = "top" == b.positionProp ? Math.ceil(a) + "px" : "0px", c[b.positionProp] = a, b.transformsEnabled === !1 ? b.$slideTrack.css(c) : (c = {}, b.cssTransitions === !1 ? (c[b.animType] = "translate(" + d + ", " + e + ")", b.$slideTrack.css(c)) : (c[b.animType] = "translate3d(" + d + ", " + e + ", 0px)", b.$slideTrack.css(c)))
	}, b.prototype.setDimensions = function () {
		var a = this;
		a.options.vertical === !1 ? a.options.centerMode === !0 && a.$list.css({
			padding: "0px " + a.options.centerPadding
		}) : (a.$list.height(a.$slides.first().outerHeight(!0) * a.options.slidesToShow), a.options.centerMode === !0 && a.$list.css({
			padding: a.options.centerPadding + " 0px"
		})), a.listWidth = a.$list.width(), a.listHeight = a.$list.height(), a.options.vertical === !1 && a.options.variableWidth === !1 ? (a.slideWidth = Math.ceil(a.listWidth / a.options.slidesToShow), a.$slideTrack.width(Math.ceil(a.slideWidth * a.$slideTrack.children(".slick-slide").length))) : a.options.variableWidth === !0 ? a.$slideTrack.width(5e3 * a.slideCount) : (a.slideWidth = Math.ceil(a.listWidth), a.$slideTrack.height(Math.ceil(a.$slides.first().outerHeight(!0) * a.$slideTrack.children(".slick-slide").length)));
		var b = a.$slides.first().outerWidth(!0) - a.$slides.first().width();
		a.options.variableWidth === !1 && a.$slideTrack.children(".slick-slide").width(a.slideWidth - b)
	}, b.prototype.setFade = function () {
		var c, b = this;
		b.$slides.each(function (d, e) {
			c = b.slideWidth * d * -1, b.options.rtl === !0 ? a(e).css({
				position: "relative"
				, right: c
				, top: 0
				, zIndex: b.options.zIndex - 2
				, opacity: 0
			}) : a(e).css({
				position: "relative"
				, left: c
				, top: 0
				, zIndex: b.options.zIndex - 2
				, opacity: 0
			})
		}), b.$slides.eq(b.currentSlide).css({
			zIndex: b.options.zIndex - 1
			, opacity: 1
		})
	}, b.prototype.setHeight = function () {
		var a = this;
		if (1 === a.options.slidesToShow && a.options.adaptiveHeight === !0 && a.options.vertical === !1) {
			var b = a.$slides.eq(a.currentSlide).outerHeight(!0);
			a.$list.css("height", b)
		}
	}, b.prototype.setOption = b.prototype.slickSetOption = function () {
		var c, d, e, f, h, b = this
			, g = !1;
		if ("object" === a.type(arguments[0]) ? (e = arguments[0], g = arguments[1], h = "multiple") : "string" === a.type(arguments[0]) && (e = arguments[0], f = arguments[1], g = arguments[2], "responsive" === arguments[0] && "array" === a.type(arguments[1]) ? h = "responsive" : "undefined" != typeof arguments[1] && (h = "single")), "single" === h) b.options[e] = f;
		else if ("multiple" === h) a.each(e, function (a, c) {
			b.options[a] = c
		});
		else if ("responsive" === h)
			for (d in f)
				if ("array" !== a.type(b.options.responsive)) b.options.responsive = [f[d]];
				else {
					for (c = b.options.responsive.length - 1; c >= 0;) b.options.responsive[c].breakpoint === f[d].breakpoint && b.options.responsive.splice(c, 1), c--;
					b.options.responsive.push(f[d])
				}
		g && (b.unload(), b.reinit())
	}, b.prototype.setPosition = function () {
		var a = this;
		a.setDimensions(), a.setHeight(), a.options.fade === !1 ? a.setCSS(a.getLeft(a.currentSlide)) : a.setFade(), a.$slider.trigger("setPosition", [a])
	}, b.prototype.setProps = function () {
		var a = this
			, b = document.body.style;
		a.positionProp = a.options.vertical === !0 ? "top" : "left", "top" === a.positionProp ? a.$slider.addClass("slick-vertical") : a.$slider.removeClass("slick-vertical"), (void 0 !== b.WebkitTransition || void 0 !== b.MozTransition || void 0 !== b.msTransition) && a.options.useCSS === !0 && (a.cssTransitions = !0), a.options.fade && ("number" == typeof a.options.zIndex ? a.options.zIndex < 3 && (a.options.zIndex = 3) : a.options.zIndex = a.defaults.zIndex), void 0 !== b.OTransform && (a.animType = "OTransform", a.transformType = "-o-transform", a.transitionType = "OTransition", void 0 === b.perspectiveProperty && void 0 === b.webkitPerspective && (a.animType = !1)), void 0 !== b.MozTransform && (a.animType = "MozTransform", a.transformType = "-moz-transform", a.transitionType = "MozTransition", void 0 === b.perspectiveProperty && void 0 === b.MozPerspective && (a.animType = !1)), void 0 !== b.webkitTransform && (a.animType = "webkitTransform", a.transformType = "-webkit-transform", a.transitionType = "webkitTransition", void 0 === b.perspectiveProperty && void 0 === b.webkitPerspective && (a.animType = !1)), void 0 !== b.msTransform && (a.animType = "msTransform", a.transformType = "-ms-transform", a.transitionType = "msTransition", void 0 === b.msTransform && (a.animType = !1)), void 0 !== b.transform && a.animType !== !1 && (a.animType = "transform", a.transformType = "transform", a.transitionType = "transition"), a.transformsEnabled = a.options.useTransform && null !== a.animType && a.animType !== !1
	}, b.prototype.setSlideClasses = function (a) {
		var c, d, e, f, b = this;
		d = b.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden", "true"), b.$slides.eq(a).addClass("slick-current"), b.options.centerMode === !0 ? (c = Math.floor(b.options.slidesToShow / 2), b.options.infinite === !0 && (a >= c && a <= b.slideCount - 1 - c ? b.$slides.slice(a - c, a + c + 1).addClass("slick-active").attr("aria-hidden", "false") : (e = b.options.slidesToShow + a
			, d.slice(e - c + 1, e + c + 2).addClass("slick-active").attr("aria-hidden", "false")), 0 === a ? d.eq(d.length - 1 - b.options.slidesToShow).addClass("slick-center") : a === b.slideCount - 1 && d.eq(b.options.slidesToShow).addClass("slick-center")), b.$slides.eq(a).addClass("slick-center")) : a >= 0 && a <= b.slideCount - b.options.slidesToShow ? b.$slides.slice(a, a + b.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false") : d.length <= b.options.slidesToShow ? d.addClass("slick-active").attr("aria-hidden", "false") : (f = b.slideCount % b.options.slidesToShow, e = b.options.infinite === !0 ? b.options.slidesToShow + a : a, b.options.slidesToShow == b.options.slidesToScroll && b.slideCount - a < b.options.slidesToShow ? d.slice(e - (b.options.slidesToShow - f), e + f).addClass("slick-active").attr("aria-hidden", "false") : d.slice(e, e + b.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false")), "ondemand" === b.options.lazyLoad && b.lazyLoad()
	}, b.prototype.setupInfinite = function () {
		var c, d, e, b = this;
		if (b.options.fade === !0 && (b.options.centerMode = !1), b.options.infinite === !0 && b.options.fade === !1 && (d = null, b.slideCount > b.options.slidesToShow)) {
			for (e = b.options.centerMode === !0 ? b.options.slidesToShow + 1 : b.options.slidesToShow, c = b.slideCount; c > b.slideCount - e; c -= 1) d = c - 1, a(b.$slides[d]).clone(!0).attr("id", "").attr("data-slick-index", d - b.slideCount).prependTo(b.$slideTrack).addClass("slick-cloned");
			for (c = 0; e > c; c += 1) d = c, a(b.$slides[d]).clone(!0).attr("id", "").attr("data-slick-index", d + b.slideCount).appendTo(b.$slideTrack).addClass("slick-cloned");
			b.$slideTrack.find(".slick-cloned").find("[id]").each(function () {
				a(this).attr("id", "")
			})
		}
	}, b.prototype.interrupt = function (a) {
		var b = this;
		a || b.autoPlay(), b.interrupted = a
	}, b.prototype.selectHandler = function (b) {
		var c = this
			, d = a(b.target).is(".slick-slide") ? a(b.target) : a(b.target).parents(".slick-slide")
			, e = parseInt(d.attr("data-slick-index"));
		return e || (e = 0), c.slideCount <= c.options.slidesToShow ? (c.setSlideClasses(e), void c.asNavFor(e)) : void c.slideHandler(e)
	}, b.prototype.slideHandler = function (a, b, c) {
		var d, e, f, g, j, h = null
			, i = this;
		return b = b || !1, i.animating === !0 && i.options.waitForAnimate === !0 || i.options.fade === !0 && i.currentSlide === a || i.slideCount <= i.options.slidesToShow ? void 0 : (b === !1 && i.asNavFor(a), d = a, h = i.getLeft(d), g = i.getLeft(i.currentSlide), i.currentLeft = null === i.swipeLeft ? g : i.swipeLeft, i.options.infinite === !1 && i.options.centerMode === !1 && (0 > a || a > i.getDotCount() * i.options.slidesToScroll) ? void(i.options.fade === !1 && (d = i.currentSlide, c !== !0 ? i.animateSlide(g, function () {
			i.postSlide(d)
		}) : i.postSlide(d))) : i.options.infinite === !1 && i.options.centerMode === !0 && (0 > a || a > i.slideCount - i.options.slidesToScroll) ? void(i.options.fade === !1 && (d = i.currentSlide, c !== !0 ? i.animateSlide(g, function () {
			i.postSlide(d)
		}) : i.postSlide(d))) : (i.options.autoplay && clearInterval(i.autoPlayTimer), e = 0 > d ? i.slideCount % i.options.slidesToScroll !== 0 ? i.slideCount - i.slideCount % i.options.slidesToScroll : i.slideCount + d : d >= i.slideCount ? i.slideCount % i.options.slidesToScroll !== 0 ? 0 : d - i.slideCount : d, i.animating = !0, i.$slider.trigger("beforeChange", [i, i.currentSlide, e]), f = i.currentSlide, i.currentSlide = e, i.setSlideClasses(i.currentSlide), i.options.asNavFor && (j = i.getNavTarget(), j = j.slick("getSlick"), j.slideCount <= j.options.slidesToShow && j.setSlideClasses(i.currentSlide)), i.updateDots(), i.updateArrows(), i.options.fade === !0 ? (c !== !0 ? (i.fadeSlideOut(f), i.fadeSlide(e, function () {
			i.postSlide(e)
		})) : i.postSlide(e), void i.animateHeight()) : void(c !== !0 ? i.animateSlide(h, function () {
			i.postSlide(e)
		}) : i.postSlide(e))))
	}, b.prototype.startLoad = function () {
		var a = this;
		a.options.arrows === !0 && a.slideCount > a.options.slidesToShow && (a.$prevArrow.hide(), a.$nextArrow.hide()), a.options.dots === !0 && a.slideCount > a.options.slidesToShow && a.$dots.hide(), a.$slider.addClass("slick-loading")
	}, b.prototype.swipeDirection = function () {
		var a, b, c, d, e = this;
		return a = e.touchObject.startX - e.touchObject.curX, b = e.touchObject.startY - e.touchObject.curY, c = Math.atan2(b, a), d = Math.round(180 * c / Math.PI), 0 > d && (d = 360 - Math.abs(d)), 45 >= d && d >= 0 ? e.options.rtl === !1 ? "left" : "right" : 360 >= d && d >= 315 ? e.options.rtl === !1 ? "left" : "right" : d >= 135 && 225 >= d ? e.options.rtl === !1 ? "right" : "left" : e.options.verticalSwiping === !0 ? d >= 35 && 135 >= d ? "down" : "up" : "vertical"
	}, b.prototype.swipeEnd = function (a) {
		var c, d, b = this;
		if (b.dragging = !1, b.interrupted = !1, b.shouldClick = b.touchObject.swipeLength > 10 ? !1 : !0, void 0 === b.touchObject.curX) return !1;
		if (b.touchObject.edgeHit === !0 && b.$slider.trigger("edge", [b, b.swipeDirection()]), b.touchObject.swipeLength >= b.touchObject.minSwipe) {
			switch (d = b.swipeDirection()) {
				case "left":
				case "down":
					c = b.options.swipeToSlide ? b.checkNavigable(b.currentSlide + b.getSlideCount()) : b.currentSlide + b.getSlideCount(), b.currentDirection = 0;
					break;
				case "right":
				case "up":
					c = b.options.swipeToSlide ? b.checkNavigable(b.currentSlide - b.getSlideCount()) : b.currentSlide - b.getSlideCount(), b.currentDirection = 1
			}
			"vertical" != d && (b.slideHandler(c), b.touchObject = {}, b.$slider.trigger("swipe", [b, d]))
		} else b.touchObject.startX !== b.touchObject.curX && (b.slideHandler(b.currentSlide), b.touchObject = {})
	}, b.prototype.swipeHandler = function (a) {
		var b = this;
		if (!(b.options.swipe === !1 || "ontouchend" in document && b.options.swipe === !1 || b.options.draggable === !1 && -1 !== a.type.indexOf("mouse"))) switch (b.touchObject.fingerCount = a.originalEvent && void 0 !== a.originalEvent.touches ? a.originalEvent.touches.length : 1, b.touchObject.minSwipe = b.listWidth / b.options.touchThreshold, b.options.verticalSwiping === !0 && (b.touchObject.minSwipe = b.listHeight / b.options.touchThreshold), a.data.action) {
			case "start":
				b.swipeStart(a);
				break;
			case "move":
				b.swipeMove(a);
				break;
			case "end":
				b.swipeEnd(a)
		}
	}, b.prototype.swipeMove = function (a) {
		var d, e, f, g, h, b = this;
		return h = void 0 !== a.originalEvent ? a.originalEvent.touches : null, !b.dragging || h && 1 !== h.length ? !1 : (d = b.getLeft(b.currentSlide), b.touchObject.curX = void 0 !== h ? h[0].pageX : a.clientX, b.touchObject.curY = void 0 !== h ? h[0].pageY : a.clientY, b.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(b.touchObject.curX - b.touchObject.startX, 2))), b.options.verticalSwiping === !0 && (b.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(b.touchObject.curY - b.touchObject.startY, 2)))), e = b.swipeDirection(), "vertical" !== e ? (void 0 !== a.originalEvent && b.touchObject.swipeLength > 4 && a.preventDefault(), g = (b.options.rtl === !1 ? 1 : -1) * (b.touchObject.curX > b.touchObject.startX ? 1 : -1), b.options.verticalSwiping === !0 && (g = b.touchObject.curY > b.touchObject.startY ? 1 : -1), f = b.touchObject.swipeLength, b.touchObject.edgeHit = !1, b.options.infinite === !1 && (0 === b.currentSlide && "right" === e || b.currentSlide >= b.getDotCount() && "left" === e) && (f = b.touchObject.swipeLength * b.options.edgeFriction, b.touchObject.edgeHit = !0), b.options.vertical === !1 ? b.swipeLeft = d + f * g : b.swipeLeft = d + f * (b.$list.height() / b.listWidth) * g, b.options.verticalSwiping === !0 && (b.swipeLeft = d + f * g), b.options.fade === !0 || b.options.touchMove === !1 ? !1 : b.animating === !0 ? (b.swipeLeft = null, !1) : void b.setCSS(b.swipeLeft)) : void 0)
	}, b.prototype.swipeStart = function (a) {
		var c, b = this;
		return b.interrupted = !0, 1 !== b.touchObject.fingerCount || b.slideCount <= b.options.slidesToShow ? (b.touchObject = {}, !1) : (void 0 !== a.originalEvent && void 0 !== a.originalEvent.touches && (c = a.originalEvent.touches[0]), b.touchObject.startX = b.touchObject.curX = void 0 !== c ? c.pageX : a.clientX, b.touchObject.startY = b.touchObject.curY = void 0 !== c ? c.pageY : a.clientY, void(b.dragging = !0))
	}, b.prototype.unfilterSlides = b.prototype.slickUnfilter = function () {
		var a = this;
		null !== a.$slidesCache && (a.unload(), a.$slideTrack.children(this.options.slide).detach(), a.$slidesCache.appendTo(a.$slideTrack), a.reinit())
	}, b.prototype.unload = function () {
		var b = this;
		a(".slick-cloned", b.$slider).remove(), b.$dots && b.$dots.remove(), b.$prevArrow && b.htmlExpr.test(b.options.prevArrow) && b.$prevArrow.remove(), b.$nextArrow && b.htmlExpr.test(b.options.nextArrow) && b.$nextArrow.remove(), b.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden", "true").css("width", "")
	}, b.prototype.unslick = function (a) {
		var b = this;
		b.$slider.trigger("unslick", [b, a]), b.destroy()
	}, b.prototype.updateArrows = function () {
		var b, a = this;
		b = Math.floor(a.options.slidesToShow / 2), a.options.arrows === !0 && a.slideCount > a.options.slidesToShow && !a.options.infinite && (a.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), a.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), 0 === a.currentSlide ? (a.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true"), a.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : a.currentSlide >= a.slideCount - a.options.slidesToShow && a.options.centerMode === !1 ? (a.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), a.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : a.currentSlide >= a.slideCount - 1 && a.options.centerMode === !0 && (a.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), a.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")))
	}, b.prototype.updateDots = function () {
		var a = this;
		null !== a.$dots && (a.$dots.find("li").removeClass("slick-active").attr("aria-hidden", "true"), a.$dots.find("li").eq(Math.floor(a.currentSlide / a.options.slidesToScroll)).addClass("slick-active").attr("aria-hidden", "false"))
	}, b.prototype.visibility = function () {
		var a = this;
		a.options.autoplay && (document[a.hidden] ? a.interrupted = !0 : a.interrupted = !1)
	}, a.fn.slick = function () {
		var f, g, a = this
			, c = arguments[0]
			, d = Array.prototype.slice.call(arguments, 1)
			, e = a.length;
		for (f = 0; e > f; f++)
			if ("object" == typeof c || "undefined" == typeof c ? a[f].slick = new b(a[f], c) : g = a[f].slick[c].apply(a[f].slick, d), "undefined" != typeof g) return g;
		return a
	}
});
/**
 * jquery.hoverdir.js v1.1.0
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 *
 * Copyright 2012, Codrops
 * http://www.codrops.com
 */
;
(function ($, window, undefined) {

	'use strict';

	$.HoverDir = function (options, element) {

		this.$el = $(element);
		this._init(options);

	};

	// the options
	$.HoverDir.defaults = {
		speed: 300
		, easing: 'ease'
		, hoverDelay: 0
		, inverse: false
	};

	$.HoverDir.prototype = {

		_init: function (options) {

			// options
			this.options = $.extend(true, {}, $.HoverDir.defaults, options);
			// transition properties
			this.transitionProp = 'all ' + this.options.speed + 'ms ' + this.options.easing;
			// support for CSS transitions
			this.support = Modernizr.csstransitions;
			// load the events
			this._loadEvents();

		}
		, _loadEvents: function () {

			var self = this;

			this.$el.on('mouseenter.hoverdir, mouseleave.hoverdir', function (event) {

				var $el = $(this)
					, $hoverElem = $el.find('div')
					, direction = self._getDir($el, {
					x: event.pageX
					, y: event.pageY
				})
					, styleCSS = self._getStyle(direction);

				if (event.type === 'mouseenter') {

					$hoverElem.hide().css(styleCSS.from);
					clearTimeout(self.tmhover);

					self.tmhover = setTimeout(function () {

						$hoverElem.show(0, function () {

							var $el = $(this);
							if (self.support) {
								$el.css('transition', self.transitionProp);
							}
							self._applyAnimation($el, styleCSS.to, self.options.speed);

						});


					}, self.options.hoverDelay);

				} else {

					if (self.support) {
						$hoverElem.css('transition', self.transitionProp);
					}
					clearTimeout(self.tmhover);
					self._applyAnimation($hoverElem, styleCSS.from, self.options.speed);

				}

			});

		}
		, // credits : http://stackoverflow.com/a/3647634
		_getDir: function ($el, coordinates) {

			// the width and height of the current div
			var w = $el.width()
				, h = $el.height(),

			// calculate the x and y to get an angle to the center of the div from that x and y.
			// gets the x value relative to the center of the DIV and "normalize" it
				x = (coordinates.x - $el.offset().left - (w / 2)) * (w > h ? (h / w) : 1)
				, y = (coordinates.y - $el.offset().top - (h / 2)) * (h > w ? (w / h) : 1),

			// the angle and the direction from where the mouse came in/went out clockwise (TRBL=0123);
			// first calculate the angle of the point,
			// add 180 deg to get rid of the negative values
			// divide by 90 to get the quadrant
			// add 3 and do a modulo by 4  to shift the quadrants to a proper clockwise TRBL (top/right/bottom/left) **/
				direction = Math.round((((Math.atan2(y, x) * (180 / Math.PI)) + 180) / 90) + 3) % 4;

			return direction;

		}
		, _getStyle: function (direction) {

			var fromStyle, toStyle
				, slideFromTop = {
				left: '0px'
				, top: '-100%'
			}
				, slideFromBottom = {
				left: '0px'
				, top: '100%'
			}
				, slideFromLeft = {
				left: '-100%'
				, top: '0px'
			}
				, slideFromRight = {
				left: '100%'
				, top: '0px'
			}
				, slideTop = {
				top: '0px'
			}
				, slideLeft = {
				left: '0px'
			};

			switch (direction) {
				case 0:
					// from top
					fromStyle = !this.options.inverse ? slideFromTop : slideFromBottom;
					toStyle = slideTop;
					break;
				case 1:
					// from right
					fromStyle = !this.options.inverse ? slideFromRight : slideFromLeft;
					toStyle = slideLeft;
					break;
				case 2:
					// from bottom
					fromStyle = !this.options.inverse ? slideFromBottom : slideFromTop;
					toStyle = slideTop;
					break;
				case 3:
					// from left
					fromStyle = !this.options.inverse ? slideFromLeft : slideFromRight;
					toStyle = slideLeft;
					break;
			};

			return {
				from: fromStyle
				, to: toStyle
			};

		}
		, // apply a transition or fallback to jquery animate based on Modernizr.csstransitions support
		_applyAnimation: function (el, styleCSS, speed) {

			$.fn.applyStyle = this.support ? $.fn.css : $.fn.animate;
			el.stop().applyStyle(styleCSS, $.extend(true, [], {
				duration: speed + 'ms'
			}));

		},

	};

	var logError = function (message) {

		if (window.console) {

			window.console.error(message);

		}

	};

	$.fn.hoverdir = function (options) {

		var instance = $.data(this, 'hoverdir');

		if (typeof options === 'string') {

			var args = Array.prototype.slice.call(arguments, 1);

			this.each(function () {

				if (!instance) {

					logError("cannot call methods on hoverdir prior to initialization; " +
						"attempted to call method '" + options + "'");
					return;

				}

				if (!$.isFunction(instance[options]) || options.charAt(0) === "_") {

					logError("no such method '" + options + "' for hoverdir instance");
					return;

				}

				instance[options].apply(instance, args);

			});

		} else {

			this.each(function () {

				if (instance) {

					instance._init();

				} else {

					instance = $.data(this, 'hoverdir', new $.HoverDir(options, this));

				}

			});

		}

		return instance;

	};

})(jQuery, window);

/* jshint debug: true, expr: true */

// ;(function($){

//   /* Constants & defaults. */
//   var DATA_COLOR    = 'data-ab-color';
//   var DATA_PARENT   = 'data-ab-parent';
//   var DATA_CSS_BG   = 'data-ab-css-background';
//   var EVENT_CF      = 'ab-color-found';

//   var DEFAULTS      = {
//     selector:             '[data-adaptive-background]',
//     parent:               null,
//     exclude:              [ 'rgb(0,0,0)', 'rgb(255,255,255)' ],
//     normalizeTextColor:   true,
//     normalizedTextColors:  {
//       light:      "#fff",
//       dark:       "#000"
//     },
//     lumaClasses:  {
//       light:      "ab-light",
//       dark:       "ab-dark"
//     }
//   };

//   // Include RGBaster - https://github.com/briangonzalez/rgbaster.js
//   /* jshint ignore:start */
//   !function(n){"use strict";var t=function(){return document.createElement("canvas").getContext("2d")},e=function(n,e){var a=new Image,o=n.src||n;"data:"!==o.substring(0,5)&&(a.crossOrigin="Anonymous"),a.onload=function(){var n=t("2d");n.drawImage(a,0,0);var o=n.getImageData(0,0,a.width,a.height);e&&e(o.data)},a.src=o},a=function(n){return["rgb(",n,")"].join("")},o=function(n){return n.map(function(n){return a(n.name)})},r=5,i=10,c={};c.colors=function(n,t){t=t||{};var c=t.exclude||[],u=t.paletteSize||i;e(n,function(e){for(var i=n.width*n.height||e.length,m={},s="",d=[],f={dominant:{name:"",count:0},palette:Array.apply(null,new Array(u)).map(Boolean).map(function(){return{name:"0,0,0",count:0}})},l=0;i>l;){if(d[0]=e[l],d[1]=e[l+1],d[2]=e[l+2],s=d.join(","),m[s]=s in m?m[s]+1:1,-1===c.indexOf(a(s))){var g=m[s];g>f.dominant.count?(f.dominant.name=s,f.dominant.count=g):f.palette.some(function(n){return g>n.count?(n.name=s,n.count=g,!0):void 0})}l+=4*r}if(t.success){var p=o(f.palette);t.success({dominant:a(f.dominant.name),secondary:p[0],palette:p})}})},n.RGBaster=n.RGBaster||c}(window);
//   /* jshint ignore:end */


//   /*
//     Our main function declaration.
//     */
//     $.adaptiveBackground = {
//       run: function( options ){
//         var opts = $.extend({}, DEFAULTS, options);

//       /* Loop over each element, waiting for it to load
//          then finding its color, and triggering the
//          color found event when color has been found.
//          */
//          $( opts.selector ).each(function(index, el){
//           var $this = $(this);

//         /*  Small helper functions which applies
//             colors, attrs, triggers events, etc.
//             */
//             var handleColors = function () {
//               var img = useCSSBackground() ? getCSSBackground() : $this[0];

//               RGBaster.colors(img, {
//                 paletteSize: 20,
//                 exclude: opts.exclude,
//                 success: function(colors) {
//                   $this.attr(DATA_COLOR, colors.dominant);
//                   $this.trigger(EVENT_CF, { color: colors.dominant, palette: colors.palette });
//                 }
//               });

//             };

//             var useCSSBackground = function(){
//               var attr = $this.attr( DATA_CSS_BG );
//               return (typeof attr !== typeof undefined && attr !== false);
//             };

//             var getCSSBackground = function(){
//               var str = $this.css('background-image');
//               var regex = /\(([^)]+)\)/;
// var match = regex.exec(str)[1].replace(/"/g, '')
// return match;
// };

// /* Subscribe to our color-found event. */
// $this.on( EVENT_CF, function(ev, data){

//           // Try to find the parent.
//           var $parent;
//           if ( opts.parent && $this.parents( opts.parent ).length ) {
//             $parent = $this.parents( opts.parent );
//           }
//           else if ( $this.attr( DATA_PARENT ) && $this.parents( $this.attr( DATA_PARENT ) ).length ){
//             $parent = $this.parents( $this.attr( DATA_PARENT ) );
//           }
//           else if ( useCSSBackground() ){
//             $parent = $this;
//           }
//           else if (opts.parent) {
//             $parent = $this.parents( opts.parent );
//           }
//           else {
//             $parent = $this.next();
//           }

//           $parent.css({ backgroundColor: data.color });

//           // Helper function to calculate yiq - http://en.wikipedia.org/wiki/YIQ
//           var getYIQ = function(color){
//             var rgb = data.color.match(/\d+/g);
//             return ((rgb[0]*299)+(rgb[1]*587)+(rgb[2]*114))/1000;
//           };

//           var getNormalizedTextColor = function (color){
//             return getYIQ(color) >= 128 ? opts.normalizedTextColors.dark : opts.normalizedTextColors.light;
//           };

//           var getLumaClass = function (color){
//             return getYIQ(color) <= 128 ? opts.lumaClasses.dark : opts.lumaClasses.light;
//           };

//           // Normalize the text color based on luminance.
//           if ( opts.normalizeTextColor )
//             $parent.css({ color: getNormalizedTextColor(data.color) });

//           // Add a class based on luminance.
//           $parent.addClass( getLumaClass(data.color) )
//           .attr('data-ab-yaq', getYIQ(data.color));

//           opts.success && opts.success($this, data);
//         });

// /* Handle the colors. */
// handleColors();

// });
// }
// };

// })(jQuery);

/* Modernizr 2.6.2 (Custom Build) | MIT & BSD
 * Build: http://modernizr.com/download/#-csstransitions-shiv-cssclasses-testprop-testallprops-domprefixes-load
 */
;
window.Modernizr = function (a, b, c) {
	function x(a) {
		j.cssText = a
	}

	function y(a, b) {
		return x(prefixes.join(a + ";") + (b || ""))
	}

	function z(a, b) {
		return typeof a === b
	}

	function A(a, b) {
		return !!~("" + a).indexOf(b)
	}

	function B(a, b) {
		for (var d in a) {
			var e = a[d];
			if (!A(e, "-") && j[e] !== c) return b == "pfx" ? e : !0
		}
		return !1
	}

	function C(a, b, d) {
		for (var e in a) {
			var f = b[a[e]];
			if (f !== c) return d === !1 ? a[e] : z(f, "function") ? f.bind(d || b) : f
		}
		return !1
	}

	function D(a, b, c) {
		var d = a.charAt(0).toUpperCase() + a.slice(1)
			, e = (a + " " + n.join(d + " ") + d).split(" ");
		return z(b, "string") || z(b, "undefined") ? B(e, b) : (e = (a + " " + o.join(d + " ") + d).split(" "), C(e, b, c))
	}
	var d = "2.6.2"
		, e = {}
		, f = !0
		, g = b.documentElement
		, h = "modernizr"
		, i = b.createElement(h)
		, j = i.style
		, k, l = {}.toString
		, m = "Webkit Moz O ms"
		, n = m.split(" ")
		, o = m.toLowerCase().split(" ")
		, p = {}
		, q = {}
		, r = {}
		, s = []
		, t = s.slice
		, u, v = {}.hasOwnProperty
		, w;
	!z(v, "undefined") && !z(v.call, "undefined") ? w = function (a, b) {
		return v.call(a, b)
	} : w = function (a, b) {
		return b in a && z(a.constructor.prototype[b], "undefined")
	}, Function.prototype.bind || (Function.prototype.bind = function (b) {
		var c = this;
		if (typeof c != "function") throw new TypeError;
		var d = t.call(arguments, 1)
			, e = function () {
			if (this instanceof e) {
				var a = function () {};
				a.prototype = c.prototype;
				var f = new a
					, g = c.apply(f, d.concat(t.call(arguments)));
				return Object(g) === g ? g : f
			}
			return c.apply(b, d.concat(t.call(arguments)))
		};
		return e
	}), p.csstransitions = function () {
		return D("transition")
	};
	for (var E in p) w(p, E) && (u = E.toLowerCase(), e[u] = p[E](), s.push((e[u] ? "" : "no-") + u));
	return e.addTest = function (a, b) {
		if (typeof a == "object")
			for (var d in a) w(a, d) && e.addTest(d, a[d]);
		else {
			a = a.toLowerCase();
			if (e[a] !== c) return e;
			b = typeof b == "function" ? b() : b, typeof f != "undefined" && f && (g.className += " " + (b ? "" : "no-") + a), e[a] = b
		}
		return e
	}, x(""), i = k = null
		, function (a, b) {
		function k(a, b) {
			var c = a.createElement("p")
				, d = a.getElementsByTagName("head")[0] || a.documentElement;
			return c.innerHTML = "x<style>" + b + "</style>", d.insertBefore(c.lastChild, d.firstChild)
		}

		function l() {
			var a = r.elements;
			return typeof a == "string" ? a.split(" ") : a
		}

		function m(a) {
			var b = i[a[g]];
			return b || (b = {}, h++, a[g] = h, i[h] = b), b
		}

		function n(a, c, f) {
			c || (c = b);
			if (j) return c.createElement(a);
			f || (f = m(c));
			var g;
			return f.cache[a] ? g = f.cache[a].cloneNode() : e.test(a) ? g = (f.cache[a] = f.createElem(a)).cloneNode() : g = f.createElem(a), g.canHaveChildren && !d.test(a) ? f.frag.appendChild(g) : g
		}

		function o(a, c) {
			a || (a = b);
			if (j) return a.createDocumentFragment();
			c = c || m(a);
			var d = c.frag.cloneNode()
				, e = 0
				, f = l()
				, g = f.length;
			for (; e < g; e++) d.createElement(f[e]);
			return d
		}

		function p(a, b) {
			b.cache || (b.cache = {}, b.createElem = a.createElement, b.createFrag = a.createDocumentFragment, b.frag = b.createFrag()), a.createElement = function (c) {
				return r.shivMethods ? n(c, a, b) : b.createElem(c)
			}, a.createDocumentFragment = Function("h,f", "return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&(" + l().join().replace(/\w+/g, function (a) {
					return b.createElem(a), b.frag.createElement(a), 'c("' + a + '")'
				}) + ");return n}")(r, b.frag)
		}

		function q(a) {
			a || (a = b);
			var c = m(a);
			return r.shivCSS && !f && !c.hasCSS && (c.hasCSS = !!k(a, "article,aside,figcaption,figure,footer,header,hgroup,nav,section{display:block}mark{background:#FF0;color:#000}")), j || p(a, c), a
		}
		var c = a.html5 || {}
			, d = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i
			, e = /^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i
			, f, g = "_html5shiv"
			, h = 0
			, i = {}
			, j;
		(function () {
			try {
				var a = b.createElement("a");
				a.innerHTML = "<xyz></xyz>", f = "hidden" in a, j = a.childNodes.length == 1 || function () {
						b.createElement("a");
						var a = b.createDocumentFragment();
						return typeof a.cloneNode == "undefined" || typeof a.createDocumentFragment == "undefined" || typeof a.createElement == "undefined"
					}()
			} catch (c) {
				f = !0, j = !0
			}
		})();
		var r = {
			elements: c.elements || "abbr article aside audio bdi canvas data datalist details figcaption figure footer header hgroup mark meter nav output progress section summary time video"
			, shivCSS: c.shivCSS !== !1
			, supportsUnknownElements: j
			, shivMethods: c.shivMethods !== !1
			, type: "default"
			, shivDocument: q
			, createElement: n
			, createDocumentFragment: o
		};
		a.html5 = r, q(b)
	}(this, b), e._version = d, e._domPrefixes = o, e._cssomPrefixes = n, e.testProp = function (a) {
		return B([a])
	}, e.testAllProps = D, g.className = g.className.replace(/(^|\s)no-js(\s|$)/, "$1$2") + (f ? " js " + s.join(" ") : ""), e
}(this, this.document)
	, function (a, b, c) {
	function d(a) {
		return "[object Function]" == o.call(a)
	}

	function e(a) {
		return "string" == typeof a
	}

	function f() {}

	function g(a) {
		return !a || "loaded" == a || "complete" == a || "uninitialized" == a
	}

	function h() {
		var a = p.shift();
		q = 1, a ? a.t ? m(function () {
			("c" == a.t ? B.injectCss : B.injectJs)(a.s, 0, a.a, a.x, a.e, 1)
		}, 0) : (a(), h()) : q = 0
	}

	function i(a, c, d, e, f, i, j) {
		function k(b) {
			if (!o && g(l.readyState) && (u.r = o = 1, !q && h(), l.onload = l.onreadystatechange = null, b)) {
				"img" != a && m(function () {
					t.removeChild(l)
				}, 50);
				for (var d in y[c]) y[c].hasOwnProperty(d) && y[c][d].onload()
			}
		}
		var j = j || B.errorTimeout
			, l = b.createElement(a)
			, o = 0
			, r = 0
			, u = {
			t: d
			, s: c
			, e: f
			, a: i
			, x: j
		};
		1 === y[c] && (r = 1, y[c] = []), "object" == a ? l.data = c : (l.src = c, l.type = a), l.width = l.height = "0", l.onerror = l.onload = l.onreadystatechange = function () {
			k.call(this, r)
		}, p.splice(e, 0, u), "img" != a && (r || 2 === y[c] ? (t.insertBefore(l, s ? null : n), m(k, j)) : y[c].push(l))
	}

	function j(a, b, c, d, f) {
		return q = 0, b = b || "j", e(a) ? i("c" == b ? v : u, a, b, this.i++, c, d, f) : (p.splice(this.i++, 0, a), 1 == p.length && h()), this
	}

	function k() {
		var a = B;
		return a.loader = {
			load: j
			, i: 0
		}, a
	}
	var l = b.documentElement
		, m = a.setTimeout
		, n = b.getElementsByTagName("script")[0]
		, o = {}.toString
		, p = []
		, q = 0
		, r = "MozAppearance" in l.style
		, s = r && !!b.createRange().compareNode
		, t = s ? l : n.parentNode
		, l = a.opera && "[object Opera]" == o.call(a.opera)
		, l = !!b.attachEvent && !l
		, u = r ? "object" : l ? "script" : "img"
		, v = l ? "script" : u
		, w = Array.isArray || function (a) {
			return "[object Array]" == o.call(a)
		}
		, x = []
		, y = {}
		, z = {
		timeout: function (a, b) {
			return b.length && (a.timeout = b[0]), a
		}
	}
		, A, B;
	B = function (a) {
		function b(a) {
			var a = a.split("!")
				, b = x.length
				, c = a.pop()
				, d = a.length
				, c = {
				url: c
				, origUrl: c
				, prefixes: a
			}
				, e, f, g;
			for (f = 0; f < d; f++) g = a[f].split("="), (e = z[g.shift()]) && (c = e(c, g));
			for (f = 0; f < b; f++) c = x[f](c);
			return c
		}

		function g(a, e, f, g, h) {
			var i = b(a)
				, j = i.autoCallback;
			i.url.split(".").pop().split("?").shift(), i.bypass || (e && (e = d(e) ? e : e[a] || e[g] || e[a.split("/").pop().split("?")[0]]), i.instead ? i.instead(a, e, f, g, h) : (y[i.url] ? i.noexec = !0 : y[i.url] = 1, f.load(i.url, i.forceCSS || !i.forceJS && "css" == i.url.split(".").pop().split("?").shift() ? "c" : c, i.noexec, i.attrs, i.timeout), (d(e) || d(j)) && f.load(function () {
				k(), e && e(i.origUrl, h, g), j && j(i.origUrl, h, g), y[i.url] = 2
			})))
		}

		function h(a, b) {
			function c(a, c) {
				if (a) {
					if (e(a)) c || (j = function () {
						var a = [].slice.call(arguments);
						k.apply(this, a), l()
					}), g(a, j, b, 0, h);
					else if (Object(a) === a)
						for (n in m = function () {
							var b = 0
								, c;
							for (c in a) a.hasOwnProperty(c) && b++;
							return b
						}(), a) a.hasOwnProperty(n) && (!c && !--m && (d(j) ? j = function () {
							var a = [].slice.call(arguments);
							k.apply(this, a), l()
						} : j[n] = function (a) {
							return function () {
								var b = [].slice.call(arguments);
								a && a.apply(this, b), l()
							}
						}(k[n])), g(a[n], j, b, n, h))
				} else !c && l()
			}
			var h = !!a.test
				, i = a.load || a.both
				, j = a.callback || f
				, k = j
				, l = a.complete || f
				, m, n;
			c(h ? a.yep : a.nope, !!i), i && c(i)
		}
		var i, j, l = this.yepnope.loader;
		if (e(a)) g(a, 0, l, 0);
		else if (w(a))
			for (i = 0; i < a.length; i++) j = a[i], e(j) ? g(j, 0, l, 0) : w(j) ? B(j) : Object(j) === j && h(j, l);
		else Object(a) === a && h(a, l)
	}, B.addPrefix = function (a, b) {
		z[a] = b
	}, B.addFilter = function (a) {
		x.push(a)
	}, B.errorTimeout = 1e4, null == b.readyState && b.addEventListener && (b.readyState = "loading", b.addEventListener("DOMContentLoaded", A = function () {
		b.removeEventListener("DOMContentLoaded", A, 0), b.readyState = "complete"
	}, 0)), a.yepnope = k(), a.yepnope.executeStack = h, a.yepnope.injectJs = function (a, c, d, e, i, j) {
		var k = b.createElement("script")
			, l, o, e = e || B.errorTimeout;
		k.src = a;
		for (o in d) k.setAttribute(o, d[o]);
		c = j ? h : c || f, k.onreadystatechange = k.onload = function () {
			!l && g(k.readyState) && (l = 1, c(), k.onload = k.onreadystatechange = null)
		}, m(function () {
			l || (l = 1, c(1))
		}, e), i ? k.onload() : n.parentNode.insertBefore(k, n)
	}, a.yepnope.injectCss = function (a, c, d, e, g, i) {
		var e = b.createElement("link")
			, j, c = i ? h : c || f;
		e.href = a, e.rel = "stylesheet", e.type = "text/css";
		for (j in d) e.setAttribute(j, d[j]);
		g || (n.parentNode.insertBefore(e, n), m(c, 0))
	}
}(this, document), Modernizr.load = function () {
	yepnope.apply(window, [].slice.call(arguments, 0))
};
/*!
 * fancyBox - jQuery Plugin
 * version: 2.1.5 (Fri, 14 Jun 2013)
 * @requires jQuery v1.6 or later
 *
 * Examples at http://fancyapps.com/fancybox/
 * License: www.fancyapps.com/fancybox/#license
 *
 * Copyright 2012 Janis Skarnelis - janis@fancyapps.com
 *
 */

(function (window, document, $, undefined) {
	"use strict";

	var H = $("html")
		, W = $(window)
		, D = $(document)
		, F = $.fancybox = function () {
			F.open.apply(this, arguments);
		}
		, IE = navigator.userAgent.match(/msie/i)
		, didUpdate = null
		, isTouch = document.createTouch !== undefined,

		isQuery = function (obj) {
			return obj && obj.hasOwnProperty && obj instanceof $;
		}
		, isString = function (str) {
			return str && $.type(str) === "string";
		}
		, isPercentage = function (str) {
			return isString(str) && str.indexOf('%') > 0;
		}
		, isScrollable = function (el) {
			return (el && !(el.style.overflow && el.style.overflow === 'hidden') && ((el.clientWidth && el.scrollWidth > el.clientWidth) || (el.clientHeight && el.scrollHeight > el.clientHeight)));
		}
		, getScalar = function (orig, dim) {
			var value = parseInt(orig, 10) || 0;

			if (dim && isPercentage(orig)) {
				value = F.getViewport()[dim] / 100 * value;
			}

			return Math.ceil(value);
		}
		, getValue = function (value, dim) {
			return getScalar(value, dim) + 'px';
		};

	$.extend(F, {
		// The current version of fancyBox
		version: '2.1.5',

		defaults: {
			padding: 15
			, margin: 20,

			width: 800
			, height: 600
			, minWidth: 100
			, minHeight: 100
			, maxWidth: 9999
			, maxHeight: 9999
			, pixelRatio: 1, // Set to 2 for retina display support

			autoSize: true
			, autoHeight: false
			, autoWidth: false,

			autoResize: true
			, autoCenter: !isTouch
			, fitToView: true
			, aspectRatio: false
			, topRatio: 0.5
			, leftRatio: 0.5,

			scrolling: 'auto', // 'auto', 'yes' or 'no'
			wrapCSS: '',

			arrows: true
			, closeBtn: true
			, closeClick: false
			, nextClick: false
			, mouseWheel: true
			, autoPlay: false
			, playSpeed: 3000
			, preload: 3
			, modal: false
			, loop: true,

			ajax: {
				dataType: 'html'
				, headers: {
					'X-fancyBox': true
				}
			}
			, iframe: {
				scrolling: 'auto'
				, preload: true
			}
			, swf: {
				wmode: 'transparent'
				, allowfullscreen: 'true'
				, allowscriptaccess: 'always'
			},

			keys: {
				next: {
					13: 'left', // enter
					34: 'up', // page down
					39: 'left', // right arrow
					40: 'up' // down arrow
				}
				, prev: {
					8: 'right', // backspace
					33: 'down', // page up
					37: 'right', // left arrow
					38: 'down' // up arrow
				}
				, close: [27], // escape key
				play: [32], // space - start/stop slideshow
				toggle: [70] // letter "f" - toggle fullscreen
			},

			direction: {
				next: 'left'
				, prev: 'right'
			},

			scrollOutside: true,

			// Override some properties
			index: 0
			, type: null
			, href: null
			, content: null
			, title: null,

			// HTML templates
			tpl: {
				wrap: '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>'
				, image: '<img class="fancybox-image" src="{href}" alt="" />'
				, iframe: '<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen' + (IE ? ' allowtransparency="true"' : '') + '></iframe>'
				, error: '<p class="fancybox-error">The requested content cannot be loaded.<br/>Please try again later.</p>'
				, closeBtn: '<a title="Close" class="fancybox-item fancybox-close" href="javascript:;"></a>'
				, next: '<a title="Next" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>'
				, prev: '<a title="Previous" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
			},

			// Properties for each animation type
			// Opening fancyBox
			openEffect: 'fade', // 'elastic', 'fade' or 'none'
			openSpeed: 250
			, openEasing: 'swing'
			, openOpacity: true
			, openMethod: 'zoomIn',

			// Closing fancyBox
			closeEffect: 'fade', // 'elastic', 'fade' or 'none'
			closeSpeed: 250
			, closeEasing: 'swing'
			, closeOpacity: true
			, closeMethod: 'zoomOut',

			// Changing next gallery item
			nextEffect: 'elastic', // 'elastic', 'fade' or 'none'
			nextSpeed: 250
			, nextEasing: 'swing'
			, nextMethod: 'changeIn',

			// Changing previous gallery item
			prevEffect: 'elastic', // 'elastic', 'fade' or 'none'
			prevSpeed: 250
			, prevEasing: 'swing'
			, prevMethod: 'changeOut',

			// Enable default helpers
			helpers: {
				overlay: true
				, title: true
			},

			// Callbacks
			onCancel: $.noop, // If canceling
			beforeLoad: $.noop, // Before loading
			afterLoad: $.noop, // After loading
			beforeShow: $.noop, // Before changing in current item
			afterShow: $.noop, // After opening
			beforeChange: $.noop, // Before changing gallery item
			beforeClose: $.noop, // Before closing
			afterClose: $.noop // After closing
		},

		//Current state
		group: {}, // Selected group
		opts: {}, // Group options
		previous: null, // Previous element
		coming: null, // Element being loaded
		current: null, // Currently loaded element
		isActive: false, // Is activated
		isOpen: false, // Is currently open
		isOpened: false, // Have been fully opened at least once

		wrap: null
		, skin: null
		, outer: null
		, inner: null,

		player: {
			timer: null
			, isActive: false
		},

		// Loaders
		ajaxLoad: null
		, imgPreload: null,

		// Some collections
		transitions: {}
		, helpers: {},

		/*
		 *	Static methods
		 */

		open: function (group, opts) {
			if (!group) {
				return;
			}

			if (!$.isPlainObject(opts)) {
				opts = {};
			}

			// Close if already active
			if (false === F.close(true)) {
				return;
			}

			// Normalize group
			if (!$.isArray(group)) {
				group = isQuery(group) ? $(group).get() : [group];
			}

			// Recheck if the type of each element is `object` and set content type (image, ajax, etc)
			$.each(group, function (i, element) {
				var obj = {}
					, href
					, title
					, content
					, type
					, rez
					, hrefParts
					, selector;

				if ($.type(element) === "object") {
					// Check if is DOM element
					if (element.nodeType) {
						element = $(element);
					}

					if (isQuery(element)) {
						obj = {
							href: element.data('fancybox-href') || element.attr('href')
							, title: element.data('fancybox-title') || element.attr('title')
							, isDom: true
							, element: element
						};

						if ($.metadata) {
							$.extend(true, obj, element.metadata());
						}

					} else {
						obj = element;
					}
				}

				href = opts.href || obj.href || (isString(element) ? element : null);
				title = opts.title !== undefined ? opts.title : obj.title || '';

				content = opts.content || obj.content;
				type = content ? 'html' : (opts.type || obj.type);

				if (!type && obj.isDom) {
					type = element.data('fancybox-type');

					if (!type) {
						rez = element.prop('class').match(/fancybox\.(\w+)/);
						type = rez ? rez[1] : null;
					}
				}

				if (isString(href)) {
					// Try to guess the content type
					if (!type) {
						if (F.isImage(href)) {
							type = 'image';

						} else if (F.isSWF(href)) {
							type = 'swf';

						} else if (href.charAt(0) === '#') {
							type = 'inline';

						} else if (isString(element)) {
							type = 'html';
							content = element;
						}
					}

					// Split url into two pieces with source url and content selector, e.g,
					// "/mypage.html #my_id" will load "/mypage.html" and display element having id "my_id"
					if (type === 'ajax') {
						hrefParts = href.split(/\s+/, 2);
						href = hrefParts.shift();
						selector = hrefParts.shift();
					}
				}

				if (!content) {
					if (type === 'inline') {
						if (href) {
							content = $(isString(href) ? href.replace(/.*(?=#[^\s]+$)/, '') : href); //strip for ie7

						} else if (obj.isDom) {
							content = element;
						}

					} else if (type === 'html') {
						content = href;

					} else if (!type && !href && obj.isDom) {
						type = 'inline';
						content = element;
					}
				}

				$.extend(obj, {
					href: href
					, type: type
					, content: content
					, title: title
					, selector: selector
				});

				group[i] = obj;
			});

			// Extend the defaults
			F.opts = $.extend(true, {}, F.defaults, opts);

			// All options are merged recursive except keys
			if (opts.keys !== undefined) {
				F.opts.keys = opts.keys ? $.extend({}, F.defaults.keys, opts.keys) : false;
			}

			F.group = group;

			return F._start(F.opts.index);
		},

		// Cancel image loading or abort ajax request
		cancel: function () {
			var coming = F.coming;

			if (!coming || false === F.trigger('onCancel')) {
				return;
			}

			F.hideLoading();

			if (F.ajaxLoad) {
				F.ajaxLoad.abort();
			}

			F.ajaxLoad = null;

			if (F.imgPreload) {
				F.imgPreload.onload = F.imgPreload.onerror = null;
			}

			if (coming.wrap) {
				coming.wrap.stop(true, true).trigger('onReset').remove();
			}

			F.coming = null;

			// If the first item has been canceled, then clear everything
			if (!F.current) {
				F._afterZoomOut(coming);
			}
		},

		// Start closing animation if is open; remove immediately if opening/closing
		close: function (event) {
			F.cancel();

			if (false === F.trigger('beforeClose')) {
				return;
			}

			F.unbindEvents();

			if (!F.isActive) {
				return;
			}

			if (!F.isOpen || event === true) {
				$('.fancybox-wrap').stop(true).trigger('onReset').remove();

				F._afterZoomOut();

			} else {
				F.isOpen = F.isOpened = false;
				F.isClosing = true;

				$('.fancybox-item, .fancybox-nav').remove();

				F.wrap.stop(true, true).removeClass('fancybox-opened');

				F.transitions[F.current.closeMethod]();
			}
		},

		// Manage slideshow:
		//   $.fancybox.play(); - toggle slideshow
		//   $.fancybox.play( true ); - start
		//   $.fancybox.play( false ); - stop
		play: function (action) {
			var clear = function () {
				clearTimeout(F.player.timer);
			}
				, set = function () {
				clear();

				if (F.current && F.player.isActive) {
					F.player.timer = setTimeout(F.next, F.current.playSpeed);
				}
			}
				, stop = function () {
				clear();

				D.unbind('.player');

				F.player.isActive = false;

				F.trigger('onPlayEnd');
			}
				, start = function () {
				if (F.current && (F.current.loop || F.current.index < F.group.length - 1)) {
					F.player.isActive = true;

					D.bind({
						'onCancel.player beforeClose.player': stop
						, 'onUpdate.player': set
						, 'beforeLoad.player': clear
					});

					set();

					F.trigger('onPlayStart');
				}
			};

			if (action === true || (!F.player.isActive && action !== false)) {
				start();
			} else {
				stop();
			}
		},

		// Navigate to next gallery item
		next: function (direction) {
			var current = F.current;

			if (current) {
				if (!isString(direction)) {
					direction = current.direction.next;
				}

				F.jumpto(current.index + 1, direction, 'next');
			}
		},

		// Navigate to previous gallery item
		prev: function (direction) {
			var current = F.current;

			if (current) {
				if (!isString(direction)) {
					direction = current.direction.prev;
				}

				F.jumpto(current.index - 1, direction, 'prev');
			}
		},

		// Navigate to gallery item by index
		jumpto: function (index, direction, router) {
			var current = F.current;

			if (!current) {
				return;
			}

			index = getScalar(index);

			F.direction = direction || current.direction[(index >= current.index ? 'next' : 'prev')];
			F.router = router || 'jumpto';

			if (current.loop) {
				if (index < 0) {
					index = current.group.length + (index % current.group.length);
				}

				index = index % current.group.length;
			}

			if (current.group[index] !== undefined) {
				F.cancel();

				F._start(index);
			}
		},

		// Center inside viewport and toggle position type to fixed or absolute if needed
		reposition: function (e, onlyAbsolute) {
			var current = F.current
				, wrap = current ? current.wrap : null
				, pos;

			if (wrap) {
				pos = F._getPosition(onlyAbsolute);

				if (e && e.type === 'scroll') {
					delete pos.position;

					wrap.stop(true, true).animate(pos, 200);

				} else {
					wrap.css(pos);

					current.pos = $.extend({}, current.dim, pos);
				}
			}
		},

		update: function (e) {
			var type = (e && e.type)
				, anyway = !type || type === 'orientationchange';

			if (anyway) {
				clearTimeout(didUpdate);

				didUpdate = null;
			}

			if (!F.isOpen || didUpdate) {
				return;
			}

			didUpdate = setTimeout(function () {
				var current = F.current;

				if (!current || F.isClosing) {
					return;
				}

				F.wrap.removeClass('fancybox-tmp');

				if (anyway || type === 'load' || (type === 'resize' && current.autoResize)) {
					F._setDimension();
				}

				if (!(type === 'scroll' && current.canShrink)) {
					F.reposition(e);
				}

				F.trigger('onUpdate');

				didUpdate = null;

			}, (anyway && !isTouch ? 0 : 300));
		},

		// Shrink content to fit inside viewport or restore if resized
		toggle: function (action) {
			if (F.isOpen) {
				F.current.fitToView = $.type(action) === "boolean" ? action : !F.current.fitToView;

				// Help browser to restore document dimensions
				if (isTouch) {
					F.wrap.removeAttr('style').addClass('fancybox-tmp');

					F.trigger('onUpdate');
				}

				F.update();
			}
		},

		hideLoading: function () {
			D.unbind('.loading');

			$('#fancybox-loading').remove();
		},

		showLoading: function () {
			var el, viewport;

			F.hideLoading();

			el = $('<div id="fancybox-loading"><div></div></div>').click(F.cancel).appendTo('body');

			// If user will press the escape-button, the request will be canceled
			D.bind('keydown.loading', function (e) {
				if ((e.which || e.keyCode) === 27) {
					e.preventDefault();

					F.cancel();
				}
			});

			if (!F.defaults.fixed) {
				viewport = F.getViewport();

				el.css({
					position: 'absolute'
					, top: (viewport.h * 0.5) + viewport.y
					, left: (viewport.w * 0.5) + viewport.x
				});
			}
		},

		getViewport: function () {
			var locked = (F.current && F.current.locked) || false
				, rez = {
				x: W.scrollLeft()
				, y: W.scrollTop()
			};

			if (locked) {
				rez.w = locked[0].clientWidth;
				rez.h = locked[0].clientHeight;

			} else {
				// See http://bugs.jquery.com/ticket/6724
				rez.w = isTouch && window.innerWidth ? window.innerWidth : W.width();
				rez.h = isTouch && window.innerHeight ? window.innerHeight : W.height();
			}

			return rez;
		},

		// Unbind the keyboard / clicking actions
		unbindEvents: function () {
			if (F.wrap && isQuery(F.wrap)) {
				F.wrap.unbind('.fb');
			}

			D.unbind('.fb');
			W.unbind('.fb');
		},

		bindEvents: function () {
			var current = F.current
				, keys;

			if (!current) {
				return;
			}

			// Changing document height on iOS devices triggers a 'resize' event,
			// that can change document height... repeating infinitely
			W.bind('orientationchange.fb' + (isTouch ? '' : ' resize.fb') + (current.autoCenter && !current.locked ? ' scroll.fb' : ''), F.update);

			keys = current.keys;

			if (keys) {
				D.bind('keydown.fb', function (e) {
					var code = e.which || e.keyCode
						, target = e.target || e.srcElement;

					// Skip esc key if loading, because showLoading will cancel preloading
					if (code === 27 && F.coming) {
						return false;
					}

					// Ignore key combinations and key events within form elements
					if (!e.ctrlKey && !e.altKey && !e.shiftKey && !e.metaKey && !(target && (target.type || $(target).is('[contenteditable]')))) {
						$.each(keys, function (i, val) {
							if (current.group.length > 1 && val[code] !== undefined) {
								F[i](val[code]);

								e.preventDefault();
								return false;
							}

							if ($.inArray(code, val) > -1) {
								F[i]();

								e.preventDefault();
								return false;
							}
						});
					}
				});
			}

			if ($.fn.mousewheel && current.mouseWheel) {
				F.wrap.bind('mousewheel.fb', function (e, delta, deltaX, deltaY) {
					var target = e.target || null
						, parent = $(target)
						, canScroll = false;

					while (parent.length) {
						if (canScroll || parent.is('.fancybox-skin') || parent.is('.fancybox-wrap')) {
							break;
						}

						canScroll = isScrollable(parent[0]);
						parent = $(parent).parent();
					}

					if (delta !== 0 && !canScroll) {
						if (F.group.length > 1 && !current.canShrink) {
							if (deltaY > 0 || deltaX > 0) {
								F.prev(deltaY > 0 ? 'down' : 'left');

							} else if (deltaY < 0 || deltaX < 0) {
								F.next(deltaY < 0 ? 'up' : 'right');
							}

							e.preventDefault();
						}
					}
				});
			}
		},

		trigger: function (event, o) {
			var ret, obj = o || F.coming || F.current;

			if (!obj) {
				return;
			}

			if ($.isFunction(obj[event])) {
				ret = obj[event].apply(obj, Array.prototype.slice.call(arguments, 1));
			}

			if (ret === false) {
				return false;
			}

			if (obj.helpers) {
				$.each(obj.helpers, function (helper, opts) {
					if (opts && F.helpers[helper] && $.isFunction(F.helpers[helper][event])) {
						F.helpers[helper][event]($.extend(true, {}, F.helpers[helper].defaults, opts), obj);
					}
				});
			}

			D.trigger(event);
		},

		isImage: function (str) {
			return isString(str) && str.match(/(^data:image\/.*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp|svg)((\?|#).*)?$)/i);
		},

		isSWF: function (str) {
			return isString(str) && str.match(/\.(swf)((\?|#).*)?$/i);
		},

		_start: function (index) {
			var coming = {}
				, obj
				, href
				, type
				, margin
				, padding;

			index = getScalar(index);
			obj = F.group[index] || null;

			if (!obj) {
				return false;
			}

			coming = $.extend(true, {}, F.opts, obj);

			// Convert margin and padding properties to array - top, right, bottom, left
			margin = coming.margin;
			padding = coming.padding;

			if ($.type(margin) === 'number') {
				coming.margin = [margin, margin, margin, margin];
			}

			if ($.type(padding) === 'number') {
				coming.padding = [padding, padding, padding, padding];
			}

			// 'modal' propery is just a shortcut
			if (coming.modal) {
				$.extend(true, coming, {
					closeBtn: false
					, closeClick: false
					, nextClick: false
					, arrows: false
					, mouseWheel: false
					, keys: null
					, helpers: {
						overlay: {
							closeClick: false
						}
					}
				});
			}

			// 'autoSize' property is a shortcut, too
			if (coming.autoSize) {
				coming.autoWidth = coming.autoHeight = true;
			}

			if (coming.width === 'auto') {
				coming.autoWidth = true;
			}

			if (coming.height === 'auto') {
				coming.autoHeight = true;
			}

			/*
			 * Add reference to the group, so it`s possible to access from callbacks, example:
			 * afterLoad : function() {
			 *     this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
			 * }
			 */

			coming.group = F.group;
			coming.index = index;

			// Give a chance for callback or helpers to update coming item (type, title, etc)
			F.coming = coming;

			if (false === F.trigger('beforeLoad')) {
				F.coming = null;

				return;
			}

			type = coming.type;
			href = coming.href;

			if (!type) {
				F.coming = null;

				//If we can not determine content type then drop silently or display next/prev item if looping through gallery
				if (F.current && F.router && F.router !== 'jumpto') {
					F.current.index = index;

					return F[F.router](F.direction);
				}

				return false;
			}

			F.isActive = true;

			if (type === 'image' || type === 'swf') {
				coming.autoHeight = coming.autoWidth = false;
				coming.scrolling = 'visible';
			}

			if (type === 'image') {
				coming.aspectRatio = true;
			}

			if (type === 'iframe' && isTouch) {
				coming.scrolling = 'scroll';
			}

			// Build the neccessary markup
			coming.wrap = $(coming.tpl.wrap).addClass('fancybox-' + (isTouch ? 'mobile' : 'desktop') + ' fancybox-type-' + type + ' fancybox-tmp ' + coming.wrapCSS).appendTo(coming.parent || 'body');

			$.extend(coming, {
				skin: $('.fancybox-skin', coming.wrap)
				, outer: $('.fancybox-outer', coming.wrap)
				, inner: $('.fancybox-inner', coming.wrap)
			});

			$.each(["Top", "Right", "Bottom", "Left"], function (i, v) {
				coming.skin.css('padding' + v, getValue(coming.padding[i]));
			});

			F.trigger('onReady');

			// Check before try to load; 'inline' and 'html' types need content, others - href
			if (type === 'inline' || type === 'html') {
				if (!coming.content || !coming.content.length) {
					return F._error('content');
				}

			} else if (!href) {
				return F._error('href');
			}

			if (type === 'image') {
				F._loadImage();

			} else if (type === 'ajax') {
				F._loadAjax();

			} else if (type === 'iframe') {
				F._loadIframe();

			} else {
				F._afterLoad();
			}
		},

		_error: function (type) {
			$.extend(F.coming, {
				type: 'html'
				, autoWidth: true
				, autoHeight: true
				, minWidth: 0
				, minHeight: 0
				, scrolling: 'no'
				, hasError: type
				, content: F.coming.tpl.error
			});

			F._afterLoad();
		},

		_loadImage: function () {
			// Reset preload image so it is later possible to check "complete" property
			var img = F.imgPreload = new Image();

			img.onload = function () {
				this.onload = this.onerror = null;

				F.coming.width = this.width / F.opts.pixelRatio;
				F.coming.height = this.height / F.opts.pixelRatio;

				F._afterLoad();
			};

			img.onerror = function () {
				this.onload = this.onerror = null;

				F._error('image');
			};

			img.src = F.coming.href;

			if (img.complete !== true) {
				F.showLoading();
			}
		},

		_loadAjax: function () {
			var coming = F.coming;

			F.showLoading();

			F.ajaxLoad = $.ajax($.extend({}, coming.ajax, {
				url: coming.href
				, error: function (jqXHR, textStatus) {
					if (F.coming && textStatus !== 'abort') {
						F._error('ajax', jqXHR);

					} else {
						F.hideLoading();
					}
				}
				, success: function (data, textStatus) {
					if (textStatus === 'success') {
						coming.content = data;

						F._afterLoad();
					}
				}
			}));
		},

		_loadIframe: function () {
			var coming = F.coming
				, iframe = $(coming.tpl.iframe.replace(/\{rnd\}/g, new Date().getTime()))
				.attr('scrolling', isTouch ? 'auto' : coming.iframe.scrolling)
				.attr('src', coming.href);

			// This helps IE
			$(coming.wrap).bind('onReset', function () {
				try {
					$(this).find('iframe').hide().attr('src', '//about:blank').end().empty();
				} catch (e) {}
			});

			if (coming.iframe.preload) {
				F.showLoading();

				iframe.one('load', function () {
					$(this).data('ready', 1);

					// iOS will lose scrolling if we resize
					if (!isTouch) {
						$(this).bind('load.fb', F.update);
					}

					// Without this trick:
					//   - iframe won't scroll on iOS devices
					//   - IE7 sometimes displays empty iframe
					$(this).parents('.fancybox-wrap').width('100%').removeClass('fancybox-tmp').show();

					F._afterLoad();
				});
			}

			coming.content = iframe.appendTo(coming.inner);

			if (!coming.iframe.preload) {
				F._afterLoad();
			}
		},

		_preloadImages: function () {
			var group = F.group
				, current = F.current
				, len = group.length
				, cnt = current.preload ? Math.min(current.preload, len - 1) : 0
				, item
				, i;

			for (i = 1; i <= cnt; i += 1) {
				item = group[(current.index + i) % len];

				if (item.type === 'image' && item.href) {
					new Image().src = item.href;
				}
			}
		},

		_afterLoad: function () {
			var coming = F.coming
				, previous = F.current
				, placeholder = 'fancybox-placeholder'
				, current
				, content
				, type
				, scrolling
				, href
				, embed;

			F.hideLoading();

			if (!coming || F.isActive === false) {
				return;
			}

			if (false === F.trigger('afterLoad', coming, previous)) {
				coming.wrap.stop(true).trigger('onReset').remove();

				F.coming = null;

				return;
			}

			if (previous) {
				F.trigger('beforeChange', previous);

				previous.wrap.stop(true).removeClass('fancybox-opened')
					.find('.fancybox-item, .fancybox-nav')
					.remove();
			}

			F.unbindEvents();

			current = coming;
			content = coming.content;
			type = coming.type;
			scrolling = coming.scrolling;

			$.extend(F, {
				wrap: current.wrap
				, skin: current.skin
				, outer: current.outer
				, inner: current.inner
				, current: current
				, previous: previous
			});

			href = current.href;

			switch (type) {
				case 'inline':
				case 'ajax':
				case 'html':
					if (current.selector) {
						content = $('<div>').html(content).find(current.selector);

					} else if (isQuery(content)) {
						if (!content.data(placeholder)) {
							content.data(placeholder, $('<div class="' + placeholder + '"></div>').insertAfter(content).hide());
						}

						content = content.show().detach();

						current.wrap.bind('onReset', function () {
							if ($(this).find(content).length) {
								content.hide().replaceAll(content.data(placeholder)).data(placeholder, false);
							}
						});
					}
					break;

				case 'image':
					content = current.tpl.image.replace('{href}', href);
					break;

				case 'swf':
					content = '<object id="fancybox-swf" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="100%" height="100%"><param name="movie" value="' + href + '"></param>';
					embed = '';

					$.each(current.swf, function (name, val) {
						content += '<param name="' + name + '" value="' + val + '"></param>';
						embed += ' ' + name + '="' + val + '"';
					});

					content += '<embed src="' + href + '" type="application/x-shockwave-flash" width="100%" height="100%"' + embed + '></embed></object>';
					break;
			}

			if (!(isQuery(content) && content.parent().is(current.inner))) {
				current.inner.append(content);
			}

			// Give a chance for helpers or callbacks to update elements
			F.trigger('beforeShow');

			// Set scrolling before calculating dimensions
			current.inner.css('overflow', scrolling === 'yes' ? 'scroll' : (scrolling === 'no' ? 'hidden' : scrolling));

			// Set initial dimensions and start position
			F._setDimension();

			F.reposition();

			F.isOpen = false;
			F.coming = null;

			F.bindEvents();

			if (!F.isOpened) {
				$('.fancybox-wrap').not(current.wrap).stop(true).trigger('onReset').remove();

			} else if (previous.prevMethod) {
				F.transitions[previous.prevMethod]();
			}

			F.transitions[F.isOpened ? current.nextMethod : current.openMethod]();

			F._preloadImages();
		},

		_setDimension: function () {
			var viewport = F.getViewport()
				, steps = 0
				, canShrink = false
				, canExpand = false
				, wrap = F.wrap
				, skin = F.skin
				, inner = F.inner
				, current = F.current
				, width = current.width
				, height = current.height
				, minWidth = current.minWidth
				, minHeight = current.minHeight
				, maxWidth = current.maxWidth
				, maxHeight = current.maxHeight
				, scrolling = current.scrolling
				, scrollOut = current.scrollOutside ? current.scrollbarWidth : 0
				, margin = current.margin
				, wMargin = getScalar(margin[1] + margin[3])
				, hMargin = getScalar(margin[0] + margin[2])
				, wPadding
				, hPadding
				, wSpace
				, hSpace
				, origWidth
				, origHeight
				, origMaxWidth
				, origMaxHeight
				, ratio
				, width_
				, height_
				, maxWidth_
				, maxHeight_
				, iframe
				, body;

			// Reset dimensions so we could re-check actual size
			wrap.add(skin).add(inner).width('auto').height('auto').removeClass('fancybox-tmp');

			wPadding = getScalar(skin.outerWidth(true) - skin.width());
			hPadding = getScalar(skin.outerHeight(true) - skin.height());

			// Any space between content and viewport (margin, padding, border, title)
			wSpace = wMargin + wPadding;
			hSpace = hMargin + hPadding;

			origWidth = isPercentage(width) ? (viewport.w - wSpace) * getScalar(width) / 100 : width;
			origHeight = isPercentage(height) ? (viewport.h - hSpace) * getScalar(height) / 100 : height;

			if (current.type === 'iframe') {
				iframe = current.content;

				if (current.autoHeight && iframe.data('ready') === 1) {
					try {
						if (iframe[0].contentWindow.document.location) {
							inner.width(origWidth).height(9999);

							body = iframe.contents().find('body');

							if (scrollOut) {
								body.css('overflow-x', 'hidden');
							}

							origHeight = body.outerHeight(true);
						}

					} catch (e) {}
				}

			} else if (current.autoWidth || current.autoHeight) {
				inner.addClass('fancybox-tmp');

				// Set width or height in case we need to calculate only one dimension
				if (!current.autoWidth) {
					inner.width(origWidth);
				}

				if (!current.autoHeight) {
					inner.height(origHeight);
				}

				if (current.autoWidth) {
					origWidth = inner.width();
				}

				if (current.autoHeight) {
					origHeight = inner.height();
				}

				inner.removeClass('fancybox-tmp');
			}

			width = getScalar(origWidth);
			height = getScalar(origHeight);

			ratio = origWidth / origHeight;

			// Calculations for the content
			minWidth = getScalar(isPercentage(minWidth) ? getScalar(minWidth, 'w') - wSpace : minWidth);
			maxWidth = getScalar(isPercentage(maxWidth) ? getScalar(maxWidth, 'w') - wSpace : maxWidth);

			minHeight = getScalar(isPercentage(minHeight) ? getScalar(minHeight, 'h') - hSpace : minHeight);
			maxHeight = getScalar(isPercentage(maxHeight) ? getScalar(maxHeight, 'h') - hSpace : maxHeight);

			// These will be used to determine if wrap can fit in the viewport
			origMaxWidth = maxWidth;
			origMaxHeight = maxHeight;

			if (current.fitToView) {
				maxWidth = Math.min(viewport.w - wSpace, maxWidth);
				maxHeight = Math.min(viewport.h - hSpace, maxHeight);
			}

			maxWidth_ = viewport.w - wMargin;
			maxHeight_ = viewport.h - hMargin;

			if (current.aspectRatio) {
				if (width > maxWidth) {
					width = maxWidth;
					height = getScalar(width / ratio);
				}

				if (height > maxHeight) {
					height = maxHeight;
					width = getScalar(height * ratio);
				}

				if (width < minWidth) {
					width = minWidth;
					height = getScalar(width / ratio);
				}

				if (height < minHeight) {
					height = minHeight;
					width = getScalar(height * ratio);
				}

			} else {
				width = Math.max(minWidth, Math.min(width, maxWidth));

				if (current.autoHeight && current.type !== 'iframe') {
					inner.width(width);

					height = inner.height();
				}

				height = Math.max(minHeight, Math.min(height, maxHeight));
			}

			// Try to fit inside viewport (including the title)
			if (current.fitToView) {
				inner.width(width).height(height);

				wrap.width(width + wPadding);

				// Real wrap dimensions
				width_ = wrap.width();
				height_ = wrap.height();

				if (current.aspectRatio) {
					while ((width_ > maxWidth_ || height_ > maxHeight_) && width > minWidth && height > minHeight) {
						if (steps++ > 19) {
							break;
						}

						height = Math.max(minHeight, Math.min(maxHeight, height - 10));
						width = getScalar(height * ratio);

						if (width < minWidth) {
							width = minWidth;
							height = getScalar(width / ratio);
						}

						if (width > maxWidth) {
							width = maxWidth;
							height = getScalar(width / ratio);
						}

						inner.width(width).height(height);

						wrap.width(width + wPadding);

						width_ = wrap.width();
						height_ = wrap.height();
					}

				} else {
					width = Math.max(minWidth, Math.min(width, width - (width_ - maxWidth_)));
					height = Math.max(minHeight, Math.min(height, height - (height_ - maxHeight_)));
				}
			}

			if (scrollOut && scrolling === 'auto' && height < origHeight && (width + wPadding + scrollOut) < maxWidth_) {
				width += scrollOut;
			}

			inner.width(width).height(height);

			wrap.width(width + wPadding);

			width_ = wrap.width();
			height_ = wrap.height();

			canShrink = (width_ > maxWidth_ || height_ > maxHeight_) && width > minWidth && height > minHeight;
			canExpand = current.aspectRatio ? (width < origMaxWidth && height < origMaxHeight && width < origWidth && height < origHeight) : ((width < origMaxWidth || height < origMaxHeight) && (width < origWidth || height < origHeight));

			$.extend(current, {
				dim: {
					width: getValue(width_)
					, height: getValue(height_)
				}
				, origWidth: origWidth
				, origHeight: origHeight
				, canShrink: canShrink
				, canExpand: canExpand
				, wPadding: wPadding
				, hPadding: hPadding
				, wrapSpace: height_ - skin.outerHeight(true)
				, skinSpace: skin.height() - height
			});

			if (!iframe && current.autoHeight && height > minHeight && height < maxHeight && !canExpand) {
				inner.height('auto');
			}
		},

		_getPosition: function (onlyAbsolute) {
			var current = F.current
				, viewport = F.getViewport()
				, margin = current.margin
				, width = F.wrap.width() + margin[1] + margin[3]
				, height = F.wrap.height() + margin[0] + margin[2]
				, rez = {
				position: 'absolute'
				, top: margin[0]
				, left: margin[3]
			};

			if (current.autoCenter && current.fixed && !onlyAbsolute && height <= viewport.h && width <= viewport.w) {
				rez.position = 'fixed';

			} else if (!current.locked) {
				rez.top += viewport.y;
				rez.left += viewport.x;
			}

			rez.top = getValue(Math.max(rez.top, rez.top + ((viewport.h - height) * current.topRatio)));
			rez.left = getValue(Math.max(rez.left, rez.left + ((viewport.w - width) * current.leftRatio)));

			return rez;
		},

		_afterZoomIn: function () {
			var current = F.current;

			if (!current) {
				return;
			}

			F.isOpen = F.isOpened = true;

			F.wrap.css('overflow', 'visible').addClass('fancybox-opened');

			F.update();

			// Assign a click event
			if (current.closeClick || (current.nextClick && F.group.length > 1)) {
				F.inner.css('cursor', 'pointer').bind('click.fb', function (e) {
					if (!$(e.target).is('a') && !$(e.target).parent().is('a')) {
						e.preventDefault();

						F[current.closeClick ? 'close' : 'next']();
					}
				});
			}

			// Create a close button
			if (current.closeBtn) {
				$(current.tpl.closeBtn).appendTo(F.skin).bind('click.fb', function (e) {
					e.preventDefault();

					F.close();
				});
			}

			// Create navigation arrows
			if (current.arrows && F.group.length > 1) {
				if (current.loop || current.index > 0) {
					$(current.tpl.prev).appendTo(F.outer).bind('click.fb', F.prev);
				}

				if (current.loop || current.index < F.group.length - 1) {
					$(current.tpl.next).appendTo(F.outer).bind('click.fb', F.next);
				}
			}

			F.trigger('afterShow');

			// Stop the slideshow if this is the last item
			if (!current.loop && current.index === current.group.length - 1) {
				F.play(false);

			} else if (F.opts.autoPlay && !F.player.isActive) {
				F.opts.autoPlay = false;

				F.play();
			}
		},

		_afterZoomOut: function (obj) {
			obj = obj || F.current;

			$('.fancybox-wrap').trigger('onReset').remove();

			$.extend(F, {
				group: {}
				, opts: {}
				, router: false
				, current: null
				, isActive: false
				, isOpened: false
				, isOpen: false
				, isClosing: false
				, wrap: null
				, skin: null
				, outer: null
				, inner: null
			});

			F.trigger('afterClose', obj);
		}
	});

	/*
	 *	Default transitions
	 */

	F.transitions = {
		getOrigPosition: function () {
			var current = F.current
				, element = current.element
				, orig = current.orig
				, pos = {}
				, width = 50
				, height = 50
				, hPadding = current.hPadding
				, wPadding = current.wPadding
				, viewport = F.getViewport();

			if (!orig && current.isDom && element.is(':visible')) {
				orig = element.find('img:first');

				if (!orig.length) {
					orig = element;
				}
			}

			if (isQuery(orig)) {
				pos = orig.offset();

				if (orig.is('img')) {
					width = orig.outerWidth();
					height = orig.outerHeight();
				}

			} else {
				pos.top = viewport.y + (viewport.h - height) * current.topRatio;
				pos.left = viewport.x + (viewport.w - width) * current.leftRatio;
			}

			if (F.wrap.css('position') === 'fixed' || current.locked) {
				pos.top -= viewport.y;
				pos.left -= viewport.x;
			}

			pos = {
				top: getValue(pos.top - hPadding * current.topRatio)
				, left: getValue(pos.left - wPadding * current.leftRatio)
				, width: getValue(width + wPadding)
				, height: getValue(height + hPadding)
			};

			return pos;
		},

		step: function (now, fx) {
			var ratio
				, padding
				, value
				, prop = fx.prop
				, current = F.current
				, wrapSpace = current.wrapSpace
				, skinSpace = current.skinSpace;

			if (prop === 'width' || prop === 'height') {
				ratio = fx.end === fx.start ? 1 : (now - fx.start) / (fx.end - fx.start);

				if (F.isClosing) {
					ratio = 1 - ratio;
				}

				padding = prop === 'width' ? current.wPadding : current.hPadding;
				value = now - padding;

				F.skin[prop](getScalar(prop === 'width' ? value : value - (wrapSpace * ratio)));
				F.inner[prop](getScalar(prop === 'width' ? value : value - (wrapSpace * ratio) - (skinSpace * ratio)));
			}
		},

		zoomIn: function () {
			var current = F.current
				, startPos = current.pos
				, effect = current.openEffect
				, elastic = effect === 'elastic'
				, endPos = $.extend({
				opacity: 1
			}, startPos);

			// Remove "position" property that breaks older IE
			delete endPos.position;

			if (elastic) {
				startPos = this.getOrigPosition();

				if (current.openOpacity) {
					startPos.opacity = 0.1;
				}

			} else if (effect === 'fade') {
				startPos.opacity = 0.1;
			}

			F.wrap.css(startPos).animate(endPos, {
				duration: effect === 'none' ? 0 : current.openSpeed
				, easing: current.openEasing
				, step: elastic ? this.step : null
				, complete: F._afterZoomIn
			});
		},

		zoomOut: function () {
			var current = F.current
				, effect = current.closeEffect
				, elastic = effect === 'elastic'
				, endPos = {
				opacity: 0.1
			};

			if (elastic) {
				endPos = this.getOrigPosition();

				if (current.closeOpacity) {
					endPos.opacity = 0.1;
				}
			}

			F.wrap.animate(endPos, {
				duration: effect === 'none' ? 0 : current.closeSpeed
				, easing: current.closeEasing
				, step: elastic ? this.step : null
				, complete: F._afterZoomOut
			});
		},

		changeIn: function () {
			var current = F.current
				, effect = current.nextEffect
				, startPos = current.pos
				, endPos = {
				opacity: 1
			}
				, direction = F.direction
				, distance = 200
				, field;

			startPos.opacity = 0.1;

			if (effect === 'elastic') {
				field = direction === 'down' || direction === 'up' ? 'top' : 'left';

				if (direction === 'down' || direction === 'right') {
					startPos[field] = getValue(getScalar(startPos[field]) - distance);
					endPos[field] = '+=' + distance + 'px';

				} else {
					startPos[field] = getValue(getScalar(startPos[field]) + distance);
					endPos[field] = '-=' + distance + 'px';
				}
			}

			// Workaround for http://bugs.jquery.com/ticket/12273
			if (effect === 'none') {
				F._afterZoomIn();

			} else {
				F.wrap.css(startPos).animate(endPos, {
					duration: current.nextSpeed
					, easing: current.nextEasing
					, complete: F._afterZoomIn
				});
			}
		},

		changeOut: function () {
			var previous = F.previous
				, effect = previous.prevEffect
				, endPos = {
				opacity: 0.1
			}
				, direction = F.direction
				, distance = 200;

			if (effect === 'elastic') {
				endPos[direction === 'down' || direction === 'up' ? 'top' : 'left'] = (direction === 'up' || direction === 'left' ? '-' : '+') + '=' + distance + 'px';
			}

			previous.wrap.animate(endPos, {
				duration: effect === 'none' ? 0 : previous.prevSpeed
				, easing: previous.prevEasing
				, complete: function () {
					$(this).trigger('onReset').remove();
				}
			});
		}
	};

	/*
	 *	Overlay helper
	 */

	F.helpers.overlay = {
		defaults: {
			closeClick: true, // if true, fancyBox will be closed when user clicks on the overlay
			speedOut: 200, // duration of fadeOut animation
			showEarly: true, // indicates if should be opened immediately or wait until the content is ready
			css: {}, // custom CSS properties
			locked: !isTouch, // if true, the content will be locked into overlay
			fixed: true // if false, the overlay CSS position property will not be set to "fixed"
		},

		overlay: null, // current handle
		fixed: false, // indicates if the overlay has position "fixed"
		el: $('html'), // element that contains "the lock"

		// Public methods
		create: function (opts) {
			opts = $.extend({}, this.defaults, opts);

			if (this.overlay) {
				this.close();
			}

			this.overlay = $('<div class="fancybox-overlay"></div>').appendTo(F.coming ? F.coming.parent : opts.parent);
			this.fixed = false;

			if (opts.fixed && F.defaults.fixed) {
				this.overlay.addClass('fancybox-overlay-fixed');

				this.fixed = true;
			}
		},

		open: function (opts) {
			var that = this;

			opts = $.extend({}, this.defaults, opts);

			if (this.overlay) {
				this.overlay.unbind('.overlay').width('auto').height('auto');

			} else {
				this.create(opts);
			}

			if (!this.fixed) {
				W.bind('resize.overlay', $.proxy(this.update, this));

				this.update();
			}

			if (opts.closeClick) {
				this.overlay.bind('click.overlay', function (e) {
					if ($(e.target).hasClass('fancybox-overlay')) {
						if (F.isActive) {
							F.close();
						} else {
							that.close();
						}

						return false;
					}
				});
			}

			this.overlay.css(opts.css).show();
		},

		close: function () {
			var scrollV, scrollH;

			W.unbind('resize.overlay');

			if (this.el.hasClass('fancybox-lock')) {
				$('.fancybox-margin').removeClass('fancybox-margin');

				scrollV = W.scrollTop();
				scrollH = W.scrollLeft();

				this.el.removeClass('fancybox-lock');

				W.scrollTop(scrollV).scrollLeft(scrollH);
			}

			$('.fancybox-overlay').remove().hide();

			$.extend(this, {
				overlay: null
				, fixed: false
			});
		},

		// Private, callbacks

		update: function () {
			var width = '100%'
				, offsetWidth;

			// Reset width/height so it will not mess
			this.overlay.width(width).height('100%');

			// jQuery does not return reliable result for IE
			if (IE) {
				offsetWidth = Math.max(document.documentElement.offsetWidth, document.body.offsetWidth);

				if (D.width() > offsetWidth) {
					width = D.width();
				}

			} else if (D.width() > W.width()) {
				width = D.width();
			}

			this.overlay.width(width).height(D.height());
		},

		// This is where we can manipulate DOM, because later it would cause iframes to reload
		onReady: function (opts, obj) {
			var overlay = this.overlay;

			$('.fancybox-overlay').stop(true, true);

			if (!overlay) {
				this.create(opts);
			}

			if (opts.locked && this.fixed && obj.fixed) {
				if (!overlay) {
					this.margin = D.height() > W.height() ? $('html').css('margin-right').replace("px", "") : false;
				}

				obj.locked = this.overlay.append(obj.wrap);
				obj.fixed = false;
			}

			if (opts.showEarly === true) {
				this.beforeShow.apply(this, arguments);
			}
		},

		beforeShow: function (opts, obj) {
			var scrollV, scrollH;

			if (obj.locked) {
				if (this.margin !== false) {
					$('*').filter(function () {
						return ($(this).css('position') === 'fixed' && !$(this).hasClass("fancybox-overlay") && !$(this).hasClass("fancybox-wrap"));
					}).addClass('fancybox-margin');

					this.el.addClass('fancybox-margin');
				}

				scrollV = W.scrollTop();
				scrollH = W.scrollLeft();

				this.el.addClass('fancybox-lock');

				W.scrollTop(scrollV).scrollLeft(scrollH);
			}

			this.open(opts);
		},

		onUpdate: function () {
			if (!this.fixed) {
				this.update();
			}
		},

		afterClose: function (opts) {
			// Remove overlay if exists and fancyBox is not opening
			// (e.g., it is not being open using afterClose callback)
			//if (this.overlay && !F.isActive) {
			if (this.overlay && !F.coming) {
				this.overlay.fadeOut(opts.speedOut, $.proxy(this.close, this));
			}
		}
	};

	/*
	 *	Title helper
	 */

	F.helpers.title = {
		defaults: {
			type: 'float', // 'float', 'inside', 'outside' or 'over',
			position: 'bottom' // 'top' or 'bottom'
		},

		beforeShow: function (opts) {
			var current = F.current
				, text = current.title
				, type = opts.type
				, title
				, target;

			if ($.isFunction(text)) {
				text = text.call(current.element, current);
			}

			if (!isString(text) || $.trim(text) === '') {
				return;
			}

			title = $('<div class="fancybox-title fancybox-title-' + type + '-wrap">' + text + '</div>');

			switch (type) {
				case 'inside':
					target = F.skin;
					break;

				case 'outside':
					target = F.wrap;
					break;

				case 'over':
					target = F.inner;
					break;

				default: // 'float'
					target = F.skin;

					title.appendTo('body');

					if (IE) {
						title.width(title.width());
					}

					title.wrapInner('<span class="child"></span>');

					//Increase bottom margin so this title will also fit into viewport
					F.current.margin[2] += Math.abs(getScalar(title.css('margin-bottom')));
					break;
			}

			title[(opts.position === 'top' ? 'prependTo' : 'appendTo')](target);
		}
	};

	// jQuery plugin initialization
	$.fn.fancybox = function (options) {
		var index
			, that = $(this)
			, selector = this.selector || ''
			, run = function (e) {
			var what = $(this).blur()
				, idx = index
				, relType, relVal;

			if (!(e.ctrlKey || e.altKey || e.shiftKey || e.metaKey) && !what.is('.fancybox-wrap')) {
				relType = options.groupAttr || 'data-fancybox-group';
				relVal = what.attr(relType);

				if (!relVal) {
					relType = 'rel';
					relVal = what.get(0)[relType];
				}

				if (relVal && relVal !== '' && relVal !== 'nofollow') {
					what = selector.length ? $(selector) : that;
					what = what.filter('[' + relType + '="' + relVal + '"]');
					idx = what.index(this);
				}

				options.index = idx;

				// Stop an event from bubbling if everything is fine
				if (F.open(what, options) !== false) {
					e.preventDefault();
				}
			}
		};

		options = options || {};
		index = options.index || 0;

		if (!selector || options.live === false) {
			that.unbind('click.fb-start').bind('click.fb-start', run);

		} else {
			D.undelegate(selector, 'click.fb-start').delegate(selector + ":not('.fancybox-item, .fancybox-nav')", 'click.fb-start', run);
		}

		this.filter('[data-fancybox-start=1]').trigger('click');

		return this;
	};

	// Tests that need a body at doc ready
	D.ready(function () {
		var w1, w2;

		if ($.scrollbarWidth === undefined) {
			// http://benalman.com/projects/jquery-misc-plugins/#scrollbarwidth
			$.scrollbarWidth = function () {
				var parent = $('<div style="width:50px;height:50px;overflow:auto"><div/></div>').appendTo('body')
					, child = parent.children()
					, width = child.innerWidth() - child.height(99).innerWidth();

				parent.remove();

				return width;
			};
		}

		if ($.support.fixedPosition === undefined) {
			$.support.fixedPosition = (function () {
				var elem = $('<div style="position:fixed;top:20px;"></div>').appendTo('body')
					, fixed = (elem[0].offsetTop === 20 || elem[0].offsetTop === 15);

				elem.remove();

				return fixed;
			}());
		}

		$.extend(F.defaults, {
			scrollbarWidth: $.scrollbarWidth()
			, fixed: $.support.fixedPosition
			, parent: $('body')
		});

		//Get real width of page scroll-bar
		w1 = $(window).width();

		H.addClass('fancybox-lock-test');

		w2 = $(window).width();

		H.removeClass('fancybox-lock-test');

		$("<style type='text/css'>.fancybox-margin{margin-right:" + (w2 - w1) + "px;}</style>").appendTo("head");
	});

}(window, document, jQuery));
//Animate CSS + WayPoints javaScript Plugin
//Example: $(".element").animated("zoomInUp", "zoomOutDown");
//Author URL: http://webdesign-master.ru
(function ($) {
	$.fn.animated = function (inEffect, outEffect) {
		$(this).css("opacity", "0").addClass("animated").waypoint(function (dir) {
			if (dir === "down") {
				$(this).removeClass(outEffect).addClass(inEffect).css("opacity", "1");
			} else {
				$(this).removeClass(inEffect).addClass(outEffect).css("opacity", "1");
			};
		}, {
			offset: "100%"
		}).waypoint(function (dir) {
			if (dir === "down") {
				$(this).removeClass(inEffect).addClass(outEffect).css("opacity", "1");
			} else {
				$(this).removeClass(outEffect).addClass(inEffect).css("opacity", "1");
			};
		}, {
			offset: -$(window).height()
		});
	};
})(jQuery);
// Generated by CoffeeScript 1.6.2
/*!
 jQuery Waypoints - v2.0.5
 Copyright (c) 2011-2014 Caleb Troughton
 Licensed under the MIT license.
 https://github.com/imakewebthings/jquery-waypoints/blob/master/licenses.txt
 */
(function () {
	var t = [].indexOf || function (t) {
			for (var e = 0, n = this.length; e < n; e++) {
				if (e in this && this[e] === t) return e
			}
			return -1
		}
		, e = [].slice;
	(function (t, e) {
		if (typeof define === "function" && define.amd) {
			return define("waypoints", ["jquery"], function (n) {
				return e(n, t)
			})
		} else {
			return e(t.jQuery, t)
		}
	})(window, function (n, r) {
		var i, o, l, s, f, u, c, a, h, d, p, y, v, w, g, m;
		i = n(r);
		a = t.call(r, "ontouchstart") >= 0;
		s = {
			horizontal: {}
			, vertical: {}
		};
		f = 1;
		c = {};
		u = "waypoints-context-id";
		p = "resize.waypoints";
		y = "scroll.waypoints";
		v = 1;
		w = "waypoints-waypoint-ids";
		g = "waypoint";
		m = "waypoints";
		o = function () {
			function t(t) {
				var e = this;
				this.$element = t;
				this.element = t[0];
				this.didResize = false;
				this.didScroll = false;
				this.id = "context" + f++;
				this.oldScroll = {
					x: t.scrollLeft()
					, y: t.scrollTop()
				};
				this.waypoints = {
					horizontal: {}
					, vertical: {}
				};
				this.element[u] = this.id;
				c[this.id] = this;
				t.bind(y, function () {
					var t;
					if (!(e.didScroll || a)) {
						e.didScroll = true;
						t = function () {
							e.doScroll();
							return e.didScroll = false
						};
						return r.setTimeout(t, n[m].settings.scrollThrottle)
					}
				});
				t.bind(p, function () {
					var t;
					if (!e.didResize) {
						e.didResize = true;
						t = function () {
							n[m]("refresh");
							return e.didResize = false
						};
						return r.setTimeout(t, n[m].settings.resizeThrottle)
					}
				})
			}
			t.prototype.doScroll = function () {
				var t, e = this;
				t = {
					horizontal: {
						newScroll: this.$element.scrollLeft()
						, oldScroll: this.oldScroll.x
						, forward: "right"
						, backward: "left"
					}
					, vertical: {
						newScroll: this.$element.scrollTop()
						, oldScroll: this.oldScroll.y
						, forward: "down"
						, backward: "up"
					}
				};
				if (a && (!t.vertical.oldScroll || !t.vertical.newScroll)) {
					n[m]("refresh")
				}
				n.each(t, function (t, r) {
					var i, o, l;
					l = [];
					o = r.newScroll > r.oldScroll;
					i = o ? r.forward : r.backward;
					n.each(e.waypoints[t], function (t, e) {
						var n, i;
						if (r.oldScroll < (n = e.offset) && n <= r.newScroll) {
							return l.push(e)
						} else if (r.newScroll < (i = e.offset) && i <= r.oldScroll) {
							return l.push(e)
						}
					});
					l.sort(function (t, e) {
						return t.offset - e.offset
					});
					if (!o) {
						l.reverse()
					}
					return n.each(l, function (t, e) {
						if (e.options.continuous || t === l.length - 1) {
							return e.trigger([i])
						}
					})
				});
				return this.oldScroll = {
					x: t.horizontal.newScroll
					, y: t.vertical.newScroll
				}
			};
			t.prototype.refresh = function () {
				var t, e, r, i = this;
				r = n.isWindow(this.element);
				e = this.$element.offset();
				this.doScroll();
				t = {
					horizontal: {
						contextOffset: r ? 0 : e.left
						, contextScroll: r ? 0 : this.oldScroll.x
						, contextDimension: this.$element.width()
						, oldScroll: this.oldScroll.x
						, forward: "right"
						, backward: "left"
						, offsetProp: "left"
					}
					, vertical: {
						contextOffset: r ? 0 : e.top
						, contextScroll: r ? 0 : this.oldScroll.y
						, contextDimension: r ? n[m]("viewportHeight") : this.$element.height()
						, oldScroll: this.oldScroll.y
						, forward: "down"
						, backward: "up"
						, offsetProp: "top"
					}
				};
				return n.each(t, function (t, e) {
					return n.each(i.waypoints[t], function (t, r) {
						var i, o, l, s, f;
						i = r.options.offset;
						l = r.offset;
						o = n.isWindow(r.element) ? 0 : r.$element.offset()[e.offsetProp];
						if (n.isFunction(i)) {
							i = i.apply(r.element)
						} else if (typeof i === "string") {
							i = parseFloat(i);
							if (r.options.offset.indexOf("%") > -1) {
								i = Math.ceil(e.contextDimension * i / 100)
							}
						}
						r.offset = o - e.contextOffset + e.contextScroll - i;
						if (r.options.onlyOnScroll && l != null || !r.enabled) {
							return
						}
						if (l !== null && l < (s = e.oldScroll) && s <= r.offset) {
							return r.trigger([e.backward])
						} else if (l !== null && l > (f = e.oldScroll) && f >= r.offset) {
							return r.trigger([e.forward])
						} else if (l === null && e.oldScroll >= r.offset) {
							return r.trigger([e.forward])
						}
					})
				})
			};
			t.prototype.checkEmpty = function () {
				if (n.isEmptyObject(this.waypoints.horizontal) && n.isEmptyObject(this.waypoints.vertical)) {
					this.$element.unbind([p, y].join(" "));
					return delete c[this.id]
				}
			};
			return t
		}();
		l = function () {
			function t(t, e, r) {
				var i, o;
				if (r.offset === "bottom-in-view") {
					r.offset = function () {
						var t;
						t = n[m]("viewportHeight");
						if (!n.isWindow(e.element)) {
							t = e.$element.height()
						}
						return t - n(this).outerHeight()
					}
				}
				this.$element = t;
				this.element = t[0];
				this.axis = r.horizontal ? "horizontal" : "vertical";
				this.callback = r.handler;
				this.context = e;
				this.enabled = r.enabled;
				this.id = "waypoints" + v++;
				this.offset = null;
				this.options = r;
				e.waypoints[this.axis][this.id] = this;
				s[this.axis][this.id] = this;
				i = (o = this.element[w]) != null ? o : [];
				i.push(this.id);
				this.element[w] = i
			}
			t.prototype.trigger = function (t) {
				if (!this.enabled) {
					return
				}
				if (this.callback != null) {
					this.callback.apply(this.element, t)
				}
				if (this.options.triggerOnce) {
					return this.destroy()
				}
			};
			t.prototype.disable = function () {
				return this.enabled = false
			};
			t.prototype.enable = function () {
				this.context.refresh();
				return this.enabled = true
			};
			t.prototype.destroy = function () {
				delete s[this.axis][this.id];
				delete this.context.waypoints[this.axis][this.id];
				return this.context.checkEmpty()
			};
			t.getWaypointsByElement = function (t) {
				var e, r;
				r = t[w];
				if (!r) {
					return []
				}
				e = n.extend({}, s.horizontal, s.vertical);
				return n.map(r, function (t) {
					return e[t]
				})
			};
			return t
		}();
		d = {
			init: function (t, e) {
				var r;
				e = n.extend({}, n.fn[g].defaults, e);
				if ((r = e.handler) == null) {
					e.handler = t
				}
				this.each(function () {
					var t, r, i, s;
					t = n(this);
					i = (s = e.context) != null ? s : n.fn[g].defaults.context;
					if (!n.isWindow(i)) {
						i = t.closest(i)
					}
					i = n(i);
					r = c[i[0][u]];
					if (!r) {
						r = new o(i)
					}
					return new l(t, r, e)
				});
				n[m]("refresh");
				return this
			}
			, disable: function () {
				return d._invoke.call(this, "disable")
			}
			, enable: function () {
				return d._invoke.call(this, "enable")
			}
			, destroy: function () {
				return d._invoke.call(this, "destroy")
			}
			, prev: function (t, e) {
				return d._traverse.call(this, t, e, function (t, e, n) {
					if (e > 0) {
						return t.push(n[e - 1])
					}
				})
			}
			, next: function (t, e) {
				return d._traverse.call(this, t, e, function (t, e, n) {
					if (e < n.length - 1) {
						return t.push(n[e + 1])
					}
				})
			}
			, _traverse: function (t, e, i) {
				var o, l;
				if (t == null) {
					t = "vertical"
				}
				if (e == null) {
					e = r
				}
				l = h.aggregate(e);
				o = [];
				this.each(function () {
					var e;
					e = n.inArray(this, l[t]);
					return i(o, e, l[t])
				});
				return this.pushStack(o)
			}
			, _invoke: function (t) {
				this.each(function () {
					var e;
					e = l.getWaypointsByElement(this);
					return n.each(e, function (e, n) {
						n[t]();
						return true
					})
				});
				return this
			}
		};
		n.fn[g] = function () {
			var t, r;
			r = arguments[0], t = 2 <= arguments.length ? e.call(arguments, 1) : [];
			if (d[r]) {
				return d[r].apply(this, t)
			} else if (n.isFunction(r)) {
				return d.init.apply(this, arguments)
			} else if (n.isPlainObject(r)) {
				return d.init.apply(this, [null, r])
			} else if (!r) {
				return n.error("jQuery Waypoints needs a callback function or handler option.")
			} else {
				return n.error("The " + r + " method does not exist in jQuery Waypoints.")
			}
		};
		n.fn[g].defaults = {
			context: r
			, continuous: true
			, enabled: true
			, horizontal: false
			, offset: 0
			, triggerOnce: false
		};
		h = {
			refresh: function () {
				return n.each(c, function (t, e) {
					return e.refresh()
				})
			}
			, viewportHeight: function () {
				var t;
				return (t = r.innerHeight) != null ? t : i.height()
			}
			, aggregate: function (t) {
				var e, r, i;
				e = s;
				if (t) {
					e = (i = c[n(t)[0][u]]) != null ? i.waypoints : void 0
				}
				if (!e) {
					return []
				}
				r = {
					horizontal: []
					, vertical: []
				};
				n.each(r, function (t, i) {
					n.each(e[t], function (t, e) {
						return i.push(e)
					});
					i.sort(function (t, e) {
						return t.offset - e.offset
					});
					r[t] = n.map(i, function (t) {
						return t.element
					});
					return r[t] = n.unique(r[t])
				});
				return r
			}
			, above: function (t) {
				if (t == null) {
					t = r
				}
				return h._filter(t, "vertical", function (t, e) {
					return e.offset <= t.oldScroll.y
				})
			}
			, below: function (t) {
				if (t == null) {
					t = r
				}
				return h._filter(t, "vertical", function (t, e) {
					return e.offset > t.oldScroll.y
				})
			}
			, left: function (t) {
				if (t == null) {
					t = r
				}
				return h._filter(t, "horizontal", function (t, e) {
					return e.offset <= t.oldScroll.x
				})
			}
			, right: function (t) {
				if (t == null) {
					t = r
				}
				return h._filter(t, "horizontal", function (t, e) {
					return e.offset > t.oldScroll.x
				})
			}
			, enable: function () {
				return h._invoke("enable")
			}
			, disable: function () {
				return h._invoke("disable")
			}
			, destroy: function () {
				return h._invoke("destroy")
			}
			, extendFn: function (t, e) {
				return d[t] = e
			}
			, _invoke: function (t) {
				var e;
				e = n.extend({}, s.vertical, s.horizontal);
				return n.each(e, function (e, n) {
					n[t]();
					return true
				})
			}
			, _filter: function (t, e, r) {
				var i, o;
				i = c[n(t)[0][u]];
				if (!i) {
					return []
				}
				o = [];
				n.each(i.waypoints[e], function (t, e) {
					if (r(i, e)) {
						return o.push(e)
					}
				});
				o.sort(function (t, e) {
					return t.offset - e.offset
				});
				return n.map(o, function (t) {
					return t.element
				})
			}
		};
		n[m] = function () {
			var t, n;
			n = arguments[0], t = 2 <= arguments.length ? e.call(arguments, 1) : [];
			if (h[n]) {
				return h[n].apply(null, t)
			} else {
				return h.aggregate.call(null, n)
			}
		};
		n[m].settings = {
			resizeThrottle: 100
			, scrollThrottle: 30
		};
		return i.on("load.waypoints", function () {
			return n[m]("refresh")
		})
	})
}).call(this);
/*!
 * VERSION: 1.18.4
 * DATE: 2016-04-26
 * UPDATES AND DOCS AT: http://greensock.com
 *
 * Includes all of the following: TweenLite, TweenMax, TimelineLite, TimelineMax, EasePack, CSSPlugin, RoundPropsPlugin, BezierPlugin, AttrPlugin, DirectionalRotationPlugin
 *
 * @license Copyright (c) 2008-2016, GreenSock. All rights reserved.
 * This work is subject to the terms at http://greensock.com/standard-license or for
 * Club GreenSock members, the software agreement that was issued with your membership.
 *
 * @author: Jack Doyle, jack@greensock.com
 **/
var _gsScope = (typeof (module) !== "undefined" && module.exports && typeof (global) !== "undefined") ? global : this || window; //helps ensure compatibility with AMD/RequireJS and CommonJS/Node
(_gsScope._gsQueue || (_gsScope._gsQueue = [])).push(function () {

	"use strict";

	_gsScope._gsDefine("TweenMax", ["core.Animation", "core.SimpleTimeline", "TweenLite"], function (Animation, SimpleTimeline, TweenLite) {

		var _slice = function (a) { //don't use [].slice because that doesn't work in IE8 with a NodeList that's returned by querySelectorAll()
			var b = []
				, l = a.length
				, i;
			for (i = 0; i !== l; b.push(a[i++]));
			return b;
		}
			, _applyCycle = function (vars, targets, i) {
			var alt = vars.cycle
				, p, val;
			for (p in alt) {
				val = alt[p];
				vars[p] = (typeof (val) === "function") ? val.call(targets[i], i) : val[i % val.length];
			}
			delete vars.cycle;
		}
			, TweenMax = function (target, duration, vars) {
			TweenLite.call(this, target, duration, vars);
			this._cycle = 0;
			this._yoyo = (this.vars.yoyo === true);
			this._repeat = this.vars.repeat || 0;
			this._repeatDelay = this.vars.repeatDelay || 0;
			this._dirty = true; //ensures that if there is any repeat, the totalDuration will get recalculated to accurately report it.
			this.render = TweenMax.prototype.render; //speed optimization (avoid prototype lookup on this "hot" method)
		}
			, _tinyNum = 0.0000000001
			, TweenLiteInternals = TweenLite._internals
			, _isSelector = TweenLiteInternals.isSelector
			, _isArray = TweenLiteInternals.isArray
			, p = TweenMax.prototype = TweenLite.to({}, 0.1, {})
			, _blankArray = [];

		TweenMax.version = "1.18.4";
		p.constructor = TweenMax;
		p.kill()._gc = false;
		TweenMax.killTweensOf = TweenMax.killDelayedCallsTo = TweenLite.killTweensOf;
		TweenMax.getTweensOf = TweenLite.getTweensOf;
		TweenMax.lagSmoothing = TweenLite.lagSmoothing;
		TweenMax.ticker = TweenLite.ticker;
		TweenMax.render = TweenLite.render;

		p.invalidate = function () {
			this._yoyo = (this.vars.yoyo === true);
			this._repeat = this.vars.repeat || 0;
			this._repeatDelay = this.vars.repeatDelay || 0;
			this._uncache(true);
			return TweenLite.prototype.invalidate.call(this);
		};

		p.updateTo = function (vars, resetDuration) {
			var curRatio = this.ratio
				, immediate = this.vars.immediateRender || vars.immediateRender
				, p;
			if (resetDuration && this._startTime < this._timeline._time) {
				this._startTime = this._timeline._time;
				this._uncache(false);
				if (this._gc) {
					this._enabled(true, false);
				} else {
					this._timeline.insert(this, this._startTime - this._delay); //ensures that any necessary re-sequencing of Animations in the timeline occurs to make sure the rendering order is correct.
				}
			}
			for (p in vars) {
				this.vars[p] = vars[p];
			}
			if (this._initted || immediate) {
				if (resetDuration) {
					this._initted = false;
					if (immediate) {
						this.render(0, true, true);
					}
				} else {
					if (this._gc) {
						this._enabled(true, false);
					}
					if (this._notifyPluginsOfEnabled && this._firstPT) {
						TweenLite._onPluginEvent("_onDisable", this); //in case a plugin like MotionBlur must perform some cleanup tasks
					}
					if (this._time / this._duration > 0.998) { //if the tween has finished (or come extremely close to finishing), we just need to rewind it to 0 and then render it again at the end which forces it to re-initialize (parsing the new vars). We allow tweens that are close to finishing (but haven't quite finished) to work this way too because otherwise, the values are so small when determining where to project the starting values that binary math issues creep in and can make the tween appear to render incorrectly when run backwards.
						var prevTime = this._totalTime;
						this.render(0, true, false);
						this._initted = false;
						this.render(prevTime, true, false);
					} else {
						this._initted = false;
						this._init();
						if (this._time > 0 || immediate) {
							var inv = 1 / (1 - curRatio)
								, pt = this._firstPT
								, endValue;
							while (pt) {
								endValue = pt.s + pt.c;
								pt.c *= inv;
								pt.s = endValue - pt.c;
								pt = pt._next;
							}
						}
					}
				}
			}
			return this;
		};

		p.render = function (time, suppressEvents, force) {
			if (!this._initted)
				if (this._duration === 0 && this.vars.repeat) { //zero duration tweens that render immediately have render() called from TweenLite's constructor, before TweenMax's constructor has finished setting _repeat, _repeatDelay, and _yoyo which are critical in determining totalDuration() so we need to call invalidate() which is a low-kb way to get those set properly.
					this.invalidate();
				}
			var totalDur = (!this._dirty) ? this._totalDuration : this.totalDuration()
				, prevTime = this._time
				, prevTotalTime = this._totalTime
				, prevCycle = this._cycle
				, duration = this._duration
				, prevRawPrevTime = this._rawPrevTime
				, isComplete, callback, pt, cycleDuration, r, type, pow, rawPrevTime;
			if (time >= totalDur - 0.0000001) { //to work around occasional floating point math artifacts.
				this._totalTime = totalDur;
				this._cycle = this._repeat;
				if (this._yoyo && (this._cycle & 1) !== 0) {
					this._time = 0;
					this.ratio = this._ease._calcEnd ? this._ease.getRatio(0) : 0;
				} else {
					this._time = duration;
					this.ratio = this._ease._calcEnd ? this._ease.getRatio(1) : 1;
				}
				if (!this._reversed) {
					isComplete = true;
					callback = "onComplete";
					force = (force || this._timeline.autoRemoveChildren); //otherwise, if the animation is unpaused/activated after it's already finished, it doesn't get removed from the parent timeline.
				}
				if (duration === 0)
					if (this._initted || !this.vars.lazy || force) { //zero-duration tweens are tricky because we must discern the momentum/direction of time in order to determine whether the starting values should be rendered or the ending values. If the "playhead" of its timeline goes past the zero-duration tween in the forward direction or lands directly on it, the end values should be rendered, but if the timeline's "playhead" moves past it in the backward direction (from a postitive time to a negative time), the starting values must be rendered.
						if (this._startTime === this._timeline._duration) { //if a zero-duration tween is at the VERY end of a timeline and that timeline renders at its end, it will typically add a tiny bit of cushion to the render time to prevent rounding errors from getting in the way of tweens rendering their VERY end. If we then reverse() that timeline, the zero-duration tween will trigger its onReverseComplete even though technically the playhead didn't pass over it again. It's a very specific edge case we must accommodate.
							time = 0;
						}
						if (prevRawPrevTime < 0 || (time <= 0 && time >= -0.0000001) || (prevRawPrevTime === _tinyNum && this.data !== "isPause"))
							if (prevRawPrevTime !== time) { //note: when this.data is "isPause", it's a callback added by addPause() on a timeline that we should not be triggered when LEAVING its exact start time. In other words, tl.addPause(1).play(1) shouldn't pause.
								force = true;
								if (prevRawPrevTime > _tinyNum) {
									callback = "onReverseComplete";
								}
							}
						this._rawPrevTime = rawPrevTime = (!suppressEvents || time || prevRawPrevTime === time) ? time : _tinyNum; //when the playhead arrives at EXACTLY time 0 (right on top) of a zero-duration tween, we need to discern if events are suppressed so that when the playhead moves again (next time), it'll trigger the callback. If events are NOT suppressed, obviously the callback would be triggered in this render. Basically, the callback should fire either when the playhead ARRIVES or LEAVES this exact spot, not both. Imagine doing a timeline.seek(0) and there's a callback that sits at 0. Since events are suppressed on that seek() by default, nothing will fire, but when the playhead moves off of that position, the callback should fire. This behavior is what people intuitively expect. We set the _rawPrevTime to be a precise tiny number to indicate this scenario rather than using another property/variable which would increase memory usage. This technique is less readable, but more efficient.
					}

			} else if (time < 0.0000001) { //to work around occasional floating point math artifacts, round super small values to 0.
				this._totalTime = this._time = this._cycle = 0;
				this.ratio = this._ease._calcEnd ? this._ease.getRatio(0) : 0;
				if (prevTotalTime !== 0 || (duration === 0 && prevRawPrevTime > 0)) {
					callback = "onReverseComplete";
					isComplete = this._reversed;
				}
				if (time < 0) {
					this._active = false;
					if (duration === 0)
						if (this._initted || !this.vars.lazy || force) { //zero-duration tweens are tricky because we must discern the momentum/direction of time in order to determine whether the starting values should be rendered or the ending values. If the "playhead" of its timeline goes past the zero-duration tween in the forward direction or lands directly on it, the end values should be rendered, but if the timeline's "playhead" moves past it in the backward direction (from a postitive time to a negative time), the starting values must be rendered.
							if (prevRawPrevTime >= 0) {
								force = true;
							}
							this._rawPrevTime = rawPrevTime = (!suppressEvents || time || prevRawPrevTime === time) ? time : _tinyNum; //when the playhead arrives at EXACTLY time 0 (right on top) of a zero-duration tween, we need to discern if events are suppressed so that when the playhead moves again (next time), it'll trigger the callback. If events are NOT suppressed, obviously the callback would be triggered in this render. Basically, the callback should fire either when the playhead ARRIVES or LEAVES this exact spot, not both. Imagine doing a timeline.seek(0) and there's a callback that sits at 0. Since events are suppressed on that seek() by default, nothing will fire, but when the playhead moves off of that position, the callback should fire. This behavior is what people intuitively expect. We set the _rawPrevTime to be a precise tiny number to indicate this scenario rather than using another property/variable which would increase memory usage. This technique is less readable, but more efficient.
						}
				}
				if (!this._initted) { //if we render the very beginning (time == 0) of a fromTo(), we must force the render (normal tweens wouldn't need to render at a time of 0 when the prevTime was also 0). This is also mandatory to make sure overwriting kicks in immediately.
					force = true;
				}
			} else {
				this._totalTime = this._time = time;
				if (this._repeat !== 0) {
					cycleDuration = duration + this._repeatDelay;
					this._cycle = (this._totalTime / cycleDuration) >> 0; //originally _totalTime % cycleDuration but floating point errors caused problems, so I normalized it. (4 % 0.8 should be 0 but some browsers report it as 0.79999999!)
					if (this._cycle !== 0)
						if (this._cycle === this._totalTime / cycleDuration && prevTotalTime <= time) {
							this._cycle--; //otherwise when rendered exactly at the end time, it will act as though it is repeating (at the beginning)
						}
					this._time = this._totalTime - (this._cycle * cycleDuration);
					if (this._yoyo)
						if ((this._cycle & 1) !== 0) {
							this._time = duration - this._time;
						}
					if (this._time > duration) {
						this._time = duration;
					} else if (this._time < 0) {
						this._time = 0;
					}
				}

				if (this._easeType) {
					r = this._time / duration;
					type = this._easeType;
					pow = this._easePower;
					if (type === 1 || (type === 3 && r >= 0.5)) {
						r = 1 - r;
					}
					if (type === 3) {
						r *= 2;
					}
					if (pow === 1) {
						r *= r;
					} else if (pow === 2) {
						r *= r * r;
					} else if (pow === 3) {
						r *= r * r * r;
					} else if (pow === 4) {
						r *= r * r * r * r;
					}

					if (type === 1) {
						this.ratio = 1 - r;
					} else if (type === 2) {
						this.ratio = r;
					} else if (this._time / duration < 0.5) {
						this.ratio = r / 2;
					} else {
						this.ratio = 1 - (r / 2);
					}

				} else {
					this.ratio = this._ease.getRatio(this._time / duration);
				}

			}

			if (prevTime === this._time && !force && prevCycle === this._cycle) {
				if (prevTotalTime !== this._totalTime)
					if (this._onUpdate)
						if (!suppressEvents) { //so that onUpdate fires even during the repeatDelay - as long as the totalTime changed, we should trigger onUpdate.
							this._callback("onUpdate");
						}
				return;
			} else if (!this._initted) {
				this._init();
				if (!this._initted || this._gc) { //immediateRender tweens typically won't initialize until the playhead advances (_time is greater than 0) in order to ensure that overwriting occurs properly. Also, if all of the tweening properties have been overwritten (which would cause _gc to be true, as set in _init()), we shouldn't continue otherwise an onStart callback could be called for example.
					return;
				} else if (!force && this._firstPT && ((this.vars.lazy !== false && this._duration) || (this.vars.lazy && !this._duration))) { //we stick it in the queue for rendering at the very end of the tick - this is a performance optimization because browsers invalidate styles and force a recalculation if you read, write, and then read style data (so it's better to read/read/read/write/write/write than read/write/read/write/read/write). The down side, of course, is that usually you WANT things to render immediately because you may have code running right after that which depends on the change. Like imagine running TweenLite.set(...) and then immediately after that, creating a nother tween that animates the same property to another value; the starting values of that 2nd tween wouldn't be accurate if lazy is true.
					this._time = prevTime;
					this._totalTime = prevTotalTime;
					this._rawPrevTime = prevRawPrevTime;
					this._cycle = prevCycle;
					TweenLiteInternals.lazyTweens.push(this);
					this._lazy = [time, suppressEvents];
					return;
				}
				//_ease is initially set to defaultEase, so now that init() has run, _ease is set properly and we need to recalculate the ratio. Overall this is faster than using conditional logic earlier in the method to avoid having to set ratio twice because we only init() once but renderTime() gets called VERY frequently.
				if (this._time && !isComplete) {
					this.ratio = this._ease.getRatio(this._time / duration);
				} else if (isComplete && this._ease._calcEnd) {
					this.ratio = this._ease.getRatio((this._time === 0) ? 0 : 1);
				}
			}
			if (this._lazy !== false) {
				this._lazy = false;
			}

			if (!this._active)
				if (!this._paused && this._time !== prevTime && time >= 0) {
					this._active = true; //so that if the user renders a tween (as opposed to the timeline rendering it), the timeline is forced to re-render and align it with the proper time/frame on the next rendering cycle. Maybe the tween already finished but the user manually re-renders it as halfway done.
				}
			if (prevTotalTime === 0) {
				if (this._initted === 2 && time > 0) {
					//this.invalidate();
					this._init(); //will just apply overwriting since _initted of (2) means it was a from() tween that had immediateRender:true
				}
				if (this._startAt) {
					if (time >= 0) {
						this._startAt.render(time, suppressEvents, force);
					} else if (!callback) {
						callback = "_dummyGS"; //if no callback is defined, use a dummy value just so that the condition at the end evaluates as true because _startAt should render AFTER the normal render loop when the time is negative. We could handle this in a more intuitive way, of course, but the render loop is the MOST important thing to optimize, so this technique allows us to avoid adding extra conditional logic in a high-frequency area.
					}
				}
				if (this.vars.onStart)
					if (this._totalTime !== 0 || duration === 0)
						if (!suppressEvents) {
							this._callback("onStart");
						}
			}

			pt = this._firstPT;
			while (pt) {
				if (pt.f) {
					pt.t[pt.p](pt.c * this.ratio + pt.s);
				} else {
					pt.t[pt.p] = pt.c * this.ratio + pt.s;
				}
				pt = pt._next;
			}

			if (this._onUpdate) {
				if (time < 0)
					if (this._startAt && this._startTime) { //if the tween is positioned at the VERY beginning (_startTime 0) of its parent timeline, it's illegal for the playhead to go back further, so we should not render the recorded startAt values.
						this._startAt.render(time, suppressEvents, force); //note: for performance reasons, we tuck this conditional logic inside less traveled areas (most tweens don't have an onUpdate). We'd just have it at the end before the onComplete, but the values should be updated before any onUpdate is called, so we ALSO put it here and then if it's not called, we do so later near the onComplete.
					}
				if (!suppressEvents)
					if (this._totalTime !== prevTotalTime || callback) {
						this._callback("onUpdate");
					}
			}
			if (this._cycle !== prevCycle)
				if (!suppressEvents)
					if (!this._gc)
						if (this.vars.onRepeat) {
							this._callback("onRepeat");
						}
			if (callback)
				if (!this._gc || force) { //check gc because there's a chance that kill() could be called in an onUpdate
					if (time < 0 && this._startAt && !this._onUpdate && this._startTime) { //if the tween is positioned at the VERY beginning (_startTime 0) of its parent timeline, it's illegal for the playhead to go back further, so we should not render the recorded startAt values.
						this._startAt.render(time, suppressEvents, force);
					}
					if (isComplete) {
						if (this._timeline.autoRemoveChildren) {
							this._enabled(false, false);
						}
						this._active = false;
					}
					if (!suppressEvents && this.vars[callback]) {
						this._callback(callback);
					}
					if (duration === 0 && this._rawPrevTime === _tinyNum && rawPrevTime !== _tinyNum) { //the onComplete or onReverseComplete could trigger movement of the playhead and for zero-duration tweens (which must discern direction) that land directly back on their start time, we don't want to fire again on the next render. Think of several addPause()'s in a timeline that forces the playhead to a certain spot, but what if it's already paused and another tween is tweening the "time" of the timeline? Each time it moves [forward] past that spot, it would move back, and since suppressEvents is true, it'd reset _rawPrevTime to _tinyNum so that when it begins again, the callback would fire (so ultimately it could bounce back and forth during that tween). Again, this is a very uncommon scenario, but possible nonetheless.
						this._rawPrevTime = 0;
					}
				}
		};

		//---- STATIC FUNCTIONS -----------------------------------------------------------------------------------------------------------

		TweenMax.to = function (target, duration, vars) {
			return new TweenMax(target, duration, vars);
		};

		TweenMax.from = function (target, duration, vars) {
			vars.runBackwards = true;
			vars.immediateRender = (vars.immediateRender != false);
			return new TweenMax(target, duration, vars);
		};

		TweenMax.fromTo = function (target, duration, fromVars, toVars) {
			toVars.startAt = fromVars;
			toVars.immediateRender = (toVars.immediateRender != false && fromVars.immediateRender != false);
			return new TweenMax(target, duration, toVars);
		};

		TweenMax.staggerTo = TweenMax.allTo = function (targets, duration, vars, stagger, onCompleteAll, onCompleteAllParams, onCompleteAllScope) {
			stagger = stagger || 0;
			var delay = 0
				, a = []
				, finalComplete = function () {
				if (vars.onComplete) {
					vars.onComplete.apply(vars.onCompleteScope || this, arguments);
				}
				onCompleteAll.apply(onCompleteAllScope || vars.callbackScope || this, onCompleteAllParams || _blankArray);
			}
				, cycle = vars.cycle
				, fromCycle = (vars.startAt && vars.startAt.cycle)
				, l, copy, i, p;
			if (!_isArray(targets)) {
				if (typeof (targets) === "string") {
					targets = TweenLite.selector(targets) || targets;
				}
				if (_isSelector(targets)) {
					targets = _slice(targets);
				}
			}
			targets = targets || [];
			if (stagger < 0) {
				targets = _slice(targets);
				targets.reverse();
				stagger *= -1;
			}
			l = targets.length - 1;
			for (i = 0; i <= l; i++) {
				copy = {};
				for (p in vars) {
					copy[p] = vars[p];
				}
				if (cycle) {
					_applyCycle(copy, targets, i);
				}
				if (fromCycle) {
					fromCycle = copy.startAt = {};
					for (p in vars.startAt) {
						fromCycle[p] = vars.startAt[p];
					}
					_applyCycle(copy.startAt, targets, i);
				}
				copy.delay = delay + (copy.delay || 0);
				if (i === l && onCompleteAll) {
					copy.onComplete = finalComplete;
				}
				a[i] = new TweenMax(targets[i], duration, copy);
				delay += stagger;
			}
			return a;
		};

		TweenMax.staggerFrom = TweenMax.allFrom = function (targets, duration, vars, stagger, onCompleteAll, onCompleteAllParams, onCompleteAllScope) {
			vars.runBackwards = true;
			vars.immediateRender = (vars.immediateRender != false);
			return TweenMax.staggerTo(targets, duration, vars, stagger, onCompleteAll, onCompleteAllParams, onCompleteAllScope);
		};

		TweenMax.staggerFromTo = TweenMax.allFromTo = function (targets, duration, fromVars, toVars, stagger, onCompleteAll, onCompleteAllParams, onCompleteAllScope) {
			toVars.startAt = fromVars;
			toVars.immediateRender = (toVars.immediateRender != false && fromVars.immediateRender != false);
			return TweenMax.staggerTo(targets, duration, toVars, stagger, onCompleteAll, onCompleteAllParams, onCompleteAllScope);
		};

		TweenMax.delayedCall = function (delay, callback, params, scope, useFrames) {
			return new TweenMax(callback, 0, {
				delay: delay
				, onComplete: callback
				, onCompleteParams: params
				, callbackScope: scope
				, onReverseComplete: callback
				, onReverseCompleteParams: params
				, immediateRender: false
				, useFrames: useFrames
				, overwrite: 0
			});
		};

		TweenMax.set = function (target, vars) {
			return new TweenMax(target, 0, vars);
		};

		TweenMax.isTweening = function (target) {
			return (TweenLite.getTweensOf(target, true).length > 0);
		};

		var _getChildrenOf = function (timeline, includeTimelines) {
			var a = []
				, cnt = 0
				, tween = timeline._first;
			while (tween) {
				if (tween instanceof TweenLite) {
					a[cnt++] = tween;
				} else {
					if (includeTimelines) {
						a[cnt++] = tween;
					}
					a = a.concat(_getChildrenOf(tween, includeTimelines));
					cnt = a.length;
				}
				tween = tween._next;
			}
			return a;
		}
			, getAllTweens = TweenMax.getAllTweens = function (includeTimelines) {
			return _getChildrenOf(Animation._rootTimeline, includeTimelines).concat(_getChildrenOf(Animation._rootFramesTimeline, includeTimelines));
		};

		TweenMax.killAll = function (complete, tweens, delayedCalls, timelines) {
			if (tweens == null) {
				tweens = true;
			}
			if (delayedCalls == null) {
				delayedCalls = true;
			}
			var a = getAllTweens((timelines != false))
				, l = a.length
				, allTrue = (tweens && delayedCalls && timelines)
				, isDC, tween, i;
			for (i = 0; i < l; i++) {
				tween = a[i];
				if (allTrue || (tween instanceof SimpleTimeline) || ((isDC = (tween.target === tween.vars.onComplete)) && delayedCalls) || (tweens && !isDC)) {
					if (complete) {
						tween.totalTime(tween._reversed ? 0 : tween.totalDuration());
					} else {
						tween._enabled(false, false);
					}
				}
			}
		};

		TweenMax.killChildTweensOf = function (parent, complete) {
			if (parent == null) {
				return;
			}
			var tl = TweenLiteInternals.tweenLookup
				, a, curParent, p, i, l;
			if (typeof (parent) === "string") {
				parent = TweenLite.selector(parent) || parent;
			}
			if (_isSelector(parent)) {
				parent = _slice(parent);
			}
			if (_isArray(parent)) {
				i = parent.length;
				while (--i > -1) {
					TweenMax.killChildTweensOf(parent[i], complete);
				}
				return;
			}
			a = [];
			for (p in tl) {
				curParent = tl[p].target.parentNode;
				while (curParent) {
					if (curParent === parent) {
						a = a.concat(tl[p].tweens);
					}
					curParent = curParent.parentNode;
				}
			}
			l = a.length;
			for (i = 0; i < l; i++) {
				if (complete) {
					a[i].totalTime(a[i].totalDuration());
				}
				a[i]._enabled(false, false);
			}
		};

		var _changePause = function (pause, tweens, delayedCalls, timelines) {
			tweens = (tweens !== false);
			delayedCalls = (delayedCalls !== false);
			timelines = (timelines !== false);
			var a = getAllTweens(timelines)
				, allTrue = (tweens && delayedCalls && timelines)
				, i = a.length
				, isDC, tween;
			while (--i > -1) {
				tween = a[i];
				if (allTrue || (tween instanceof SimpleTimeline) || ((isDC = (tween.target === tween.vars.onComplete)) && delayedCalls) || (tweens && !isDC)) {
					tween.paused(pause);
				}
			}
		};

		TweenMax.pauseAll = function (tweens, delayedCalls, timelines) {
			_changePause(true, tweens, delayedCalls, timelines);
		};

		TweenMax.resumeAll = function (tweens, delayedCalls, timelines) {
			_changePause(false, tweens, delayedCalls, timelines);
		};

		TweenMax.globalTimeScale = function (value) {
			var tl = Animation._rootTimeline
				, t = TweenLite.ticker.time;
			if (!arguments.length) {
				return tl._timeScale;
			}
			value = value || _tinyNum; //can't allow zero because it'll throw the math off
			tl._startTime = t - ((t - tl._startTime) * tl._timeScale / value);
			tl = Animation._rootFramesTimeline;
			t = TweenLite.ticker.frame;
			tl._startTime = t - ((t - tl._startTime) * tl._timeScale / value);
			tl._timeScale = Animation._rootTimeline._timeScale = value;
			return value;
		};


		//---- GETTERS / SETTERS ----------------------------------------------------------------------------------------------------------

		p.progress = function (value, suppressEvents) {
			return (!arguments.length) ? this._time / this.duration() : this.totalTime(this.duration() * ((this._yoyo && (this._cycle & 1) !== 0) ? 1 - value : value) + (this._cycle * (this._duration + this._repeatDelay)), suppressEvents);
		};

		p.totalProgress = function (value, suppressEvents) {
			return (!arguments.length) ? this._totalTime / this.totalDuration() : this.totalTime(this.totalDuration() * value, suppressEvents);
		};

		p.time = function (value, suppressEvents) {
			if (!arguments.length) {
				return this._time;
			}
			if (this._dirty) {
				this.totalDuration();
			}
			if (value > this._duration) {
				value = this._duration;
			}
			if (this._yoyo && (this._cycle & 1) !== 0) {
				value = (this._duration - value) + (this._cycle * (this._duration + this._repeatDelay));
			} else if (this._repeat !== 0) {
				value += this._cycle * (this._duration + this._repeatDelay);
			}
			return this.totalTime(value, suppressEvents);
		};

		p.duration = function (value) {
			if (!arguments.length) {
				return this._duration; //don't set _dirty = false because there could be repeats that haven't been factored into the _totalDuration yet. Otherwise, if you create a repeated TweenMax and then immediately check its duration(), it would cache the value and the totalDuration would not be correct, thus repeats wouldn't take effect.
			}
			return Animation.prototype.duration.call(this, value);
		};

		p.totalDuration = function (value) {
			if (!arguments.length) {
				if (this._dirty) {
					//instead of Infinity, we use 999999999999 so that we can accommodate reverses
					this._totalDuration = (this._repeat === -1) ? 999999999999 : this._duration * (this._repeat + 1) + (this._repeatDelay * this._repeat);
					this._dirty = false;
				}
				return this._totalDuration;
			}
			return (this._repeat === -1) ? this : this.duration((value - (this._repeat * this._repeatDelay)) / (this._repeat + 1));
		};

		p.repeat = function (value) {
			if (!arguments.length) {
				return this._repeat;
			}
			this._repeat = value;
			return this._uncache(true);
		};

		p.repeatDelay = function (value) {
			if (!arguments.length) {
				return this._repeatDelay;
			}
			this._repeatDelay = value;
			return this._uncache(true);
		};

		p.yoyo = function (value) {
			if (!arguments.length) {
				return this._yoyo;
			}
			this._yoyo = value;
			return this;
		};


		return TweenMax;

	}, true);








	/*
	 * ----------------------------------------------------------------
	 * TimelineLite
	 * ----------------------------------------------------------------
	 */
	_gsScope._gsDefine("TimelineLite", ["core.Animation", "core.SimpleTimeline", "TweenLite"], function (Animation, SimpleTimeline, TweenLite) {

		var TimelineLite = function (vars) {
			SimpleTimeline.call(this, vars);
			this._labels = {};
			this.autoRemoveChildren = (this.vars.autoRemoveChildren === true);
			this.smoothChildTiming = (this.vars.smoothChildTiming === true);
			this._sortChildren = true;
			this._onUpdate = this.vars.onUpdate;
			var v = this.vars
				, val, p;
			for (p in v) {
				val = v[p];
				if (_isArray(val))
					if (val.join("").indexOf("{self}") !== -1) {
						v[p] = this._swapSelfInParams(val);
					}
			}
			if (_isArray(v.tweens)) {
				this.add(v.tweens, 0, v.align, v.stagger);
			}
		}
			, _tinyNum = 0.0000000001
			, TweenLiteInternals = TweenLite._internals
			, _internals = TimelineLite._internals = {}
			, _isSelector = TweenLiteInternals.isSelector
			, _isArray = TweenLiteInternals.isArray
			, _lazyTweens = TweenLiteInternals.lazyTweens
			, _lazyRender = TweenLiteInternals.lazyRender
			, _globals = _gsScope._gsDefine.globals
			, _copy = function (vars) {
			var copy = {}
				, p;
			for (p in vars) {
				copy[p] = vars[p];
			}
			return copy;
		}
			, _applyCycle = function (vars, targets, i) {
			var alt = vars.cycle
				, p, val;
			for (p in alt) {
				val = alt[p];
				vars[p] = (typeof (val) === "function") ? val.call(targets[i], i) : val[i % val.length];
			}
			delete vars.cycle;
		}
			, _pauseCallback = _internals.pauseCallback = function () {}
			, _slice = function (a) { //don't use [].slice because that doesn't work in IE8 with a NodeList that's returned by querySelectorAll()
			var b = []
				, l = a.length
				, i;
			for (i = 0; i !== l; b.push(a[i++]));
			return b;
		}
			, p = TimelineLite.prototype = new SimpleTimeline();

		TimelineLite.version = "1.18.4";
		p.constructor = TimelineLite;
		p.kill()._gc = p._forcingPlayhead = p._hasPause = false;

		/* might use later...
		 //translates a local time inside an animation to the corresponding time on the root/global timeline, factoring in all nesting and timeScales.
		 function localToGlobal(time, animation) {
		 while (animation) {
		 time = (time / animation._timeScale) + animation._startTime;
		 animation = animation.timeline;
		 }
		 return time;
		 }

		 //translates the supplied time on the root/global timeline into the corresponding local time inside a particular animation, factoring in all nesting and timeScales
		 function globalToLocal(time, animation) {
		 var scale = 1;
		 time -= localToGlobal(0, animation);
		 while (animation) {
		 scale *= animation._timeScale;
		 animation = animation.timeline;
		 }
		 return time * scale;
		 }
		 */

		p.to = function (target, duration, vars, position) {
			var Engine = (vars.repeat && _globals.TweenMax) || TweenLite;
			return duration ? this.add(new Engine(target, duration, vars), position) : this.set(target, vars, position);
		};

		p.from = function (target, duration, vars, position) {
			return this.add(((vars.repeat && _globals.TweenMax) || TweenLite).from(target, duration, vars), position);
		};

		p.fromTo = function (target, duration, fromVars, toVars, position) {
			var Engine = (toVars.repeat && _globals.TweenMax) || TweenLite;
			return duration ? this.add(Engine.fromTo(target, duration, fromVars, toVars), position) : this.set(target, toVars, position);
		};

		p.staggerTo = function (targets, duration, vars, stagger, position, onCompleteAll, onCompleteAllParams, onCompleteAllScope) {
			var tl = new TimelineLite({
				onComplete: onCompleteAll
				, onCompleteParams: onCompleteAllParams
				, callbackScope: onCompleteAllScope
				, smoothChildTiming: this.smoothChildTiming
			})
				, cycle = vars.cycle
				, copy, i;
			if (typeof (targets) === "string") {
				targets = TweenLite.selector(targets) || targets;
			}
			targets = targets || [];
			if (_isSelector(targets)) { //senses if the targets object is a selector. If it is, we should translate it into an array.
				targets = _slice(targets);
			}
			stagger = stagger || 0;
			if (stagger < 0) {
				targets = _slice(targets);
				targets.reverse();
				stagger *= -1;
			}
			for (i = 0; i < targets.length; i++) {
				copy = _copy(vars);
				if (copy.startAt) {
					copy.startAt = _copy(copy.startAt);
					if (copy.startAt.cycle) {
						_applyCycle(copy.startAt, targets, i);
					}
				}
				if (cycle) {
					_applyCycle(copy, targets, i);
				}
				tl.to(targets[i], duration, copy, i * stagger);
			}
			return this.add(tl, position);
		};

		p.staggerFrom = function (targets, duration, vars, stagger, position, onCompleteAll, onCompleteAllParams, onCompleteAllScope) {
			vars.immediateRender = (vars.immediateRender != false);
			vars.runBackwards = true;
			return this.staggerTo(targets, duration, vars, stagger, position, onCompleteAll, onCompleteAllParams, onCompleteAllScope);
		};

		p.staggerFromTo = function (targets, duration, fromVars, toVars, stagger, position, onCompleteAll, onCompleteAllParams, onCompleteAllScope) {
			toVars.startAt = fromVars;
			toVars.immediateRender = (toVars.immediateRender != false && fromVars.immediateRender != false);
			return this.staggerTo(targets, duration, toVars, stagger, position, onCompleteAll, onCompleteAllParams, onCompleteAllScope);
		};

		p.call = function (callback, params, scope, position) {
			return this.add(TweenLite.delayedCall(0, callback, params, scope), position);
		};

		p.set = function (target, vars, position) {
			position = this._parseTimeOrLabel(position, 0, true);
			if (vars.immediateRender == null) {
				vars.immediateRender = (position === this._time && !this._paused);
			}
			return this.add(new TweenLite(target, 0, vars), position);
		};

		TimelineLite.exportRoot = function (vars, ignoreDelayedCalls) {
			vars = vars || {};
			if (vars.smoothChildTiming == null) {
				vars.smoothChildTiming = true;
			}
			var tl = new TimelineLite(vars)
				, root = tl._timeline
				, tween, next;
			if (ignoreDelayedCalls == null) {
				ignoreDelayedCalls = true;
			}
			root._remove(tl, true);
			tl._startTime = 0;
			tl._rawPrevTime = tl._time = tl._totalTime = root._time;
			tween = root._first;
			while (tween) {
				next = tween._next;
				if (!ignoreDelayedCalls || !(tween instanceof TweenLite && tween.target === tween.vars.onComplete)) {
					tl.add(tween, tween._startTime - tween._delay);
				}
				tween = next;
			}
			root.add(tl, 0);
			return tl;
		};

		p.add = function (value, position, align, stagger) {
			var curTime, l, i, child, tl, beforeRawTime;
			if (typeof (position) !== "number") {
				position = this._parseTimeOrLabel(position, 0, true, value);
			}
			if (!(value instanceof Animation)) {
				if ((value instanceof Array) || (value && value.push && _isArray(value))) {
					align = align || "normal";
					stagger = stagger || 0;
					curTime = position;
					l = value.length;
					for (i = 0; i < l; i++) {
						if (_isArray(child = value[i])) {
							child = new TimelineLite({
								tweens: child
							});
						}
						this.add(child, curTime);
						if (typeof (child) !== "string" && typeof (child) !== "function") {
							if (align === "sequence") {
								curTime = child._startTime + (child.totalDuration() / child._timeScale);
							} else if (align === "start") {
								child._startTime -= child.delay();
							}
						}
						curTime += stagger;
					}
					return this._uncache(true);
				} else if (typeof (value) === "string") {
					return this.addLabel(value, position);
				} else if (typeof (value) === "function") {
					value = TweenLite.delayedCall(0, value);
				} else {
					throw ("Cannot add " + value + " into the timeline; it is not a tween, timeline, function, or string.");
				}
			}

			SimpleTimeline.prototype.add.call(this, value, position);

			//if the timeline has already ended but the inserted tween/timeline extends the duration, we should enable this timeline again so that it renders properly. We should also align the playhead with the parent timeline's when appropriate.
			if (this._gc || this._time === this._duration)
				if (!this._paused)
					if (this._duration < this.duration()) {
						//in case any of the ancestors had completed but should now be enabled...
						tl = this;
						beforeRawTime = (tl.rawTime() > value._startTime); //if the tween is placed on the timeline so that it starts BEFORE the current rawTime, we should align the playhead (move the timeline). This is because sometimes users will create a timeline, let it finish, and much later append a tween and expect it to run instead of jumping to its end state. While technically one could argue that it should jump to its end state, that's not what users intuitively expect.
						while (tl._timeline) {
							if (beforeRawTime && tl._timeline.smoothChildTiming) {
								tl.totalTime(tl._totalTime, true); //moves the timeline (shifts its startTime) if necessary, and also enables it.
							} else if (tl._gc) {
								tl._enabled(true, false);
							}
							tl = tl._timeline;
						}
					}

			return this;
		};

		p.remove = function (value) {
			if (value instanceof Animation) {
				this._remove(value, false);
				var tl = value._timeline = value.vars.useFrames ? Animation._rootFramesTimeline : Animation._rootTimeline; //now that it's removed, default it to the root timeline so that if it gets played again, it doesn't jump back into this timeline.
				value._startTime = (value._paused ? value._pauseTime : tl._time) - ((!value._reversed ? value._totalTime : value.totalDuration() - value._totalTime) / value._timeScale); //ensure that if it gets played again, the timing is correct.
				return this;
			} else if (value instanceof Array || (value && value.push && _isArray(value))) {
				var i = value.length;
				while (--i > -1) {
					this.remove(value[i]);
				}
				return this;
			} else if (typeof (value) === "string") {
				return this.removeLabel(value);
			}
			return this.kill(null, value);
		};

		p._remove = function (tween, skipDisable) {
			SimpleTimeline.prototype._remove.call(this, tween, skipDisable);
			var last = this._last;
			if (!last) {
				this._time = this._totalTime = this._duration = this._totalDuration = 0;
			} else if (this._time > last._startTime + last._totalDuration / last._timeScale) {
				this._time = this.duration();
				this._totalTime = this._totalDuration;
			}
			return this;
		};

		p.append = function (value, offsetOrLabel) {
			return this.add(value, this._parseTimeOrLabel(null, offsetOrLabel, true, value));
		};

		p.insert = p.insertMultiple = function (value, position, align, stagger) {
			return this.add(value, position || 0, align, stagger);
		};

		p.appendMultiple = function (tweens, offsetOrLabel, align, stagger) {
			return this.add(tweens, this._parseTimeOrLabel(null, offsetOrLabel, true, tweens), align, stagger);
		};

		p.addLabel = function (label, position) {
			this._labels[label] = this._parseTimeOrLabel(position);
			return this;
		};

		p.addPause = function (position, callback, params, scope) {
			var t = TweenLite.delayedCall(0, _pauseCallback, params, scope || this);
			t.vars.onComplete = t.vars.onReverseComplete = callback;
			t.data = "isPause";
			this._hasPause = true;
			return this.add(t, position);
		};

		p.removeLabel = function (label) {
			delete this._labels[label];
			return this;
		};

		p.getLabelTime = function (label) {
			return (this._labels[label] != null) ? this._labels[label] : -1;
		};

		p._parseTimeOrLabel = function (timeOrLabel, offsetOrLabel, appendIfAbsent, ignore) {
			var i;
			//if we're about to add a tween/timeline (or an array of them) that's already a child of this timeline, we should remove it first so that it doesn't contaminate the duration().
			if (ignore instanceof Animation && ignore.timeline === this) {
				this.remove(ignore);
			} else if (ignore && ((ignore instanceof Array) || (ignore.push && _isArray(ignore)))) {
				i = ignore.length;
				while (--i > -1) {
					if (ignore[i] instanceof Animation && ignore[i].timeline === this) {
						this.remove(ignore[i]);
					}
				}
			}
			if (typeof (offsetOrLabel) === "string") {
				return this._parseTimeOrLabel(offsetOrLabel, (appendIfAbsent && typeof (timeOrLabel) === "number" && this._labels[offsetOrLabel] == null) ? timeOrLabel - this.duration() : 0, appendIfAbsent);
			}
			offsetOrLabel = offsetOrLabel || 0;
			if (typeof (timeOrLabel) === "string" && (isNaN(timeOrLabel) || this._labels[timeOrLabel] != null)) { //if the string is a number like "1", check to see if there's a label with that name, otherwise interpret it as a number (absolute value).
				i = timeOrLabel.indexOf("=");
				if (i === -1) {
					if (this._labels[timeOrLabel] == null) {
						return appendIfAbsent ? (this._labels[timeOrLabel] = this.duration() + offsetOrLabel) : offsetOrLabel;
					}
					return this._labels[timeOrLabel] + offsetOrLabel;
				}
				offsetOrLabel = parseInt(timeOrLabel.charAt(i - 1) + "1", 10) * Number(timeOrLabel.substr(i + 1));
				timeOrLabel = (i > 1) ? this._parseTimeOrLabel(timeOrLabel.substr(0, i - 1), 0, appendIfAbsent) : this.duration();
			} else if (timeOrLabel == null) {
				timeOrLabel = this.duration();
			}
			return Number(timeOrLabel) + offsetOrLabel;
		};

		p.seek = function (position, suppressEvents) {
			return this.totalTime((typeof (position) === "number") ? position : this._parseTimeOrLabel(position), (suppressEvents !== false));
		};

		p.stop = function () {
			return this.paused(true);
		};

		p.gotoAndPlay = function (position, suppressEvents) {
			return this.play(position, suppressEvents);
		};

		p.gotoAndStop = function (position, suppressEvents) {
			return this.pause(position, suppressEvents);
		};

		p.render = function (time, suppressEvents, force) {
			if (this._gc) {
				this._enabled(true, false);
			}
			var totalDur = (!this._dirty) ? this._totalDuration : this.totalDuration()
				, prevTime = this._time
				, prevStart = this._startTime
				, prevTimeScale = this._timeScale
				, prevPaused = this._paused
				, tween, isComplete, next, callback, internalForce, pauseTween, curTime;
			if (time >= totalDur - 0.0000001) { //to work around occasional floating point math artifacts.
				this._totalTime = this._time = totalDur;
				if (!this._reversed)
					if (!this._hasPausedChild()) {
						isComplete = true;
						callback = "onComplete";
						internalForce = !!this._timeline.autoRemoveChildren; //otherwise, if the animation is unpaused/activated after it's already finished, it doesn't get removed from the parent timeline.
						if (this._duration === 0)
							if ((time <= 0 && time >= -0.0000001) || this._rawPrevTime < 0 || this._rawPrevTime === _tinyNum)
								if (this._rawPrevTime !== time && this._first) {
									internalForce = true;
									if (this._rawPrevTime > _tinyNum) {
										callback = "onReverseComplete";
									}
								}
					}
				this._rawPrevTime = (this._duration || !suppressEvents || time || this._rawPrevTime === time) ? time : _tinyNum; //when the playhead arrives at EXACTLY time 0 (right on top) of a zero-duration timeline or tween, we need to discern if events are suppressed so that when the playhead moves again (next time), it'll trigger the callback. If events are NOT suppressed, obviously the callback would be triggered in this render. Basically, the callback should fire either when the playhead ARRIVES or LEAVES this exact spot, not both. Imagine doing a timeline.seek(0) and there's a callback that sits at 0. Since events are suppressed on that seek() by default, nothing will fire, but when the playhead moves off of that position, the callback should fire. This behavior is what people intuitively expect. We set the _rawPrevTime to be a precise tiny number to indicate this scenario rather than using another property/variable which would increase memory usage. This technique is less readable, but more efficient.
				time = totalDur + 0.0001; //to avoid occasional floating point rounding errors - sometimes child tweens/timelines were not being fully completed (their progress might be 0.999999999999998 instead of 1 because when _time - tween._startTime is performed, floating point errors would return a value that was SLIGHTLY off). Try (999999999999.7 - 999999999999) * 1 = 0.699951171875 instead of 0.7.

			} else if (time < 0.0000001) { //to work around occasional floating point math artifacts, round super small values to 0.
				this._totalTime = this._time = 0;
				if (prevTime !== 0 || (this._duration === 0 && this._rawPrevTime !== _tinyNum && (this._rawPrevTime > 0 || (time < 0 && this._rawPrevTime >= 0)))) {
					callback = "onReverseComplete";
					isComplete = this._reversed;
				}
				if (time < 0) {
					this._active = false;
					if (this._timeline.autoRemoveChildren && this._reversed) { //ensures proper GC if a timeline is resumed after it's finished reversing.
						internalForce = isComplete = true;
						callback = "onReverseComplete";
					} else if (this._rawPrevTime >= 0 && this._first) { //when going back beyond the start, force a render so that zero-duration tweens that sit at the very beginning render their start values properly. Otherwise, if the parent timeline's playhead lands exactly at this timeline's startTime, and then moves backwards, the zero-duration tweens at the beginning would still be at their end state.
						internalForce = true;
					}
					this._rawPrevTime = time;
				} else {
					this._rawPrevTime = (this._duration || !suppressEvents || time || this._rawPrevTime === time) ? time : _tinyNum; //when the playhead arrives at EXACTLY time 0 (right on top) of a zero-duration timeline or tween, we need to discern if events are suppressed so that when the playhead moves again (next time), it'll trigger the callback. If events are NOT suppressed, obviously the callback would be triggered in this render. Basically, the callback should fire either when the playhead ARRIVES or LEAVES this exact spot, not both. Imagine doing a timeline.seek(0) and there's a callback that sits at 0. Since events are suppressed on that seek() by default, nothing will fire, but when the playhead moves off of that position, the callback should fire. This behavior is what people intuitively expect. We set the _rawPrevTime to be a precise tiny number to indicate this scenario rather than using another property/variable which would increase memory usage. This technique is less readable, but more efficient.
					if (time === 0 && isComplete) { //if there's a zero-duration tween at the very beginning of a timeline and the playhead lands EXACTLY at time 0, that tween will correctly render its end values, but we need to keep the timeline alive for one more render so that the beginning values render properly as the parent's playhead keeps moving beyond the begining. Imagine obj.x starts at 0 and then we do tl.set(obj, {x:100}).to(obj, 1, {x:200}) and then later we tl.reverse()...the goal is to have obj.x revert to 0. If the playhead happens to land on exactly 0, without this chunk of code, it'd complete the timeline and remove it from the rendering queue (not good).
						tween = this._first;
						while (tween && tween._startTime === 0) {
							if (!tween._duration) {
								isComplete = false;
							}
							tween = tween._next;
						}
					}
					time = 0; //to avoid occasional floating point rounding errors (could cause problems especially with zero-duration tweens at the very beginning of the timeline)
					if (!this._initted) {
						internalForce = true;
					}
				}

			} else {

				if (this._hasPause && !this._forcingPlayhead && !suppressEvents) {
					if (time >= prevTime) {
						tween = this._first;
						while (tween && tween._startTime <= time && !pauseTween) {
							if (!tween._duration)
								if (tween.data === "isPause" && !tween.ratio && !(tween._startTime === 0 && this._rawPrevTime === 0)) {
									pauseTween = tween;
								}
							tween = tween._next;
						}
					} else {
						tween = this._last;
						while (tween && tween._startTime >= time && !pauseTween) {
							if (!tween._duration)
								if (tween.data === "isPause" && tween._rawPrevTime > 0) {
									pauseTween = tween;
								}
							tween = tween._prev;
						}
					}
					if (pauseTween) {
						this._time = time = pauseTween._startTime;
						this._totalTime = time + (this._cycle * (this._totalDuration + this._repeatDelay));
					}
				}

				this._totalTime = this._time = this._rawPrevTime = time;
			}
			if ((this._time === prevTime || !this._first) && !force && !internalForce && !pauseTween) {
				return;
			} else if (!this._initted) {
				this._initted = true;
			}

			if (!this._active)
				if (!this._paused && this._time !== prevTime && time > 0) {
					this._active = true; //so that if the user renders the timeline (as opposed to the parent timeline rendering it), it is forced to re-render and align it with the proper time/frame on the next rendering cycle. Maybe the timeline already finished but the user manually re-renders it as halfway done, for example.
				}

			if (prevTime === 0)
				if (this.vars.onStart)
					if (this._time !== 0)
						if (!suppressEvents) {
							this._callback("onStart");
						}

			curTime = this._time;
			if (curTime >= prevTime) {
				tween = this._first;
				while (tween) {
					next = tween._next; //record it here because the value could change after rendering...
					if (curTime !== this._time || (this._paused && !prevPaused)) { //in case a tween pauses or seeks the timeline when rendering, like inside of an onUpdate/onComplete
						break;
					} else if (tween._active || (tween._startTime <= curTime && !tween._paused && !tween._gc)) {
						if (pauseTween === tween) {
							this.pause();
						}
						if (!tween._reversed) {
							tween.render((time - tween._startTime) * tween._timeScale, suppressEvents, force);
						} else {
							tween.render(((!tween._dirty) ? tween._totalDuration : tween.totalDuration()) - ((time - tween._startTime) * tween._timeScale), suppressEvents, force);
						}
					}
					tween = next;
				}
			} else {
				tween = this._last;
				while (tween) {
					next = tween._prev; //record it here because the value could change after rendering...
					if (curTime !== this._time || (this._paused && !prevPaused)) { //in case a tween pauses or seeks the timeline when rendering, like inside of an onUpdate/onComplete
						break;
					} else if (tween._active || (tween._startTime <= prevTime && !tween._paused && !tween._gc)) {
						if (pauseTween === tween) {
							pauseTween = tween._prev; //the linked list is organized by _startTime, thus it's possible that a tween could start BEFORE the pause and end after it, in which case it would be positioned before the pause tween in the linked list, but we should render it before we pause() the timeline and cease rendering. This is only a concern when going in reverse.
							while (pauseTween && pauseTween.endTime() > this._time) {
								pauseTween.render((pauseTween._reversed ? pauseTween.totalDuration() - ((time - pauseTween._startTime) * pauseTween._timeScale) : (time - pauseTween._startTime) * pauseTween._timeScale), suppressEvents, force);
								pauseTween = pauseTween._prev;
							}
							pauseTween = null;
							this.pause();
						}
						if (!tween._reversed) {
							tween.render((time - tween._startTime) * tween._timeScale, suppressEvents, force);
						} else {
							tween.render(((!tween._dirty) ? tween._totalDuration : tween.totalDuration()) - ((time - tween._startTime) * tween._timeScale), suppressEvents, force);
						}
					}
					tween = next;
				}
			}

			if (this._onUpdate)
				if (!suppressEvents) {
					if (_lazyTweens.length) { //in case rendering caused any tweens to lazy-init, we should render them because typically when a timeline finishes, users expect things to have rendered fully. Imagine an onUpdate on a timeline that reports/checks tweened values.
						_lazyRender();
					}
					this._callback("onUpdate");
				}

			if (callback)
				if (!this._gc)
					if (prevStart === this._startTime || prevTimeScale !== this._timeScale)
						if (this._time === 0 || totalDur >= this.totalDuration()) { //if one of the tweens that was rendered altered this timeline's startTime (like if an onComplete reversed the timeline), it probably isn't complete. If it is, don't worry, because whatever call altered the startTime would complete if it was necessary at the new time. The only exception is the timeScale property. Also check _gc because there's a chance that kill() could be called in an onUpdate
							if (isComplete) {
								if (_lazyTweens.length) { //in case rendering caused any tweens to lazy-init, we should render them because typically when a timeline finishes, users expect things to have rendered fully. Imagine an onComplete on a timeline that reports/checks tweened values.
									_lazyRender();
								}
								if (this._timeline.autoRemoveChildren) {
									this._enabled(false, false);
								}
								this._active = false;
							}
							if (!suppressEvents && this.vars[callback]) {
								this._callback(callback);
							}
						}
		};

		p._hasPausedChild = function () {
			var tween = this._first;
			while (tween) {
				if (tween._paused || ((tween instanceof TimelineLite) && tween._hasPausedChild())) {
					return true;
				}
				tween = tween._next;
			}
			return false;
		};

		p.getChildren = function (nested, tweens, timelines, ignoreBeforeTime) {
			ignoreBeforeTime = ignoreBeforeTime || -9999999999;
			var a = []
				, tween = this._first
				, cnt = 0;
			while (tween) {
				if (tween._startTime < ignoreBeforeTime) {
					//do nothing
				} else if (tween instanceof TweenLite) {
					if (tweens !== false) {
						a[cnt++] = tween;
					}
				} else {
					if (timelines !== false) {
						a[cnt++] = tween;
					}
					if (nested !== false) {
						a = a.concat(tween.getChildren(true, tweens, timelines));
						cnt = a.length;
					}
				}
				tween = tween._next;
			}
			return a;
		};

		p.getTweensOf = function (target, nested) {
			var disabled = this._gc
				, a = []
				, cnt = 0
				, tweens, i;
			if (disabled) {
				this._enabled(true, true); //getTweensOf() filters out disabled tweens, and we have to mark them as _gc = true when the timeline completes in order to allow clean garbage collection, so temporarily re-enable the timeline here.
			}
			tweens = TweenLite.getTweensOf(target);
			i = tweens.length;
			while (--i > -1) {
				if (tweens[i].timeline === this || (nested && this._contains(tweens[i]))) {
					a[cnt++] = tweens[i];
				}
			}
			if (disabled) {
				this._enabled(false, true);
			}
			return a;
		};

		p.recent = function () {
			return this._recent;
		};

		p._contains = function (tween) {
			var tl = tween.timeline;
			while (tl) {
				if (tl === this) {
					return true;
				}
				tl = tl.timeline;
			}
			return false;
		};

		p.shiftChildren = function (amount, adjustLabels, ignoreBeforeTime) {
			ignoreBeforeTime = ignoreBeforeTime || 0;
			var tween = this._first
				, labels = this._labels
				, p;
			while (tween) {
				if (tween._startTime >= ignoreBeforeTime) {
					tween._startTime += amount;
				}
				tween = tween._next;
			}
			if (adjustLabels) {
				for (p in labels) {
					if (labels[p] >= ignoreBeforeTime) {
						labels[p] += amount;
					}
				}
			}
			return this._uncache(true);
		};

		p._kill = function (vars, target) {
			if (!vars && !target) {
				return this._enabled(false, false);
			}
			var tweens = (!target) ? this.getChildren(true, true, false) : this.getTweensOf(target)
				, i = tweens.length
				, changed = false;
			while (--i > -1) {
				if (tweens[i]._kill(vars, target)) {
					changed = true;
				}
			}
			return changed;
		};

		p.clear = function (labels) {
			var tweens = this.getChildren(false, true, true)
				, i = tweens.length;
			this._time = this._totalTime = 0;
			while (--i > -1) {
				tweens[i]._enabled(false, false);
			}
			if (labels !== false) {
				this._labels = {};
			}
			return this._uncache(true);
		};

		p.invalidate = function () {
			var tween = this._first;
			while (tween) {
				tween.invalidate();
				tween = tween._next;
			}
			return Animation.prototype.invalidate.call(this);;
		};

		p._enabled = function (enabled, ignoreTimeline) {
			if (enabled === this._gc) {
				var tween = this._first;
				while (tween) {
					tween._enabled(enabled, true);
					tween = tween._next;
				}
			}
			return SimpleTimeline.prototype._enabled.call(this, enabled, ignoreTimeline);
		};

		p.totalTime = function (time, suppressEvents, uncapped) {
			this._forcingPlayhead = true;
			var val = Animation.prototype.totalTime.apply(this, arguments);
			this._forcingPlayhead = false;
			return val;
		};

		p.duration = function (value) {
			if (!arguments.length) {
				if (this._dirty) {
					this.totalDuration(); //just triggers recalculation
				}
				return this._duration;
			}
			if (this.duration() !== 0 && value !== 0) {
				this.timeScale(this._duration / value);
			}
			return this;
		};

		p.totalDuration = function (value) {
			if (!arguments.length) {
				if (this._dirty) {
					var max = 0
						, tween = this._last
						, prevStart = 999999999999
						, prev, end;
					while (tween) {
						prev = tween._prev; //record it here in case the tween changes position in the sequence...
						if (tween._dirty) {
							tween.totalDuration(); //could change the tween._startTime, so make sure the tween's cache is clean before analyzing it.
						}
						if (tween._startTime > prevStart && this._sortChildren && !tween._paused) { //in case one of the tweens shifted out of order, it needs to be re-inserted into the correct position in the sequence
							this.add(tween, tween._startTime - tween._delay);
						} else {
							prevStart = tween._startTime;
						}
						if (tween._startTime < 0 && !tween._paused) { //children aren't allowed to have negative startTimes unless smoothChildTiming is true, so adjust here if one is found.
							max -= tween._startTime;
							if (this._timeline.smoothChildTiming) {
								this._startTime += tween._startTime / this._timeScale;
							}
							this.shiftChildren(-tween._startTime, false, -9999999999);
							prevStart = 0;
						}
						end = tween._startTime + (tween._totalDuration / tween._timeScale);
						if (end > max) {
							max = end;
						}
						tween = prev;
					}
					this._duration = this._totalDuration = max;
					this._dirty = false;
				}
				return this._totalDuration;
			}
			return (value && this.totalDuration()) ? this.timeScale(this._totalDuration / value) : this;
		};

		p.paused = function (value) {
			if (!value) { //if there's a pause directly at the spot from where we're unpausing, skip it.
				var tween = this._first
					, time = this._time;
				while (tween) {
					if (tween._startTime === time && tween.data === "isPause") {
						tween._rawPrevTime = 0; //remember, _rawPrevTime is how zero-duration tweens/callbacks sense directionality and determine whether or not to fire. If _rawPrevTime is the same as _startTime on the next render, it won't fire.
					}
					tween = tween._next;
				}
			}
			return Animation.prototype.paused.apply(this, arguments);
		};

		p.usesFrames = function () {
			var tl = this._timeline;
			while (tl._timeline) {
				tl = tl._timeline;
			}
			return (tl === Animation._rootFramesTimeline);
		};

		p.rawTime = function () {
			return this._paused ? this._totalTime : (this._timeline.rawTime() - this._startTime) * this._timeScale;
		};

		return TimelineLite;

	}, true);









	/*
	 * ----------------------------------------------------------------
	 * TimelineMax
	 * ----------------------------------------------------------------
	 */
	_gsScope._gsDefine("TimelineMax", ["TimelineLite", "TweenLite", "easing.Ease"], function (TimelineLite, TweenLite, Ease) {

		var TimelineMax = function (vars) {
			TimelineLite.call(this, vars);
			this._repeat = this.vars.repeat || 0;
			this._repeatDelay = this.vars.repeatDelay || 0;
			this._cycle = 0;
			this._yoyo = (this.vars.yoyo === true);
			this._dirty = true;
		}
			, _tinyNum = 0.0000000001
			, TweenLiteInternals = TweenLite._internals
			, _lazyTweens = TweenLiteInternals.lazyTweens
			, _lazyRender = TweenLiteInternals.lazyRender
			, _easeNone = new Ease(null, null, 1, 0)
			, p = TimelineMax.prototype = new TimelineLite();

		p.constructor = TimelineMax;
		p.kill()._gc = false;
		TimelineMax.version = "1.18.4";

		p.invalidate = function () {
			this._yoyo = (this.vars.yoyo === true);
			this._repeat = this.vars.repeat || 0;
			this._repeatDelay = this.vars.repeatDelay || 0;
			this._uncache(true);
			return TimelineLite.prototype.invalidate.call(this);
		};

		p.addCallback = function (callback, position, params, scope) {
			return this.add(TweenLite.delayedCall(0, callback, params, scope), position);
		};

		p.removeCallback = function (callback, position) {
			if (callback) {
				if (position == null) {
					this._kill(null, callback);
				} else {
					var a = this.getTweensOf(callback, false)
						, i = a.length
						, time = this._parseTimeOrLabel(position);
					while (--i > -1) {
						if (a[i]._startTime === time) {
							a[i]._enabled(false, false);
						}
					}
				}
			}
			return this;
		};

		p.removePause = function (position) {
			return this.removeCallback(TimelineLite._internals.pauseCallback, position);
		};

		p.tweenTo = function (position, vars) {
			vars = vars || {};
			var copy = {
				ease: _easeNone
				, useFrames: this.usesFrames()
				, immediateRender: false
			}
				, duration, p, t;
			for (p in vars) {
				copy[p] = vars[p];
			}
			copy.time = this._parseTimeOrLabel(position);
			duration = (Math.abs(Number(copy.time) - this._time) / this._timeScale) || 0.001;
			t = new TweenLite(this, duration, copy);
			copy.onStart = function () {
				t.target.paused(true);
				if (t.vars.time !== t.target.time() && duration === t.duration()) { //don't make the duration zero - if it's supposed to be zero, don't worry because it's already initting the tween and will complete immediately, effectively making the duration zero anyway. If we make duration zero, the tween won't run at all.
					t.duration(Math.abs(t.vars.time - t.target.time()) / t.target._timeScale);
				}
				if (vars.onStart) { //in case the user had an onStart in the vars - we don't want to overwrite it.
					t._callback("onStart");
				}
			};
			return t;
		};

		p.tweenFromTo = function (fromPosition, toPosition, vars) {
			vars = vars || {};
			fromPosition = this._parseTimeOrLabel(fromPosition);
			vars.startAt = {
				onComplete: this.seek
				, onCompleteParams: [fromPosition]
				, callbackScope: this
			};
			vars.immediateRender = (vars.immediateRender !== false);
			var t = this.tweenTo(toPosition, vars);
			return t.duration((Math.abs(t.vars.time - fromPosition) / this._timeScale) || 0.001);
		};

		p.render = function (time, suppressEvents, force) {
			if (this._gc) {
				this._enabled(true, false);
			}
			var totalDur = (!this._dirty) ? this._totalDuration : this.totalDuration()
				, dur = this._duration
				, prevTime = this._time
				, prevTotalTime = this._totalTime
				, prevStart = this._startTime
				, prevTimeScale = this._timeScale
				, prevRawPrevTime = this._rawPrevTime
				, prevPaused = this._paused
				, prevCycle = this._cycle
				, tween, isComplete, next, callback, internalForce, cycleDuration, pauseTween, curTime;
			if (time >= totalDur - 0.0000001) { //to work around occasional floating point math artifacts.
				if (!this._locked) {
					this._totalTime = totalDur;
					this._cycle = this._repeat;
				}
				if (!this._reversed)
					if (!this._hasPausedChild()) {
						isComplete = true;
						callback = "onComplete";
						internalForce = !!this._timeline.autoRemoveChildren; //otherwise, if the animation is unpaused/activated after it's already finished, it doesn't get removed from the parent timeline.
						if (this._duration === 0)
							if ((time <= 0 && time >= -0.0000001) || prevRawPrevTime < 0 || prevRawPrevTime === _tinyNum)
								if (prevRawPrevTime !== time && this._first) {
									internalForce = true;
									if (prevRawPrevTime > _tinyNum) {
										callback = "onReverseComplete";
									}
								}
					}
				this._rawPrevTime = (this._duration || !suppressEvents || time || this._rawPrevTime === time) ? time : _tinyNum; //when the playhead arrives at EXACTLY time 0 (right on top) of a zero-duration timeline or tween, we need to discern if events are suppressed so that when the playhead moves again (next time), it'll trigger the callback. If events are NOT suppressed, obviously the callback would be triggered in this render. Basically, the callback should fire either when the playhead ARRIVES or LEAVES this exact spot, not both. Imagine doing a timeline.seek(0) and there's a callback that sits at 0. Since events are suppressed on that seek() by default, nothing will fire, but when the playhead moves off of that position, the callback should fire. This behavior is what people intuitively expect. We set the _rawPrevTime to be a precise tiny number to indicate this scenario rather than using another property/variable which would increase memory usage. This technique is less readable, but more efficient.
				if (this._yoyo && (this._cycle & 1) !== 0) {
					this._time = time = 0;
				} else {
					this._time = dur;
					time = dur + 0.0001; //to avoid occasional floating point rounding errors - sometimes child tweens/timelines were not being fully completed (their progress might be 0.999999999999998 instead of 1 because when _time - tween._startTime is performed, floating point errors would return a value that was SLIGHTLY off). Try (999999999999.7 - 999999999999) * 1 = 0.699951171875 instead of 0.7. We cannot do less then 0.0001 because the same issue can occur when the duration is extremely large like 999999999999 in which case adding 0.00000001, for example, causes it to act like nothing was added.
				}

			} else if (time < 0.0000001) { //to work around occasional floating point math artifacts, round super small values to 0.
				if (!this._locked) {
					this._totalTime = this._cycle = 0;
				}
				this._time = 0;
				if (prevTime !== 0 || (dur === 0 && prevRawPrevTime !== _tinyNum && (prevRawPrevTime > 0 || (time < 0 && prevRawPrevTime >= 0)) && !this._locked)) { //edge case for checking time < 0 && prevRawPrevTime >= 0: a zero-duration fromTo() tween inside a zero-duration timeline (yeah, very rare)
					callback = "onReverseComplete";
					isComplete = this._reversed;
				}
				if (time < 0) {
					this._active = false;
					if (this._timeline.autoRemoveChildren && this._reversed) {
						internalForce = isComplete = true;
						callback = "onReverseComplete";
					} else if (prevRawPrevTime >= 0 && this._first) { //when going back beyond the start, force a render so that zero-duration tweens that sit at the very beginning render their start values properly. Otherwise, if the parent timeline's playhead lands exactly at this timeline's startTime, and then moves backwards, the zero-duration tweens at the beginning would still be at their end state.
						internalForce = true;
					}
					this._rawPrevTime = time;
				} else {
					this._rawPrevTime = (dur || !suppressEvents || time || this._rawPrevTime === time) ? time : _tinyNum; //when the playhead arrives at EXACTLY time 0 (right on top) of a zero-duration timeline or tween, we need to discern if events are suppressed so that when the playhead moves again (next time), it'll trigger the callback. If events are NOT suppressed, obviously the callback would be triggered in this render. Basically, the callback should fire either when the playhead ARRIVES or LEAVES this exact spot, not both. Imagine doing a timeline.seek(0) and there's a callback that sits at 0. Since events are suppressed on that seek() by default, nothing will fire, but when the playhead moves off of that position, the callback should fire. This behavior is what people intuitively expect. We set the _rawPrevTime to be a precise tiny number to indicate this scenario rather than using another property/variable which would increase memory usage. This technique is less readable, but more efficient.
					if (time === 0 && isComplete) { //if there's a zero-duration tween at the very beginning of a timeline and the playhead lands EXACTLY at time 0, that tween will correctly render its end values, but we need to keep the timeline alive for one more render so that the beginning values render properly as the parent's playhead keeps moving beyond the begining. Imagine obj.x starts at 0 and then we do tl.set(obj, {x:100}).to(obj, 1, {x:200}) and then later we tl.reverse()...the goal is to have obj.x revert to 0. If the playhead happens to land on exactly 0, without this chunk of code, it'd complete the timeline and remove it from the rendering queue (not good).
						tween = this._first;
						while (tween && tween._startTime === 0) {
							if (!tween._duration) {
								isComplete = false;
							}
							tween = tween._next;
						}
					}
					time = 0; //to avoid occasional floating point rounding errors (could cause problems especially with zero-duration tweens at the very beginning of the timeline)
					if (!this._initted) {
						internalForce = true;
					}
				}

			} else {
				if (dur === 0 && prevRawPrevTime < 0) { //without this, zero-duration repeating timelines (like with a simple callback nested at the very beginning and a repeatDelay) wouldn't render the first time through.
					internalForce = true;
				}
				this._time = this._rawPrevTime = time;
				if (!this._locked) {
					this._totalTime = time;
					if (this._repeat !== 0) {
						cycleDuration = dur + this._repeatDelay;
						this._cycle = (this._totalTime / cycleDuration) >> 0; //originally _totalTime % cycleDuration but floating point errors caused problems, so I normalized it. (4 % 0.8 should be 0 but it gets reported as 0.79999999!)
						if (this._cycle !== 0)
							if (this._cycle === this._totalTime / cycleDuration && prevTotalTime <= time) {
								this._cycle--; //otherwise when rendered exactly at the end time, it will act as though it is repeating (at the beginning)
							}
						this._time = this._totalTime - (this._cycle * cycleDuration);
						if (this._yoyo)
							if ((this._cycle & 1) !== 0) {
								this._time = dur - this._time;
							}
						if (this._time > dur) {
							this._time = dur;
							time = dur + 0.0001; //to avoid occasional floating point rounding error
						} else if (this._time < 0) {
							this._time = time = 0;
						} else {
							time = this._time;
						}
					}
				}

				if (this._hasPause && !this._forcingPlayhead && !suppressEvents) {
					time = this._time;
					if (time >= prevTime) {
						tween = this._first;
						while (tween && tween._startTime <= time && !pauseTween) {
							if (!tween._duration)
								if (tween.data === "isPause" && !tween.ratio && !(tween._startTime === 0 && this._rawPrevTime === 0)) {
									pauseTween = tween;
								}
							tween = tween._next;
						}
					} else {
						tween = this._last;
						while (tween && tween._startTime >= time && !pauseTween) {
							if (!tween._duration)
								if (tween.data === "isPause" && tween._rawPrevTime > 0) {
									pauseTween = tween;
								}
							tween = tween._prev;
						}
					}
					if (pauseTween) {
						this._time = time = pauseTween._startTime;
						this._totalTime = time + (this._cycle * (this._totalDuration + this._repeatDelay));
					}
				}

			}

			if (this._cycle !== prevCycle)
				if (!this._locked) {
					/*
					 make sure children at the end/beginning of the timeline are rendered properly. If, for example,
					 a 3-second long timeline rendered at 2.9 seconds previously, and now renders at 3.2 seconds (which
					 would get transated to 2.8 seconds if the timeline yoyos or 0.2 seconds if it just repeats), there
					 could be a callback or a short tween that's at 2.95 or 3 seconds in which wouldn't render. So
					 we need to push the timeline to the end (and/or beginning depending on its yoyo value). Also we must
					 ensure that zero-duration tweens at the very beginning or end of the TimelineMax work.
					 */
					var backwards = (this._yoyo && (prevCycle & 1) !== 0)
						, wrap = (backwards === (this._yoyo && (this._cycle & 1) !== 0))
						, recTotalTime = this._totalTime
						, recCycle = this._cycle
						, recRawPrevTime = this._rawPrevTime
						, recTime = this._time;

					this._totalTime = prevCycle * dur;
					if (this._cycle < prevCycle) {
						backwards = !backwards;
					} else {
						this._totalTime += dur;
					}
					this._time = prevTime; //temporarily revert _time so that render() renders the children in the correct order. Without this, tweens won't rewind correctly. We could arhictect things in a "cleaner" way by splitting out the rendering queue into a separate method but for performance reasons, we kept it all inside this method.

					this._rawPrevTime = (dur === 0) ? prevRawPrevTime - 0.0001 : prevRawPrevTime;
					this._cycle = prevCycle;
					this._locked = true; //prevents changes to totalTime and skips repeat/yoyo behavior when we recursively call render()
					prevTime = (backwards) ? 0 : dur;
					this.render(prevTime, suppressEvents, (dur === 0));
					if (!suppressEvents)
						if (!this._gc) {
							if (this.vars.onRepeat) {
								this._callback("onRepeat");
							}
						}
					if (prevTime !== this._time) { //in case there's a callback like onComplete in a nested tween/timeline that changes the playhead position, like via seek(), we should just abort.
						return;
					}
					if (wrap) {
						prevTime = (backwards) ? dur + 0.0001 : -0.0001;
						this.render(prevTime, true, false);
					}
					this._locked = false;
					if (this._paused && !prevPaused) { //if the render() triggered callback that paused this timeline, we should abort (very rare, but possible)
						return;
					}
					this._time = recTime;
					this._totalTime = recTotalTime;
					this._cycle = recCycle;
					this._rawPrevTime = recRawPrevTime;
				}

			if ((this._time === prevTime || !this._first) && !force && !internalForce && !pauseTween) {
				if (prevTotalTime !== this._totalTime)
					if (this._onUpdate)
						if (!suppressEvents) { //so that onUpdate fires even during the repeatDelay - as long as the totalTime changed, we should trigger onUpdate.
							this._callback("onUpdate");
						}
				return;
			} else if (!this._initted) {
				this._initted = true;
			}

			if (!this._active)
				if (!this._paused && this._totalTime !== prevTotalTime && time > 0) {
					this._active = true; //so that if the user renders the timeline (as opposed to the parent timeline rendering it), it is forced to re-render and align it with the proper time/frame on the next rendering cycle. Maybe the timeline already finished but the user manually re-renders it as halfway done, for example.
				}

			if (prevTotalTime === 0)
				if (this.vars.onStart)
					if (this._totalTime !== 0)
						if (!suppressEvents) {
							this._callback("onStart");
						}

			curTime = this._time;
			if (curTime >= prevTime) {
				tween = this._first;
				while (tween) {
					next = tween._next; //record it here because the value could change after rendering...
					if (curTime !== this._time || (this._paused && !prevPaused)) { //in case a tween pauses or seeks the timeline when rendering, like inside of an onUpdate/onComplete
						break;
					} else if (tween._active || (tween._startTime <= this._time && !tween._paused && !tween._gc)) {
						if (pauseTween === tween) {
							this.pause();
						}
						if (!tween._reversed) {
							tween.render((time - tween._startTime) * tween._timeScale, suppressEvents, force);
						} else {
							tween.render(((!tween._dirty) ? tween._totalDuration : tween.totalDuration()) - ((time - tween._startTime) * tween._timeScale), suppressEvents, force);
						}
					}
					tween = next;
				}
			} else {
				tween = this._last;
				while (tween) {
					next = tween._prev; //record it here because the value could change after rendering...
					if (curTime !== this._time || (this._paused && !prevPaused)) { //in case a tween pauses or seeks the timeline when rendering, like inside of an onUpdate/onComplete
						break;
					} else if (tween._active || (tween._startTime <= prevTime && !tween._paused && !tween._gc)) {
						if (pauseTween === tween) {
							pauseTween = tween._prev; //the linked list is organized by _startTime, thus it's possible that a tween could start BEFORE the pause and end after it, in which case it would be positioned before the pause tween in the linked list, but we should render it before we pause() the timeline and cease rendering. This is only a concern when going in reverse.
							while (pauseTween && pauseTween.endTime() > this._time) {
								pauseTween.render((pauseTween._reversed ? pauseTween.totalDuration() - ((time - pauseTween._startTime) * pauseTween._timeScale) : (time - pauseTween._startTime) * pauseTween._timeScale), suppressEvents, force);
								pauseTween = pauseTween._prev;
							}
							pauseTween = null;
							this.pause();
						}
						if (!tween._reversed) {
							tween.render((time - tween._startTime) * tween._timeScale, suppressEvents, force);
						} else {
							tween.render(((!tween._dirty) ? tween._totalDuration : tween.totalDuration()) - ((time - tween._startTime) * tween._timeScale), suppressEvents, force);
						}
					}
					tween = next;
				}
			}

			if (this._onUpdate)
				if (!suppressEvents) {
					if (_lazyTweens.length) { //in case rendering caused any tweens to lazy-init, we should render them because typically when a timeline finishes, users expect things to have rendered fully. Imagine an onUpdate on a timeline that reports/checks tweened values.
						_lazyRender();
					}
					this._callback("onUpdate");
				}
			if (callback)
				if (!this._locked)
					if (!this._gc)
						if (prevStart === this._startTime || prevTimeScale !== this._timeScale)
							if (this._time === 0 || totalDur >= this.totalDuration()) { //if one of the tweens that was rendered altered this timeline's startTime (like if an onComplete reversed the timeline), it probably isn't complete. If it is, don't worry, because whatever call altered the startTime would complete if it was necessary at the new time. The only exception is the timeScale property. Also check _gc because there's a chance that kill() could be called in an onUpdate
								if (isComplete) {
									if (_lazyTweens.length) { //in case rendering caused any tweens to lazy-init, we should render them because typically when a timeline finishes, users expect things to have rendered fully. Imagine an onComplete on a timeline that reports/checks tweened values.
										_lazyRender();
									}
									if (this._timeline.autoRemoveChildren) {
										this._enabled(false, false);
									}
									this._active = false;
								}
								if (!suppressEvents && this.vars[callback]) {
									this._callback(callback);
								}
							}
		};

		p.getActive = function (nested, tweens, timelines) {
			if (nested == null) {
				nested = true;
			}
			if (tweens == null) {
				tweens = true;
			}
			if (timelines == null) {
				timelines = false;
			}
			var a = []
				, all = this.getChildren(nested, tweens, timelines)
				, cnt = 0
				, l = all.length
				, i, tween;
			for (i = 0; i < l; i++) {
				tween = all[i];
				if (tween.isActive()) {
					a[cnt++] = tween;
				}
			}
			return a;
		};


		p.getLabelAfter = function (time) {
			if (!time)
				if (time !== 0) { //faster than isNan()
					time = this._time;
				}
			var labels = this.getLabelsArray()
				, l = labels.length
				, i;
			for (i = 0; i < l; i++) {
				if (labels[i].time > time) {
					return labels[i].name;
				}
			}
			return null;
		};

		p.getLabelBefore = function (time) {
			if (time == null) {
				time = this._time;
			}
			var labels = this.getLabelsArray()
				, i = labels.length;
			while (--i > -1) {
				if (labels[i].time < time) {
					return labels[i].name;
				}
			}
			return null;
		};

		p.getLabelsArray = function () {
			var a = []
				, cnt = 0
				, p;
			for (p in this._labels) {
				a[cnt++] = {
					time: this._labels[p]
					, name: p
				};
			}
			a.sort(function (a, b) {
				return a.time - b.time;
			});
			return a;
		};


		//---- GETTERS / SETTERS -------------------------------------------------------------------------------------------------------

		p.progress = function (value, suppressEvents) {
			return (!arguments.length) ? this._time / this.duration() : this.totalTime(this.duration() * ((this._yoyo && (this._cycle & 1) !== 0) ? 1 - value : value) + (this._cycle * (this._duration + this._repeatDelay)), suppressEvents);
		};

		p.totalProgress = function (value, suppressEvents) {
			return (!arguments.length) ? this._totalTime / this.totalDuration() : this.totalTime(this.totalDuration() * value, suppressEvents);
		};

		p.totalDuration = function (value) {
			if (!arguments.length) {
				if (this._dirty) {
					TimelineLite.prototype.totalDuration.call(this); //just forces refresh
					//Instead of Infinity, we use 999999999999 so that we can accommodate reverses.
					this._totalDuration = (this._repeat === -1) ? 999999999999 : this._duration * (this._repeat + 1) + (this._repeatDelay * this._repeat);
				}
				return this._totalDuration;
			}
			return (this._repeat === -1 || !value) ? this : this.timeScale(this.totalDuration() / value);
		};

		p.time = function (value, suppressEvents) {
			if (!arguments.length) {
				return this._time;
			}
			if (this._dirty) {
				this.totalDuration();
			}
			if (value > this._duration) {
				value = this._duration;
			}
			if (this._yoyo && (this._cycle & 1) !== 0) {
				value = (this._duration - value) + (this._cycle * (this._duration + this._repeatDelay));
			} else if (this._repeat !== 0) {
				value += this._cycle * (this._duration + this._repeatDelay);
			}
			return this.totalTime(value, suppressEvents);
		};

		p.repeat = function (value) {
			if (!arguments.length) {
				return this._repeat;
			}
			this._repeat = value;
			return this._uncache(true);
		};

		p.repeatDelay = function (value) {
			if (!arguments.length) {
				return this._repeatDelay;
			}
			this._repeatDelay = value;
			return this._uncache(true);
		};

		p.yoyo = function (value) {
			if (!arguments.length) {
				return this._yoyo;
			}
			this._yoyo = value;
			return this;
		};

		p.currentLabel = function (value) {
			if (!arguments.length) {
				return this.getLabelBefore(this._time + 0.00000001);
			}
			return this.seek(value, true);
		};

		return TimelineMax;

	}, true);









	/*
	 * ----------------------------------------------------------------
	 * BezierPlugin
	 * ----------------------------------------------------------------
	 */
	(function () {

		var _RAD2DEG = 180 / Math.PI
			, _r1 = []
			, _r2 = []
			, _r3 = []
			, _corProps = {}
			, _globals = _gsScope._gsDefine.globals
			, Segment = function (a, b, c, d) {
				this.a = a;
				this.b = b;
				this.c = c;
				this.d = d;
				this.da = d - a;
				this.ca = c - a;
				this.ba = b - a;
			}
			, _correlate = ",x,y,z,left,top,right,bottom,marginTop,marginLeft,marginRight,marginBottom,paddingLeft,paddingTop,paddingRight,paddingBottom,backgroundPosition,backgroundPosition_y,"
			, cubicToQuadratic = function (a, b, c, d) {
				var q1 = {
					a: a
				}
					, q2 = {}
					, q3 = {}
					, q4 = {
					c: d
				}
					, mab = (a + b) / 2
					, mbc = (b + c) / 2
					, mcd = (c + d) / 2
					, mabc = (mab + mbc) / 2
					, mbcd = (mbc + mcd) / 2
					, m8 = (mbcd - mabc) / 8;
				q1.b = mab + (a - mab) / 4;
				q2.b = mabc + m8;
				q1.c = q2.a = (q1.b + q2.b) / 2;
				q2.c = q3.a = (mabc + mbcd) / 2;
				q3.b = mbcd - m8;
				q4.b = mcd + (d - mcd) / 4;
				q3.c = q4.a = (q3.b + q4.b) / 2;
				return [q1, q2, q3, q4];
			}
			, _calculateControlPoints = function (a, curviness, quad, basic, correlate) {
				var l = a.length - 1
					, ii = 0
					, cp1 = a[0].a
					, i, p1, p2, p3, seg, m1, m2, mm, cp2, qb, r1, r2, tl;
				for (i = 0; i < l; i++) {
					seg = a[ii];
					p1 = seg.a;
					p2 = seg.d;
					p3 = a[ii + 1].d;

					if (correlate) {
						r1 = _r1[i];
						r2 = _r2[i];
						tl = ((r2 + r1) * curviness * 0.25) / (basic ? 0.5 : _r3[i] || 0.5);
						m1 = p2 - (p2 - p1) * (basic ? curviness * 0.5 : (r1 !== 0 ? tl / r1 : 0));
						m2 = p2 + (p3 - p2) * (basic ? curviness * 0.5 : (r2 !== 0 ? tl / r2 : 0));
						mm = p2 - (m1 + (((m2 - m1) * ((r1 * 3 / (r1 + r2)) + 0.5) / 4) || 0));
					} else {
						m1 = p2 - (p2 - p1) * curviness * 0.5;
						m2 = p2 + (p3 - p2) * curviness * 0.5;
						mm = p2 - (m1 + m2) / 2;
					}
					m1 += mm;
					m2 += mm;

					seg.c = cp2 = m1;
					if (i !== 0) {
						seg.b = cp1;
					} else {
						seg.b = cp1 = seg.a + (seg.c - seg.a) * 0.6; //instead of placing b on a exactly, we move it inline with c so that if the user specifies an ease like Back.easeIn or Elastic.easeIn which goes BEYOND the beginning, it will do so smoothly.
					}

					seg.da = p2 - p1;
					seg.ca = cp2 - p1;
					seg.ba = cp1 - p1;

					if (quad) {
						qb = cubicToQuadratic(p1, cp1, cp2, p2);
						a.splice(ii, 1, qb[0], qb[1], qb[2], qb[3]);
						ii += 4;
					} else {
						ii++;
					}

					cp1 = m2;
				}
				seg = a[ii];
				seg.b = cp1;
				seg.c = cp1 + (seg.d - cp1) * 0.4; //instead of placing c on d exactly, we move it inline with b so that if the user specifies an ease like Back.easeOut or Elastic.easeOut which goes BEYOND the end, it will do so smoothly.
				seg.da = seg.d - seg.a;
				seg.ca = seg.c - seg.a;
				seg.ba = cp1 - seg.a;
				if (quad) {
					qb = cubicToQuadratic(seg.a, cp1, seg.c, seg.d);
					a.splice(ii, 1, qb[0], qb[1], qb[2], qb[3]);
				}
			}
			, _parseAnchors = function (values, p, correlate, prepend) {
				var a = []
					, l, i, p1, p2, p3, tmp;
				if (prepend) {
					values = [prepend].concat(values);
					i = values.length;
					while (--i > -1) {
						if (typeof ((tmp = values[i][p])) === "string")
							if (tmp.charAt(1) === "=") {
								values[i][p] = prepend[p] + Number(tmp.charAt(0) + tmp.substr(2)); //accommodate relative values. Do it inline instead of breaking it out into a function for speed reasons
							}
					}
				}
				l = values.length - 2;
				if (l < 0) {
					a[0] = new Segment(values[0][p], 0, 0, values[(l < -1) ? 0 : 1][p]);
					return a;
				}
				for (i = 0; i < l; i++) {
					p1 = values[i][p];
					p2 = values[i + 1][p];
					a[i] = new Segment(p1, 0, 0, p2);
					if (correlate) {
						p3 = values[i + 2][p];
						_r1[i] = (_r1[i] || 0) + (p2 - p1) * (p2 - p1);
						_r2[i] = (_r2[i] || 0) + (p3 - p2) * (p3 - p2);
					}
				}
				a[i] = new Segment(values[i][p], 0, 0, values[i + 1][p]);
				return a;
			}
			, bezierThrough = function (values, curviness, quadratic, basic, correlate, prepend) {
				var obj = {}
					, props = []
					, first = prepend || values[0]
					, i, p, a, j, r, l, seamless, last;
				correlate = (typeof (correlate) === "string") ? "," + correlate + "," : _correlate;
				if (curviness == null) {
					curviness = 1;
				}
				for (p in values[0]) {
					props.push(p);
				}
				//check to see if the last and first values are identical (well, within 0.05). If so, make seamless by appending the second element to the very end of the values array and the 2nd-to-last element to the very beginning (we'll remove those segments later)
				if (values.length > 1) {
					last = values[values.length - 1];
					seamless = true;
					i = props.length;
					while (--i > -1) {
						p = props[i];
						if (Math.abs(first[p] - last[p]) > 0.05) { //build in a tolerance of +/-0.05 to accommodate rounding errors.
							seamless = false;
							break;
						}
					}
					if (seamless) {
						values = values.concat(); //duplicate the array to avoid contaminating the original which the user may be reusing for other tweens
						if (prepend) {
							values.unshift(prepend);
						}
						values.push(values[1]);
						prepend = values[values.length - 3];
					}
				}
				_r1.length = _r2.length = _r3.length = 0;
				i = props.length;
				while (--i > -1) {
					p = props[i];
					_corProps[p] = (correlate.indexOf("," + p + ",") !== -1);
					obj[p] = _parseAnchors(values, p, _corProps[p], prepend);
				}
				i = _r1.length;
				while (--i > -1) {
					_r1[i] = Math.sqrt(_r1[i]);
					_r2[i] = Math.sqrt(_r2[i]);
				}
				if (!basic) {
					i = props.length;
					while (--i > -1) {
						if (_corProps[p]) {
							a = obj[props[i]];
							l = a.length - 1;
							for (j = 0; j < l; j++) {
								r = (a[j + 1].da / _r2[j] + a[j].da / _r1[j]) || 0;
								_r3[j] = (_r3[j] || 0) + r * r;
							}
						}
					}
					i = _r3.length;
					while (--i > -1) {
						_r3[i] = Math.sqrt(_r3[i]);
					}
				}
				i = props.length;
				j = quadratic ? 4 : 1;
				while (--i > -1) {
					p = props[i];
					a = obj[p];
					_calculateControlPoints(a, curviness, quadratic, basic, _corProps[p]); //this method requires that _parseAnchors() and _setSegmentRatios() ran first so that _r1, _r2, and _r3 values are populated for all properties
					if (seamless) {
						a.splice(0, j);
						a.splice(a.length - j, j);
					}
				}
				return obj;
			}
			, _parseBezierData = function (values, type, prepend) {
				type = type || "soft";
				var obj = {}
					, inc = (type === "cubic") ? 3 : 2
					, soft = (type === "soft")
					, props = []
					, a, b, c, d, cur, i, j, l, p, cnt, tmp;
				if (soft && prepend) {
					values = [prepend].concat(values);
				}
				if (values == null || values.length < inc + 1) {
					throw "invalid Bezier data";
				}
				for (p in values[0]) {
					props.push(p);
				}
				i = props.length;
				while (--i > -1) {
					p = props[i];
					obj[p] = cur = [];
					cnt = 0;
					l = values.length;
					for (j = 0; j < l; j++) {
						a = (prepend == null) ? values[j][p] : (typeof ((tmp = values[j][p])) === "string" && tmp.charAt(1) === "=") ? prepend[p] + Number(tmp.charAt(0) + tmp.substr(2)) : Number(tmp);
						if (soft)
							if (j > 1)
								if (j < l - 1) {
									cur[cnt++] = (a + cur[cnt - 2]) / 2;
								}
						cur[cnt++] = a;
					}
					l = cnt - inc + 1;
					cnt = 0;
					for (j = 0; j < l; j += inc) {
						a = cur[j];
						b = cur[j + 1];
						c = cur[j + 2];
						d = (inc === 2) ? 0 : cur[j + 3];
						cur[cnt++] = tmp = (inc === 3) ? new Segment(a, b, c, d) : new Segment(a, (2 * b + a) / 3, (2 * b + c) / 3, c);
					}
					cur.length = cnt;
				}
				return obj;
			}
			, _addCubicLengths = function (a, steps, resolution) {
				var inc = 1 / resolution
					, j = a.length
					, d, d1, s, da, ca, ba, p, i, inv, bez, index;
				while (--j > -1) {
					bez = a[j];
					s = bez.a;
					da = bez.d - s;
					ca = bez.c - s;
					ba = bez.b - s;
					d = d1 = 0;
					for (i = 1; i <= resolution; i++) {
						p = inc * i;
						inv = 1 - p;
						d = d1 - (d1 = (p * p * da + 3 * inv * (p * ca + inv * ba)) * p);
						index = j * resolution + i - 1;
						steps[index] = (steps[index] || 0) + d * d;
					}
				}
			}
			, _parseLengthData = function (obj, resolution) {
				resolution = resolution >> 0 || 6;
				var a = []
					, lengths = []
					, d = 0
					, total = 0
					, threshold = resolution - 1
					, segments = []
					, curLS = [], //current length segments array
					p, i, l, index;
				for (p in obj) {
					_addCubicLengths(obj[p], a, resolution);
				}
				l = a.length;
				for (i = 0; i < l; i++) {
					d += Math.sqrt(a[i]);
					index = i % resolution;
					curLS[index] = d;
					if (index === threshold) {
						total += d;
						index = (i / resolution) >> 0;
						segments[index] = curLS;
						lengths[index] = total;
						d = 0;
						curLS = [];
					}
				}
				return {
					length: total
					, lengths: lengths
					, segments: segments
				};
			},



			BezierPlugin = _gsScope._gsDefine.plugin({
				propName: "bezier"
				, priority: -1
				, version: "1.3.5"
				, API: 2
				, global: true,

				//gets called when the tween renders for the first time. This is where initial values should be recorded and any setup routines should run.
				init: function (target, vars, tween) {
					this._target = target;
					if (vars instanceof Array) {
						vars = {
							values: vars
						};
					}
					this._func = {};
					this._round = {};
					this._props = [];
					this._timeRes = (vars.timeResolution == null) ? 6 : parseInt(vars.timeResolution, 10);
					var values = vars.values || []
						, first = {}
						, second = values[0]
						, autoRotate = vars.autoRotate || tween.vars.orientToBezier
						, p, isFunc, i, j, prepend;

					this._autoRotate = autoRotate ? (autoRotate instanceof Array) ? autoRotate : [["x", "y", "rotation", ((autoRotate === true) ? 0 : Number(autoRotate) || 0)]] : null;
					for (p in second) {
						this._props.push(p);
					}

					i = this._props.length;
					while (--i > -1) {
						p = this._props[i];

						this._overwriteProps.push(p);
						isFunc = this._func[p] = (typeof (target[p]) === "function");
						first[p] = (!isFunc) ? parseFloat(target[p]) : target[((p.indexOf("set") || typeof (target["get" + p.substr(3)]) !== "function") ? p : "get" + p.substr(3))]();
						if (!prepend)
							if (first[p] !== values[0][p]) {
								prepend = first;
							}
					}
					this._beziers = (vars.type !== "cubic" && vars.type !== "quadratic" && vars.type !== "soft") ? bezierThrough(values, isNaN(vars.curviness) ? 1 : vars.curviness, false, (vars.type === "thruBasic"), vars.correlate, prepend) : _parseBezierData(values, vars.type, first);
					this._segCount = this._beziers[p].length;

					if (this._timeRes) {
						var ld = _parseLengthData(this._beziers, this._timeRes);
						this._length = ld.length;
						this._lengths = ld.lengths;
						this._segments = ld.segments;
						this._l1 = this._li = this._s1 = this._si = 0;
						this._l2 = this._lengths[0];
						this._curSeg = this._segments[0];
						this._s2 = this._curSeg[0];
						this._prec = 1 / this._curSeg.length;
					}

					if ((autoRotate = this._autoRotate)) {
						this._initialRotations = [];
						if (!(autoRotate[0] instanceof Array)) {
							this._autoRotate = autoRotate = [autoRotate];
						}
						i = autoRotate.length;
						while (--i > -1) {
							for (j = 0; j < 3; j++) {
								p = autoRotate[i][j];
								this._func[p] = (typeof (target[p]) === "function") ? target[((p.indexOf("set") || typeof (target["get" + p.substr(3)]) !== "function") ? p : "get" + p.substr(3))] : false;
							}
							p = autoRotate[i][2];
							this._initialRotations[i] = (this._func[p] ? this._func[p].call(this._target) : this._target[p]) || 0;
						}
					}
					this._startRatio = tween.vars.runBackwards ? 1 : 0; //we determine the starting ratio when the tween inits which is always 0 unless the tween has runBackwards:true (indicating it's a from() tween) in which case it's 1.
					return true;
				},

				//called each time the values should be updated, and the ratio gets passed as the only parameter (typically it's a value between 0 and 1, but it can exceed those when using an ease like Elastic.easeOut or Back.easeOut, etc.)
				set: function (v) {
					var segments = this._segCount
						, func = this._func
						, target = this._target
						, notStart = (v !== this._startRatio)
						, curIndex, inv, i, p, b, t, val, l, lengths, curSeg;
					if (!this._timeRes) {
						curIndex = (v < 0) ? 0 : (v >= 1) ? segments - 1 : (segments * v) >> 0;
						t = (v - (curIndex * (1 / segments))) * segments;
					} else {
						lengths = this._lengths;
						curSeg = this._curSeg;
						v *= this._length;
						i = this._li;
						//find the appropriate segment (if the currently cached one isn't correct)
						if (v > this._l2 && i < segments - 1) {
							l = segments - 1;
							while (i < l && (this._l2 = lengths[++i]) <= v) {}
							this._l1 = lengths[i - 1];
							this._li = i;
							this._curSeg = curSeg = this._segments[i];
							this._s2 = curSeg[(this._s1 = this._si = 0)];
						} else if (v < this._l1 && i > 0) {
							while (i > 0 && (this._l1 = lengths[--i]) >= v) {}
							if (i === 0 && v < this._l1) {
								this._l1 = 0;
							} else {
								i++;
							}
							this._l2 = lengths[i];
							this._li = i;
							this._curSeg = curSeg = this._segments[i];
							this._s1 = curSeg[(this._si = curSeg.length - 1) - 1] || 0;
							this._s2 = curSeg[this._si];
						}
						curIndex = i;
						//now find the appropriate sub-segment (we split it into the number of pieces that was defined by "precision" and measured each one)
						v -= this._l1;
						i = this._si;
						if (v > this._s2 && i < curSeg.length - 1) {
							l = curSeg.length - 1;
							while (i < l && (this._s2 = curSeg[++i]) <= v) {}
							this._s1 = curSeg[i - 1];
							this._si = i;
						} else if (v < this._s1 && i > 0) {
							while (i > 0 && (this._s1 = curSeg[--i]) >= v) {}
							if (i === 0 && v < this._s1) {
								this._s1 = 0;
							} else {
								i++;
							}
							this._s2 = curSeg[i];
							this._si = i;
						}
						t = ((i + (v - this._s1) / (this._s2 - this._s1)) * this._prec) || 0;
					}
					inv = 1 - t;

					i = this._props.length;
					while (--i > -1) {
						p = this._props[i];
						b = this._beziers[p][curIndex];
						val = (t * t * b.da + 3 * inv * (t * b.ca + inv * b.ba)) * t + b.a;
						if (this._round[p]) {
							val = Math.round(val);
						}
						if (func[p]) {
							target[p](val);
						} else {
							target[p] = val;
						}
					}

					if (this._autoRotate) {
						var ar = this._autoRotate
							, b2, x1, y1, x2, y2, add, conv;
						i = ar.length;
						while (--i > -1) {
							p = ar[i][2];
							add = ar[i][3] || 0;
							conv = (ar[i][4] === true) ? 1 : _RAD2DEG;
							b = this._beziers[ar[i][0]];
							b2 = this._beziers[ar[i][1]];

							if (b && b2) { //in case one of the properties got overwritten.
								b = b[curIndex];
								b2 = b2[curIndex];

								x1 = b.a + (b.b - b.a) * t;
								x2 = b.b + (b.c - b.b) * t;
								x1 += (x2 - x1) * t;
								x2 += ((b.c + (b.d - b.c) * t) - x2) * t;

								y1 = b2.a + (b2.b - b2.a) * t;
								y2 = b2.b + (b2.c - b2.b) * t;
								y1 += (y2 - y1) * t;
								y2 += ((b2.c + (b2.d - b2.c) * t) - y2) * t;

								val = notStart ? Math.atan2(y2 - y1, x2 - x1) * conv + add : this._initialRotations[i];

								if (func[p]) {
									target[p](val);
								} else {
									target[p] = val;
								}
							}
						}
					}
				}
			})
			, p = BezierPlugin.prototype;


		BezierPlugin.bezierThrough = bezierThrough;
		BezierPlugin.cubicToQuadratic = cubicToQuadratic;
		BezierPlugin._autoCSS = true; //indicates that this plugin can be inserted into the "css" object using the autoCSS feature of TweenLite
		BezierPlugin.quadraticToCubic = function (a, b, c) {
			return new Segment(a, (2 * b + a) / 3, (2 * b + c) / 3, c);
		};

		BezierPlugin._cssRegister = function () {
			var CSSPlugin = _globals.CSSPlugin;
			if (!CSSPlugin) {
				return;
			}
			var _internals = CSSPlugin._internals
				, _parseToProxy = _internals._parseToProxy
				, _setPluginRatio = _internals._setPluginRatio
				, CSSPropTween = _internals.CSSPropTween;
			_internals._registerComplexSpecialProp("bezier", {
				parser: function (t, e, prop, cssp, pt, plugin) {
					if (e instanceof Array) {
						e = {
							values: e
						};
					}
					plugin = new BezierPlugin();
					var values = e.values
						, l = values.length - 1
						, pluginValues = []
						, v = {}
						, i, p, data;
					if (l < 0) {
						return pt;
					}
					for (i = 0; i <= l; i++) {
						data = _parseToProxy(t, values[i], cssp, pt, plugin, (l !== i));
						pluginValues[i] = data.end;
					}
					for (p in e) {
						v[p] = e[p]; //duplicate the vars object because we need to alter some things which would cause problems if the user plans to reuse the same vars object for another tween.
					}
					v.values = pluginValues;
					pt = new CSSPropTween(t, "bezier", 0, 0, data.pt, 2);
					pt.data = data;
					pt.plugin = plugin;
					pt.setRatio = _setPluginRatio;
					if (v.autoRotate === 0) {
						v.autoRotate = true;
					}
					if (v.autoRotate && !(v.autoRotate instanceof Array)) {
						i = (v.autoRotate === true) ? 0 : Number(v.autoRotate);
						v.autoRotate = (data.end.left != null) ? [["left", "top", "rotation", i, false]] : (data.end.x != null) ? [["x", "y", "rotation", i, false]] : false;
					}
					if (v.autoRotate) {
						if (!cssp._transform) {
							cssp._enableTransforms(false);
						}
						data.autoRotate = cssp._target._gsTransform;
					}
					plugin._onInitTween(data.proxy, v, cssp._tween);
					return pt;
				}
			});
		};

		p._roundProps = function (lookup, value) {
			var op = this._overwriteProps
				, i = op.length;
			while (--i > -1) {
				if (lookup[op[i]] || lookup.bezier || lookup.bezierThrough) {
					this._round[op[i]] = value;
				}
			}
		};

		p._kill = function (lookup) {
			var a = this._props
				, p, i;
			for (p in this._beziers) {
				if (p in lookup) {
					delete this._beziers[p];
					delete this._func[p];
					i = a.length;
					while (--i > -1) {
						if (a[i] === p) {
							a.splice(i, 1);
						}
					}
				}
			}
			return this._super._kill.call(this, lookup);
		};

	}());









	/*
	 * ----------------------------------------------------------------
	 * CSSPlugin
	 * ----------------------------------------------------------------
	 */
	_gsScope._gsDefine("plugins.CSSPlugin", ["plugins.TweenPlugin", "TweenLite"], function (TweenPlugin, TweenLite) {

		/** @constructor **/
		var CSSPlugin = function () {
				TweenPlugin.call(this, "css");
				this._overwriteProps.length = 0;
				this.setRatio = CSSPlugin.prototype.setRatio; //speed optimization (avoid prototype lookup on this "hot" method)
			}
			, _globals = _gsScope._gsDefine.globals
			, _hasPriority, //turns true whenever a CSSPropTween instance is created that has a priority other than 0. This helps us discern whether or not we should spend the time organizing the linked list or not after a CSSPlugin's _onInitTween() method is called.
			_suffixMap, //we set this in _onInitTween() each time as a way to have a persistent variable we can use in other methods like _parse() without having to pass it around as a parameter and we keep _parse() decoupled from a particular CSSPlugin instance
			_cs, //computed style (we store this in a shared variable to conserve memory and make minification tighter
			_overwriteProps, //alias to the currently instantiating CSSPlugin's _overwriteProps array. We use this closure in order to avoid having to pass a reference around from method to method and aid in minification.
			_specialProps = {}
			, p = CSSPlugin.prototype = new TweenPlugin("css");

		p.constructor = CSSPlugin;
		CSSPlugin.version = "1.18.4";
		CSSPlugin.API = 2;
		CSSPlugin.defaultTransformPerspective = 0;
		CSSPlugin.defaultSkewType = "compensated";
		CSSPlugin.defaultSmoothOrigin = true;
		p = "px"; //we'll reuse the "p" variable to keep file size down
		CSSPlugin.suffixMap = {
			top: p
			, right: p
			, bottom: p
			, left: p
			, width: p
			, height: p
			, fontSize: p
			, padding: p
			, margin: p
			, perspective: p
			, lineHeight: ""
		};


		var _numExp = /(?:\-|\.|\b)(\d|\.|e\-)+/g
			, _relNumExp = /(?:\d|\-\d|\.\d|\-\.\d|\+=\d|\-=\d|\+=.\d|\-=\.\d)+/g
			, _valuesExp = /(?:\+=|\-=|\-|\b)[\d\-\.]+[a-zA-Z0-9]*(?:%|\b)/gi, //finds all the values that begin with numbers or += or -= and then a number. Includes suffixes. We use this to split complex values apart like "1px 5px 20px rgb(255,102,51)"
			_NaNExp = /(?![+-]?\d*\.?\d+|[+-]|e[+-]\d+)[^0-9]/g, //also allows scientific notation and doesn't kill the leading -/+ in -= and +=
			_suffixExp = /(?:\d|\-|\+|=|#|\.)*/g
			, _opacityExp = /opacity *= *([^)]*)/i
			, _opacityValExp = /opacity:([^;]*)/i
			, _alphaFilterExp = /alpha\(opacity *=.+?\)/i
			, _rgbhslExp = /^(rgb|hsl)/
			, _capsExp = /([A-Z])/g
			, _camelExp = /-([a-z])/gi
			, _urlExp = /(^(?:url\(\"|url\())|(?:(\"\))$|\)$)/gi, //for pulling out urls from url(...) or url("...") strings (some browsers wrap urls in quotes, some don't when reporting things like backgroundImage)
			_camelFunc = function (s, g) {
				return g.toUpperCase();
			}
			, _horizExp = /(?:Left|Right|Width)/i
			, _ieGetMatrixExp = /(M11|M12|M21|M22)=[\d\-\.e]+/gi
			, _ieSetMatrixExp = /progid\:DXImageTransform\.Microsoft\.Matrix\(.+?\)/i
			, _commasOutsideParenExp = /,(?=[^\)]*(?:\(|$))/gi, //finds any commas that are not within parenthesis
			_complexExp = /[\s,\(]/i, //for testing a string to find if it has a space, comma, or open parenthesis (clues that it's a complex value)
			_DEG2RAD = Math.PI / 180
			, _RAD2DEG = 180 / Math.PI
			, _forcePT = {}
			, _doc = document
			, _createElement = function (type) {
				return _doc.createElementNS ? _doc.createElementNS("http://www.w3.org/1999/xhtml", type) : _doc.createElement(type);
			}
			, _tempDiv = _createElement("div")
			, _tempImg = _createElement("img")
			, _internals = CSSPlugin._internals = {
				_specialProps: _specialProps
			}, //provides a hook to a few internal methods that we need to access from inside other plugins
			_agent = navigator.userAgent
			, _autoRound
			, _reqSafariFix, //we won't apply the Safari transform fix until we actually come across a tween that affects a transform property (to maintain best performance).

			_isSafari
			, _isFirefox, //Firefox has a bug that causes 3D transformed elements to randomly disappear unless a repaint is forced after each update on each element.
			_isSafariLT6, //Safari (and Android 4 which uses a flavor of Safari) has a bug that prevents changes to "top" and "left" properties from rendering properly if changed on the same frame as a transform UNLESS we set the element's WebkitBackfaceVisibility to hidden (weird, I know). Doing this for Android 3 and earlier seems to actually cause other problems, though (fun!)
			_ieVers
			, _supportsOpacity = (function () { //we set _isSafari, _ieVers, _isFirefox, and _supportsOpacity all in one function here to reduce file size slightly, especially in the minified version.
				var i = _agent.indexOf("Android")
					, a = _createElement("a");
				_isSafari = (_agent.indexOf("Safari") !== -1 && _agent.indexOf("Chrome") === -1 && (i === -1 || Number(_agent.substr(i + 8, 1)) > 3));
				_isSafariLT6 = (_isSafari && (Number(_agent.substr(_agent.indexOf("Version/") + 8, 1)) < 6));
				_isFirefox = (_agent.indexOf("Firefox") !== -1);
				if ((/MSIE ([0-9]{1,}[\.0-9]{0,})/).exec(_agent) || (/Trident\/.*rv:([0-9]{1,}[\.0-9]{0,})/).exec(_agent)) {
					_ieVers = parseFloat(RegExp.$1);
				}
				if (!a) {
					return false;
				}
				a.style.cssText = "top:1px;opacity:.55;";
				return /^0.55/.test(a.style.opacity);
			}())
			, _getIEOpacity = function (v) {
				return (_opacityExp.test(((typeof (v) === "string") ? v : (v.currentStyle ? v.currentStyle.filter : v.style.filter) || "")) ? (parseFloat(RegExp.$1) / 100) : 1);
			}
			, _log = function (s) { //for logging messages, but in a way that won't throw errors in old versions of IE.
				if (window.console) {
					console.log(s);
				}
			},

			_prefixCSS = "", //the non-camelCase vendor prefix like "-o-", "-moz-", "-ms-", or "-webkit-"
			_prefix = "", //camelCase vendor prefix like "O", "ms", "Webkit", or "Moz".

		// @private feed in a camelCase property name like "transform" and it will check to see if it is valid as-is or if it needs a vendor prefix. It returns the corrected camelCase property name (i.e. "WebkitTransform" or "MozTransform" or "transform" or null if no such property is found, like if the browser is IE8 or before, "transform" won't be found at all)
			_checkPropPrefix = function (p, e) {
				e = e || _tempDiv;
				var s = e.style
					, a, i;
				if (s[p] !== undefined) {
					return p;
				}
				p = p.charAt(0).toUpperCase() + p.substr(1);
				a = ["O", "Moz", "ms", "Ms", "Webkit"];
				i = 5;
				while (--i > -1 && s[a[i] + p] === undefined) {}
				if (i >= 0) {
					_prefix = (i === 3) ? "ms" : a[i];
					_prefixCSS = "-" + _prefix.toLowerCase() + "-";
					return _prefix + p;
				}
				return null;
			},

			_getComputedStyle = _doc.defaultView ? _doc.defaultView.getComputedStyle : function () {},

			/**
			 * @private Returns the css style for a particular property of an element. For example, to get whatever the current "left" css value for an element with an ID of "myElement", you could do:
			 * var currentLeft = CSSPlugin.getStyle( document.getElementById("myElement"), "left");
			 *
			 * @param {!Object} t Target element whose style property you want to query
			 * @param {!string} p Property name (like "left" or "top" or "marginTop", etc.)
			 * @param {Object=} cs Computed style object. This just provides a way to speed processing if you're going to get several properties on the same element in quick succession - you can reuse the result of the getComputedStyle() call.
			 * @param {boolean=} calc If true, the value will not be read directly from the element's "style" property (if it exists there), but instead the getComputedStyle() result will be used. This can be useful when you want to ensure that the browser itself is interpreting the value.
			 * @param {string=} dflt Default value that should be returned in the place of null, "none", "auto" or "auto auto".
			 * @return {?string} The current property value
			 */
			_getStyle = CSSPlugin.getStyle = function (t, p, cs, calc, dflt) {
				var rv;
				if (!_supportsOpacity)
					if (p === "opacity") { //several versions of IE don't use the standard "opacity" property - they use things like filter:alpha(opacity=50), so we parse that here.
						return _getIEOpacity(t);
					}
				if (!calc && t.style[p]) {
					rv = t.style[p];
				} else if ((cs = cs || _getComputedStyle(t))) {
					rv = cs[p] || cs.getPropertyValue(p) || cs.getPropertyValue(p.replace(_capsExp, "-$1").toLowerCase());
				} else if (t.currentStyle) {
					rv = t.currentStyle[p];
				}
				return (dflt != null && (!rv || rv === "none" || rv === "auto" || rv === "auto auto")) ? dflt : rv;
			},

			/**
			 * @private Pass the target element, the property name, the numeric value, and the suffix (like "%", "em", "px", etc.) and it will spit back the equivalent pixel number.
			 * @param {!Object} t Target element
			 * @param {!string} p Property name (like "left", "top", "marginLeft", etc.)
			 * @param {!number} v Value
			 * @param {string=} sfx Suffix (like "px" or "%" or "em")
			 * @param {boolean=} recurse If true, the call is a recursive one. In some browsers (like IE7/8), occasionally the value isn't accurately reported initially, but if we run the function again it will take effect.
			 * @return {number} value in pixels
			 */
			_convertToPixels = _internals.convertToPixels = function (t, p, v, sfx, recurse) {
				if (sfx === "px" || !sfx) {
					return v;
				}
				if (sfx === "auto" || !v) {
					return 0;
				}
				var horiz = _horizExp.test(p)
					, node = t
					, style = _tempDiv.style
					, neg = (v < 0)
					, pix, cache, time;
				if (neg) {
					v = -v;
				}
				if (sfx === "%" && p.indexOf("border") !== -1) {
					pix = (v / 100) * (horiz ? t.clientWidth : t.clientHeight);
				} else {
					style.cssText = "border:0 solid red;position:" + _getStyle(t, "position") + ";line-height:0;";
					if (sfx === "%" || !node.appendChild || sfx.charAt(0) === "v" || sfx === "rem") {
						node = t.parentNode || _doc.body;
						cache = node._gsCache;
						time = TweenLite.ticker.frame;
						if (cache && horiz && cache.time === time) { //performance optimization: we record the width of elements along with the ticker frame so that we can quickly get it again on the same tick (seems relatively safe to assume it wouldn't change on the same tick)
							return cache.width * v / 100;
						}
						style[(horiz ? "width" : "height")] = v + sfx;
					} else {
						style[(horiz ? "borderLeftWidth" : "borderTopWidth")] = v + sfx;
					}
					node.appendChild(_tempDiv);
					pix = parseFloat(_tempDiv[(horiz ? "offsetWidth" : "offsetHeight")]);
					node.removeChild(_tempDiv);
					if (horiz && sfx === "%" && CSSPlugin.cacheWidths !== false) {
						cache = node._gsCache = node._gsCache || {};
						cache.time = time;
						cache.width = pix / v * 100;
					}
					if (pix === 0 && !recurse) {
						pix = _convertToPixels(t, p, v, sfx, true);
					}
				}
				return neg ? -pix : pix;
			}
			, _calculateOffset = _internals.calculateOffset = function (t, p, cs) { //for figuring out "top" or "left" in px when it's "auto". We need to factor in margin with the offsetLeft/offsetTop
				if (_getStyle(t, "position", cs) !== "absolute") {
					return 0;
				}
				var dim = ((p === "left") ? "Left" : "Top")
					, v = _getStyle(t, "margin" + dim, cs);
				return t["offset" + dim] - (_convertToPixels(t, p, parseFloat(v), v.replace(_suffixExp, "")) || 0);
			},

		// @private returns at object containing ALL of the style properties in camelCase and their associated values.
			_getAllStyles = function (t, cs) {
				var s = {}
					, i, tr, p;
				if ((cs = cs || _getComputedStyle(t, null))) {
					if ((i = cs.length)) {
						while (--i > -1) {
							p = cs[i];
							if (p.indexOf("-transform") === -1 || _transformPropCSS === p) { //Some webkit browsers duplicate transform values, one non-prefixed and one prefixed ("transform" and "WebkitTransform"), so we must weed out the extra one here.
								s[p.replace(_camelExp, _camelFunc)] = cs.getPropertyValue(p);
							}
						}
					} else { //some browsers behave differently - cs.length is always 0, so we must do a for...in loop.
						for (i in cs) {
							if (i.indexOf("Transform") === -1 || _transformProp === i) { //Some webkit browsers duplicate transform values, one non-prefixed and one prefixed ("transform" and "WebkitTransform"), so we must weed out the extra one here.
								s[i] = cs[i];
							}
						}
					}
				} else if ((cs = t.currentStyle || t.style)) {
					for (i in cs) {
						if (typeof (i) === "string" && s[i] === undefined) {
							s[i.replace(_camelExp, _camelFunc)] = cs[i];
						}
					}
				}
				if (!_supportsOpacity) {
					s.opacity = _getIEOpacity(t);
				}
				tr = _getTransform(t, cs, false);
				s.rotation = tr.rotation;
				s.skewX = tr.skewX;
				s.scaleX = tr.scaleX;
				s.scaleY = tr.scaleY;
				s.x = tr.x;
				s.y = tr.y;
				if (_supports3D) {
					s.z = tr.z;
					s.rotationX = tr.rotationX;
					s.rotationY = tr.rotationY;
					s.scaleZ = tr.scaleZ;
				}
				if (s.filters) {
					delete s.filters;
				}
				return s;
			},

		// @private analyzes two style objects (as returned by _getAllStyles()) and only looks for differences between them that contain tweenable values (like a number or color). It returns an object with a "difs" property which refers to an object containing only those isolated properties and values for tweening, and a "firstMPT" property which refers to the first MiniPropTween instance in a linked list that recorded all the starting values of the different properties so that we can revert to them at the end or beginning of the tween - we don't want the cascading to get messed up. The forceLookup parameter is an optional generic object with properties that should be forced into the results - this is necessary for className tweens that are overwriting others because imagine a scenario where a rollover/rollout adds/removes a class and the user swipes the mouse over the target SUPER fast, thus nothing actually changed yet and the subsequent comparison of the properties would indicate they match (especially when px rounding is taken into consideration), thus no tweening is necessary even though it SHOULD tween and remove those properties after the tween (otherwise the inline styles will contaminate things). See the className SpecialProp code for details.
			_cssDif = function (t, s1, s2, vars, forceLookup) {
				var difs = {}
					, style = t.style
					, val, p, mpt;
				for (p in s2) {
					if (p !== "cssText")
						if (p !== "length")
							if (isNaN(p))
								if (s1[p] !== (val = s2[p]) || (forceLookup && forceLookup[p]))
									if (p.indexOf("Origin") === -1)
										if (typeof (val) === "number" || typeof (val) === "string") {
											difs[p] = (val === "auto" && (p === "left" || p === "top")) ? _calculateOffset(t, p) : ((val === "" || val === "auto" || val === "none") && typeof (s1[p]) === "string" && s1[p].replace(_NaNExp, "") !== "") ? 0 : val; //if the ending value is defaulting ("" or "auto"), we check the starting value and if it can be parsed into a number (a string which could have a suffix too, like 700px), then we swap in 0 for "" or "auto" so that things actually tween.
											if (style[p] !== undefined) { //for className tweens, we must remember which properties already existed inline - the ones that didn't should be removed when the tween isn't in progress because they were only introduced to facilitate the transition between classes.
												mpt = new MiniPropTween(style, p, style[p], mpt);
											}
										}
				}
				if (vars) {
					for (p in vars) { //copy properties (except className)
						if (p !== "className") {
							difs[p] = vars[p];
						}
					}
				}
				return {
					difs: difs
					, firstMPT: mpt
				};
			}
			, _dimensions = {
				width: ["Left", "Right"]
				, height: ["Top", "Bottom"]
			}
			, _margins = ["marginLeft", "marginRight", "marginTop", "marginBottom"],

			/**
			 * @private Gets the width or height of an element
			 * @param {!Object} t Target element
			 * @param {!string} p Property name ("width" or "height")
			 * @param {Object=} cs Computed style object (if one exists). Just a speed optimization.
			 * @return {number} Dimension (in pixels)
			 */
			_getDimension = function (t, p, cs) {
				if ((t.nodeName + "").toLowerCase() === "svg") { //Chrome no longer supports offsetWidth/offsetHeight on SVG elements.
					return (cs || _getComputedStyle(t))[p] || 0;
				} else if (t.getBBox && _isSVG(t)) {
					return t.getBBox()[p] || 0;
				}
				var v = parseFloat((p === "width") ? t.offsetWidth : t.offsetHeight)
					, a = _dimensions[p]
					, i = a.length;
				cs = cs || _getComputedStyle(t, null);
				while (--i > -1) {
					v -= parseFloat(_getStyle(t, "padding" + a[i], cs, true)) || 0;
					v -= parseFloat(_getStyle(t, "border" + a[i] + "Width", cs, true)) || 0;
				}
				return v;
			},

		// @private Parses position-related complex strings like "top left" or "50px 10px" or "70% 20%", etc. which are used for things like transformOrigin or backgroundPosition. Optionally decorates a supplied object (recObj) with the following properties: "ox" (offsetX), "oy" (offsetY), "oxp" (if true, "ox" is a percentage not a pixel value), and "oxy" (if true, "oy" is a percentage not a pixel value)
			_parsePosition = function (v, recObj) {
				if (v === "contain" || v === "auto" || v === "auto auto") {
					return v + " ";
				}
				if (v == null || v === "") { //note: Firefox uses "auto auto" as default whereas Chrome uses "auto".
					v = "0 0";
				}
				var a = v.split(" ")
					, x = (v.indexOf("left") !== -1) ? "0%" : (v.indexOf("right") !== -1) ? "100%" : a[0]
					, y = (v.indexOf("top") !== -1) ? "0%" : (v.indexOf("bottom") !== -1) ? "100%" : a[1]
					, i;
				if (a.length > 3 && !recObj) { //multiple positions
					a = v.split(", ").join(",").split(",");
					v = [];
					for (i = 0; i < a.length; i++) {
						v.push(_parsePosition(a[i]));
					}
					return v.join(",");
				}
				if (y == null) {
					y = (x === "center") ? "50%" : "0";
				} else if (y === "center") {
					y = "50%";
				}
				if (x === "center" || (isNaN(parseFloat(x)) && (x + "").indexOf("=") === -1)) { //remember, the user could flip-flop the values and say "bottom center" or "center bottom", etc. "center" is ambiguous because it could be used to describe horizontal or vertical, hence the isNaN(). If there's an "=" sign in the value, it's relative.
					x = "50%";
				}
				v = x + " " + y + ((a.length > 2) ? " " + a[2] : "");
				if (recObj) {
					recObj.oxp = (x.indexOf("%") !== -1);
					recObj.oyp = (y.indexOf("%") !== -1);
					recObj.oxr = (x.charAt(1) === "=");
					recObj.oyr = (y.charAt(1) === "=");
					recObj.ox = parseFloat(x.replace(_NaNExp, ""));
					recObj.oy = parseFloat(y.replace(_NaNExp, ""));
					recObj.v = v;
				}
				return recObj || v;
			},

			/**
			 * @private Takes an ending value (typically a string, but can be a number) and a starting value and returns the change between the two, looking for relative value indicators like += and -= and it also ignores suffixes (but make sure the ending value starts with a number or +=/-= and that the starting value is a NUMBER!)
			 * @param {(number|string)} e End value which is typically a string, but could be a number
			 * @param {(number|string)} b Beginning value which is typically a string but could be a number
			 * @return {number} Amount of change between the beginning and ending values (relative values that have a "+=" or "-=" are recognized)
			 */
			_parseChange = function (e, b) {
				return (typeof (e) === "string" && e.charAt(1) === "=") ? parseInt(e.charAt(0) + "1", 10) * parseFloat(e.substr(2)) : (parseFloat(e) - parseFloat(b)) || 0;
			},

			/**
			 * @private Takes a value and a default number, checks if the value is relative, null, or numeric and spits back a normalized number accordingly. Primarily used in the _parseTransform() function.
			 * @param {Object} v Value to be parsed
			 * @param {!number} d Default value (which is also used for relative calculations if "+=" or "-=" is found in the first parameter)
			 * @return {number} Parsed value
			 */
			_parseVal = function (v, d) {
				return (v == null) ? d : (typeof (v) === "string" && v.charAt(1) === "=") ? parseInt(v.charAt(0) + "1", 10) * parseFloat(v.substr(2)) + d : parseFloat(v) || 0;
			},

			/**
			 * @private Translates strings like "40deg" or "40" or 40rad" or "+=40deg" or "270_short" or "-90_cw" or "+=45_ccw" to a numeric radian angle. Of course a starting/default value must be fed in too so that relative values can be calculated properly.
			 * @param {Object} v Value to be parsed
			 * @param {!number} d Default value (which is also used for relative calculations if "+=" or "-=" is found in the first parameter)
			 * @param {string=} p property name for directionalEnd (optional - only used when the parsed value is directional ("_short", "_cw", or "_ccw" suffix). We need a way to store the uncompensated value so that at the end of the tween, we set it to exactly what was requested with no directional compensation). Property name would be "rotation", "rotationX", or "rotationY"
			 * @param {Object=} directionalEnd An object that will store the raw end values for directional angles ("_short", "_cw", or "_ccw" suffix). We need a way to store the uncompensated value so that at the end of the tween, we set it to exactly what was requested with no directional compensation.
			 * @return {number} parsed angle in radians
			 */
			_parseAngle = function (v, d, p, directionalEnd) {
				var min = 0.000001
					, cap, split, dif, result, isRelative;
				if (v == null) {
					result = d;
				} else if (typeof (v) === "number") {
					result = v;
				} else {
					cap = 360;
					split = v.split("_");
					isRelative = (v.charAt(1) === "=");
					dif = (isRelative ? parseInt(v.charAt(0) + "1", 10) * parseFloat(split[0].substr(2)) : parseFloat(split[0])) * ((v.indexOf("rad") === -1) ? 1 : _RAD2DEG) - (isRelative ? 0 : d);
					if (split.length) {
						if (directionalEnd) {
							directionalEnd[p] = d + dif;
						}
						if (v.indexOf("short") !== -1) {
							dif = dif % cap;
							if (dif !== dif % (cap / 2)) {
								dif = (dif < 0) ? dif + cap : dif - cap;
							}
						}
						if (v.indexOf("_cw") !== -1 && dif < 0) {
							dif = ((dif + cap * 9999999999) % cap) - ((dif / cap) | 0) * cap;
						} else if (v.indexOf("ccw") !== -1 && dif > 0) {
							dif = ((dif - cap * 9999999999) % cap) - ((dif / cap) | 0) * cap;
						}
					}
					result = d + dif;
				}
				if (result < min && result > -min) {
					result = 0;
				}
				return result;
			},

			_colorLookup = {
				aqua: [0, 255, 255]
				, lime: [0, 255, 0]
				, silver: [192, 192, 192]
				, black: [0, 0, 0]
				, maroon: [128, 0, 0]
				, teal: [0, 128, 128]
				, blue: [0, 0, 255]
				, navy: [0, 0, 128]
				, white: [255, 255, 255]
				, fuchsia: [255, 0, 255]
				, olive: [128, 128, 0]
				, yellow: [255, 255, 0]
				, orange: [255, 165, 0]
				, gray: [128, 128, 128]
				, purple: [128, 0, 128]
				, green: [0, 128, 0]
				, red: [255, 0, 0]
				, pink: [255, 192, 203]
				, cyan: [0, 255, 255]
				, transparent: [255, 255, 255, 0]
			},

			_hue = function (h, m1, m2) {
				h = (h < 0) ? h + 1 : (h > 1) ? h - 1 : h;
				return ((((h * 6 < 1) ? m1 + (m2 - m1) * h * 6 : (h < 0.5) ? m2 : (h * 3 < 2) ? m1 + (m2 - m1) * (2 / 3 - h) * 6 : m1) * 255) + 0.5) | 0;
			},

			/**
			 * @private Parses a color (like #9F0, #FF9900, rgb(255,51,153) or hsl(108, 50%, 10%)) into an array with 3 elements for red, green, and blue or if toHSL parameter is true, it will populate the array with hue, saturation, and lightness values. If a relative value is found in an hsl() or hsla() string, it will preserve those relative prefixes and all the values in the array will be strings instead of numbers (in all other cases it will be populated with numbers).
			 * @param {(string|number)} v The value the should be parsed which could be a string like #9F0 or rgb(255,102,51) or rgba(255,0,0,0.5) or it could be a number like 0xFF00CC or even a named color like red, blue, purple, etc.
			 * @param {(boolean)} toHSL If true, an hsl() or hsla() value will be returned instead of rgb() or rgba()
			 * @return {Array.<number>} An array containing red, green, and blue (and optionally alpha) in that order, or if the toHSL parameter was true, the array will contain hue, saturation and lightness (and optionally alpha) in that order. Always numbers unless there's a relative prefix found in an hsl() or hsla() string and toHSL is true.
			 */
			_parseColor = CSSPlugin.parseColor = function (v, toHSL) {
				var a, r, g, b, h, s, l, max, min, d, wasHSL;
				if (!v) {
					a = _colorLookup.black;
				} else if (typeof (v) === "number") {
					a = [v >> 16, (v >> 8) & 255, v & 255];
				} else {
					if (v.charAt(v.length - 1) === ",") { //sometimes a trailing comma is included and we should chop it off (typically from a comma-delimited list of values like a textShadow:"2px 2px 2px blue, 5px 5px 5px rgb(255,0,0)" - in this example "blue," has a trailing comma. We could strip it out inside parseComplex() but we'd need to do it to the beginning and ending values plus it wouldn't provide protection from other potential scenarios like if the user passes in a similar value.
						v = v.substr(0, v.length - 1);
					}
					if (_colorLookup[v]) {
						a = _colorLookup[v];
					} else if (v.charAt(0) === "#") {
						if (v.length === 4) { //for shorthand like #9F0
							r = v.charAt(1);
							g = v.charAt(2);
							b = v.charAt(3);
							v = "#" + r + r + g + g + b + b;
						}
						v = parseInt(v.substr(1), 16);
						a = [v >> 16, (v >> 8) & 255, v & 255];
					} else if (v.substr(0, 3) === "hsl") {
						a = wasHSL = v.match(_numExp);
						if (!toHSL) {
							h = (Number(a[0]) % 360) / 360;
							s = Number(a[1]) / 100;
							l = Number(a[2]) / 100;
							g = (l <= 0.5) ? l * (s + 1) : l + s - l * s;
							r = l * 2 - g;
							if (a.length > 3) {
								a[3] = Number(v[3]);
							}
							a[0] = _hue(h + 1 / 3, r, g);
							a[1] = _hue(h, r, g);
							a[2] = _hue(h - 1 / 3, r, g);
						} else if (v.indexOf("=") !== -1) { //if relative values are found, just return the raw strings with the relative prefixes in place.
							return v.match(_relNumExp);
						}
					} else {
						a = v.match(_numExp) || _colorLookup.transparent;
					}
					a[0] = Number(a[0]);
					a[1] = Number(a[1]);
					a[2] = Number(a[2]);
					if (a.length > 3) {
						a[3] = Number(a[3]);
					}
				}
				if (toHSL && !wasHSL) {
					r = a[0] / 255;
					g = a[1] / 255;
					b = a[2] / 255;
					max = Math.max(r, g, b);
					min = Math.min(r, g, b);
					l = (max + min) / 2;
					if (max === min) {
						h = s = 0;
					} else {
						d = max - min;
						s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
						h = (max === r) ? (g - b) / d + (g < b ? 6 : 0) : (max === g) ? (b - r) / d + 2 : (r - g) / d + 4;
						h *= 60;
					}
					a[0] = (h + 0.5) | 0;
					a[1] = (s * 100 + 0.5) | 0;
					a[2] = (l * 100 + 0.5) | 0;
				}
				return a;
			}
			, _formatColors = function (s, toHSL) {
				var colors = s.match(_colorExp) || []
					, charIndex = 0
					, parsed = colors.length ? "" : s
					, i, color, temp;
				for (i = 0; i < colors.length; i++) {
					color = colors[i];
					temp = s.substr(charIndex, s.indexOf(color, charIndex) - charIndex);
					charIndex += temp.length + color.length;
					color = _parseColor(color, toHSL);
					if (color.length === 3) {
						color.push(1);
					}
					parsed += temp + (toHSL ? "hsla(" + color[0] + "," + color[1] + "%," + color[2] + "%," + color[3] : "rgba(" + color.join(",")) + ")";
				}
				return parsed + s.substr(charIndex);
			}
			, _colorExp = "(?:\\b(?:(?:rgb|rgba|hsl|hsla)\\(.+?\\))|\\B#(?:[0-9a-f]{3}){1,2}\\b"; //we'll dynamically build this Regular Expression to conserve file size. After building it, it will be able to find rgb(), rgba(), # (hexadecimal), and named color values like red, blue, purple, etc.

		for (p in _colorLookup) {
			_colorExp += "|" + p + "\\b";
		}
		_colorExp = new RegExp(_colorExp + ")", "gi");

		CSSPlugin.colorStringFilter = function (a) {
			var combined = a[0] + a[1]
				, toHSL;
			if (_colorExp.test(combined)) {
				toHSL = (combined.indexOf("hsl(") !== -1 || combined.indexOf("hsla(") !== -1);
				a[0] = _formatColors(a[0], toHSL);
				a[1] = _formatColors(a[1], toHSL);
			}
			_colorExp.lastIndex = 0;
		};

		if (!TweenLite.defaultStringFilter) {
			TweenLite.defaultStringFilter = CSSPlugin.colorStringFilter;
		}

		/**
		 * @private Returns a formatter function that handles taking a string (or number in some cases) and returning a consistently formatted one in terms of delimiters, quantity of values, etc. For example, we may get boxShadow values defined as "0px red" or "0px 0px 10px rgb(255,0,0)" or "0px 0px 20px 20px #F00" and we need to ensure that what we get back is described with 4 numbers and a color. This allows us to feed it into the _parseComplex() method and split the values up appropriately. The neat thing about this _getFormatter() function is that the dflt defines a pattern as well as a default, so for example, _getFormatter("0px 0px 0px 0px #777", true) not only sets the default as 0px for all distances and #777 for the color, but also sets the pattern such that 4 numbers and a color will always get returned.
		 * @param {!string} dflt The default value and pattern to follow. So "0px 0px 0px 0px #777" will ensure that 4 numbers and a color will always get returned.
		 * @param {boolean=} clr If true, the values should be searched for color-related data. For example, boxShadow values typically contain a color whereas borderRadius don't.
		 * @param {boolean=} collapsible If true, the value is a top/left/right/bottom style one that acts like margin or padding, where if only one value is received, it's used for all 4; if 2 are received, the first is duplicated for 3rd (bottom) and the 2nd is duplicated for the 4th spot (left), etc.
		 * @return {Function} formatter function
		 */
		var _getFormatter = function (dflt, clr, collapsible, multi) {
				if (dflt == null) {
					return function (v) {
						return v;
					};
				}
				var dColor = clr ? (dflt.match(_colorExp) || [""])[0] : ""
					, dVals = dflt.split(dColor).join("").match(_valuesExp) || []
					, pfx = dflt.substr(0, dflt.indexOf(dVals[0]))
					, sfx = (dflt.charAt(dflt.length - 1) === ")") ? ")" : ""
					, delim = (dflt.indexOf(" ") !== -1) ? " " : ","
					, numVals = dVals.length
					, dSfx = (numVals > 0) ? dVals[0].replace(_numExp, "") : ""
					, formatter;
				if (!numVals) {
					return function (v) {
						return v;
					};
				}
				if (clr) {
					formatter = function (v) {
						var color, vals, i, a;
						if (typeof (v) === "number") {
							v += dSfx;
						} else if (multi && _commasOutsideParenExp.test(v)) {
							a = v.replace(_commasOutsideParenExp, "|").split("|");
							for (i = 0; i < a.length; i++) {
								a[i] = formatter(a[i]);
							}
							return a.join(",");
						}
						color = (v.match(_colorExp) || [dColor])[0];
						vals = v.split(color).join("").match(_valuesExp) || [];
						i = vals.length;
						if (numVals > i--) {
							while (++i < numVals) {
								vals[i] = collapsible ? vals[(((i - 1) / 2) | 0)] : dVals[i];
							}
						}
						return pfx + vals.join(delim) + delim + color + sfx + (v.indexOf("inset") !== -1 ? " inset" : "");
					};
					return formatter;

				}
				formatter = function (v) {
					var vals, a, i;
					if (typeof (v) === "number") {
						v += dSfx;
					} else if (multi && _commasOutsideParenExp.test(v)) {
						a = v.replace(_commasOutsideParenExp, "|").split("|");
						for (i = 0; i < a.length; i++) {
							a[i] = formatter(a[i]);
						}
						return a.join(",");
					}
					vals = v.match(_valuesExp) || [];
					i = vals.length;
					if (numVals > i--) {
						while (++i < numVals) {
							vals[i] = collapsible ? vals[(((i - 1) / 2) | 0)] : dVals[i];
						}
					}
					return pfx + vals.join(delim) + sfx;
				};
				return formatter;
			},

			/**
			 * @private returns a formatter function that's used for edge-related values like marginTop, marginLeft, paddingBottom, paddingRight, etc. Just pass a comma-delimited list of property names related to the edges.
			 * @param {!string} props a comma-delimited list of property names in order from top to left, like "marginTop,marginRight,marginBottom,marginLeft"
			 * @return {Function} a formatter function
			 */
			_getEdgeParser = function (props) {
				props = props.split(",");
				return function (t, e, p, cssp, pt, plugin, vars) {
					var a = (e + "").split(" ")
						, i;
					vars = {};
					for (i = 0; i < 4; i++) {
						vars[props[i]] = a[i] = a[i] || a[(((i - 1) / 2) >> 0)];
					}
					return cssp.parse(t, vars, pt, plugin);
				};
			},

		// @private used when other plugins must tween values first, like BezierPlugin or ThrowPropsPlugin, etc. That plugin's setRatio() gets called first so that the values are updated, and then we loop through the MiniPropTweens  which handle copying the values into their appropriate slots so that they can then be applied correctly in the main CSSPlugin setRatio() method. Remember, we typically create a proxy object that has a bunch of uniquely-named properties that we feed to the sub-plugin and it does its magic normally, and then we must interpret those values and apply them to the css because often numbers must get combined/concatenated, suffixes added, etc. to work with css, like boxShadow could have 4 values plus a color.
			_setPluginRatio = _internals._setPluginRatio = function (v) {
				this.plugin.setRatio(v);
				var d = this.data
					, proxy = d.proxy
					, mpt = d.firstMPT
					, min = 0.000001
					, val, pt, i, str, p;
				while (mpt) {
					val = proxy[mpt.v];
					if (mpt.r) {
						val = Math.round(val);
					} else if (val < min && val > -min) {
						val = 0;
					}
					mpt.t[mpt.p] = val;
					mpt = mpt._next;
				}
				if (d.autoRotate) {
					d.autoRotate.rotation = proxy.rotation;
				}
				//at the end, we must set the CSSPropTween's "e" (end) value dynamically here because that's what is used in the final setRatio() method. Same for "b" at the beginning.
				if (v === 1 || v === 0) {
					mpt = d.firstMPT;
					p = (v === 1) ? "e" : "b";
					while (mpt) {
						pt = mpt.t;
						if (!pt.type) {
							pt[p] = pt.s + pt.xs0;
						} else if (pt.type === 1) {
							str = pt.xs0 + pt.s + pt.xs1;
							for (i = 1; i < pt.l; i++) {
								str += pt["xn" + i] + pt["xs" + (i + 1)];
							}
							pt[p] = str;
						}
						mpt = mpt._next;
					}
				}
			},

			/**
			 * @private @constructor Used by a few SpecialProps to hold important values for proxies. For example, _parseToProxy() creates a MiniPropTween instance for each property that must get tweened on the proxy, and we record the original property name as well as the unique one we create for the proxy, plus whether or not the value needs to be rounded plus the original value.
			 * @param {!Object} t target object whose property we're tweening (often a CSSPropTween)
			 * @param {!string} p property name
			 * @param {(number|string|object)} v value
			 * @param {MiniPropTween=} next next MiniPropTween in the linked list
			 * @param {boolean=} r if true, the tweened value should be rounded to the nearest integer
			 */
			MiniPropTween = function (t, p, v, next, r) {
				this.t = t;
				this.p = p;
				this.v = v;
				this.r = r;
				if (next) {
					next._prev = this;
					this._next = next;
				}
			},

			/**
			 * @private Most other plugins (like BezierPlugin and ThrowPropsPlugin and others) can only tween numeric values, but CSSPlugin must accommodate special values that have a bunch of extra data (like a suffix or strings between numeric values, etc.). For example, boxShadow has values like "10px 10px 20px 30px rgb(255,0,0)" which would utterly confuse other plugins. This method allows us to split that data apart and grab only the numeric data and attach it to uniquely-named properties of a generic proxy object ({}) so that we can feed that to virtually any plugin to have the numbers tweened. However, we must also keep track of which properties from the proxy go with which CSSPropTween values and instances. So we create a linked list of MiniPropTweens. Each one records a target (the original CSSPropTween), property (like "s" or "xn1" or "xn2") that we're tweening and the unique property name that was used for the proxy (like "boxShadow_xn1" and "boxShadow_xn2") and whether or not they need to be rounded. That way, in the _setPluginRatio() method we can simply copy the values over from the proxy to the CSSPropTween instance(s). Then, when the main CSSPlugin setRatio() method runs and applies the CSSPropTween values accordingly, they're updated nicely. So the external plugin tweens the numbers, _setPluginRatio() copies them over, and setRatio() acts normally, applying css-specific values to the element.
			 * This method returns an object that has the following properties:
			 *  - proxy: a generic object containing the starting values for all the properties that will be tweened by the external plugin.  This is what we feed to the external _onInitTween() as the target
			 *  - end: a generic object containing the ending values for all the properties that will be tweened by the external plugin. This is what we feed to the external plugin's _onInitTween() as the destination values
			 *  - firstMPT: the first MiniPropTween in the linked list
			 *  - pt: the first CSSPropTween in the linked list that was created when parsing. If shallow is true, this linked list will NOT attach to the one passed into the _parseToProxy() as the "pt" (4th) parameter.
			 * @param {!Object} t target object to be tweened
			 * @param {!(Object|string)} vars the object containing the information about the tweening values (typically the end/destination values) that should be parsed
			 * @param {!CSSPlugin} cssp The CSSPlugin instance
			 * @param {CSSPropTween=} pt the next CSSPropTween in the linked list
			 * @param {TweenPlugin=} plugin the external TweenPlugin instance that will be handling tweening the numeric values
			 * @param {boolean=} shallow if true, the resulting linked list from the parse will NOT be attached to the CSSPropTween that was passed in as the "pt" (4th) parameter.
			 * @return An object containing the following properties: proxy, end, firstMPT, and pt (see above for descriptions)
			 */
			_parseToProxy = _internals._parseToProxy = function (t, vars, cssp, pt, plugin, shallow) {
				var bpt = pt
					, start = {}
					, end = {}
					, transform = cssp._transform
					, oldForce = _forcePT
					, i, p, xp, mpt, firstPT;
				cssp._transform = null;
				_forcePT = vars;
				pt = firstPT = cssp.parse(t, vars, pt, plugin);
				_forcePT = oldForce;
				//break off from the linked list so the new ones are isolated.
				if (shallow) {
					cssp._transform = transform;
					if (bpt) {
						bpt._prev = null;
						if (bpt._prev) {
							bpt._prev._next = null;
						}
					}
				}
				while (pt && pt !== bpt) {
					if (pt.type <= 1) {
						p = pt.p;
						end[p] = pt.s + pt.c;
						start[p] = pt.s;
						if (!shallow) {
							mpt = new MiniPropTween(pt, "s", p, mpt, pt.r);
							pt.c = 0;
						}
						if (pt.type === 1) {
							i = pt.l;
							while (--i > 0) {
								xp = "xn" + i;
								p = pt.p + "_" + xp;
								end[p] = pt.data[xp];
								start[p] = pt[xp];
								if (!shallow) {
									mpt = new MiniPropTween(pt, xp, p, mpt, pt.rxp[xp]);
								}
							}
						}
					}
					pt = pt._next;
				}
				return {
					proxy: start
					, end: end
					, firstMPT: mpt
					, pt: firstPT
				};
			},



			/**
			 * @constructor Each property that is tweened has at least one CSSPropTween associated with it. These instances store important information like the target, property, starting value, amount of change, etc. They can also optionally have a number of "extra" strings and numeric values named xs1, xn1, xs2, xn2, xs3, xn3, etc. where "s" indicates string and "n" indicates number. These can be pieced together in a complex-value tween (type:1) that has alternating types of data like a string, number, string, number, etc. For example, boxShadow could be "5px 5px 8px rgb(102, 102, 51)". In that value, there are 6 numbers that may need to tween and then pieced back together into a string again with spaces, suffixes, etc. xs0 is special in that it stores the suffix for standard (type:0) tweens, -OR- the first string (prefix) in a complex-value (type:1) CSSPropTween -OR- it can be the non-tweening value in a type:-1 CSSPropTween. We do this to conserve memory.
			 * CSSPropTweens have the following optional properties as well (not defined through the constructor):
			 *  - l: Length in terms of the number of extra properties that the CSSPropTween has (default: 0). For example, for a boxShadow we may need to tween 5 numbers in which case l would be 5; Keep in mind that the start/end values for the first number that's tweened are always stored in the s and c properties to conserve memory. All additional values thereafter are stored in xn1, xn2, etc.
			 *  - xfirst: The first instance of any sub-CSSPropTweens that are tweening properties of this instance. For example, we may split up a boxShadow tween so that there's a main CSSPropTween of type:1 that has various xs* and xn* values associated with the h-shadow, v-shadow, blur, color, etc. Then we spawn a CSSPropTween for each of those that has a higher priority and runs BEFORE the main CSSPropTween so that the values are all set by the time it needs to re-assemble them. The xfirst gives us an easy way to identify the first one in that chain which typically ends at the main one (because they're all prepende to the linked list)
			 *  - plugin: The TweenPlugin instance that will handle the tweening of any complex values. For example, sometimes we don't want to use normal subtweens (like xfirst refers to) to tween the values - we might want ThrowPropsPlugin or BezierPlugin some other plugin to do the actual tweening, so we create a plugin instance and store a reference here. We need this reference so that if we get a request to round values or disable a tween, we can pass along that request.
			 *  - data: Arbitrary data that needs to be stored with the CSSPropTween. Typically if we're going to have a plugin handle the tweening of a complex-value tween, we create a generic object that stores the END values that we're tweening to and the CSSPropTween's xs1, xs2, etc. have the starting values. We store that object as data. That way, we can simply pass that object to the plugin and use the CSSPropTween as the target.
			 *  - setRatio: Only used for type:2 tweens that require custom functionality. In this case, we call the CSSPropTween's setRatio() method and pass the ratio each time the tween updates. This isn't quite as efficient as doing things directly in the CSSPlugin's setRatio() method, but it's very convenient and flexible.
			 * @param {!Object} t Target object whose property will be tweened. Often a DOM element, but not always. It could be anything.
			 * @param {string} p Property to tween (name). For example, to tween element.width, p would be "width".
			 * @param {number} s Starting numeric value
			 * @param {number} c Change in numeric value over the course of the entire tween. For example, if element.width starts at 5 and should end at 100, c would be 95.
			 * @param {CSSPropTween=} next The next CSSPropTween in the linked list. If one is defined, we will define its _prev as the new instance, and the new instance's _next will be pointed at it.
			 * @param {number=} type The type of CSSPropTween where -1 = a non-tweening value, 0 = a standard simple tween, 1 = a complex value (like one that has multiple numbers in a comma- or space-delimited string like border:"1px solid red"), and 2 = one that uses a custom setRatio function that does all of the work of applying the values on each update.
			 * @param {string=} n Name of the property that should be used for overwriting purposes which is typically the same as p but not always. For example, we may need to create a subtween for the 2nd part of a "clip:rect(...)" tween in which case "p" might be xs1 but "n" is still "clip"
			 * @param {boolean=} r If true, the value(s) should be rounded
			 * @param {number=} pr Priority in the linked list order. Higher priority CSSPropTweens will be updated before lower priority ones. The default priority is 0.
			 * @param {string=} b Beginning value. We store this to ensure that it is EXACTLY what it was when the tween began without any risk of interpretation issues.
			 * @param {string=} e Ending value. We store this to ensure that it is EXACTLY what the user defined at the end of the tween without any risk of interpretation issues.
			 */
			CSSPropTween = _internals.CSSPropTween = function (t, p, s, c, next, type, n, r, pr, b, e) {
				this.t = t; //target
				this.p = p; //property
				this.s = s; //starting value
				this.c = c; //change value
				this.n = n || p; //name that this CSSPropTween should be associated to (usually the same as p, but not always - n is what overwriting looks at)
				if (!(t instanceof CSSPropTween)) {
					_overwriteProps.push(this.n);
				}
				this.r = r; //round (boolean)
				this.type = type || 0; //0 = normal tween, -1 = non-tweening (in which case xs0 will be applied to the target's property, like tp.t[tp.p] = tp.xs0), 1 = complex-value SpecialProp, 2 = custom setRatio() that does all the work
				if (pr) {
					this.pr = pr;
					_hasPriority = true;
				}
				this.b = (b === undefined) ? s : b;
				this.e = (e === undefined) ? s + c : e;
				if (next) {
					this._next = next;
					next._prev = this;
				}
			},

			_addNonTweeningNumericPT = function (target, prop, start, end, next, overwriteProp) { //cleans up some code redundancies and helps minification. Just a fast way to add a NUMERIC non-tweening CSSPropTween
				var pt = new CSSPropTween(target, prop, start, end - start, next, -1, overwriteProp);
				pt.b = start;
				pt.e = pt.xs0 = end;
				return pt;
			},

			/**
			 * Takes a target, the beginning value and ending value (as strings) and parses them into a CSSPropTween (possibly with child CSSPropTweens) that accommodates multiple numbers, colors, comma-delimited values, etc. For example:
			 * sp.parseComplex(element, "boxShadow", "5px 10px 20px rgb(255,102,51)", "0px 0px 0px red", true, "0px 0px 0px rgb(0,0,0,0)", pt);
			 * It will walk through the beginning and ending values (which should be in the same format with the same number and type of values) and figure out which parts are numbers, what strings separate the numeric/tweenable values, and then create the CSSPropTweens accordingly. If a plugin is defined, no child CSSPropTweens will be created. Instead, the ending values will be stored in the "data" property of the returned CSSPropTween like: {s:-5, xn1:-10, xn2:-20, xn3:255, xn4:0, xn5:0} so that it can be fed to any other plugin and it'll be plain numeric tweens but the recomposition of the complex value will be handled inside CSSPlugin's setRatio().
			 * If a setRatio is defined, the type of the CSSPropTween will be set to 2 and recomposition of the values will be the responsibility of that method.
			 *
			 * @param {!Object} t Target whose property will be tweened
			 * @param {!string} p Property that will be tweened (its name, like "left" or "backgroundColor" or "boxShadow")
			 * @param {string} b Beginning value
			 * @param {string} e Ending value
			 * @param {boolean} clrs If true, the value could contain a color value like "rgb(255,0,0)" or "#F00" or "red". The default is false, so no colors will be recognized (a performance optimization)
			 * @param {(string|number|Object)} dflt The default beginning value that should be used if no valid beginning value is defined or if the number of values inside the complex beginning and ending values don't match
			 * @param {?CSSPropTween} pt CSSPropTween instance that is the current head of the linked list (we'll prepend to this).
			 * @param {number=} pr Priority in the linked list order. Higher priority properties will be updated before lower priority ones. The default priority is 0.
			 * @param {TweenPlugin=} plugin If a plugin should handle the tweening of extra properties, pass the plugin instance here. If one is defined, then NO subtweens will be created for any extra properties (the properties will be created - just not additional CSSPropTween instances to tween them) because the plugin is expected to do so. However, the end values WILL be populated in the "data" property, like {s:100, xn1:50, xn2:300}
			 * @param {function(number)=} setRatio If values should be set in a custom function instead of being pieced together in a type:1 (complex-value) CSSPropTween, define that custom function here.
			 * @return {CSSPropTween} The first CSSPropTween in the linked list which includes the new one(s) added by the parseComplex() call.
			 */
			_parseComplex = CSSPlugin.parseComplex = function (t, p, b, e, clrs, dflt, pt, pr, plugin, setRatio) {
				//DEBUG: _log("parseComplex: "+p+", b: "+b+", e: "+e);
				b = b || dflt || "";
				pt = new CSSPropTween(t, p, 0, 0, pt, (setRatio ? 2 : 1), null, false, pr, b, e);
				e += ""; //ensures it's a string
				if (clrs && _colorExp.test(e + b)) { //if colors are found, normalize the formatting to rgba() or hsla().
					e = [b, e];
					CSSPlugin.colorStringFilter(e);
					b = e[0];
					e = e[1];
				}
				var ba = b.split(", ").join(",").split(" "), //beginning array
					ea = e.split(", ").join(",").split(" "), //ending array
					l = ba.length
					, autoRound = (_autoRound !== false)
					, i, xi, ni, bv, ev, bnums, enums, bn, hasAlpha, temp, cv, str, useHSL;
				if (e.indexOf(",") !== -1 || b.indexOf(",") !== -1) {
					ba = ba.join(" ").replace(_commasOutsideParenExp, ", ").split(" ");
					ea = ea.join(" ").replace(_commasOutsideParenExp, ", ").split(" ");
					l = ba.length;
				}
				if (l !== ea.length) {
					//DEBUG: _log("mismatched formatting detected on " + p + " (" + b + " vs " + e + ")");
					ba = (dflt || "").split(" ");
					l = ba.length;
				}
				pt.plugin = plugin;
				pt.setRatio = setRatio;
				_colorExp.lastIndex = 0;
				for (i = 0; i < l; i++) {
					bv = ba[i];
					ev = ea[i];
					bn = parseFloat(bv);
					//if the value begins with a number (most common). It's fine if it has a suffix like px
					if (bn || bn === 0) {
						pt.appendXtra("", bn, _parseChange(ev, bn), ev.replace(_relNumExp, ""), (autoRound && ev.indexOf("px") !== -1), true);

						//if the value is a color
					} else if (clrs && _colorExp.test(bv)) {
						str = ev.indexOf(")") + 1;
						str = ")" + (str ? ev.substr(str) : ""); //if there's a comma or ) at the end, retain it.
						useHSL = (ev.indexOf("hsl") !== -1 && _supportsOpacity);
						bv = _parseColor(bv, useHSL);
						ev = _parseColor(ev, useHSL);
						hasAlpha = (bv.length + ev.length > 6);
						if (hasAlpha && !_supportsOpacity && ev[3] === 0) { //older versions of IE don't support rgba(), so if the destination alpha is 0, just use "transparent" for the end color
							pt["xs" + pt.l] += pt.l ? " transparent" : "transparent";
							pt.e = pt.e.split(ea[i]).join("transparent");
						} else {
							if (!_supportsOpacity) { //old versions of IE don't support rgba().
								hasAlpha = false;
							}
							if (useHSL) {
								pt.appendXtra((hasAlpha ? "hsla(" : "hsl("), bv[0], _parseChange(ev[0], bv[0]), ",", false, true)
									.appendXtra("", bv[1], _parseChange(ev[1], bv[1]), "%,", false)
									.appendXtra("", bv[2], _parseChange(ev[2], bv[2]), (hasAlpha ? "%," : "%" + str), false);
							} else {
								pt.appendXtra((hasAlpha ? "rgba(" : "rgb("), bv[0], ev[0] - bv[0], ",", true, true)
									.appendXtra("", bv[1], ev[1] - bv[1], ",", true)
									.appendXtra("", bv[2], ev[2] - bv[2], (hasAlpha ? "," : str), true);
							}

							if (hasAlpha) {
								bv = (bv.length < 4) ? 1 : bv[3];
								pt.appendXtra("", bv, ((ev.length < 4) ? 1 : ev[3]) - bv, str, false);
							}
						}
						_colorExp.lastIndex = 0; //otherwise the test() on the RegExp could move the lastIndex and taint future results.

					} else {
						bnums = bv.match(_numExp); //gets each group of numbers in the beginning value string and drops them into an array

						//if no number is found, treat it as a non-tweening value and just append the string to the current xs.
						if (!bnums) {
							pt["xs" + pt.l] += (pt.l || pt["xs" + pt.l]) ? " " + ev : ev;

							//loop through all the numbers that are found and construct the extra values on the pt.
						} else {
							enums = ev.match(_relNumExp); //get each group of numbers in the end value string and drop them into an array. We allow relative values too, like +=50 or -=.5
							if (!enums || enums.length !== bnums.length) {
								//DEBUG: _log("mismatched formatting detected on " + p + " (" + b + " vs " + e + ")");
								return pt;
							}
							ni = 0;
							for (xi = 0; xi < bnums.length; xi++) {
								cv = bnums[xi];
								temp = bv.indexOf(cv, ni);
								pt.appendXtra(bv.substr(ni, temp - ni), Number(cv), _parseChange(enums[xi], cv), "", (autoRound && bv.substr(temp + cv.length, 2) === "px"), (xi === 0));
								ni = temp + cv.length;
							}
							pt["xs" + pt.l] += bv.substr(ni);
						}
					}
				}
				//if there are relative values ("+=" or "-=" prefix), we need to adjust the ending value to eliminate the prefixes and combine the values properly.
				if (e.indexOf("=") !== -1)
					if (pt.data) {
						str = pt.xs0 + pt.data.s;
						for (i = 1; i < pt.l; i++) {
							str += pt["xs" + i] + pt.data["xn" + i];
						}
						pt.e = str + pt["xs" + i];
					}
				if (!pt.l) {
					pt.type = -1;
					pt.xs0 = pt.e;
				}
				return pt.xfirst || pt;
			}
			, i = 9;


		p = CSSPropTween.prototype;
		p.l = p.pr = 0; //length (number of extra properties like xn1, xn2, xn3, etc.
		while (--i > 0) {
			p["xn" + i] = 0;
			p["xs" + i] = "";
		}
		p.xs0 = "";
		p._next = p._prev = p.xfirst = p.data = p.plugin = p.setRatio = p.rxp = null;


		/**
		 * Appends and extra tweening value to a CSSPropTween and automatically manages any prefix and suffix strings. The first extra value is stored in the s and c of the main CSSPropTween instance, but thereafter any extras are stored in the xn1, xn2, xn3, etc. The prefixes and suffixes are stored in the xs0, xs1, xs2, etc. properties. For example, if I walk through a clip value like "rect(10px, 5px, 0px, 20px)", the values would be stored like this:
		 * xs0:"rect(", s:10, xs1:"px, ", xn1:5, xs2:"px, ", xn2:0, xs3:"px, ", xn3:20, xn4:"px)"
		 * And they'd all get joined together when the CSSPlugin renders (in the setRatio() method).
		 * @param {string=} pfx Prefix (if any)
		 * @param {!number} s Starting value
		 * @param {!number} c Change in numeric value over the course of the entire tween. For example, if the start is 5 and the end is 100, the change would be 95.
		 * @param {string=} sfx Suffix (if any)
		 * @param {boolean=} r Round (if true).
		 * @param {boolean=} pad If true, this extra value should be separated by the previous one by a space. If there is no previous extra and pad is true, it will automatically drop the space.
		 * @return {CSSPropTween} returns itself so that multiple methods can be chained together.
		 */
		p.appendXtra = function (pfx, s, c, sfx, r, pad) {
			var pt = this
				, l = pt.l;
			pt["xs" + l] += (pad && (l || pt["xs" + l])) ? " " + pfx : pfx || "";
			if (!c)
				if (l !== 0 && !pt.plugin) { //typically we'll combine non-changing values right into the xs to optimize performance, but we don't combine them when there's a plugin that will be tweening the values because it may depend on the values being split apart, like for a bezier, if a value doesn't change between the first and second iteration but then it does on the 3rd, we'll run into trouble because there's no xn slot for that value!
					pt["xs" + l] += s + (sfx || "");
					return pt;
				}
			pt.l++;
			pt.type = pt.setRatio ? 2 : 1;
			pt["xs" + pt.l] = sfx || "";
			if (l > 0) {
				pt.data["xn" + l] = s + c;
				pt.rxp["xn" + l] = r; //round extra property (we need to tap into this in the _parseToProxy() method)
				pt["xn" + l] = s;
				if (!pt.plugin) {
					pt.xfirst = new CSSPropTween(pt, "xn" + l, s, c, pt.xfirst || pt, 0, pt.n, r, pt.pr);
					pt.xfirst.xs0 = 0; //just to ensure that the property stays numeric which helps modern browsers speed up processing. Remember, in the setRatio() method, we do pt.t[pt.p] = val + pt.xs0 so if pt.xs0 is "" (the default), it'll cast the end value as a string. When a property is a number sometimes and a string sometimes, it prevents the compiler from locking in the data type, slowing things down slightly.
				}
				return pt;
			}
			pt.data = {
				s: s + c
			};
			pt.rxp = {};
			pt.s = s;
			pt.c = c;
			pt.r = r;
			return pt;
		};

		/**
		 * @constructor A SpecialProp is basically a css property that needs to be treated in a non-standard way, like if it may contain a complex value like boxShadow:"5px 10px 15px rgb(255, 102, 51)" or if it is associated with another plugin like ThrowPropsPlugin or BezierPlugin. Every SpecialProp is associated with a particular property name like "boxShadow" or "throwProps" or "bezier" and it will intercept those values in the vars object that's passed to the CSSPlugin and handle them accordingly.
		 * @param {!string} p Property name (like "boxShadow" or "throwProps")
		 * @param {Object=} options An object containing any of the following configuration options:
		 *                      - defaultValue: the default value
		 *                      - parser: A function that should be called when the associated property name is found in the vars. This function should return a CSSPropTween instance and it should ensure that it is properly inserted into the linked list. It will receive 4 paramters: 1) The target, 2) The value defined in the vars, 3) The CSSPlugin instance (whose _firstPT should be used for the linked list), and 4) A computed style object if one was calculated (this is a speed optimization that allows retrieval of starting values quicker)
		 *                      - formatter: a function that formats any value received for this special property (for example, boxShadow could take "5px 5px red" and format it to "5px 5px 0px 0px red" so that both the beginning and ending values have a common order and quantity of values.)
		 *                      - prefix: if true, we'll determine whether or not this property requires a vendor prefix (like Webkit or Moz or ms or O)
		 *                      - color: set this to true if the value for this SpecialProp may contain color-related values like rgb(), rgba(), etc.
		 *                      - priority: priority in the linked list order. Higher priority SpecialProps will be updated before lower priority ones. The default priority is 0.
		 *                      - multi: if true, the formatter should accommodate a comma-delimited list of values, like boxShadow could have multiple boxShadows listed out.
		 *                      - collapsible: if true, the formatter should treat the value like it's a top/right/bottom/left value that could be collapsed, like "5px" would apply to all, "5px, 10px" would use 5px for top/bottom and 10px for right/left, etc.
		 *                      - keyword: a special keyword that can [optionally] be found inside the value (like "inset" for boxShadow). This allows us to validate beginning/ending values to make sure they match (if the keyword is found in one, it'll be added to the other for consistency by default).
		 */
		var SpecialProp = function (p, options) {
				options = options || {};
				this.p = options.prefix ? _checkPropPrefix(p) || p : p;
				_specialProps[p] = _specialProps[this.p] = this;
				this.format = options.formatter || _getFormatter(options.defaultValue, options.color, options.collapsible, options.multi);
				if (options.parser) {
					this.parse = options.parser;
				}
				this.clrs = options.color;
				this.multi = options.multi;
				this.keyword = options.keyword;
				this.dflt = options.defaultValue;
				this.pr = options.priority || 0;
			},

		//shortcut for creating a new SpecialProp that can accept multiple properties as a comma-delimited list (helps minification). dflt can be an array for multiple values (we don't do a comma-delimited list because the default value may contain commas, like rect(0px,0px,0px,0px)). We attach this method to the SpecialProp class/object instead of using a private _createSpecialProp() method so that we can tap into it externally if necessary, like from another plugin.
			_registerComplexSpecialProp = _internals._registerComplexSpecialProp = function (p, options, defaults) {
				if (typeof (options) !== "object") {
					options = {
						parser: defaults
					}; //to make backwards compatible with older versions of BezierPlugin and ThrowPropsPlugin
				}
				var a = p.split(",")
					, d = options.defaultValue
					, i, temp;
				defaults = defaults || [d];
				for (i = 0; i < a.length; i++) {
					options.prefix = (i === 0 && options.prefix);
					options.defaultValue = defaults[i] || d;
					temp = new SpecialProp(a[i], options);
				}
			},

		//creates a placeholder special prop for a plugin so that the property gets caught the first time a tween of it is attempted, and at that time it makes the plugin register itself, thus taking over for all future tweens of that property. This allows us to not mandate that things load in a particular order and it also allows us to log() an error that informs the user when they attempt to tween an external plugin-related property without loading its .js file.
			_registerPluginProp = function (p) {
				if (!_specialProps[p]) {
					var pluginName = p.charAt(0).toUpperCase() + p.substr(1) + "Plugin";
					_registerComplexSpecialProp(p, {
						parser: function (t, e, p, cssp, pt, plugin, vars) {
							var pluginClass = _globals.com.greensock.plugins[pluginName];
							if (!pluginClass) {
								_log("Error: " + pluginName + " js file not loaded.");
								return pt;
							}
							pluginClass._cssRegister();
							return _specialProps[p].parse(t, e, p, cssp, pt, plugin, vars);
						}
					});
				}
			};


		p = SpecialProp.prototype;

		/**
		 * Alias for _parseComplex() that automatically plugs in certain values for this SpecialProp, like its property name, whether or not colors should be sensed, the default value, and priority. It also looks for any keyword that the SpecialProp defines (like "inset" for boxShadow) and ensures that the beginning and ending values have the same number of values for SpecialProps where multi is true (like boxShadow and textShadow can have a comma-delimited list)
		 * @param {!Object} t target element
		 * @param {(string|number|object)} b beginning value
		 * @param {(string|number|object)} e ending (destination) value
		 * @param {CSSPropTween=} pt next CSSPropTween in the linked list
		 * @param {TweenPlugin=} plugin If another plugin will be tweening the complex value, that TweenPlugin instance goes here.
		 * @param {function=} setRatio If a custom setRatio() method should be used to handle this complex value, that goes here.
		 * @return {CSSPropTween=} First CSSPropTween in the linked list
		 */
		p.parseComplex = function (t, b, e, pt, plugin, setRatio) {
			var kwd = this.keyword
				, i, ba, ea, l, bi, ei;
			//if this SpecialProp's value can contain a comma-delimited list of values (like boxShadow or textShadow), we must parse them in a special way, and look for a keyword (like "inset" for boxShadow) and ensure that the beginning and ending BOTH have it if the end defines it as such. We also must ensure that there are an equal number of values specified (we can't tween 1 boxShadow to 3 for example)
			if (this.multi)
				if (_commasOutsideParenExp.test(e) || _commasOutsideParenExp.test(b)) {
					ba = b.replace(_commasOutsideParenExp, "|").split("|");
					ea = e.replace(_commasOutsideParenExp, "|").split("|");
				} else if (kwd) {
					ba = [b];
					ea = [e];
				}
			if (ea) {
				l = (ea.length > ba.length) ? ea.length : ba.length;
				for (i = 0; i < l; i++) {
					b = ba[i] = ba[i] || this.dflt;
					e = ea[i] = ea[i] || this.dflt;
					if (kwd) {
						bi = b.indexOf(kwd);
						ei = e.indexOf(kwd);
						if (bi !== ei) {
							if (ei === -1) { //if the keyword isn't in the end value, remove it from the beginning one.
								ba[i] = ba[i].split(kwd).join("");
							} else if (bi === -1) { //if the keyword isn't in the beginning, add it.
								ba[i] += " " + kwd;
							}
						}
					}
				}
				b = ba.join(", ");
				e = ea.join(", ");
			}
			return _parseComplex(t, this.p, b, e, this.clrs, this.dflt, pt, this.pr, plugin, setRatio);
		};

		/**
		 * Accepts a target and end value and spits back a CSSPropTween that has been inserted into the CSSPlugin's linked list and conforms with all the conventions we use internally, like type:-1, 0, 1, or 2, setting up any extra property tweens, priority, etc. For example, if we have a boxShadow SpecialProp and call:
		 * this._firstPT = sp.parse(element, "5px 10px 20px rgb(2550,102,51)", "boxShadow", this);
		 * It should figure out the starting value of the element's boxShadow, compare it to the provided end value and create all the necessary CSSPropTweens of the appropriate types to tween the boxShadow. The CSSPropTween that gets spit back should already be inserted into the linked list (the 4th parameter is the current head, so prepend to that).
		 * @param {!Object} t Target object whose property is being tweened
		 * @param {Object} e End value as provided in the vars object (typically a string, but not always - like a throwProps would be an object).
		 * @param {!string} p Property name
		 * @param {!CSSPlugin} cssp The CSSPlugin instance that should be associated with this tween.
		 * @param {?CSSPropTween} pt The CSSPropTween that is the current head of the linked list (we'll prepend to it)
		 * @param {TweenPlugin=} plugin If a plugin will be used to tween the parsed value, this is the plugin instance.
		 * @param {Object=} vars Original vars object that contains the data for parsing.
		 * @return {CSSPropTween} The first CSSPropTween in the linked list which includes the new one(s) added by the parse() call.
		 */
		p.parse = function (t, e, p, cssp, pt, plugin, vars) {
			return this.parseComplex(t.style, this.format(_getStyle(t, this.p, _cs, false, this.dflt)), this.format(e), pt, plugin);
		};

		/**
		 * Registers a special property that should be intercepted from any "css" objects defined in tweens. This allows you to handle them however you want without CSSPlugin doing it for you. The 2nd parameter should be a function that accepts 3 parameters:
		 *  1) Target object whose property should be tweened (typically a DOM element)
		 *  2) The end/destination value (could be a string, number, object, or whatever you want)
		 *  3) The tween instance (you probably don't need to worry about this, but it can be useful for looking up information like the duration)
		 *
		 * Then, your function should return a function which will be called each time the tween gets rendered, passing a numeric "ratio" parameter to your function that indicates the change factor (usually between 0 and 1). For example:
		 *
		 * CSSPlugin.registerSpecialProp("myCustomProp", function(target, value, tween) {
		 *      var start = target.style.width;
		 *      return function(ratio) {
		 *              target.style.width = (start + value * ratio) + "px";
		 *              console.log("set width to " + target.style.width);
		 *          }
		 * }, 0);
		 *
		 * Then, when I do this tween, it will trigger my special property:
		 *
		 * TweenLite.to(element, 1, {css:{myCustomProp:100}});
		 *
		 * In the example, of course, we're just changing the width, but you can do anything you want.
		 *
		 * @param {!string} name Property name (or comma-delimited list of property names) that should be intercepted and handled by your function. For example, if I define "myCustomProp", then it would handle that portion of the following tween: TweenLite.to(element, 1, {css:{myCustomProp:100}})
		 * @param {!function(Object, Object, Object, string):function(number)} onInitTween The function that will be called when a tween of this special property is performed. The function will receive 4 parameters: 1) Target object that should be tweened, 2) Value that was passed to the tween, 3) The tween instance itself (rarely used), and 4) The property name that's being tweened. Your function should return a function that should be called on every update of the tween. That function will receive a single parameter that is a "change factor" value (typically between 0 and 1) indicating the amount of change as a ratio. You can use this to determine how to set the values appropriately in your function.
		 * @param {number=} priority Priority that helps the engine determine the order in which to set the properties (default: 0). Higher priority properties will be updated before lower priority ones.
		 */
		CSSPlugin.registerSpecialProp = function (name, onInitTween, priority) {
			_registerComplexSpecialProp(name, {
				parser: function (t, e, p, cssp, pt, plugin, vars) {
					var rv = new CSSPropTween(t, p, 0, 0, pt, 2, p, false, priority);
					rv.plugin = plugin;
					rv.setRatio = onInitTween(t, e, cssp._tween, p);
					return rv;
				}
				, priority: priority
			});
		};






		//transform-related methods and properties
		CSSPlugin.useSVGTransformAttr = _isSafari || _isFirefox; //Safari and Firefox both have some rendering bugs when applying CSS transforms to SVG elements, so default to using the "transform" attribute instead (users can override this).
		var _transformProps = ("scaleX,scaleY,scaleZ,x,y,z,skewX,skewY,rotation,rotationX,rotationY,perspective,xPercent,yPercent").split(",")
			, _transformProp = _checkPropPrefix("transform"), //the Javascript (camelCase) transform property, like msTransform, WebkitTransform, MozTransform, or OTransform.
			_transformPropCSS = _prefixCSS + "transform"
			, _transformOriginProp = _checkPropPrefix("transformOrigin")
			, _supports3D = (_checkPropPrefix("perspective") !== null)
			, Transform = _internals.Transform = function () {
				this.perspective = parseFloat(CSSPlugin.defaultTransformPerspective) || 0;
				this.force3D = (CSSPlugin.defaultForce3D === false || !_supports3D) ? false : CSSPlugin.defaultForce3D || "auto";
			}
			, _SVGElement = window.SVGElement
			, _useSVGTransformAttr
			, //Some browsers (like Firefox and IE) don't honor transform-origin properly in SVG elements, so we need to manually adjust the matrix accordingly. We feature detect here rather than always doing the conversion for certain browsers because they may fix the problem at some point in the future.

			_createSVG = function (type, container, attributes) {
				var element = _doc.createElementNS("http://www.w3.org/2000/svg", type)
					, reg = /([a-z])([A-Z])/g
					, p;
				for (p in attributes) {
					element.setAttributeNS(null, p.replace(reg, "$1-$2").toLowerCase(), attributes[p]);
				}
				container.appendChild(element);
				return element;
			}
			, _docElement = _doc.documentElement
			, _forceSVGTransformAttr = (function () {
				//IE and Android stock don't support CSS transforms on SVG elements, so we must write them to the "transform" attribute. We populate this variable in the _parseTransform() method, and only if/when we come across an SVG element
				var force = _ieVers || (/Android/i.test(_agent) && !window.chrome)
					, svg, rect, width;
				if (_doc.createElementNS && !force) { //IE8 and earlier doesn't support SVG anyway
					svg = _createSVG("svg", _docElement);
					rect = _createSVG("rect", svg, {
						width: 100
						, height: 50
						, x: 100
					});
					width = rect.getBoundingClientRect().width;
					rect.style[_transformOriginProp] = "50% 50%";
					rect.style[_transformProp] = "scaleX(0.5)";
					force = (width === rect.getBoundingClientRect().width && !(_isFirefox && _supports3D)); //note: Firefox fails the test even though it does support CSS transforms in 3D. Since we can't push 3D stuff into the transform attribute, we force Firefox to pass the test here (as long as it does truly support 3D).
					_docElement.removeChild(svg);
				}
				return force;
			})()
			, _parseSVGOrigin = function (e, local, decoratee, absolute, smoothOrigin, skipRecord) {
				var tm = e._gsTransform
					, m = _getMatrix(e, true)
					, v, x, y, xOrigin, yOrigin, a, b, c, d, tx, ty, determinant, xOriginOld, yOriginOld;
				if (tm) {
					xOriginOld = tm.xOrigin; //record the original values before we alter them.
					yOriginOld = tm.yOrigin;
				}
				if (!absolute || (v = absolute.split(" ")).length < 2) {
					b = e.getBBox();
					local = _parsePosition(local).split(" ");
					v = [(local[0].indexOf("%") !== -1 ? parseFloat(local[0]) / 100 * b.width : parseFloat(local[0])) + b.x
						, (local[1].indexOf("%") !== -1 ? parseFloat(local[1]) / 100 * b.height : parseFloat(local[1])) + b.y];
				}
				decoratee.xOrigin = xOrigin = parseFloat(v[0]);
				decoratee.yOrigin = yOrigin = parseFloat(v[1]);
				if (absolute && m !== _identity2DMatrix) { //if svgOrigin is being set, we must invert the matrix and determine where the absolute point is, factoring in the current transforms. Otherwise, the svgOrigin would be based on the element's non-transformed position on the canvas.
					a = m[0];
					b = m[1];
					c = m[2];
					d = m[3];
					tx = m[4];
					ty = m[5];
					determinant = (a * d - b * c);
					x = xOrigin * (d / determinant) + yOrigin * (-c / determinant) + ((c * ty - d * tx) / determinant);
					y = xOrigin * (-b / determinant) + yOrigin * (a / determinant) - ((a * ty - b * tx) / determinant);
					xOrigin = decoratee.xOrigin = v[0] = x;
					yOrigin = decoratee.yOrigin = v[1] = y;
				}
				if (tm) { //avoid jump when transformOrigin is changed - adjust the x/y values accordingly
					if (skipRecord) {
						decoratee.xOffset = tm.xOffset;
						decoratee.yOffset = tm.yOffset;
						tm = decoratee;
					}
					if (smoothOrigin || (smoothOrigin !== false && CSSPlugin.defaultSmoothOrigin !== false)) {
						x = xOrigin - xOriginOld;
						y = yOrigin - yOriginOld;
						//originally, we simply adjusted the x and y values, but that would cause problems if, for example, you created a rotational tween part-way through an x/y tween. Managing the offset in a separate variable gives us ultimate flexibility.
						//tm.x -= x - (x * m[0] + y * m[2]);
						//tm.y -= y - (x * m[1] + y * m[3]);
						tm.xOffset += (x * m[0] + y * m[2]) - x;
						tm.yOffset += (x * m[1] + y * m[3]) - y;
					} else {
						tm.xOffset = tm.yOffset = 0;
					}
				}
				if (!skipRecord) {
					e.setAttribute("data-svg-origin", v.join(" "));
				}
			}
			, _canGetBBox = function (e) {
				try {
					return e.getBBox(); //Firefox throws errors if you try calling getBBox() on an SVG element that's not rendered (like in a <symbol> or <defs>). https://bugzilla.mozilla.org/show_bug.cgi?id=612118
				} catch (e) {}
			}
			, _isSVG = function (e) { //reports if the element is an SVG on which getBBox() actually works
				return !!(_SVGElement && e.getBBox && e.getCTM && _canGetBBox(e) && (!e.parentNode || (e.parentNode.getBBox && e.parentNode.getCTM)));
			}
			, _identity2DMatrix = [1, 0, 0, 1, 0, 0]
			, _getMatrix = function (e, force2D) {
				var tm = e._gsTransform || new Transform()
					, rnd = 100000
					, isDefault, s, m, n, dec;
				if (_transformProp) {
					s = _getStyle(e, _transformPropCSS, null, true);
				} else if (e.currentStyle) {
					//for older versions of IE, we need to interpret the filter portion that is in the format: progid:DXImageTransform.Microsoft.Matrix(M11=6.123233995736766e-17, M12=-1, M21=1, M22=6.123233995736766e-17, sizingMethod='auto expand') Notice that we need to swap b and c compared to a normal matrix.
					s = e.currentStyle.filter.match(_ieGetMatrixExp);
					s = (s && s.length === 4) ? [s[0].substr(4), Number(s[2].substr(4)), Number(s[1].substr(4)), s[3].substr(4), (tm.x || 0), (tm.y || 0)].join(",") : "";
				}
				isDefault = (!s || s === "none" || s === "matrix(1, 0, 0, 1, 0, 0)");
				if (tm.svg || (e.getBBox && _isSVG(e))) {
					if (isDefault && (e.style[_transformProp] + "").indexOf("matrix") !== -1) { //some browsers (like Chrome 40) don't correctly report transforms that are applied inline on an SVG element (they don't get included in the computed style), so we double-check here and accept matrix values
						s = e.style[_transformProp];
						isDefault = 0;
					}
					m = e.getAttribute("transform");
					if (isDefault && m) {
						if (m.indexOf("matrix") !== -1) { //just in case there's a "transform" value specified as an attribute instead of CSS style. Accept either a matrix() or simple translate() value though.
							s = m;
							isDefault = 0;
						} else if (m.indexOf("translate") !== -1) {
							s = "matrix(1,0,0,1," + m.match(/(?:\-|\b)[\d\-\.e]+\b/gi).join(",") + ")";
							isDefault = 0;
						}
					}
				}
				if (isDefault) {
					return _identity2DMatrix;
				}
				//split the matrix values out into an array (m for matrix)
				m = (s || "").match(_numExp) || [];
				i = m.length;
				while (--i > -1) {
					n = Number(m[i]);
					m[i] = (dec = n - (n |= 0)) ? ((dec * rnd + (dec < 0 ? -0.5 : 0.5)) | 0) / rnd + n : n; //convert strings to Numbers and round to 5 decimal places to avoid issues with tiny numbers. Roughly 20x faster than Number.toFixed(). We also must make sure to round before dividing so that values like 0.9999999999 become 1 to avoid glitches in browser rendering and interpretation of flipped/rotated 3D matrices. And don't just multiply the number by rnd, floor it, and then divide by rnd because the bitwise operations max out at a 32-bit signed integer, thus it could get clipped at a relatively low value (like 22,000.00000 for example).
				}
				return (force2D && m.length > 6) ? [m[0], m[1], m[4], m[5], m[12], m[13]] : m;
			},

			/**
			 * Parses the transform values for an element, returning an object with x, y, z, scaleX, scaleY, scaleZ, rotation, rotationX, rotationY, skewX, and skewY properties. Note: by default (for performance reasons), all skewing is combined into skewX and rotation but skewY still has a place in the transform object so that we can record how much of the skew is attributed to skewX vs skewY. Remember, a skewY of 10 looks the same as a rotation of 10 and skewX of -10.
			 * @param {!Object} t target element
			 * @param {Object=} cs computed style object (optional)
			 * @param {boolean=} rec if true, the transform values will be recorded to the target element's _gsTransform object, like target._gsTransform = {x:0, y:0, z:0, scaleX:1...}
			 * @param {boolean=} parse if true, we'll ignore any _gsTransform values that already exist on the element, and force a reparsing of the css (calculated style)
			 * @return {object} object containing all of the transform properties/values like {x:0, y:0, z:0, scaleX:1...}
			 */
			_getTransform = _internals.getTransform = function (t, cs, rec, parse) {
				if (t._gsTransform && rec && !parse) {
					return t._gsTransform; //if the element already has a _gsTransform, use that. Note: some browsers don't accurately return the calculated style for the transform (particularly for SVG), so it's almost always safest to just use the values we've already applied rather than re-parsing things.
				}
				var tm = rec ? t._gsTransform || new Transform() : new Transform()
					, invX = (tm.scaleX < 0), //in order to interpret things properly, we need to know if the user applied a negative scaleX previously so that we can adjust the rotation and skewX accordingly. Otherwise, if we always interpret a flipped matrix as affecting scaleY and the user only wants to tween the scaleX on multiple sequential tweens, it would keep the negative scaleY without that being the user's intent.
					min = 0.00002
					, rnd = 100000
					, zOrigin = _supports3D ? parseFloat(_getStyle(t, _transformOriginProp, cs, false, "0 0 0").split(" ")[2]) || tm.zOrigin || 0 : 0
					, defaultTransformPerspective = parseFloat(CSSPlugin.defaultTransformPerspective) || 0
					, m, i, scaleX, scaleY, rotation, skewX;

				tm.svg = !!(t.getBBox && _isSVG(t));
				if (tm.svg) {
					_parseSVGOrigin(t, _getStyle(t, _transformOriginProp, _cs, false, "50% 50%") + "", tm, t.getAttribute("data-svg-origin"));
					_useSVGTransformAttr = CSSPlugin.useSVGTransformAttr || _forceSVGTransformAttr;
				}
				m = _getMatrix(t);
				if (m !== _identity2DMatrix) {

					if (m.length === 16) {
						//we'll only look at these position-related 6 variables first because if x/y/z all match, it's relatively safe to assume we don't need to re-parse everything which risks losing important rotational information (like rotationX:180 plus rotationY:180 would look the same as rotation:180 - there's no way to know for sure which direction was taken based solely on the matrix3d() values)
						var a11 = m[0]
							, a21 = m[1]
							, a31 = m[2]
							, a41 = m[3]
							, a12 = m[4]
							, a22 = m[5]
							, a32 = m[6]
							, a42 = m[7]
							, a13 = m[8]
							, a23 = m[9]
							, a33 = m[10]
							, a14 = m[12]
							, a24 = m[13]
							, a34 = m[14]
							, a43 = m[11]
							, angle = Math.atan2(a32, a33)
							, t1, t2, t3, t4, cos, sin;

						//we manually compensate for non-zero z component of transformOrigin to work around bugs in Safari
						if (tm.zOrigin) {
							a34 = -tm.zOrigin;
							a14 = a13 * a34 - m[12];
							a24 = a23 * a34 - m[13];
							a34 = a33 * a34 + tm.zOrigin - m[14];
						}
						tm.rotationX = angle * _RAD2DEG;
						//rotationX
						if (angle) {
							cos = Math.cos(-angle);
							sin = Math.sin(-angle);
							t1 = a12 * cos + a13 * sin;
							t2 = a22 * cos + a23 * sin;
							t3 = a32 * cos + a33 * sin;
							a13 = a12 * -sin + a13 * cos;
							a23 = a22 * -sin + a23 * cos;
							a33 = a32 * -sin + a33 * cos;
							a43 = a42 * -sin + a43 * cos;
							a12 = t1;
							a22 = t2;
							a32 = t3;
						}
						//rotationY
						angle = Math.atan2(-a31, a33);
						tm.rotationY = angle * _RAD2DEG;
						if (angle) {
							cos = Math.cos(-angle);
							sin = Math.sin(-angle);
							t1 = a11 * cos - a13 * sin;
							t2 = a21 * cos - a23 * sin;
							t3 = a31 * cos - a33 * sin;
							a23 = a21 * sin + a23 * cos;
							a33 = a31 * sin + a33 * cos;
							a43 = a41 * sin + a43 * cos;
							a11 = t1;
							a21 = t2;
							a31 = t3;
						}
						//rotationZ
						angle = Math.atan2(a21, a11);
						tm.rotation = angle * _RAD2DEG;
						if (angle) {
							cos = Math.cos(-angle);
							sin = Math.sin(-angle);
							a11 = a11 * cos + a12 * sin;
							t2 = a21 * cos + a22 * sin;
							a22 = a21 * -sin + a22 * cos;
							a32 = a31 * -sin + a32 * cos;
							a21 = t2;
						}

						if (tm.rotationX && Math.abs(tm.rotationX) + Math.abs(tm.rotation) > 359.9) { //when rotationY is set, it will often be parsed as 180 degrees different than it should be, and rotationX and rotation both being 180 (it looks the same), so we adjust for that here.
							tm.rotationX = tm.rotation = 0;
							tm.rotationY = 180 - tm.rotationY;
						}

						tm.scaleX = ((Math.sqrt(a11 * a11 + a21 * a21) * rnd + 0.5) | 0) / rnd;
						tm.scaleY = ((Math.sqrt(a22 * a22 + a23 * a23) * rnd + 0.5) | 0) / rnd;
						tm.scaleZ = ((Math.sqrt(a32 * a32 + a33 * a33) * rnd + 0.5) | 0) / rnd;
						tm.skewX = (a12 || a22) ? Math.atan2(a12, a22) * _RAD2DEG + tm.rotation : tm.skewX || 0;
						if (Math.abs(tm.skewX) > 90 && Math.abs(tm.skewX) < 270) {
							if (invX) {
								tm.scaleX *= -1;
								tm.skewX += (tm.rotation <= 0) ? 180 : -180;
								tm.rotation += (tm.rotation <= 0) ? 180 : -180;
							} else {
								tm.scaleY *= -1;
								tm.skewX += (tm.skewX <= 0) ? 180 : -180;
							}
						}
						tm.perspective = a43 ? 1 / ((a43 < 0) ? -a43 : a43) : 0;
						tm.x = a14;
						tm.y = a24;
						tm.z = a34;
						if (tm.svg) {
							tm.x -= tm.xOrigin - (tm.xOrigin * a11 - tm.yOrigin * a12);
							tm.y -= tm.yOrigin - (tm.yOrigin * a21 - tm.xOrigin * a22);
						}

					} else if ((!_supports3D || parse || !m.length || tm.x !== m[4] || tm.y !== m[5] || (!tm.rotationX && !tm.rotationY)) && !(tm.x !== undefined && _getStyle(t, "display", cs) === "none")) { //sometimes a 6-element matrix is returned even when we performed 3D transforms, like if rotationX and rotationY are 180. In cases like this, we still need to honor the 3D transforms. If we just rely on the 2D info, it could affect how the data is interpreted, like scaleY might get set to -1 or rotation could get offset by 180 degrees. For example, do a TweenLite.to(element, 1, {css:{rotationX:180, rotationY:180}}) and then later, TweenLite.to(element, 1, {css:{rotationX:0}}) and without this conditional logic in place, it'd jump to a state of being unrotated when the 2nd tween starts. Then again, we need to honor the fact that the user COULD alter the transforms outside of CSSPlugin, like by manually applying new css, so we try to sense that by looking at x and y because if those changed, we know the changes were made outside CSSPlugin and we force a reinterpretation of the matrix values. Also, in Webkit browsers, if the element's "display" is "none", its calculated style value will always return empty, so if we've already recorded the values in the _gsTransform object, we'll just rely on those.
						var k = (m.length >= 6)
							, a = k ? m[0] : 1
							, b = m[1] || 0
							, c = m[2] || 0
							, d = k ? m[3] : 1;
						tm.x = m[4] || 0;
						tm.y = m[5] || 0;
						scaleX = Math.sqrt(a * a + b * b);
						scaleY = Math.sqrt(d * d + c * c);
						rotation = (a || b) ? Math.atan2(b, a) * _RAD2DEG : tm.rotation || 0; //note: if scaleX is 0, we cannot accurately measure rotation. Same for skewX with a scaleY of 0. Therefore, we default to the previously recorded value (or zero if that doesn't exist).
						skewX = (c || d) ? Math.atan2(c, d) * _RAD2DEG + rotation : tm.skewX || 0;
						if (Math.abs(skewX) > 90 && Math.abs(skewX) < 270) {
							if (invX) {
								scaleX *= -1;
								skewX += (rotation <= 0) ? 180 : -180;
								rotation += (rotation <= 0) ? 180 : -180;
							} else {
								scaleY *= -1;
								skewX += (skewX <= 0) ? 180 : -180;
							}
						}
						tm.scaleX = scaleX;
						tm.scaleY = scaleY;
						tm.rotation = rotation;
						tm.skewX = skewX;
						if (_supports3D) {
							tm.rotationX = tm.rotationY = tm.z = 0;
							tm.perspective = defaultTransformPerspective;
							tm.scaleZ = 1;
						}
						if (tm.svg) {
							tm.x -= tm.xOrigin - (tm.xOrigin * a + tm.yOrigin * c);
							tm.y -= tm.yOrigin - (tm.xOrigin * b + tm.yOrigin * d);
						}
					}
					tm.zOrigin = zOrigin;
					//some browsers have a hard time with very small values like 2.4492935982947064e-16 (notice the "e-" towards the end) and would render the object slightly off. So we round to 0 in these cases. The conditional logic here is faster than calling Math.abs(). Also, browsers tend to render a SLIGHTLY rotated object in a fuzzy way, so we need to snap to exactly 0 when appropriate.
					for (i in tm) {
						if (tm[i] < min)
							if (tm[i] > -min) {
								tm[i] = 0;
							}
					}
				}
				//DEBUG: _log("parsed rotation of " + t.getAttribute("id")+": "+(tm.rotationX)+", "+(tm.rotationY)+", "+(tm.rotation)+", scale: "+tm.scaleX+", "+tm.scaleY+", "+tm.scaleZ+", position: "+tm.x+", "+tm.y+", "+tm.z+", perspective: "+tm.perspective+ ", origin: "+ tm.xOrigin+ ","+ tm.yOrigin);
				if (rec) {
					t._gsTransform = tm; //record to the object's _gsTransform which we use so that tweens can control individual properties independently (we need all the properties to accurately recompose the matrix in the setRatio() method)
					if (tm.svg) { //if we're supposed to apply transforms to the SVG element's "transform" attribute, make sure there aren't any CSS transforms applied or they'll override the attribute ones. Also clear the transform attribute if we're using CSS, just to be clean.
						if (_useSVGTransformAttr && t.style[_transformProp]) {
							TweenLite.delayedCall(0.001, function () { //if we apply this right away (before anything has rendered), we risk there being no transforms for a brief moment and it also interferes with adjusting the transformOrigin in a tween with immediateRender:true (it'd try reading the matrix and it wouldn't have the appropriate data in place because we just removed it).
								_removeProp(t.style, _transformProp);
							});
						} else if (!_useSVGTransformAttr && t.getAttribute("transform")) {
							TweenLite.delayedCall(0.001, function () {
								t.removeAttribute("transform");
							});
						}
					}
				}
				return tm;
			},

		//for setting 2D transforms in IE6, IE7, and IE8 (must use a "filter" to emulate the behavior of modern day browser transforms)
			_setIETransformRatio = function (v) {
				var t = this.data, //refers to the element's _gsTransform object
					ang = -t.rotation * _DEG2RAD
					, skew = ang + t.skewX * _DEG2RAD
					, rnd = 100000
					, a = ((Math.cos(ang) * t.scaleX * rnd) | 0) / rnd
					, b = ((Math.sin(ang) * t.scaleX * rnd) | 0) / rnd
					, c = ((Math.sin(skew) * -t.scaleY * rnd) | 0) / rnd
					, d = ((Math.cos(skew) * t.scaleY * rnd) | 0) / rnd
					, style = this.t.style
					, cs = this.t.currentStyle
					, filters, val;
				if (!cs) {
					return;
				}
				val = b; //just for swapping the variables an inverting them (reused "val" to avoid creating another variable in memory). IE's filter matrix uses a non-standard matrix configuration (angle goes the opposite way, and b and c are reversed and inverted)
				b = -c;
				c = -val;
				filters = cs.filter;
				style.filter = ""; //remove filters so that we can accurately measure offsetWidth/offsetHeight
				var w = this.t.offsetWidth
					, h = this.t.offsetHeight
					, clip = (cs.position !== "absolute")
					, m = "progid:DXImageTransform.Microsoft.Matrix(M11=" + a + ", M12=" + b + ", M21=" + c + ", M22=" + d
					, ox = t.x + (w * t.xPercent / 100)
					, oy = t.y + (h * t.yPercent / 100)
					, dx, dy;

				//if transformOrigin is being used, adjust the offset x and y
				if (t.ox != null) {
					dx = ((t.oxp) ? w * t.ox * 0.01 : t.ox) - w / 2;
					dy = ((t.oyp) ? h * t.oy * 0.01 : t.oy) - h / 2;
					ox += dx - (dx * a + dy * b);
					oy += dy - (dx * c + dy * d);
				}

				if (!clip) {
					m += ", sizingMethod='auto expand')";
				} else {
					dx = (w / 2);
					dy = (h / 2);
					//translate to ensure that transformations occur around the correct origin (default is center).
					m += ", Dx=" + (dx - (dx * a + dy * b) + ox) + ", Dy=" + (dy - (dx * c + dy * d) + oy) + ")";
				}
				if (filters.indexOf("DXImageTransform.Microsoft.Matrix(") !== -1) {
					style.filter = filters.replace(_ieSetMatrixExp, m);
				} else {
					style.filter = m + " " + filters; //we must always put the transform/matrix FIRST (before alpha(opacity=xx)) to avoid an IE bug that slices part of the object when rotation is applied with alpha.
				}

				//at the end or beginning of the tween, if the matrix is normal (1, 0, 0, 1) and opacity is 100 (or doesn't exist), remove the filter to improve browser performance.
				if (v === 0 || v === 1)
					if (a === 1)
						if (b === 0)
							if (c === 0)
								if (d === 1)
									if (!clip || m.indexOf("Dx=0, Dy=0") !== -1)
										if (!_opacityExp.test(filters) || parseFloat(RegExp.$1) === 100)
											if (filters.indexOf("gradient(" && filters.indexOf("Alpha")) === -1) {
												style.removeAttribute("filter");
											}

				//we must set the margins AFTER applying the filter in order to avoid some bugs in IE8 that could (in rare scenarios) cause them to be ignored intermittently (vibration).
				if (!clip) {
					var mult = (_ieVers < 8) ? 1 : -1, //in Internet Explorer 7 and before, the box model is broken, causing the browser to treat the width/height of the actual rotated filtered image as the width/height of the box itself, but Microsoft corrected that in IE8. We must use a negative offset in IE8 on the right/bottom
						marg, prop, dif;
					dx = t.ieOffsetX || 0;
					dy = t.ieOffsetY || 0;
					t.ieOffsetX = Math.round((w - ((a < 0 ? -a : a) * w + (b < 0 ? -b : b) * h)) / 2 + ox);
					t.ieOffsetY = Math.round((h - ((d < 0 ? -d : d) * h + (c < 0 ? -c : c) * w)) / 2 + oy);
					for (i = 0; i < 4; i++) {
						prop = _margins[i];
						marg = cs[prop];
						//we need to get the current margin in case it is being tweened separately (we want to respect that tween's changes)
						val = (marg.indexOf("px") !== -1) ? parseFloat(marg) : _convertToPixels(this.t, prop, parseFloat(marg), marg.replace(_suffixExp, "")) || 0;
						if (val !== t[prop]) {
							dif = (i < 2) ? -t.ieOffsetX : -t.ieOffsetY; //if another tween is controlling a margin, we cannot only apply the difference in the ieOffsets, so we essentially zero-out the dx and dy here in that case. We record the margin(s) later so that we can keep comparing them, making this code very flexible.
						} else {
							dif = (i < 2) ? dx - t.ieOffsetX : dy - t.ieOffsetY;
						}
						style[prop] = (t[prop] = Math.round(val - dif * ((i === 0 || i === 2) ? 1 : mult))) + "px";
					}
				}
			},

		/* translates a super small decimal to a string WITHOUT scientific notation
		 _safeDecimal = function(n) {
		 var s = (n < 0 ? -n : n) + "",
		 a = s.split("e-");
		 return (n < 0 ? "-0." : "0.") + new Array(parseInt(a[1], 10) || 0).join("0") + a[0].split(".").join("");
		 },
		 */

			_setTransformRatio = _internals.set3DTransformRatio = _internals.setTransformRatio = function (v) {
				var t = this.data, //refers to the element's _gsTransform object
					style = this.t.style
					, angle = t.rotation
					, rotationX = t.rotationX
					, rotationY = t.rotationY
					, sx = t.scaleX
					, sy = t.scaleY
					, sz = t.scaleZ
					, x = t.x
					, y = t.y
					, z = t.z
					, isSVG = t.svg
					, perspective = t.perspective
					, force3D = t.force3D
					, a11, a12, a13, a21, a22, a23, a31, a32, a33, a41, a42, a43
					, zOrigin, min, cos, sin, t1, t2, transform, comma, zero, skew, rnd;
				//check to see if we should render as 2D (and SVGs must use 2D when _useSVGTransformAttr is true)
				if (((((v === 1 || v === 0) && force3D === "auto" && (this.tween._totalTime === this.tween._totalDuration || !this.tween._totalTime)) || !force3D) && !z && !perspective && !rotationY && !rotationX && sz === 1) || (_useSVGTransformAttr && isSVG) || !_supports3D) { //on the final render (which could be 0 for a from tween), if there are no 3D aspects, render in 2D to free up memory and improve performance especially on mobile devices. Check the tween's totalTime/totalDuration too in order to make sure it doesn't happen between repeats if it's a repeating tween.

					//2D
					if (angle || t.skewX || isSVG) {
						angle *= _DEG2RAD;
						skew = t.skewX * _DEG2RAD;
						rnd = 100000;
						a11 = Math.cos(angle) * sx;
						a21 = Math.sin(angle) * sx;
						a12 = Math.sin(angle - skew) * -sy;
						a22 = Math.cos(angle - skew) * sy;
						if (skew && t.skewType === "simple") { //by default, we compensate skewing on the other axis to make it look more natural, but you can set the skewType to "simple" to use the uncompensated skewing that CSS does
							t1 = Math.tan(skew);
							t1 = Math.sqrt(1 + t1 * t1);
							a12 *= t1;
							a22 *= t1;
							if (t.skewY) {
								a11 *= t1;
								a21 *= t1;
							}
						}
						if (isSVG) {
							x += t.xOrigin - (t.xOrigin * a11 + t.yOrigin * a12) + t.xOffset;
							y += t.yOrigin - (t.xOrigin * a21 + t.yOrigin * a22) + t.yOffset;
							if (_useSVGTransformAttr && (t.xPercent || t.yPercent)) { //The SVG spec doesn't support percentage-based translation in the "transform" attribute, so we merge it into the matrix to simulate it.
								min = this.t.getBBox();
								x += t.xPercent * 0.01 * min.width;
								y += t.yPercent * 0.01 * min.height;
							}
							min = 0.000001;
							if (x < min)
								if (x > -min) {
									x = 0;
								}
							if (y < min)
								if (y > -min) {
									y = 0;
								}
						}
						transform = (((a11 * rnd) | 0) / rnd) + "," + (((a21 * rnd) | 0) / rnd) + "," + (((a12 * rnd) | 0) / rnd) + "," + (((a22 * rnd) | 0) / rnd) + "," + x + "," + y + ")";
						if (isSVG && _useSVGTransformAttr) {
							this.t.setAttribute("transform", "matrix(" + transform);
						} else {
							//some browsers have a hard time with very small values like 2.4492935982947064e-16 (notice the "e-" towards the end) and would render the object slightly off. So we round to 5 decimal places.
							style[_transformProp] = ((t.xPercent || t.yPercent) ? "translate(" + t.xPercent + "%," + t.yPercent + "%) matrix(" : "matrix(") + transform;
						}
					} else {
						style[_transformProp] = ((t.xPercent || t.yPercent) ? "translate(" + t.xPercent + "%," + t.yPercent + "%) matrix(" : "matrix(") + sx + ",0,0," + sy + "," + x + "," + y + ")";
					}
					return;

				}
				if (_isFirefox) { //Firefox has a bug (at least in v25) that causes it to render the transparent part of 32-bit PNG images as black when displayed inside an iframe and the 3D scale is very small and doesn't change sufficiently enough between renders (like if you use a Power4.easeInOut to scale from 0 to 1 where the beginning values only change a tiny amount to begin the tween before accelerating). In this case, we force the scale to be 0.00002 instead which is visually the same but works around the Firefox issue.
					min = 0.0001;
					if (sx < min && sx > -min) {
						sx = sz = 0.00002;
					}
					if (sy < min && sy > -min) {
						sy = sz = 0.00002;
					}
					if (perspective && !t.z && !t.rotationX && !t.rotationY) { //Firefox has a bug that causes elements to have an odd super-thin, broken/dotted black border on elements that have a perspective set but aren't utilizing 3D space (no rotationX, rotationY, or z).
						perspective = 0;
					}
				}
				if (angle || t.skewX) {
					angle *= _DEG2RAD;
					cos = a11 = Math.cos(angle);
					sin = a21 = Math.sin(angle);
					if (t.skewX) {
						angle -= t.skewX * _DEG2RAD;
						cos = Math.cos(angle);
						sin = Math.sin(angle);
						if (t.skewType === "simple") { //by default, we compensate skewing on the other axis to make it look more natural, but you can set the skewType to "simple" to use the uncompensated skewing that CSS does
							t1 = Math.tan(t.skewX * _DEG2RAD);
							t1 = Math.sqrt(1 + t1 * t1);
							cos *= t1;
							sin *= t1;
							if (t.skewY) {
								a11 *= t1;
								a21 *= t1;
							}
						}
					}
					a12 = -sin;
					a22 = cos;

				} else if (!rotationY && !rotationX && sz === 1 && !perspective && !isSVG) { //if we're only translating and/or 2D scaling, this is faster...
					style[_transformProp] = ((t.xPercent || t.yPercent) ? "translate(" + t.xPercent + "%," + t.yPercent + "%) translate3d(" : "translate3d(") + x + "px," + y + "px," + z + "px)" + ((sx !== 1 || sy !== 1) ? " scale(" + sx + "," + sy + ")" : "");
					return;
				} else {
					a11 = a22 = 1;
					a12 = a21 = 0;
				}
				// KEY  INDEX   AFFECTS
				// a11  0       rotation, rotationY, scaleX
				// a21  1       rotation, rotationY, scaleX
				// a31  2       rotationY, scaleX
				// a41  3       rotationY, scaleX
				// a12  4       rotation, skewX, rotationX, scaleY
				// a22  5       rotation, skewX, rotationX, scaleY
				// a32  6       rotationX, scaleY
				// a42  7       rotationX, scaleY
				// a13  8       rotationY, rotationX, scaleZ
				// a23  9       rotationY, rotationX, scaleZ
				// a33  10      rotationY, rotationX, scaleZ
				// a43  11      rotationY, rotationX, perspective, scaleZ
				// a14  12      x, zOrigin, svgOrigin
				// a24  13      y, zOrigin, svgOrigin
				// a34  14      z, zOrigin
				// a44  15
				// rotation: Math.atan2(a21, a11)
				// rotationY: Math.atan2(a13, a33) (or Math.atan2(a13, a11))
				// rotationX: Math.atan2(a32, a33)
				a33 = 1;
				a13 = a23 = a31 = a32 = a41 = a42 = 0;
				a43 = (perspective) ? -1 / perspective : 0;
				zOrigin = t.zOrigin;
				min = 0.000001; //threshold below which browsers use scientific notation which won't work.
				comma = ",";
				zero = "0";
				angle = rotationY * _DEG2RAD;
				if (angle) {
					cos = Math.cos(angle);
					sin = Math.sin(angle);
					a31 = -sin;
					a41 = a43 * -sin;
					a13 = a11 * sin;
					a23 = a21 * sin;
					a33 = cos;
					a43 *= cos;
					a11 *= cos;
					a21 *= cos;
				}
				angle = rotationX * _DEG2RAD;
				if (angle) {
					cos = Math.cos(angle);
					sin = Math.sin(angle);
					t1 = a12 * cos + a13 * sin;
					t2 = a22 * cos + a23 * sin;
					a32 = a33 * sin;
					a42 = a43 * sin;
					a13 = a12 * -sin + a13 * cos;
					a23 = a22 * -sin + a23 * cos;
					a33 = a33 * cos;
					a43 = a43 * cos;
					a12 = t1;
					a22 = t2;
				}
				if (sz !== 1) {
					a13 *= sz;
					a23 *= sz;
					a33 *= sz;
					a43 *= sz;
				}
				if (sy !== 1) {
					a12 *= sy;
					a22 *= sy;
					a32 *= sy;
					a42 *= sy;
				}
				if (sx !== 1) {
					a11 *= sx;
					a21 *= sx;
					a31 *= sx;
					a41 *= sx;
				}

				if (zOrigin || isSVG) {
					if (zOrigin) {
						x += a13 * -zOrigin;
						y += a23 * -zOrigin;
						z += a33 * -zOrigin + zOrigin;
					}
					if (isSVG) { //due to bugs in some browsers, we need to manage the transform-origin of SVG manually
						x += t.xOrigin - (t.xOrigin * a11 + t.yOrigin * a12) + t.xOffset;
						y += t.yOrigin - (t.xOrigin * a21 + t.yOrigin * a22) + t.yOffset;
					}
					if (x < min && x > -min) {
						x = zero;
					}
					if (y < min && y > -min) {
						y = zero;
					}
					if (z < min && z > -min) {
						z = 0; //don't use string because we calculate perspective later and need the number.
					}
				}

				//optimized way of concatenating all the values into a string. If we do it all in one shot, it's slower because of the way browsers have to create temp strings and the way it affects memory. If we do it piece-by-piece with +=, it's a bit slower too. We found that doing it in these sized chunks works best overall:
				transform = ((t.xPercent || t.yPercent) ? "translate(" + t.xPercent + "%," + t.yPercent + "%) matrix3d(" : "matrix3d(");
				transform += ((a11 < min && a11 > -min) ? zero : a11) + comma + ((a21 < min && a21 > -min) ? zero : a21) + comma + ((a31 < min && a31 > -min) ? zero : a31);
				transform += comma + ((a41 < min && a41 > -min) ? zero : a41) + comma + ((a12 < min && a12 > -min) ? zero : a12) + comma + ((a22 < min && a22 > -min) ? zero : a22);
				if (rotationX || rotationY || sz !== 1) { //performance optimization (often there's no rotationX or rotationY, so we can skip these calculations)
					transform += comma + ((a32 < min && a32 > -min) ? zero : a32) + comma + ((a42 < min && a42 > -min) ? zero : a42) + comma + ((a13 < min && a13 > -min) ? zero : a13);
					transform += comma + ((a23 < min && a23 > -min) ? zero : a23) + comma + ((a33 < min && a33 > -min) ? zero : a33) + comma + ((a43 < min && a43 > -min) ? zero : a43) + comma;
				} else {
					transform += ",0,0,0,0,1,0,";
				}
				transform += x + comma + y + comma + z + comma + (perspective ? (1 + (-z / perspective)) : 1) + ")";

				style[_transformProp] = transform;
			};

		p = Transform.prototype;
		p.x = p.y = p.z = p.skewX = p.skewY = p.rotation = p.rotationX = p.rotationY = p.zOrigin = p.xPercent = p.yPercent = p.xOffset = p.yOffset = 0;
		p.scaleX = p.scaleY = p.scaleZ = 1;

		_registerComplexSpecialProp("transform,scale,scaleX,scaleY,scaleZ,x,y,z,rotation,rotationX,rotationY,rotationZ,skewX,skewY,shortRotation,shortRotationX,shortRotationY,shortRotationZ,transformOrigin,svgOrigin,transformPerspective,directionalRotation,parseTransform,force3D,skewType,xPercent,yPercent,smoothOrigin", {
			parser: function (t, e, p, cssp, pt, plugin, vars) {
				if (cssp._lastParsedTransform === vars) {
					return pt;
				} //only need to parse the transform once, and only if the browser supports it.
				cssp._lastParsedTransform = vars;
				var originalGSTransform = t._gsTransform
					, style = t.style
					, min = 0.000001
					, i = _transformProps.length
					, v = vars
					, endRotations = {}
					, transformOriginString = "transformOrigin"
					, m1, m2, copy, orig, has3D, hasChange, dr, x, y, matrix;
				if (vars.display) { //if the user is setting display during this tween, it may not be instantiated yet but we must force it here in order to get accurate readings. If display is "none", some browsers refuse to report the transform properties correctly.
					copy = _getStyle(t, "display");
					style.display = "block";
					m1 = _getTransform(t, _cs, true, vars.parseTransform);
					style.display = copy;
				} else {
					m1 = _getTransform(t, _cs, true, vars.parseTransform);
				}
				cssp._transform = m1;
				if (typeof (v.transform) === "string" && _transformProp) { //for values like transform:"rotate(60deg) scale(0.5, 0.8)"
					copy = _tempDiv.style; //don't use the original target because it might be SVG in which case some browsers don't report computed style correctly.
					copy[_transformProp] = v.transform;
					copy.display = "block"; //if display is "none", the browser often refuses to report the transform properties correctly.
					copy.position = "absolute";
					_doc.body.appendChild(_tempDiv);
					m2 = _getTransform(_tempDiv, null, false);
					if (m1.svg) { //if it's an SVG element, x/y part of the matrix will be affected by whatever we use as the origin and the offsets, so compensate here...
						x = m1.xOrigin;
						y = m1.yOrigin;
						m2.x -= m1.xOffset;
						m2.y -= m1.yOffset;
						if (v.transformOrigin || v.svgOrigin) { //if this tween is altering the origin, we must factor that in here. The actual work of recording the transformOrigin values and setting up the PropTween is done later (still inside this function) so we cannot leave the changes intact here - we only want to update the x/y accordingly.
							orig = {};
							_parseSVGOrigin(t, _parsePosition(v.transformOrigin), orig, v.svgOrigin, v.smoothOrigin, true);
							x = orig.xOrigin;
							y = orig.yOrigin;
							m2.x -= orig.xOffset - m1.xOffset;
							m2.y -= orig.yOffset - m1.yOffset;
						}
						if (x || y) {
							matrix = _getMatrix(_tempDiv);
							m2.x -= x - (x * matrix[0] + y * matrix[2]);
							m2.y -= y - (x * matrix[1] + y * matrix[3]);
						}
					}
					_doc.body.removeChild(_tempDiv);
					if (!m2.perspective) {
						m2.perspective = m1.perspective; //tweening to no perspective gives very unintuitive results - just keep the same perspective in that case.
					}
					if (v.xPercent != null) {
						m2.xPercent = _parseVal(v.xPercent, m1.xPercent);
					}
					if (v.yPercent != null) {
						m2.yPercent = _parseVal(v.yPercent, m1.yPercent);
					}
				} else if (typeof (v) === "object") { //for values like scaleX, scaleY, rotation, x, y, skewX, and skewY or transform:{...} (object)
					m2 = {
						scaleX: _parseVal((v.scaleX != null) ? v.scaleX : v.scale, m1.scaleX)
						, scaleY: _parseVal((v.scaleY != null) ? v.scaleY : v.scale, m1.scaleY)
						, scaleZ: _parseVal(v.scaleZ, m1.scaleZ)
						, x: _parseVal(v.x, m1.x)
						, y: _parseVal(v.y, m1.y)
						, z: _parseVal(v.z, m1.z)
						, xPercent: _parseVal(v.xPercent, m1.xPercent)
						, yPercent: _parseVal(v.yPercent, m1.yPercent)
						, perspective: _parseVal(v.transformPerspective, m1.perspective)
					};
					dr = v.directionalRotation;
					if (dr != null) {
						if (typeof (dr) === "object") {
							for (copy in dr) {
								v[copy] = dr[copy];
							}
						} else {
							v.rotation = dr;
						}
					}
					if (typeof (v.x) === "string" && v.x.indexOf("%") !== -1) {
						m2.x = 0;
						m2.xPercent = _parseVal(v.x, m1.xPercent);
					}
					if (typeof (v.y) === "string" && v.y.indexOf("%") !== -1) {
						m2.y = 0;
						m2.yPercent = _parseVal(v.y, m1.yPercent);
					}

					m2.rotation = _parseAngle(("rotation" in v) ? v.rotation : ("shortRotation" in v) ? v.shortRotation + "_short" : ("rotationZ" in v) ? v.rotationZ : m1.rotation - m1.skewY, m1.rotation - m1.skewY, "rotation", endRotations); //see notes below about skewY for why we subtract it from rotation here
					if (_supports3D) {
						m2.rotationX = _parseAngle(("rotationX" in v) ? v.rotationX : ("shortRotationX" in v) ? v.shortRotationX + "_short" : m1.rotationX || 0, m1.rotationX, "rotationX", endRotations);
						m2.rotationY = _parseAngle(("rotationY" in v) ? v.rotationY : ("shortRotationY" in v) ? v.shortRotationY + "_short" : m1.rotationY || 0, m1.rotationY, "rotationY", endRotations);
					}
					m2.skewX = _parseAngle(v.skewX, m1.skewX - m1.skewY); //see notes below about skewY and why we subtract it from skewX here

					//note: for performance reasons, we combine all skewing into the skewX and rotation values, ignoring skewY but we must still record it so that we can discern how much of the overall skew is attributed to skewX vs. skewY. Otherwise, if the skewY would always act relative (tween skewY to 10deg, for example, multiple times and if we always combine things into skewX, we can't remember that skewY was 10 from last time). Remember, a skewY of 10 degrees looks the same as a rotation of 10 degrees plus a skewX of -10 degrees.
					if ((m2.skewY = _parseAngle(v.skewY, m1.skewY))) {
						m2.skewX += m2.skewY;
						m2.rotation += m2.skewY;
					}
				}
				if (_supports3D && v.force3D != null) {
					m1.force3D = v.force3D;
					hasChange = true;
				}

				m1.skewType = v.skewType || m1.skewType || CSSPlugin.defaultSkewType;

				has3D = (m1.force3D || m1.z || m1.rotationX || m1.rotationY || m2.z || m2.rotationX || m2.rotationY || m2.perspective);
				if (!has3D && v.scale != null) {
					m2.scaleZ = 1; //no need to tween scaleZ.
				}

				while (--i > -1) {
					p = _transformProps[i];
					orig = m2[p] - m1[p];
					if (orig > min || orig < -min || v[p] != null || _forcePT[p] != null) {
						hasChange = true;
						pt = new CSSPropTween(m1, p, m1[p], orig, pt);
						if (p in endRotations) {
							pt.e = endRotations[p]; //directional rotations typically have compensated values during the tween, but we need to make sure they end at exactly what the user requested
						}
						pt.xs0 = 0; //ensures the value stays numeric in setRatio()
						pt.plugin = plugin;
						cssp._overwriteProps.push(pt.n);
					}
				}

				orig = v.transformOrigin;
				if (m1.svg && (orig || v.svgOrigin)) {
					x = m1.xOffset; //when we change the origin, in order to prevent things from jumping we adjust the x/y so we must record those here so that we can create PropTweens for them and flip them at the same time as the origin
					y = m1.yOffset;
					_parseSVGOrigin(t, _parsePosition(orig), m2, v.svgOrigin, v.smoothOrigin);
					pt = _addNonTweeningNumericPT(m1, "xOrigin", (originalGSTransform ? m1 : m2).xOrigin, m2.xOrigin, pt, transformOriginString); //note: if there wasn't a transformOrigin defined yet, just start with the destination one; it's wasteful otherwise, and it causes problems with fromTo() tweens. For example, TweenLite.to("#wheel", 3, {rotation:180, transformOrigin:"50% 50%", delay:1}); TweenLite.fromTo("#wheel", 3, {scale:0.5, transformOrigin:"50% 50%"}, {scale:1, delay:2}); would cause a jump when the from values revert at the beginning of the 2nd tween.
					pt = _addNonTweeningNumericPT(m1, "yOrigin", (originalGSTransform ? m1 : m2).yOrigin, m2.yOrigin, pt, transformOriginString);
					if (x !== m1.xOffset || y !== m1.yOffset) {
						pt = _addNonTweeningNumericPT(m1, "xOffset", (originalGSTransform ? x : m1.xOffset), m1.xOffset, pt, transformOriginString);
						pt = _addNonTweeningNumericPT(m1, "yOffset", (originalGSTransform ? y : m1.yOffset), m1.yOffset, pt, transformOriginString);
					}
					orig = _useSVGTransformAttr ? null : "0px 0px"; //certain browsers (like firefox) completely botch transform-origin, so we must remove it to prevent it from contaminating transforms. We manage it ourselves with xOrigin and yOrigin
				}
				if (orig || (_supports3D && has3D && m1.zOrigin)) { //if anything 3D is happening and there's a transformOrigin with a z component that's non-zero, we must ensure that the transformOrigin's z-component is set to 0 so that we can manually do those calculations to get around Safari bugs. Even if the user didn't specifically define a "transformOrigin" in this particular tween (maybe they did it via css directly).
					if (_transformProp) {
						hasChange = true;
						p = _transformOriginProp;
						orig = (orig || _getStyle(t, p, _cs, false, "50% 50%")) + ""; //cast as string to avoid errors
						pt = new CSSPropTween(style, p, 0, 0, pt, -1, transformOriginString);
						pt.b = style[p];
						pt.plugin = plugin;
						if (_supports3D) {
							copy = m1.zOrigin;
							orig = orig.split(" ");
							m1.zOrigin = ((orig.length > 2 && !(copy !== 0 && orig[2] === "0px")) ? parseFloat(orig[2]) : copy) || 0; //Safari doesn't handle the z part of transformOrigin correctly, so we'll manually handle it in the _set3DTransformRatio() method.
							pt.xs0 = pt.e = orig[0] + " " + (orig[1] || "50%") + " 0px"; //we must define a z value of 0px specifically otherwise iOS 5 Safari will stick with the old one (if one was defined)!
							pt = new CSSPropTween(m1, "zOrigin", 0, 0, pt, -1, pt.n); //we must create a CSSPropTween for the _gsTransform.zOrigin so that it gets reset properly at the beginning if the tween runs backward (as opposed to just setting m1.zOrigin here)
							pt.b = copy;
							pt.xs0 = pt.e = m1.zOrigin;
						} else {
							pt.xs0 = pt.e = orig;
						}

						//for older versions of IE (6-8), we need to manually calculate things inside the setRatio() function. We record origin x and y (ox and oy) and whether or not the values are percentages (oxp and oyp).
					} else {
						_parsePosition(orig + "", m1);
					}
				}
				if (hasChange) {
					cssp._transformType = (!(m1.svg && _useSVGTransformAttr) && (has3D || this._transformType === 3)) ? 3 : 2; //quicker than calling cssp._enableTransforms();
				}
				return pt;
			}
			, prefix: true
		});

		_registerComplexSpecialProp("boxShadow", {
			defaultValue: "0px 0px 0px 0px #999"
			, prefix: true
			, color: true
			, multi: true
			, keyword: "inset"
		});

		_registerComplexSpecialProp("borderRadius", {
			defaultValue: "0px"
			, parser: function (t, e, p, cssp, pt, plugin) {
				e = this.format(e);
				var props = ["borderTopLeftRadius", "borderTopRightRadius", "borderBottomRightRadius", "borderBottomLeftRadius"]
					, style = t.style
					, ea1, i, es2, bs2, bs, es, bn, en, w, h, esfx, bsfx, rel, hn, vn, em;
				w = parseFloat(t.offsetWidth);
				h = parseFloat(t.offsetHeight);
				ea1 = e.split(" ");
				for (i = 0; i < props.length; i++) { //if we're dealing with percentages, we must convert things separately for the horizontal and vertical axis!
					if (this.p.indexOf("border")) { //older browsers used a prefix
						props[i] = _checkPropPrefix(props[i]);
					}
					bs = bs2 = _getStyle(t, props[i], _cs, false, "0px");
					if (bs.indexOf(" ") !== -1) {
						bs2 = bs.split(" ");
						bs = bs2[0];
						bs2 = bs2[1];
					}
					es = es2 = ea1[i];
					bn = parseFloat(bs);
					bsfx = bs.substr((bn + "").length);
					rel = (es.charAt(1) === "=");
					if (rel) {
						en = parseInt(es.charAt(0) + "1", 10);
						es = es.substr(2);
						en *= parseFloat(es);
						esfx = es.substr((en + "").length - (en < 0 ? 1 : 0)) || "";
					} else {
						en = parseFloat(es);
						esfx = es.substr((en + "").length);
					}
					if (esfx === "") {
						esfx = _suffixMap[p] || bsfx;
					}
					if (esfx !== bsfx) {
						hn = _convertToPixels(t, "borderLeft", bn, bsfx); //horizontal number (we use a bogus "borderLeft" property just because the _convertToPixels() method searches for the keywords "Left", "Right", "Top", and "Bottom" to determine of it's a horizontal or vertical property, and we need "border" in the name so that it knows it should measure relative to the element itself, not its parent.
						vn = _convertToPixels(t, "borderTop", bn, bsfx); //vertical number
						if (esfx === "%") {
							bs = (hn / w * 100) + "%";
							bs2 = (vn / h * 100) + "%";
						} else if (esfx === "em") {
							em = _convertToPixels(t, "borderLeft", 1, "em");
							bs = (hn / em) + "em";
							bs2 = (vn / em) + "em";
						} else {
							bs = hn + "px";
							bs2 = vn + "px";
						}
						if (rel) {
							es = (parseFloat(bs) + en) + esfx;
							es2 = (parseFloat(bs2) + en) + esfx;
						}
					}
					pt = _parseComplex(style, props[i], bs + " " + bs2, es + " " + es2, false, "0px", pt);
				}
				return pt;
			}
			, prefix: true
			, formatter: _getFormatter("0px 0px 0px 0px", false, true)
		});
		_registerComplexSpecialProp("borderBottomLeftRadius,borderBottomRightRadius,borderTopLeftRadius,borderTopRightRadius", {
			defaultValue: "0px"
			, parser: function (t, e, p, cssp, pt, plugin) {
				return _parseComplex(t.style, p, this.format(_getStyle(t, p, _cs, false, "0px 0px")), this.format(e), false, "0px", pt);
			}
			, prefix: true
			, formatter: _getFormatter("0px 0px", false, true)
		});
		_registerComplexSpecialProp("backgroundPosition", {
			defaultValue: "0 0"
			, parser: function (t, e, p, cssp, pt, plugin) {
				var bp = "background-position"
					, cs = (_cs || _getComputedStyle(t, null))
					, bs = this.format(((cs) ? _ieVers ? cs.getPropertyValue(bp + "-x") + " " + cs.getPropertyValue(bp + "-y") : cs.getPropertyValue(bp) : t.currentStyle.backgroundPositionX + " " + t.currentStyle.backgroundPositionY) || "0 0"), //Internet Explorer doesn't report background-position correctly - we must query background-position-x and background-position-y and combine them (even in IE10). Before IE9, we must do the same with the currentStyle object and use camelCase
					es = this.format(e)
					, ba, ea, i, pct, overlap, src;
				if ((bs.indexOf("%") !== -1) !== (es.indexOf("%") !== -1) && es.split(",").length < 2) {
					src = _getStyle(t, "backgroundImage").replace(_urlExp, "");
					if (src && src !== "none") {
						ba = bs.split(" ");
						ea = es.split(" ");
						_tempImg.setAttribute("src", src); //set the temp IMG's src to the background-image so that we can measure its width/height
						i = 2;
						while (--i > -1) {
							bs = ba[i];
							pct = (bs.indexOf("%") !== -1);
							if (pct !== (ea[i].indexOf("%") !== -1)) {
								overlap = (i === 0) ? t.offsetWidth - _tempImg.width : t.offsetHeight - _tempImg.height;
								ba[i] = pct ? (parseFloat(bs) / 100 * overlap) + "px" : (parseFloat(bs) / overlap * 100) + "%";
							}
						}
						bs = ba.join(" ");
					}
				}
				return this.parseComplex(t.style, bs, es, pt, plugin);
			}
			, formatter: _parsePosition
		});
		_registerComplexSpecialProp("backgroundSize", {
			defaultValue: "0 0"
			, formatter: _parsePosition
		});
		_registerComplexSpecialProp("perspective", {
			defaultValue: "0px"
			, prefix: true
		});
		_registerComplexSpecialProp("perspectiveOrigin", {
			defaultValue: "50% 50%"
			, prefix: true
		});
		_registerComplexSpecialProp("transformStyle", {
			prefix: true
		});
		_registerComplexSpecialProp("backfaceVisibility", {
			prefix: true
		});
		_registerComplexSpecialProp("userSelect", {
			prefix: true
		});
		_registerComplexSpecialProp("margin", {
			parser: _getEdgeParser("marginTop,marginRight,marginBottom,marginLeft")
		});
		_registerComplexSpecialProp("padding", {
			parser: _getEdgeParser("paddingTop,paddingRight,paddingBottom,paddingLeft")
		});
		_registerComplexSpecialProp("clip", {
			defaultValue: "rect(0px,0px,0px,0px)"
			, parser: function (t, e, p, cssp, pt, plugin) {
				var b, cs, delim;
				if (_ieVers < 9) { //IE8 and earlier don't report a "clip" value in the currentStyle - instead, the values are split apart into clipTop, clipRight, clipBottom, and clipLeft. Also, in IE7 and earlier, the values inside rect() are space-delimited, not comma-delimited.
					cs = t.currentStyle;
					delim = _ieVers < 8 ? " " : ",";
					b = "rect(" + cs.clipTop + delim + cs.clipRight + delim + cs.clipBottom + delim + cs.clipLeft + ")";
					e = this.format(e).split(",").join(delim);
				} else {
					b = this.format(_getStyle(t, this.p, _cs, false, this.dflt));
					e = this.format(e);
				}
				return this.parseComplex(t.style, b, e, pt, plugin);
			}
		});
		_registerComplexSpecialProp("textShadow", {
			defaultValue: "0px 0px 0px #999"
			, color: true
			, multi: true
		});
		_registerComplexSpecialProp("autoRound,strictUnits", {
			parser: function (t, e, p, cssp, pt) {
				return pt;
			}
		}); //just so that we can ignore these properties (not tween them)
		_registerComplexSpecialProp("border", {
			defaultValue: "0px solid #000"
			, parser: function (t, e, p, cssp, pt, plugin) {
				return this.parseComplex(t.style, this.format(_getStyle(t, "borderTopWidth", _cs, false, "0px") + " " + _getStyle(t, "borderTopStyle", _cs, false, "solid") + " " + _getStyle(t, "borderTopColor", _cs, false, "#000")), this.format(e), pt, plugin);
			}
			, color: true
			, formatter: function (v) {
				var a = v.split(" ");
				return a[0] + " " + (a[1] || "solid") + " " + (v.match(_colorExp) || ["#000"])[0];
			}
		});
		_registerComplexSpecialProp("borderWidth", {
			parser: _getEdgeParser("borderTopWidth,borderRightWidth,borderBottomWidth,borderLeftWidth")
		}); //Firefox doesn't pick up on borderWidth set in style sheets (only inline).
		_registerComplexSpecialProp("float,cssFloat,styleFloat", {
			parser: function (t, e, p, cssp, pt, plugin) {
				var s = t.style
					, prop = ("cssFloat" in s) ? "cssFloat" : "styleFloat";
				return new CSSPropTween(s, prop, 0, 0, pt, -1, p, false, 0, s[prop], e);
			}
		});

		//opacity-related
		var _setIEOpacityRatio = function (v) {
			var t = this.t, //refers to the element's style property
				filters = t.filter || _getStyle(this.data, "filter") || ""
				, val = (this.s + this.c * v) | 0
				, skip;
			if (val === 100) { //for older versions of IE that need to use a filter to apply opacity, we should remove the filter if opacity hits 1 in order to improve performance, but make sure there isn't a transform (matrix) or gradient in the filters.
				if (filters.indexOf("atrix(") === -1 && filters.indexOf("radient(") === -1 && filters.indexOf("oader(") === -1) {
					t.removeAttribute("filter");
					skip = (!_getStyle(this.data, "filter")); //if a class is applied that has an alpha filter, it will take effect (we don't want that), so re-apply our alpha filter in that case. We must first remove it and then check.
				} else {
					t.filter = filters.replace(_alphaFilterExp, "");
					skip = true;
				}
			}
			if (!skip) {
				if (this.xn1) {
					t.filter = filters = filters || ("alpha(opacity=" + val + ")"); //works around bug in IE7/8 that prevents changes to "visibility" from being applied properly if the filter is changed to a different alpha on the same frame.
				}
				if (filters.indexOf("pacity") === -1) { //only used if browser doesn't support the standard opacity style property (IE 7 and 8). We omit the "O" to avoid case-sensitivity issues
					if (val !== 0 || !this.xn1) { //bugs in IE7/8 won't render the filter properly if opacity is ADDED on the same frame/render as "visibility" changes (this.xn1 is 1 if this tween is an "autoAlpha" tween)
						t.filter = filters + " alpha(opacity=" + val + ")"; //we round the value because otherwise, bugs in IE7/8 can prevent "visibility" changes from being applied properly.
					}
				} else {
					t.filter = filters.replace(_opacityExp, "opacity=" + val);
				}
			}
		};
		_registerComplexSpecialProp("opacity,alpha,autoAlpha", {
			defaultValue: "1"
			, parser: function (t, e, p, cssp, pt, plugin) {
				var b = parseFloat(_getStyle(t, "opacity", _cs, false, "1"))
					, style = t.style
					, isAutoAlpha = (p === "autoAlpha");
				if (typeof (e) === "string" && e.charAt(1) === "=") {
					e = ((e.charAt(0) === "-") ? -1 : 1) * parseFloat(e.substr(2)) + b;
				}
				if (isAutoAlpha && b === 1 && _getStyle(t, "visibility", _cs) === "hidden" && e !== 0) { //if visibility is initially set to "hidden", we should interpret that as intent to make opacity 0 (a convenience)
					b = 0;
				}
				if (_supportsOpacity) {
					pt = new CSSPropTween(style, "opacity", b, e - b, pt);
				} else {
					pt = new CSSPropTween(style, "opacity", b * 100, (e - b) * 100, pt);
					pt.xn1 = isAutoAlpha ? 1 : 0; //we need to record whether or not this is an autoAlpha so that in the setRatio(), we know to duplicate the setting of the alpha in order to work around a bug in IE7 and IE8 that prevents changes to "visibility" from taking effect if the filter is changed to a different alpha(opacity) at the same time. Setting it to the SAME value first, then the new value works around the IE7/8 bug.
					style.zoom = 1; //helps correct an IE issue.
					pt.type = 2;
					pt.b = "alpha(opacity=" + pt.s + ")";
					pt.e = "alpha(opacity=" + (pt.s + pt.c) + ")";
					pt.data = t;
					pt.plugin = plugin;
					pt.setRatio = _setIEOpacityRatio;
				}
				if (isAutoAlpha) { //we have to create the "visibility" PropTween after the opacity one in the linked list so that they run in the order that works properly in IE8 and earlier
					pt = new CSSPropTween(style, "visibility", 0, 0, pt, -1, null, false, 0, ((b !== 0) ? "inherit" : "hidden"), ((e === 0) ? "hidden" : "inherit"));
					pt.xs0 = "inherit";
					cssp._overwriteProps.push(pt.n);
					cssp._overwriteProps.push(p);
				}
				return pt;
			}
		});


		var _removeProp = function (s, p) {
			if (p) {
				if (s.removeProperty) {
					if (p.substr(0, 2) === "ms" || p.substr(0, 6) === "webkit") { //Microsoft and some Webkit browsers don't conform to the standard of capitalizing the first prefix character, so we adjust so that when we prefix the caps with a dash, it's correct (otherwise it'd be "ms-transform" instead of "-ms-transform" for IE9, for example)
						p = "-" + p;
					}
					s.removeProperty(p.replace(_capsExp, "-$1").toLowerCase());
				} else { //note: old versions of IE use "removeAttribute()" instead of "removeProperty()"
					s.removeAttribute(p);
				}
			}
		}
			, _setClassNameRatio = function (v) {
			this.t._gsClassPT = this;
			if (v === 1 || v === 0) {
				this.t.setAttribute("class", (v === 0) ? this.b : this.e);
				var mpt = this.data, //first MiniPropTween
					s = this.t.style;
				while (mpt) {
					if (!mpt.v) {
						_removeProp(s, mpt.p);
					} else {
						s[mpt.p] = mpt.v;
					}
					mpt = mpt._next;
				}
				if (v === 1 && this.t._gsClassPT === this) {
					this.t._gsClassPT = null;
				}
			} else if (this.t.getAttribute("class") !== this.e) {
				this.t.setAttribute("class", this.e);
			}
		};
		_registerComplexSpecialProp("className", {
			parser: function (t, e, p, cssp, pt, plugin, vars) {
				var b = t.getAttribute("class") || "", //don't use t.className because it doesn't work consistently on SVG elements; getAttribute("class") and setAttribute("class", value") is more reliable.
					cssText = t.style.cssText
					, difData, bs, cnpt, cnptLookup, mpt;
				pt = cssp._classNamePT = new CSSPropTween(t, p, 0, 0, pt, 2);
				pt.setRatio = _setClassNameRatio;
				pt.pr = -11;
				_hasPriority = true;
				pt.b = b;
				bs = _getAllStyles(t, _cs);
				//if there's a className tween already operating on the target, force it to its end so that the necessary inline styles are removed and the class name is applied before we determine the end state (we don't want inline styles interfering that were there just for class-specific values)
				cnpt = t._gsClassPT;
				if (cnpt) {
					cnptLookup = {};
					mpt = cnpt.data; //first MiniPropTween which stores the inline styles - we need to force these so that the inline styles don't contaminate things. Otherwise, there's a small chance that a tween could start and the inline values match the destination values and they never get cleaned.
					while (mpt) {
						cnptLookup[mpt.p] = 1;
						mpt = mpt._next;
					}
					cnpt.setRatio(1);
				}
				t._gsClassPT = pt;
				pt.e = (e.charAt(1) !== "=") ? e : b.replace(new RegExp("(?:\\s|^)" + e.substr(2) + "(?![\\w-])"), "") + ((e.charAt(0) === "+") ? " " + e.substr(2) : "");
				t.setAttribute("class", pt.e);
				difData = _cssDif(t, bs, _getAllStyles(t), vars, cnptLookup);
				t.setAttribute("class", b);
				pt.data = difData.firstMPT;
				t.style.cssText = cssText; //we recorded cssText before we swapped classes and ran _getAllStyles() because in cases when a className tween is overwritten, we remove all the related tweening properties from that class change (otherwise class-specific stuff can't override properties we've directly set on the target's style object due to specificity).
				pt = pt.xfirst = cssp.parse(t, difData.difs, pt, plugin); //we record the CSSPropTween as the xfirst so that we can handle overwriting propertly (if "className" gets overwritten, we must kill all the properties associated with the className part of the tween, so we can loop through from xfirst to the pt itself)
				return pt;
			}
		});


		var _setClearPropsRatio = function (v) {
			if (v === 1 || v === 0)
				if (this.data._totalTime === this.data._totalDuration && this.data.data !== "isFromStart") { //this.data refers to the tween. Only clear at the END of the tween (remember, from() tweens make the ratio go from 1 to 0, so we can't just check that and if the tween is the zero-duration one that's created internally to render the starting values in a from() tween, ignore that because otherwise, for example, from(...{height:100, clearProps:"height", delay:1}) would wipe the height at the beginning of the tween and after 1 second, it'd kick back in).
					var s = this.t.style
						, transformParse = _specialProps.transform.parse
						, a, p, i, clearTransform, transform;
					if (this.e === "all") {
						s.cssText = "";
						clearTransform = true;
					} else {
						a = this.e.split(" ").join("").split(",");
						i = a.length;
						while (--i > -1) {
							p = a[i];
							if (_specialProps[p]) {
								if (_specialProps[p].parse === transformParse) {
									clearTransform = true;
								} else {
									p = (p === "transformOrigin") ? _transformOriginProp : _specialProps[p].p; //ensures that special properties use the proper browser-specific property name, like "scaleX" might be "-webkit-transform" or "boxShadow" might be "-moz-box-shadow"
								}
							}
							_removeProp(s, p);
						}
					}
					if (clearTransform) {
						_removeProp(s, _transformProp);
						transform = this.t._gsTransform;
						if (transform) {
							if (transform.svg) {
								this.t.removeAttribute("data-svg-origin");
								this.t.removeAttribute("transform");
							}
							delete this.t._gsTransform;
						}
					}

				}
		};
		_registerComplexSpecialProp("clearProps", {
			parser: function (t, e, p, cssp, pt) {
				pt = new CSSPropTween(t, p, 0, 0, pt, 2);
				pt.setRatio = _setClearPropsRatio;
				pt.e = e;
				pt.pr = -10;
				pt.data = cssp._tween;
				_hasPriority = true;
				return pt;
			}
		});

		p = "bezier,throwProps,physicsProps,physics2D".split(",");
		i = p.length;
		while (i--) {
			_registerPluginProp(p[i]);
		}








		p = CSSPlugin.prototype;
		p._firstPT = p._lastParsedTransform = p._transform = null;

		//gets called when the tween renders for the first time. This kicks everything off, recording start/end values, etc.
		p._onInitTween = function (target, vars, tween) {
			if (!target.nodeType) { //css is only for dom elements
				return false;
			}
			this._target = target;
			this._tween = tween;
			this._vars = vars;
			_autoRound = vars.autoRound;
			_hasPriority = false;
			_suffixMap = vars.suffixMap || CSSPlugin.suffixMap;
			_cs = _getComputedStyle(target, "");
			_overwriteProps = this._overwriteProps;
			var style = target.style
				, v, pt, pt2, first, last, next, zIndex, tpt, threeD;
			if (_reqSafariFix)
				if (style.zIndex === "") {
					v = _getStyle(target, "zIndex", _cs);
					if (v === "auto" || v === "") {
						//corrects a bug in [non-Android] Safari that prevents it from repainting elements in their new positions if they don't have a zIndex set. We also can't just apply this inside _parseTransform() because anything that's moved in any way (like using "left" or "top" instead of transforms like "x" and "y") can be affected, so it is best to ensure that anything that's tweening has a z-index. Setting "WebkitPerspective" to a non-zero value worked too except that on iOS Safari things would flicker randomly. Plus zIndex is less memory-intensive.
						this._addLazySet(style, "zIndex", 0);
					}
				}

			if (typeof (vars) === "string") {
				first = style.cssText;
				v = _getAllStyles(target, _cs);
				style.cssText = first + ";" + vars;
				v = _cssDif(target, v, _getAllStyles(target)).difs;
				if (!_supportsOpacity && _opacityValExp.test(vars)) {
					v.opacity = parseFloat(RegExp.$1);
				}
				vars = v;
				style.cssText = first;
			}

			if (vars.className) { //className tweens will combine any differences they find in the css with the vars that are passed in, so {className:"myClass", scale:0.5, left:20} would work.
				this._firstPT = pt = _specialProps.className.parse(target, vars.className, "className", this, null, null, vars);
			} else {
				this._firstPT = pt = this.parse(target, vars, null);
			}

			if (this._transformType) {
				threeD = (this._transformType === 3);
				if (!_transformProp) {
					style.zoom = 1; //helps correct an IE issue.
				} else if (_isSafari) {
					_reqSafariFix = true;
					//if zIndex isn't set, iOS Safari doesn't repaint things correctly sometimes (seemingly at random).
					if (style.zIndex === "") {
						zIndex = _getStyle(target, "zIndex", _cs);
						if (zIndex === "auto" || zIndex === "") {
							this._addLazySet(style, "zIndex", 0);
						}
					}
					//Setting WebkitBackfaceVisibility corrects 3 bugs:
					// 1) [non-Android] Safari skips rendering changes to "top" and "left" that are made on the same frame/render as a transform update.
					// 2) iOS Safari sometimes neglects to repaint elements in their new positions. Setting "WebkitPerspective" to a non-zero value worked too except that on iOS Safari things would flicker randomly.
					// 3) Safari sometimes displayed odd artifacts when tweening the transform (or WebkitTransform) property, like ghosts of the edges of the element remained. Definitely a browser bug.
					//Note: we allow the user to override the auto-setting by defining WebkitBackfaceVisibility in the vars of the tween.
					if (_isSafariLT6) {
						this._addLazySet(style, "WebkitBackfaceVisibility", this._vars.WebkitBackfaceVisibility || (threeD ? "visible" : "hidden"));
					}
				}
				pt2 = pt;
				while (pt2 && pt2._next) {
					pt2 = pt2._next;
				}
				tpt = new CSSPropTween(target, "transform", 0, 0, null, 2);
				this._linkCSSP(tpt, null, pt2);
				tpt.setRatio = _transformProp ? _setTransformRatio : _setIETransformRatio;
				tpt.data = this._transform || _getTransform(target, _cs, true);
				tpt.tween = tween;
				tpt.pr = -1; //ensures that the transforms get applied after the components are updated.
				_overwriteProps.pop(); //we don't want to force the overwrite of all "transform" tweens of the target - we only care about individual transform properties like scaleX, rotation, etc. The CSSPropTween constructor automatically adds the property to _overwriteProps which is why we need to pop() here.
			}

			if (_hasPriority) {
				//reorders the linked list in order of pr (priority)
				while (pt) {
					next = pt._next;
					pt2 = first;
					while (pt2 && pt2.pr > pt.pr) {
						pt2 = pt2._next;
					}
					if ((pt._prev = pt2 ? pt2._prev : last)) {
						pt._prev._next = pt;
					} else {
						first = pt;
					}
					if ((pt._next = pt2)) {
						pt2._prev = pt;
					} else {
						last = pt;
					}
					pt = next;
				}
				this._firstPT = first;
			}
			return true;
		};


		p.parse = function (target, vars, pt, plugin) {
			var style = target.style
				, p, sp, bn, en, bs, es, bsfx, esfx, isStr, rel;
			for (p in vars) {
				es = vars[p]; //ending value string
				sp = _specialProps[p]; //SpecialProp lookup.
				if (sp) {
					pt = sp.parse(target, es, p, this, pt, plugin, vars);

				} else {
					bs = _getStyle(target, p, _cs) + "";
					isStr = (typeof (es) === "string");
					if (p === "color" || p === "fill" || p === "stroke" || p.indexOf("Color") !== -1 || (isStr && _rgbhslExp.test(es))) { //Opera uses background: to define color sometimes in addition to backgroundColor:
						if (!isStr) {
							es = _parseColor(es);
							es = ((es.length > 3) ? "rgba(" : "rgb(") + es.join(",") + ")";
						}
						pt = _parseComplex(style, p, bs, es, true, "transparent", pt, 0, plugin);

					} else if (isStr && _complexExp.test(es)) {
						pt = _parseComplex(style, p, bs, es, true, null, pt, 0, plugin);

					} else {
						bn = parseFloat(bs);
						bsfx = (bn || bn === 0) ? bs.substr((bn + "").length) : ""; //remember, bs could be non-numeric like "normal" for fontWeight, so we should default to a blank suffix in that case.

						if (bs === "" || bs === "auto") {
							if (p === "width" || p === "height") {
								bn = _getDimension(target, p, _cs);
								bsfx = "px";
							} else if (p === "left" || p === "top") {
								bn = _calculateOffset(target, p, _cs);
								bsfx = "px";
							} else {
								bn = (p !== "opacity") ? 0 : 1;
								bsfx = "";
							}
						}

						rel = (isStr && es.charAt(1) === "=");
						if (rel) {
							en = parseInt(es.charAt(0) + "1", 10);
							es = es.substr(2);
							en *= parseFloat(es);
							esfx = es.replace(_suffixExp, "");
						} else {
							en = parseFloat(es);
							esfx = isStr ? es.replace(_suffixExp, "") : "";
						}

						if (esfx === "") {
							esfx = (p in _suffixMap) ? _suffixMap[p] : bsfx; //populate the end suffix, prioritizing the map, then if none is found, use the beginning suffix.
						}

						es = (en || en === 0) ? (rel ? en + bn : en) + esfx : vars[p]; //ensures that any += or -= prefixes are taken care of. Record the end value before normalizing the suffix because we always want to end the tween on exactly what they intended even if it doesn't match the beginning value's suffix.

						//if the beginning/ending suffixes don't match, normalize them...
						if (bsfx !== esfx)
							if (esfx !== "")
								if (en || en === 0)
									if (bn) { //note: if the beginning value (bn) is 0, we don't need to convert units!
										bn = _convertToPixels(target, p, bn, bsfx);
										if (esfx === "%") {
											bn /= _convertToPixels(target, p, 100, "%") / 100;
											if (vars.strictUnits !== true) { //some browsers report only "px" values instead of allowing "%" with getComputedStyle(), so we assume that if we're tweening to a %, we should start there too unless strictUnits:true is defined. This approach is particularly useful for responsive designs that use from() tweens.
												bs = bn + "%";
											}

										} else if (esfx === "em" || esfx === "rem" || esfx === "vw" || esfx === "vh") {
											bn /= _convertToPixels(target, p, 1, esfx);

											//otherwise convert to pixels.
										} else if (esfx !== "px") {
											en = _convertToPixels(target, p, en, esfx);
											esfx = "px"; //we don't use bsfx after this, so we don't need to set it to px too.
										}
										if (rel)
											if (en || en === 0) {
												es = (en + bn) + esfx; //the changes we made affect relative calculations, so adjust the end value here.
											}
									}

						if (rel) {
							en += bn;
						}

						if ((bn || bn === 0) && (en || en === 0)) { //faster than isNaN(). Also, previously we required en !== bn but that doesn't really gain much performance and it prevents _parseToProxy() from working properly if beginning and ending values match but need to get tweened by an external plugin anyway. For example, a bezier tween where the target starts at left:0 and has these points: [{left:50},{left:0}] wouldn't work properly because when parsing the last point, it'd match the first (current) one and a non-tweening CSSPropTween would be recorded when we actually need a normal tween (type:0) so that things get updated during the tween properly.
							pt = new CSSPropTween(style, p, bn, en - bn, pt, 0, p, (_autoRound !== false && (esfx === "px" || p === "zIndex")), 0, bs, es);
							pt.xs0 = esfx;
							//DEBUG: _log("tween "+p+" from "+pt.b+" ("+bn+esfx+") to "+pt.e+" with suffix: "+pt.xs0);
						} else if (style[p] === undefined || !es && (es + "" === "NaN" || es == null)) {
							_log("invalid " + p + " tween value: " + vars[p]);
						} else {
							pt = new CSSPropTween(style, p, en || bn || 0, 0, pt, -1, p, false, 0, bs, es);
							pt.xs0 = (es === "none" && (p === "display" || p.indexOf("Style") !== -1)) ? bs : es; //intermediate value should typically be set immediately (end value) except for "display" or things like borderTopStyle, borderBottomStyle, etc. which should use the beginning value during the tween.
							//DEBUG: _log("non-tweening value "+p+": "+pt.xs0);
						}
					}
				}
				if (plugin)
					if (pt && !pt.plugin) {
						pt.plugin = plugin;
					}
			}
			return pt;
		};


		//gets called every time the tween updates, passing the new ratio (typically a value between 0 and 1, but not always (for example, if an Elastic.easeOut is used, the value can jump above 1 mid-tween). It will always start and 0 and end at 1.
		p.setRatio = function (v) {
			var pt = this._firstPT
				, min = 0.000001
				, val, str, i;
			//at the end of the tween, we set the values to exactly what we received in order to make sure non-tweening values (like "position" or "float" or whatever) are set and so that if the beginning/ending suffixes (units) didn't match and we normalized to px, the value that the user passed in is used here. We check to see if the tween is at its beginning in case it's a from() tween in which case the ratio will actually go from 1 to 0 over the course of the tween (backwards).
			if (v === 1 && (this._tween._time === this._tween._duration || this._tween._time === 0)) {
				while (pt) {
					if (pt.type !== 2) {
						if (pt.r && pt.type !== -1) {
							val = Math.round(pt.s + pt.c);
							if (!pt.type) {
								pt.t[pt.p] = val + pt.xs0;
							} else if (pt.type === 1) { //complex value (one that typically has multiple numbers inside a string, like "rect(5px,10px,20px,25px)"
								i = pt.l;
								str = pt.xs0 + val + pt.xs1;
								for (i = 1; i < pt.l; i++) {
									str += pt["xn" + i] + pt["xs" + (i + 1)];
								}
								pt.t[pt.p] = str;
							}
						} else {
							pt.t[pt.p] = pt.e;
						}
					} else {
						pt.setRatio(v);
					}
					pt = pt._next;
				}

			} else if (v || !(this._tween._time === this._tween._duration || this._tween._time === 0) || this._tween._rawPrevTime === -0.000001) {
				while (pt) {
					val = pt.c * v + pt.s;
					if (pt.r) {
						val = Math.round(val);
					} else if (val < min)
						if (val > -min) {
							val = 0;
						}
					if (!pt.type) {
						pt.t[pt.p] = val + pt.xs0;
					} else if (pt.type === 1) { //complex value (one that typically has multiple numbers inside a string, like "rect(5px,10px,20px,25px)"
						i = pt.l;
						if (i === 2) {
							pt.t[pt.p] = pt.xs0 + val + pt.xs1 + pt.xn1 + pt.xs2;
						} else if (i === 3) {
							pt.t[pt.p] = pt.xs0 + val + pt.xs1 + pt.xn1 + pt.xs2 + pt.xn2 + pt.xs3;
						} else if (i === 4) {
							pt.t[pt.p] = pt.xs0 + val + pt.xs1 + pt.xn1 + pt.xs2 + pt.xn2 + pt.xs3 + pt.xn3 + pt.xs4;
						} else if (i === 5) {
							pt.t[pt.p] = pt.xs0 + val + pt.xs1 + pt.xn1 + pt.xs2 + pt.xn2 + pt.xs3 + pt.xn3 + pt.xs4 + pt.xn4 + pt.xs5;
						} else {
							str = pt.xs0 + val + pt.xs1;
							for (i = 1; i < pt.l; i++) {
								str += pt["xn" + i] + pt["xs" + (i + 1)];
							}
							pt.t[pt.p] = str;
						}

					} else if (pt.type === -1) { //non-tweening value
						pt.t[pt.p] = pt.xs0;

					} else if (pt.setRatio) { //custom setRatio() for things like SpecialProps, external plugins, etc.
						pt.setRatio(v);
					}
					pt = pt._next;
				}

				//if the tween is reversed all the way back to the beginning, we need to restore the original values which may have different units (like % instead of px or em or whatever).
			} else {
				while (pt) {
					if (pt.type !== 2) {
						pt.t[pt.p] = pt.b;
					} else {
						pt.setRatio(v);
					}
					pt = pt._next;
				}
			}
		};

		/**
		 * @private
		 * Forces rendering of the target's transforms (rotation, scale, etc.) whenever the CSSPlugin's setRatio() is called.
		 * Basically, this tells the CSSPlugin to create a CSSPropTween (type 2) after instantiation that runs last in the linked
		 * list and calls the appropriate (3D or 2D) rendering function. We separate this into its own method so that we can call
		 * it from other plugins like BezierPlugin if, for example, it needs to apply an autoRotation and this CSSPlugin
		 * doesn't have any transform-related properties of its own. You can call this method as many times as you
		 * want and it won't create duplicate CSSPropTweens.
		 *
		 * @param {boolean} threeD if true, it should apply 3D tweens (otherwise, just 2D ones are fine and typically faster)
		 */
		p._enableTransforms = function (threeD) {
			this._transform = this._transform || _getTransform(this._target, _cs, true); //ensures that the element has a _gsTransform property with the appropriate values.
			this._transformType = (!(this._transform.svg && _useSVGTransformAttr) && (threeD || this._transformType === 3)) ? 3 : 2;
		};

		var lazySet = function (v) {
			this.t[this.p] = this.e;
			this.data._linkCSSP(this, this._next, null, true); //we purposefully keep this._next even though it'd make sense to null it, but this is a performance optimization, as this happens during the while (pt) {} loop in setRatio() at the bottom of which it sets pt = pt._next, so if we null it, the linked list will be broken in that loop.
		};
		/** @private Gives us a way to set a value on the first render (and only the first render). **/
		p._addLazySet = function (t, p, v) {
			var pt = this._firstPT = new CSSPropTween(t, p, 0, 0, this._firstPT, 2);
			pt.e = v;
			pt.setRatio = lazySet;
			pt.data = this;
		};

		/** @private **/
		p._linkCSSP = function (pt, next, prev, remove) {
			if (pt) {
				if (next) {
					next._prev = pt;
				}
				if (pt._next) {
					pt._next._prev = pt._prev;
				}
				if (pt._prev) {
					pt._prev._next = pt._next;
				} else if (this._firstPT === pt) {
					this._firstPT = pt._next;
					remove = true; //just to prevent resetting this._firstPT 5 lines down in case pt._next is null. (optimized for speed)
				}
				if (prev) {
					prev._next = pt;
				} else if (!remove && this._firstPT === null) {
					this._firstPT = pt;
				}
				pt._next = next;
				pt._prev = prev;
			}
			return pt;
		};

		//we need to make sure that if alpha or autoAlpha is killed, opacity is too. And autoAlpha affects the "visibility" property.
		p._kill = function (lookup) {
			var copy = lookup
				, pt, p, xfirst;
			if (lookup.autoAlpha || lookup.alpha) {
				copy = {};
				for (p in lookup) { //copy the lookup so that we're not changing the original which may be passed elsewhere.
					copy[p] = lookup[p];
				}
				copy.opacity = 1;
				if (copy.autoAlpha) {
					copy.visibility = 1;
				}
			}
			if (lookup.className && (pt = this._classNamePT)) { //for className tweens, we need to kill any associated CSSPropTweens too; a linked list starts at the className's "xfirst".
				xfirst = pt.xfirst;
				if (xfirst && xfirst._prev) {
					this._linkCSSP(xfirst._prev, pt._next, xfirst._prev._prev); //break off the prev
				} else if (xfirst === this._firstPT) {
					this._firstPT = pt._next;
				}
				if (pt._next) {
					this._linkCSSP(pt._next, pt._next._next, xfirst._prev);
				}
				this._classNamePT = null;
			}
			return TweenPlugin.prototype._kill.call(this, copy);
		};



		//used by cascadeTo() for gathering all the style properties of each child element into an array for comparison.
		var _getChildStyles = function (e, props, targets) {
			var children, i, child, type;
			if (e.slice) {
				i = e.length;
				while (--i > -1) {
					_getChildStyles(e[i], props, targets);
				}
				return;
			}
			children = e.childNodes;
			i = children.length;
			while (--i > -1) {
				child = children[i];
				type = child.type;
				if (child.style) {
					props.push(_getAllStyles(child));
					if (targets) {
						targets.push(child);
					}
				}
				if ((type === 1 || type === 9 || type === 11) && child.childNodes.length) {
					_getChildStyles(child, props, targets);
				}
			}
		};

		/**
		 * Typically only useful for className tweens that may affect child elements, this method creates a TweenLite
		 * and then compares the style properties of all the target's child elements at the tween's start and end, and
		 * if any are different, it also creates tweens for those and returns an array containing ALL of the resulting
		 * tweens (so that you can easily add() them to a TimelineLite, for example). The reason this functionality is
		 * wrapped into a separate static method of CSSPlugin instead of being integrated into all regular className tweens
		 * is because it creates entirely new tweens that may have completely different targets than the original tween,
		 * so if they were all lumped into the original tween instance, it would be inconsistent with the rest of the API
		 * and it would create other problems. For example:
		 *  - If I create a tween of elementA, that tween instance may suddenly change its target to include 50 other elements (unintuitive if I specifically defined the target I wanted)
		 *  - We can't just create new independent tweens because otherwise, what happens if the original/parent tween is reversed or pause or dropped into a TimelineLite for tight control? You'd expect that tween's behavior to affect all the others.
		 *  - Analyzing every style property of every child before and after the tween is an expensive operation when there are many children, so this behavior shouldn't be imposed on all className tweens by default, especially since it's probably rare that this extra functionality is needed.
		 *
		 * @param {Object} target object to be tweened
		 * @param {number} Duration in seconds (or frames for frames-based tweens)
		 * @param {Object} Object containing the end values, like {className:"newClass", ease:Linear.easeNone}
		 * @return {Array} An array of TweenLite instances
		 */
		CSSPlugin.cascadeTo = function (target, duration, vars) {
			var tween = TweenLite.to(target, duration, vars)
				, results = [tween]
				, b = []
				, e = []
				, targets = []
				, _reservedProps = TweenLite._internals.reservedProps
				, i, difs, p, from;
			target = tween._targets || tween.target;
			_getChildStyles(target, b, targets);
			tween.render(duration, true, true);
			_getChildStyles(target, e);
			tween.render(0, true, true);
			tween._enabled(true);
			i = targets.length;
			while (--i > -1) {
				difs = _cssDif(targets[i], b[i], e[i]);
				if (difs.firstMPT) {
					difs = difs.difs;
					for (p in vars) {
						if (_reservedProps[p]) {
							difs[p] = vars[p];
						}
					}
					from = {};
					for (p in difs) {
						from[p] = b[i][p];
					}
					results.push(TweenLite.fromTo(targets[i], duration, from, difs));
				}
			}
			return results;
		};

		TweenPlugin.activate([CSSPlugin]);
		return CSSPlugin;

	}, true);









	/*
	 * ----------------------------------------------------------------
	 * RoundPropsPlugin
	 * ----------------------------------------------------------------
	 */
	(function () {

		var RoundPropsPlugin = _gsScope._gsDefine.plugin({
			propName: "roundProps"
			, version: "1.5"
			, priority: -1
			, API: 2,

			//called when the tween renders for the first time. This is where initial values should be recorded and any setup routines should run.
			init: function (target, value, tween) {
				this._tween = tween;
				return true;
			}

		})
			, _roundLinkedList = function (node) {
			while (node) {
				if (!node.f && !node.blob) {
					node.r = 1;
				}
				node = node._next;
			}
		}
			, p = RoundPropsPlugin.prototype;

		p._onInitAllProps = function () {
			var tween = this._tween
				, rp = (tween.vars.roundProps.join) ? tween.vars.roundProps : tween.vars.roundProps.split(",")
				, i = rp.length
				, lookup = {}
				, rpt = tween._propLookup.roundProps
				, prop, pt, next;
			while (--i > -1) {
				lookup[rp[i]] = 1;
			}
			i = rp.length;
			while (--i > -1) {
				prop = rp[i];
				pt = tween._firstPT;
				while (pt) {
					next = pt._next; //record here, because it may get removed
					if (pt.pg) {
						pt.t._roundProps(lookup, true);
					} else if (pt.n === prop) {
						if (pt.f === 2 && pt.t) { //a blob (text containing multiple numeric values)
							_roundLinkedList(pt.t._firstPT);
						} else {
							this._add(pt.t, prop, pt.s, pt.c);
							//remove from linked list
							if (next) {
								next._prev = pt._prev;
							}
							if (pt._prev) {
								pt._prev._next = next;
							} else if (tween._firstPT === pt) {
								tween._firstPT = next;
							}
							pt._next = pt._prev = null;
							tween._propLookup[prop] = rpt;
						}
					}
					pt = next;
				}
			}
			return false;
		};

		p._add = function (target, p, s, c) {
			this._addTween(target, p, s, s + c, p, true);
			this._overwriteProps.push(p);
		};

	}());









	/*
	 * ----------------------------------------------------------------
	 * AttrPlugin
	 * ----------------------------------------------------------------
	 */

	(function () {

		_gsScope._gsDefine.plugin({
			propName: "attr"
			, API: 2
			, version: "0.5.0",

			//called when the tween renders for the first time. This is where initial values should be recorded and any setup routines should run.
			init: function (target, value, tween) {
				var p;
				if (typeof (target.setAttribute) !== "function") {
					return false;
				}
				for (p in value) {
					this._addTween(target, "setAttribute", target.getAttribute(p) + "", value[p] + "", p, false, p);
					this._overwriteProps.push(p);
				}
				return true;
			}

		});

	}());









	/*
	 * ----------------------------------------------------------------
	 * DirectionalRotationPlugin
	 * ----------------------------------------------------------------
	 */
	_gsScope._gsDefine.plugin({
		propName: "directionalRotation"
		, version: "0.2.1"
		, API: 2,

		//called when the tween renders for the first time. This is where initial values should be recorded and any setup routines should run.
		init: function (target, value, tween) {
			if (typeof (value) !== "object") {
				value = {
					rotation: value
				};
			}
			this.finals = {};
			var cap = (value.useRadians === true) ? Math.PI * 2 : 360
				, min = 0.000001
				, p, v, start, end, dif, split;
			for (p in value) {
				if (p !== "useRadians") {
					split = (value[p] + "").split("_");
					v = split[0];
					start = parseFloat((typeof (target[p]) !== "function") ? target[p] : target[((p.indexOf("set") || typeof (target["get" + p.substr(3)]) !== "function") ? p : "get" + p.substr(3))]());
					end = this.finals[p] = (typeof (v) === "string" && v.charAt(1) === "=") ? start + parseInt(v.charAt(0) + "1", 10) * Number(v.substr(2)) : Number(v) || 0;
					dif = end - start;
					if (split.length) {
						v = split.join("_");
						if (v.indexOf("short") !== -1) {
							dif = dif % cap;
							if (dif !== dif % (cap / 2)) {
								dif = (dif < 0) ? dif + cap : dif - cap;
							}
						}
						if (v.indexOf("_cw") !== -1 && dif < 0) {
							dif = ((dif + cap * 9999999999) % cap) - ((dif / cap) | 0) * cap;
						} else if (v.indexOf("ccw") !== -1 && dif > 0) {
							dif = ((dif - cap * 9999999999) % cap) - ((dif / cap) | 0) * cap;
						}
					}
					if (dif > min || dif < -min) {
						this._addTween(target, p, start, start + dif, p);
						this._overwriteProps.push(p);
					}
				}
			}
			return true;
		},

		//called each time the values should be updated, and the ratio gets passed as the only parameter (typically it's a value between 0 and 1, but it can exceed those when using an ease like Elastic.easeOut or Back.easeOut, etc.)
		set: function (ratio) {
			var pt;
			if (ratio !== 1) {
				this._super.setRatio.call(this, ratio);
			} else {
				pt = this._firstPT;
				while (pt) {
					if (pt.f) {
						pt.t[pt.p](this.finals[pt.p]);
					} else {
						pt.t[pt.p] = this.finals[pt.p];
					}
					pt = pt._next;
				}
			}
		}

	})._autoCSS = true;









	/*
	 * ----------------------------------------------------------------
	 * EasePack
	 * ----------------------------------------------------------------
	 */
	_gsScope._gsDefine("easing.Back", ["easing.Ease"], function (Ease) {

		var w = (_gsScope.GreenSockGlobals || _gsScope)
			, gs = w.com.greensock
			, _2PI = Math.PI * 2
			, _HALF_PI = Math.PI / 2
			, _class = gs._class
			, _create = function (n, f) {
				var C = _class("easing." + n, function () {}, true)
					, p = C.prototype = new Ease();
				p.constructor = C;
				p.getRatio = f;
				return C;
			}
			, _easeReg = Ease.register || function () {}, //put an empty function in place just as a safety measure in case someone loads an OLD version of TweenLite.js where Ease.register doesn't exist.
			_wrap = function (name, EaseOut, EaseIn, EaseInOut, aliases) {
				var C = _class("easing." + name, {
					easeOut: new EaseOut()
					, easeIn: new EaseIn()
					, easeInOut: new EaseInOut()
				}, true);
				_easeReg(C, name);
				return C;
			}
			, EasePoint = function (time, value, next) {
				this.t = time;
				this.v = value;
				if (next) {
					this.next = next;
					next.prev = this;
					this.c = next.v - value;
					this.gap = next.t - time;
				}
			},

		//Back
			_createBack = function (n, f) {
				var C = _class("easing." + n, function (overshoot) {
					this._p1 = (overshoot || overshoot === 0) ? overshoot : 1.70158;
					this._p2 = this._p1 * 1.525;
				}, true)
					, p = C.prototype = new Ease();
				p.constructor = C;
				p.getRatio = f;
				p.config = function (overshoot) {
					return new C(overshoot);
				};
				return C;
			},

			Back = _wrap("Back"
				, _createBack("BackOut", function (p) {
					return ((p = p - 1) * p * ((this._p1 + 1) * p + this._p1) + 1);
				})
				, _createBack("BackIn", function (p) {
					return p * p * ((this._p1 + 1) * p - this._p1);
				})
				, _createBack("BackInOut", function (p) {
					return ((p *= 2) < 1) ? 0.5 * p * p * ((this._p2 + 1) * p - this._p2) : 0.5 * ((p -= 2) * p * ((this._p2 + 1) * p + this._p2) + 2);
				})
			),


		//SlowMo
			SlowMo = _class("easing.SlowMo", function (linearRatio, power, yoyoMode) {
				power = (power || power === 0) ? power : 0.7;
				if (linearRatio == null) {
					linearRatio = 0.7;
				} else if (linearRatio > 1) {
					linearRatio = 1;
				}
				this._p = (linearRatio !== 1) ? power : 0;
				this._p1 = (1 - linearRatio) / 2;
				this._p2 = linearRatio;
				this._p3 = this._p1 + this._p2;
				this._calcEnd = (yoyoMode === true);
			}, true)
			, p = SlowMo.prototype = new Ease()
			, SteppedEase, RoughEase, _createElastic;

		p.constructor = SlowMo;
		p.getRatio = function (p) {
			var r = p + (0.5 - p) * this._p;
			if (p < this._p1) {
				return this._calcEnd ? 1 - ((p = 1 - (p / this._p1)) * p) : r - ((p = 1 - (p / this._p1)) * p * p * p * r);
			} else if (p > this._p3) {
				return this._calcEnd ? 1 - (p = (p - this._p3) / this._p1) * p : r + ((p - r) * (p = (p - this._p3) / this._p1) * p * p * p);
			}
			return this._calcEnd ? 1 : r;
		};
		SlowMo.ease = new SlowMo(0.7, 0.7);

		p.config = SlowMo.config = function (linearRatio, power, yoyoMode) {
			return new SlowMo(linearRatio, power, yoyoMode);
		};


		//SteppedEase
		SteppedEase = _class("easing.SteppedEase", function (steps) {
			steps = steps || 1;
			this._p1 = 1 / steps;
			this._p2 = steps + 1;
		}, true);
		p = SteppedEase.prototype = new Ease();
		p.constructor = SteppedEase;
		p.getRatio = function (p) {
			if (p < 0) {
				p = 0;
			} else if (p >= 1) {
				p = 0.999999999;
			}
			return ((this._p2 * p) >> 0) * this._p1;
		};
		p.config = SteppedEase.config = function (steps) {
			return new SteppedEase(steps);
		};


		//RoughEase
		RoughEase = _class("easing.RoughEase", function (vars) {
			vars = vars || {};
			var taper = vars.taper || "none"
				, a = []
				, cnt = 0
				, points = (vars.points || 20) | 0
				, i = points
				, randomize = (vars.randomize !== false)
				, clamp = (vars.clamp === true)
				, template = (vars.template instanceof Ease) ? vars.template : null
				, strength = (typeof (vars.strength) === "number") ? vars.strength * 0.4 : 0.4
				, x, y, bump, invX, obj, pnt;
			while (--i > -1) {
				x = randomize ? Math.random() : (1 / points) * i;
				y = template ? template.getRatio(x) : x;
				if (taper === "none") {
					bump = strength;
				} else if (taper === "out") {
					invX = 1 - x;
					bump = invX * invX * strength;
				} else if (taper === "in") {
					bump = x * x * strength;
				} else if (x < 0.5) { //"both" (start)
					invX = x * 2;
					bump = invX * invX * 0.5 * strength;
				} else { //"both" (end)
					invX = (1 - x) * 2;
					bump = invX * invX * 0.5 * strength;
				}
				if (randomize) {
					y += (Math.random() * bump) - (bump * 0.5);
				} else if (i % 2) {
					y += bump * 0.5;
				} else {
					y -= bump * 0.5;
				}
				if (clamp) {
					if (y > 1) {
						y = 1;
					} else if (y < 0) {
						y = 0;
					}
				}
				a[cnt++] = {
					x: x
					, y: y
				};
			}
			a.sort(function (a, b) {
				return a.x - b.x;
			});

			pnt = new EasePoint(1, 1, null);
			i = points;
			while (--i > -1) {
				obj = a[i];
				pnt = new EasePoint(obj.x, obj.y, pnt);
			}

			this._prev = new EasePoint(0, 0, (pnt.t !== 0) ? pnt : pnt.next);
		}, true);
		p = RoughEase.prototype = new Ease();
		p.constructor = RoughEase;
		p.getRatio = function (p) {
			var pnt = this._prev;
			if (p > pnt.t) {
				while (pnt.next && p >= pnt.t) {
					pnt = pnt.next;
				}
				pnt = pnt.prev;
			} else {
				while (pnt.prev && p <= pnt.t) {
					pnt = pnt.prev;
				}
			}
			this._prev = pnt;
			return (pnt.v + ((p - pnt.t) / pnt.gap) * pnt.c);
		};
		p.config = function (vars) {
			return new RoughEase(vars);
		};
		RoughEase.ease = new RoughEase();


		//Bounce
		_wrap("Bounce"
			, _create("BounceOut", function (p) {
				if (p < 1 / 2.75) {
					return 7.5625 * p * p;
				} else if (p < 2 / 2.75) {
					return 7.5625 * (p -= 1.5 / 2.75) * p + 0.75;
				} else if (p < 2.5 / 2.75) {
					return 7.5625 * (p -= 2.25 / 2.75) * p + 0.9375;
				}
				return 7.5625 * (p -= 2.625 / 2.75) * p + 0.984375;
			})
			, _create("BounceIn", function (p) {
				if ((p = 1 - p) < 1 / 2.75) {
					return 1 - (7.5625 * p * p);
				} else if (p < 2 / 2.75) {
					return 1 - (7.5625 * (p -= 1.5 / 2.75) * p + 0.75);
				} else if (p < 2.5 / 2.75) {
					return 1 - (7.5625 * (p -= 2.25 / 2.75) * p + 0.9375);
				}
				return 1 - (7.5625 * (p -= 2.625 / 2.75) * p + 0.984375);
			})
			, _create("BounceInOut", function (p) {
				var invert = (p < 0.5);
				if (invert) {
					p = 1 - (p * 2);
				} else {
					p = (p * 2) - 1;
				}
				if (p < 1 / 2.75) {
					p = 7.5625 * p * p;
				} else if (p < 2 / 2.75) {
					p = 7.5625 * (p -= 1.5 / 2.75) * p + 0.75;
				} else if (p < 2.5 / 2.75) {
					p = 7.5625 * (p -= 2.25 / 2.75) * p + 0.9375;
				} else {
					p = 7.5625 * (p -= 2.625 / 2.75) * p + 0.984375;
				}
				return invert ? (1 - p) * 0.5 : p * 0.5 + 0.5;
			})
		);


		//CIRC
		_wrap("Circ"
			, _create("CircOut", function (p) {
				return Math.sqrt(1 - (p = p - 1) * p);
			})
			, _create("CircIn", function (p) {
				return -(Math.sqrt(1 - (p * p)) - 1);
			})
			, _create("CircInOut", function (p) {
				return ((p *= 2) < 1) ? -0.5 * (Math.sqrt(1 - p * p) - 1) : 0.5 * (Math.sqrt(1 - (p -= 2) * p) + 1);
			})
		);


		//Elastic
		_createElastic = function (n, f, def) {
			var C = _class("easing." + n, function (amplitude, period) {
				this._p1 = (amplitude >= 1) ? amplitude : 1; //note: if amplitude is < 1, we simply adjust the period for a more natural feel. Otherwise the math doesn't work right and the curve starts at 1.
				this._p2 = (period || def) / (amplitude < 1 ? amplitude : 1);
				this._p3 = this._p2 / _2PI * (Math.asin(1 / this._p1) || 0);
				this._p2 = _2PI / this._p2; //precalculate to optimize
			}, true)
				, p = C.prototype = new Ease();
			p.constructor = C;
			p.getRatio = f;
			p.config = function (amplitude, period) {
				return new C(amplitude, period);
			};
			return C;
		};
		_wrap("Elastic"
			, _createElastic("ElasticOut", function (p) {
				return this._p1 * Math.pow(2, -10 * p) * Math.sin((p - this._p3) * this._p2) + 1;
			}, 0.3)
			, _createElastic("ElasticIn", function (p) {
				return -(this._p1 * Math.pow(2, 10 * (p -= 1)) * Math.sin((p - this._p3) * this._p2));
			}, 0.3)
			, _createElastic("ElasticInOut", function (p) {
				return ((p *= 2) < 1) ? -0.5 * (this._p1 * Math.pow(2, 10 * (p -= 1)) * Math.sin((p - this._p3) * this._p2)) : this._p1 * Math.pow(2, -10 * (p -= 1)) * Math.sin((p - this._p3) * this._p2) * 0.5 + 1;
			}, 0.45)
		);


		//Expo
		_wrap("Expo"
			, _create("ExpoOut", function (p) {
				return 1 - Math.pow(2, -10 * p);
			})
			, _create("ExpoIn", function (p) {
				return Math.pow(2, 10 * (p - 1)) - 0.001;
			})
			, _create("ExpoInOut", function (p) {
				return ((p *= 2) < 1) ? 0.5 * Math.pow(2, 10 * (p - 1)) : 0.5 * (2 - Math.pow(2, -10 * (p - 1)));
			})
		);


		//Sine
		_wrap("Sine"
			, _create("SineOut", function (p) {
				return Math.sin(p * _HALF_PI);
			})
			, _create("SineIn", function (p) {
				return -Math.cos(p * _HALF_PI) + 1;
			})
			, _create("SineInOut", function (p) {
				return -0.5 * (Math.cos(Math.PI * p) - 1);
			})
		);

		_class("easing.EaseLookup", {
			find: function (s) {
				return Ease.map[s];
			}
		}, true);

		//register the non-standard eases
		_easeReg(w.SlowMo, "SlowMo", "ease,");
		_easeReg(RoughEase, "RoughEase", "ease,");
		_easeReg(SteppedEase, "SteppedEase", "ease,");

		return Back;

	}, true);


});

if (_gsScope._gsDefine) {
	_gsScope._gsQueue.pop()();
} //necessary in case TweenLite was already loaded separately.









/*
 * ----------------------------------------------------------------
 * Base classes like TweenLite, SimpleTimeline, Ease, Ticker, etc.
 * ----------------------------------------------------------------
 */
(function (window, moduleName) {

	"use strict";
	var _globals = window.GreenSockGlobals = window.GreenSockGlobals || window;
	if (_globals.TweenLite) {
		return; //in case the core set of classes is already loaded, don't instantiate twice.
	}
	var _namespace = function (ns) {
			var a = ns.split(".")
				, p = _globals
				, i;
			for (i = 0; i < a.length; i++) {
				p[a[i]] = p = p[a[i]] || {};
			}
			return p;
		}
		, gs = _namespace("com.greensock")
		, _tinyNum = 0.0000000001
		, _slice = function (a) { //don't use Array.prototype.slice.call(target, 0) because that doesn't work in IE8 with a NodeList that's returned by querySelectorAll()
			var b = []
				, l = a.length
				, i;
			for (i = 0; i !== l; b.push(a[i++])) {}
			return b;
		}
		, _emptyFunc = function () {}
		, _isArray = (function () { //works around issues in iframe environments where the Array global isn't shared, thus if the object originates in a different window/iframe, "(obj instanceof Array)" will evaluate false. We added some speed optimizations to avoid Object.prototype.toString.call() unless it's absolutely necessary because it's VERY slow (like 20x slower)
			var toString = Object.prototype.toString
				, array = toString.call([]);
			return function (obj) {
				return obj != null && (obj instanceof Array || (typeof (obj) === "object" && !!obj.push && toString.call(obj) === array));
			};
		}())
		, a, i, p, _ticker, _tickerActive
		, _defLookup = {},

		/**
		 * @constructor
		 * Defines a GreenSock class, optionally with an array of dependencies that must be instantiated first and passed into the definition.
		 * This allows users to load GreenSock JS files in any order even if they have interdependencies (like CSSPlugin extends TweenPlugin which is
		 * inside TweenLite.js, but if CSSPlugin is loaded first, it should wait to run its code until TweenLite.js loads and instantiates TweenPlugin
		 * and then pass TweenPlugin to CSSPlugin's definition). This is all done automatically and internally.
		 *
		 * Every definition will be added to a "com.greensock" global object (typically window, but if a window.GreenSockGlobals object is found,
		 * it will go there as of v1.7). For example, TweenLite will be found at window.com.greensock.TweenLite and since it's a global class that should be available anywhere,
		 * it is ALSO referenced at window.TweenLite. However some classes aren't considered global, like the base com.greensock.core.Animation class, so
		 * those will only be at the package like window.com.greensock.core.Animation. Again, if you define a GreenSockGlobals object on the window, everything
		 * gets tucked neatly inside there instead of on the window directly. This allows you to do advanced things like load multiple versions of GreenSock
		 * files and put them into distinct objects (imagine a banner ad uses a newer version but the main site uses an older one). In that case, you could
		 * sandbox the banner one like:
		 *
		 * <script>
		 *     var gs = window.GreenSockGlobals = {}; //the newer version we're about to load could now be referenced in a "gs" object, like gs.TweenLite.to(...). Use whatever alias you want as long as it's unique, "gs" or "banner" or whatever.
		 * </script>
		 * <script src="js/greensock/v1.7/TweenMax.js"></script>
		 * <script>
		 *     window.GreenSockGlobals = window._gsQueue = window._gsDefine = null; //reset it back to null (along with the special _gsQueue variable) so that the next load of TweenMax affects the window and we can reference things directly like TweenLite.to(...)
		 * </script>
		 * <script src="js/greensock/v1.6/TweenMax.js"></script>
		 * <script>
		 *     gs.TweenLite.to(...); //would use v1.7
		 *     TweenLite.to(...); //would use v1.6
		 * </script>
		 *
		 * @param {!string} ns The namespace of the class definition, leaving off "com.greensock." as that's assumed. For example, "TweenLite" or "plugins.CSSPlugin" or "easing.Back".
		 * @param {!Array.<string>} dependencies An array of dependencies (described as their namespaces minus "com.greensock." prefix). For example ["TweenLite","plugins.TweenPlugin","core.Animation"]
		 * @param {!function():Object} func The function that should be called and passed the resolved dependencies which will return the actual class for this definition.
		 * @param {boolean=} global If true, the class will be added to the global scope (typically window unless you define a window.GreenSockGlobals object)
		 */
		Definition = function (ns, dependencies, func, global) {
			this.sc = (_defLookup[ns]) ? _defLookup[ns].sc : []; //subclasses
			_defLookup[ns] = this;
			this.gsClass = null;
			this.func = func;
			var _classes = [];
			this.check = function (init) {
				var i = dependencies.length
					, missing = i
					, cur, a, n, cl, hasModule;
				while (--i > -1) {
					if ((cur = _defLookup[dependencies[i]] || new Definition(dependencies[i], [])).gsClass) {
						_classes[i] = cur.gsClass;
						missing--;
					} else if (init) {
						cur.sc.push(this);
					}
				}
				if (missing === 0 && func) {
					a = ("com.greensock." + ns).split(".");
					n = a.pop();
					cl = _namespace(a.join("."))[n] = this.gsClass = func.apply(func, _classes);

					//exports to multiple environments
					if (global) {
						_globals[n] = cl; //provides a way to avoid global namespace pollution. By default, the main classes like TweenLite, Power1, Strong, etc. are added to window unless a GreenSockGlobals is defined. So if you want to have things added to a custom object instead, just do something like window.GreenSockGlobals = {} before loading any GreenSock files. You can even set up an alias like window.GreenSockGlobals = windows.gs = {} so that you can access everything like gs.TweenLite. Also remember that ALL classes are added to the window.com.greensock object (in their respective packages, like com.greensock.easing.Power1, com.greensock.TweenLite, etc.)
						hasModule = (typeof (module) !== "undefined" && module.exports);
						if (!hasModule && typeof (define) === "function" && define.amd) { //AMD
							define((window.GreenSockAMDPath ? window.GreenSockAMDPath + "/" : "") + ns.split(".").pop(), [], function () {
								return cl;
							});
						} else if (ns === moduleName && hasModule) { //node
							module.exports = cl;
						}
					}
					for (i = 0; i < this.sc.length; i++) {
						this.sc[i].check();
					}
				}
			};
			this.check(true);
		},

	//used to create Definition instances (which basically registers a class that has dependencies).
		_gsDefine = window._gsDefine = function (ns, dependencies, func, global) {
			return new Definition(ns, dependencies, func, global);
		},

	//a quick way to create a class that doesn't have any dependencies. Returns the class, but first registers it in the GreenSock namespace so that other classes can grab it (other classes might be dependent on the class).
		_class = gs._class = function (ns, func, global) {
			func = func || function () {};
			_gsDefine(ns, [], function () {
				return func;
			}, global);
			return func;
		};

	_gsDefine.globals = _globals;



	/*
	 * ----------------------------------------------------------------
	 * Ease
	 * ----------------------------------------------------------------
	 */
	var _baseParams = [0, 0, 1, 1]
		, _blankArray = []
		, Ease = _class("easing.Ease", function (func, extraParams, type, power) {
		this._func = func;
		this._type = type || 0;
		this._power = power || 0;
		this._params = extraParams ? _baseParams.concat(extraParams) : _baseParams;
	}, true)
		, _easeMap = Ease.map = {}
		, _easeReg = Ease.register = function (ease, names, types, create) {
		var na = names.split(",")
			, i = na.length
			, ta = (types || "easeIn,easeOut,easeInOut").split(",")
			, e, name, j, type;
		while (--i > -1) {
			name = na[i];
			e = create ? _class("easing." + name, null, true) : gs.easing[name] || {};
			j = ta.length;
			while (--j > -1) {
				type = ta[j];
				_easeMap[name + "." + type] = _easeMap[type + name] = e[type] = ease.getRatio ? ease : ease[type] || new ease();
			}
		}
	};

	p = Ease.prototype;
	p._calcEnd = false;
	p.getRatio = function (p) {
		if (this._func) {
			this._params[0] = p;
			return this._func.apply(null, this._params);
		}
		var t = this._type
			, pw = this._power
			, r = (t === 1) ? 1 - p : (t === 2) ? p : (p < 0.5) ? p * 2 : (1 - p) * 2;
		if (pw === 1) {
			r *= r;
		} else if (pw === 2) {
			r *= r * r;
		} else if (pw === 3) {
			r *= r * r * r;
		} else if (pw === 4) {
			r *= r * r * r * r;
		}
		return (t === 1) ? 1 - r : (t === 2) ? r : (p < 0.5) ? r / 2 : 1 - (r / 2);
	};

	//create all the standard eases like Linear, Quad, Cubic, Quart, Quint, Strong, Power0, Power1, Power2, Power3, and Power4 (each with easeIn, easeOut, and easeInOut)
	a = ["Linear", "Quad", "Cubic", "Quart", "Quint,Strong"];
	i = a.length;
	while (--i > -1) {
		p = a[i] + ",Power" + i;
		_easeReg(new Ease(null, null, 1, i), p, "easeOut", true);
		_easeReg(new Ease(null, null, 2, i), p, "easeIn" + ((i === 0) ? ",easeNone" : ""));
		_easeReg(new Ease(null, null, 3, i), p, "easeInOut");
	}
	_easeMap.linear = gs.easing.Linear.easeIn;
	_easeMap.swing = gs.easing.Quad.easeInOut; //for jQuery folks


	/*
	 * ----------------------------------------------------------------
	 * EventDispatcher
	 * ----------------------------------------------------------------
	 */
	var EventDispatcher = _class("events.EventDispatcher", function (target) {
		this._listeners = {};
		this._eventTarget = target || this;
	});
	p = EventDispatcher.prototype;

	p.addEventListener = function (type, callback, scope, useParam, priority) {
		priority = priority || 0;
		var list = this._listeners[type]
			, index = 0
			, listener, i;
		if (list == null) {
			this._listeners[type] = list = [];
		}
		i = list.length;
		while (--i > -1) {
			listener = list[i];
			if (listener.c === callback && listener.s === scope) {
				list.splice(i, 1);
			} else if (index === 0 && listener.pr < priority) {
				index = i + 1;
			}
		}
		list.splice(index, 0, {
			c: callback
			, s: scope
			, up: useParam
			, pr: priority
		});
		if (this === _ticker && !_tickerActive) {
			_ticker.wake();
		}
	};

	p.removeEventListener = function (type, callback) {
		var list = this._listeners[type]
			, i;
		if (list) {
			i = list.length;
			while (--i > -1) {
				if (list[i].c === callback) {
					list.splice(i, 1);
					return;
				}
			}
		}
	};

	p.dispatchEvent = function (type) {
		var list = this._listeners[type]
			, i, t, listener;
		if (list) {
			i = list.length;
			t = this._eventTarget;
			while (--i > -1) {
				listener = list[i];
				if (listener) {
					if (listener.up) {
						listener.c.call(listener.s || t, {
							type: type
							, target: t
						});
					} else {
						listener.c.call(listener.s || t);
					}
				}
			}
		}
	};


	/*
	 * ----------------------------------------------------------------
	 * Ticker
	 * ----------------------------------------------------------------
	 */
	var _reqAnimFrame = window.requestAnimationFrame
		, _cancelAnimFrame = window.cancelAnimationFrame
		, _getTime = Date.now || function () {
			return new Date().getTime();
		}
		, _lastUpdate = _getTime();

	//now try to determine the requestAnimationFrame and cancelAnimationFrame functions and if none are found, we'll use a setTimeout()/clearTimeout() polyfill.
	a = ["ms", "moz", "webkit", "o"];
	i = a.length;
	while (--i > -1 && !_reqAnimFrame) {
		_reqAnimFrame = window[a[i] + "RequestAnimationFrame"];
		_cancelAnimFrame = window[a[i] + "CancelAnimationFrame"] || window[a[i] + "CancelRequestAnimationFrame"];
	}

	_class("Ticker", function (fps, useRAF) {
		var _self = this
			, _startTime = _getTime()
			, _useRAF = (useRAF !== false && _reqAnimFrame) ? "auto" : false
			, _lagThreshold = 500
			, _adjustedLag = 33
			, _tickWord = "tick", //helps reduce gc burden
			_fps, _req, _id, _gap, _nextTime
			, _tick = function (manual) {
				var elapsed = _getTime() - _lastUpdate
					, overlap, dispatch;
				if (elapsed > _lagThreshold) {
					_startTime += elapsed - _adjustedLag;
				}
				_lastUpdate += elapsed;
				_self.time = (_lastUpdate - _startTime) / 1000;
				overlap = _self.time - _nextTime;
				if (!_fps || overlap > 0 || manual === true) {
					_self.frame++;
					_nextTime += overlap + (overlap >= _gap ? 0.004 : _gap - overlap);
					dispatch = true;
				}
				if (manual !== true) { //make sure the request is made before we dispatch the "tick" event so that timing is maintained. Otherwise, if processing the "tick" requires a bunch of time (like 15ms) and we're using a setTimeout() that's based on 16.7ms, it'd technically take 31.7ms between frames otherwise.
					_id = _req(_tick);
				}
				if (dispatch) {
					_self.dispatchEvent(_tickWord);
				}
			};

		EventDispatcher.call(_self);
		_self.time = _self.frame = 0;
		_self.tick = function () {
			_tick(true);
		};

		_self.lagSmoothing = function (threshold, adjustedLag) {
			_lagThreshold = threshold || (1 / _tinyNum); //zero should be interpreted as basically unlimited
			_adjustedLag = Math.min(adjustedLag, _lagThreshold, 0);
		};

		_self.sleep = function () {
			if (_id == null) {
				return;
			}
			if (!_useRAF || !_cancelAnimFrame) {
				clearTimeout(_id);
			} else {
				_cancelAnimFrame(_id);
			}
			_req = _emptyFunc;
			_id = null;
			if (_self === _ticker) {
				_tickerActive = false;
			}
		};

		_self.wake = function (seamless) {
			if (_id !== null) {
				_self.sleep();
			} else if (seamless) {
				_startTime += -_lastUpdate + (_lastUpdate = _getTime());
			} else if (_self.frame > 10) { //don't trigger lagSmoothing if we're just waking up, and make sure that at least 10 frames have elapsed because of the iOS bug that we work around below with the 1.5-second setTimout().
				_lastUpdate = _getTime() - _lagThreshold + 5;
			}
			_req = (_fps === 0) ? _emptyFunc : (!_useRAF || !_reqAnimFrame) ? function (f) {
				return setTimeout(f, ((_nextTime - _self.time) * 1000 + 1) | 0);
			} : _reqAnimFrame;
			if (_self === _ticker) {
				_tickerActive = true;
			}
			_tick(2);
		};

		_self.fps = function (value) {
			if (!arguments.length) {
				return _fps;
			}
			_fps = value;
			_gap = 1 / (_fps || 60);
			_nextTime = this.time + _gap;
			_self.wake();
		};

		_self.useRAF = function (value) {
			if (!arguments.length) {
				return _useRAF;
			}
			_self.sleep();
			_useRAF = value;
			_self.fps(_fps);
		};
		_self.fps(fps);

		//a bug in iOS 6 Safari occasionally prevents the requestAnimationFrame from working initially, so we use a 1.5-second timeout that automatically falls back to setTimeout() if it senses this condition.
		setTimeout(function () {
			if (_useRAF === "auto" && _self.frame < 5 && document.visibilityState !== "hidden") {
				_self.useRAF(false);
			}
		}, 1500);
	});

	p = gs.Ticker.prototype = new gs.events.EventDispatcher();
	p.constructor = gs.Ticker;


	/*
	 * ----------------------------------------------------------------
	 * Animation
	 * ----------------------------------------------------------------
	 */
	var Animation = _class("core.Animation", function (duration, vars) {
		this.vars = vars = vars || {};
		this._duration = this._totalDuration = duration || 0;
		this._delay = Number(vars.delay) || 0;
		this._timeScale = 1;
		this._active = (vars.immediateRender === true);
		this.data = vars.data;
		this._reversed = (vars.reversed === true);

		if (!_rootTimeline) {
			return;
		}
		if (!_tickerActive) { //some browsers (like iOS 6 Safari) shut down JavaScript execution when the tab is disabled and they [occasionally] neglect to start up requestAnimationFrame again when returning - this code ensures that the engine starts up again properly.
			_ticker.wake();
		}

		var tl = this.vars.useFrames ? _rootFramesTimeline : _rootTimeline;
		tl.add(this, tl._time);

		if (this.vars.paused) {
			this.paused(true);
		}
	});

	_ticker = Animation.ticker = new gs.Ticker();
	p = Animation.prototype;
	p._dirty = p._gc = p._initted = p._paused = false;
	p._totalTime = p._time = 0;
	p._rawPrevTime = -1;
	p._next = p._last = p._onUpdate = p._timeline = p.timeline = null;
	p._paused = false;


	//some browsers (like iOS) occasionally drop the requestAnimationFrame event when the user switches to a different tab and then comes back again, so we use a 2-second setTimeout() to sense if/when that condition occurs and then wake() the ticker.
	var _checkTimeout = function () {
		if (_tickerActive && _getTime() - _lastUpdate > 2000) {
			_ticker.wake();
		}
		setTimeout(_checkTimeout, 2000);
	};
	_checkTimeout();


	p.play = function (from, suppressEvents) {
		if (from != null) {
			this.seek(from, suppressEvents);
		}
		return this.reversed(false).paused(false);
	};

	p.pause = function (atTime, suppressEvents) {
		if (atTime != null) {
			this.seek(atTime, suppressEvents);
		}
		return this.paused(true);
	};

	p.resume = function (from, suppressEvents) {
		if (from != null) {
			this.seek(from, suppressEvents);
		}
		return this.paused(false);
	};

	p.seek = function (time, suppressEvents) {
		return this.totalTime(Number(time), suppressEvents !== false);
	};

	p.restart = function (includeDelay, suppressEvents) {
		return this.reversed(false).paused(false).totalTime(includeDelay ? -this._delay : 0, (suppressEvents !== false), true);
	};

	p.reverse = function (from, suppressEvents) {
		if (from != null) {
			this.seek((from || this.totalDuration()), suppressEvents);
		}
		return this.reversed(true).paused(false);
	};

	p.render = function (time, suppressEvents, force) {
		//stub - we override this method in subclasses.
	};

	p.invalidate = function () {
		this._time = this._totalTime = 0;
		this._initted = this._gc = false;
		this._rawPrevTime = -1;
		if (this._gc || !this.timeline) {
			this._enabled(true);
		}
		return this;
	};

	p.isActive = function () {
		var tl = this._timeline, //the 2 root timelines won't have a _timeline; they're always active.
			startTime = this._startTime
			, rawTime;
		return (!tl || (!this._gc && !this._paused && tl.isActive() && (rawTime = tl.rawTime()) >= startTime && rawTime < startTime + this.totalDuration() / this._timeScale));
	};

	p._enabled = function (enabled, ignoreTimeline) {
		if (!_tickerActive) {
			_ticker.wake();
		}
		this._gc = !enabled;
		this._active = this.isActive();
		if (ignoreTimeline !== true) {
			if (enabled && !this.timeline) {
				this._timeline.add(this, this._startTime - this._delay);
			} else if (!enabled && this.timeline) {
				this._timeline._remove(this, true);
			}
		}
		return false;
	};


	p._kill = function (vars, target) {
		return this._enabled(false, false);
	};

	p.kill = function (vars, target) {
		this._kill(vars, target);
		return this;
	};

	p._uncache = function (includeSelf) {
		var tween = includeSelf ? this : this.timeline;
		while (tween) {
			tween._dirty = true;
			tween = tween.timeline;
		}
		return this;
	};

	p._swapSelfInParams = function (params) {
		var i = params.length
			, copy = params.concat();
		while (--i > -1) {
			if (params[i] === "{self}") {
				copy[i] = this;
			}
		}
		return copy;
	};

	p._callback = function (type) {
		var v = this.vars;
		v[type].apply(v[type + "Scope"] || v.callbackScope || this, v[type + "Params"] || _blankArray);
	};

	//----Animation getters/setters --------------------------------------------------------

	p.eventCallback = function (type, callback, params, scope) {
		if ((type || "").substr(0, 2) === "on") {
			var v = this.vars;
			if (arguments.length === 1) {
				return v[type];
			}
			if (callback == null) {
				delete v[type];
			} else {
				v[type] = callback;
				v[type + "Params"] = (_isArray(params) && params.join("").indexOf("{self}") !== -1) ? this._swapSelfInParams(params) : params;
				v[type + "Scope"] = scope;
			}
			if (type === "onUpdate") {
				this._onUpdate = callback;
			}
		}
		return this;
	};

	p.delay = function (value) {
		if (!arguments.length) {
			return this._delay;
		}
		if (this._timeline.smoothChildTiming) {
			this.startTime(this._startTime + value - this._delay);
		}
		this._delay = value;
		return this;
	};

	p.duration = function (value) {
		if (!arguments.length) {
			this._dirty = false;
			return this._duration;
		}
		this._duration = this._totalDuration = value;
		this._uncache(true); //true in case it's a TweenMax or TimelineMax that has a repeat - we'll need to refresh the totalDuration.
		if (this._timeline.smoothChildTiming)
			if (this._time > 0)
				if (this._time < this._duration)
					if (value !== 0) {
						this.totalTime(this._totalTime * (value / this._duration), true);
					}
		return this;
	};

	p.totalDuration = function (value) {
		this._dirty = false;
		return (!arguments.length) ? this._totalDuration : this.duration(value);
	};

	p.time = function (value, suppressEvents) {
		if (!arguments.length) {
			return this._time;
		}
		if (this._dirty) {
			this.totalDuration();
		}
		return this.totalTime((value > this._duration) ? this._duration : value, suppressEvents);
	};

	p.totalTime = function (time, suppressEvents, uncapped) {
		if (!_tickerActive) {
			_ticker.wake();
		}
		if (!arguments.length) {
			return this._totalTime;
		}
		if (this._timeline) {
			if (time < 0 && !uncapped) {
				time += this.totalDuration();
			}
			if (this._timeline.smoothChildTiming) {
				if (this._dirty) {
					this.totalDuration();
				}
				var totalDuration = this._totalDuration
					, tl = this._timeline;
				if (time > totalDuration && !uncapped) {
					time = totalDuration;
				}
				this._startTime = (this._paused ? this._pauseTime : tl._time) - ((!this._reversed ? time : totalDuration - time) / this._timeScale);
				if (!tl._dirty) { //for performance improvement. If the parent's cache is already dirty, it already took care of marking the ancestors as dirty too, so skip the function call here.
					this._uncache(false);
				}
				//in case any of the ancestor timelines had completed but should now be enabled, we should reset their totalTime() which will also ensure that they're lined up properly and enabled. Skip for animations that are on the root (wasteful). Example: a TimelineLite.exportRoot() is performed when there's a paused tween on the root, the export will not complete until that tween is unpaused, but imagine a child gets restarted later, after all [unpaused] tweens have completed. The startTime of that child would get pushed out, but one of the ancestors may have completed.
				if (tl._timeline) {
					while (tl._timeline) {
						if (tl._timeline._time !== (tl._startTime + tl._totalTime) / tl._timeScale) {
							tl.totalTime(tl._totalTime, true);
						}
						tl = tl._timeline;
					}
				}
			}
			if (this._gc) {
				this._enabled(true, false);
			}
			if (this._totalTime !== time || this._duration === 0) {
				if (_lazyTweens.length) {
					_lazyRender();
				}
				this.render(time, suppressEvents, false);
				if (_lazyTweens.length) { //in case rendering caused any tweens to lazy-init, we should render them because typically when someone calls seek() or time() or progress(), they expect an immediate render.
					_lazyRender();
				}
			}
		}
		return this;
	};

	p.progress = p.totalProgress = function (value, suppressEvents) {
		var duration = this.duration();
		return (!arguments.length) ? (duration ? this._time / duration : this.ratio) : this.totalTime(duration * value, suppressEvents);
	};

	p.startTime = function (value) {
		if (!arguments.length) {
			return this._startTime;
		}
		if (value !== this._startTime) {
			this._startTime = value;
			if (this.timeline)
				if (this.timeline._sortChildren) {
					this.timeline.add(this, value - this._delay); //ensures that any necessary re-sequencing of Animations in the timeline occurs to make sure the rendering order is correct.
				}
		}
		return this;
	};

	p.endTime = function (includeRepeats) {
		return this._startTime + ((includeRepeats != false) ? this.totalDuration() : this.duration()) / this._timeScale;
	};

	p.timeScale = function (value) {
		if (!arguments.length) {
			return this._timeScale;
		}
		value = value || _tinyNum; //can't allow zero because it'll throw the math off
		if (this._timeline && this._timeline.smoothChildTiming) {
			var pauseTime = this._pauseTime
				, t = (pauseTime || pauseTime === 0) ? pauseTime : this._timeline.totalTime();
			this._startTime = t - ((t - this._startTime) * this._timeScale / value);
		}
		this._timeScale = value;
		return this._uncache(false);
	};

	p.reversed = function (value) {
		if (!arguments.length) {
			return this._reversed;
		}
		if (value != this._reversed) {
			this._reversed = value;
			this.totalTime(((this._timeline && !this._timeline.smoothChildTiming) ? this.totalDuration() - this._totalTime : this._totalTime), true);
		}
		return this;
	};

	p.paused = function (value) {
		if (!arguments.length) {
			return this._paused;
		}
		var tl = this._timeline
			, raw, elapsed;
		if (value != this._paused)
			if (tl) {
				if (!_tickerActive && !value) {
					_ticker.wake();
				}
				raw = tl.rawTime();
				elapsed = raw - this._pauseTime;
				if (!value && tl.smoothChildTiming) {
					this._startTime += elapsed;
					this._uncache(false);
				}
				this._pauseTime = value ? raw : null;
				this._paused = value;
				this._active = this.isActive();
				if (!value && elapsed !== 0 && this._initted && this.duration()) {
					raw = tl.smoothChildTiming ? this._totalTime : (raw - this._startTime) / this._timeScale;
					this.render(raw, (raw === this._totalTime), true); //in case the target's properties changed via some other tween or manual update by the user, we should force a render.
				}
			}
		if (this._gc && !value) {
			this._enabled(true, false);
		}
		return this;
	};


	/*
	 * ----------------------------------------------------------------
	 * SimpleTimeline
	 * ----------------------------------------------------------------
	 */
	var SimpleTimeline = _class("core.SimpleTimeline", function (vars) {
		Animation.call(this, 0, vars);
		this.autoRemoveChildren = this.smoothChildTiming = true;
	});

	p = SimpleTimeline.prototype = new Animation();
	p.constructor = SimpleTimeline;
	p.kill()._gc = false;
	p._first = p._last = p._recent = null;
	p._sortChildren = false;

	p.add = p.insert = function (child, position, align, stagger) {
		var prevTween, st;
		child._startTime = Number(position || 0) + child._delay;
		if (child._paused)
			if (this !== child._timeline) { //we only adjust the _pauseTime if it wasn't in this timeline already. Remember, sometimes a tween will be inserted again into the same timeline when its startTime is changed so that the tweens in the TimelineLite/Max are re-ordered properly in the linked list (so everything renders in the proper order).
				child._pauseTime = child._startTime + ((this.rawTime() - child._startTime) / child._timeScale);
			}
		if (child.timeline) {
			child.timeline._remove(child, true); //removes from existing timeline so that it can be properly added to this one.
		}
		child.timeline = child._timeline = this;
		if (child._gc) {
			child._enabled(true, true);
		}
		prevTween = this._last;
		if (this._sortChildren) {
			st = child._startTime;
			while (prevTween && prevTween._startTime > st) {
				prevTween = prevTween._prev;
			}
		}
		if (prevTween) {
			child._next = prevTween._next;
			prevTween._next = child;
		} else {
			child._next = this._first;
			this._first = child;
		}
		if (child._next) {
			child._next._prev = child;
		} else {
			this._last = child;
		}
		child._prev = prevTween;
		this._recent = child;
		if (this._timeline) {
			this._uncache(true);
		}
		return this;
	};

	p._remove = function (tween, skipDisable) {
		if (tween.timeline === this) {
			if (!skipDisable) {
				tween._enabled(false, true);
			}

			if (tween._prev) {
				tween._prev._next = tween._next;
			} else if (this._first === tween) {
				this._first = tween._next;
			}
			if (tween._next) {
				tween._next._prev = tween._prev;
			} else if (this._last === tween) {
				this._last = tween._prev;
			}
			tween._next = tween._prev = tween.timeline = null;
			if (tween === this._recent) {
				this._recent = this._last;
			}

			if (this._timeline) {
				this._uncache(true);
			}
		}
		return this;
	};

	p.render = function (time, suppressEvents, force) {
		var tween = this._first
			, next;
		this._totalTime = this._time = this._rawPrevTime = time;
		while (tween) {
			next = tween._next; //record it here because the value could change after rendering...
			if (tween._active || (time >= tween._startTime && !tween._paused)) {
				if (!tween._reversed) {
					tween.render((time - tween._startTime) * tween._timeScale, suppressEvents, force);
				} else {
					tween.render(((!tween._dirty) ? tween._totalDuration : tween.totalDuration()) - ((time - tween._startTime) * tween._timeScale), suppressEvents, force);
				}
			}
			tween = next;
		}
	};

	p.rawTime = function () {
		if (!_tickerActive) {
			_ticker.wake();
		}
		return this._totalTime;
	};

	/*
	 * ----------------------------------------------------------------
	 * TweenLite
	 * ----------------------------------------------------------------
	 */
	var TweenLite = _class("TweenLite", function (target, duration, vars) {
		Animation.call(this, duration, vars);
		this.render = TweenLite.prototype.render; //speed optimization (avoid prototype lookup on this "hot" method)

		if (target == null) {
			throw "Cannot tween a null target.";
		}

		this.target = target = (typeof (target) !== "string") ? target : TweenLite.selector(target) || target;

		var isSelector = (target.jquery || (target.length && target !== window && target[0] && (target[0] === window || (target[0].nodeType && target[0].style && !target.nodeType))))
			, overwrite = this.vars.overwrite
			, i, targ, targets;

		this._overwrite = overwrite = (overwrite == null) ? _overwriteLookup[TweenLite.defaultOverwrite] : (typeof (overwrite) === "number") ? overwrite >> 0 : _overwriteLookup[overwrite];

		if ((isSelector || target instanceof Array || (target.push && _isArray(target))) && typeof (target[0]) !== "number") {
			this._targets = targets = _slice(target); //don't use Array.prototype.slice.call(target, 0) because that doesn't work in IE8 with a NodeList that's returned by querySelectorAll()
			this._propLookup = [];
			this._siblings = [];
			for (i = 0; i < targets.length; i++) {
				targ = targets[i];
				if (!targ) {
					targets.splice(i--, 1);
					continue;
				} else if (typeof (targ) === "string") {
					targ = targets[i--] = TweenLite.selector(targ); //in case it's an array of strings
					if (typeof (targ) === "string") {
						targets.splice(i + 1, 1); //to avoid an endless loop (can't imagine why the selector would return a string, but just in case)
					}
					continue;
				} else if (targ.length && targ !== window && targ[0] && (targ[0] === window || (targ[0].nodeType && targ[0].style && !targ.nodeType))) { //in case the user is passing in an array of selector objects (like jQuery objects), we need to check one more level and pull things out if necessary. Also note that <select> elements pass all the criteria regarding length and the first child having style, so we must also check to ensure the target isn't an HTML node itself.
					targets.splice(i--, 1);
					this._targets = targets = targets.concat(_slice(targ));
					continue;
				}
				this._siblings[i] = _register(targ, this, false);
				if (overwrite === 1)
					if (this._siblings[i].length > 1) {
						_applyOverwrite(targ, this, null, 1, this._siblings[i]);
					}
			}

		} else {
			this._propLookup = {};
			this._siblings = _register(target, this, false);
			if (overwrite === 1)
				if (this._siblings.length > 1) {
					_applyOverwrite(target, this, null, 1, this._siblings);
				}
		}
		if (this.vars.immediateRender || (duration === 0 && this._delay === 0 && this.vars.immediateRender !== false)) {
			this._time = -_tinyNum; //forces a render without having to set the render() "force" parameter to true because we want to allow lazying by default (using the "force" parameter always forces an immediate full render)
			this.render(Math.min(0, -this._delay)); //in case delay is negative
		}
	}, true)
		, _isSelector = function (v) {
		return (v && v.length && v !== window && v[0] && (v[0] === window || (v[0].nodeType && v[0].style && !v.nodeType))); //we cannot check "nodeType" if the target is window from within an iframe, otherwise it will trigger a security error in some browsers like Firefox.
	}
		, _autoCSS = function (vars, target) {
		var css = {}
			, p;
		for (p in vars) {
			if (!_reservedProps[p] && (!(p in target) || p === "transform" || p === "x" || p === "y" || p === "width" || p === "height" || p === "className" || p === "border") && (!_plugins[p] || (_plugins[p] && _plugins[p]._autoCSS))) { //note: <img> elements contain read-only "x" and "y" properties. We should also prioritize editing css width/height rather than the element's properties.
				css[p] = vars[p];
				delete vars[p];
			}
		}
		vars.css = css;
	};

	p = TweenLite.prototype = new Animation();
	p.constructor = TweenLite;
	p.kill()._gc = false;

	//----TweenLite defaults, overwrite management, and root updates ----------------------------------------------------

	p.ratio = 0;
	p._firstPT = p._targets = p._overwrittenProps = p._startAt = null;
	p._notifyPluginsOfEnabled = p._lazy = false;

	TweenLite.version = "1.18.4";
	TweenLite.defaultEase = p._ease = new Ease(null, null, 1, 1);
	TweenLite.defaultOverwrite = "auto";
	TweenLite.ticker = _ticker;
	TweenLite.autoSleep = 120;
	TweenLite.lagSmoothing = function (threshold, adjustedLag) {
		_ticker.lagSmoothing(threshold, adjustedLag);
	};

	TweenLite.selector = window.$ || window.jQuery || function (e) {
			var selector = window.$ || window.jQuery;
			if (selector) {
				TweenLite.selector = selector;
				return selector(e);
			}
			return (typeof (document) === "undefined") ? e : (document.querySelectorAll ? document.querySelectorAll(e) : document.getElementById((e.charAt(0) === "#") ? e.substr(1) : e));
		};

	var _lazyTweens = []
		, _lazyLookup = {}
		, _numbersExp = /(?:(-|-=|\+=)?\d*\.?\d*(?:e[\-+]?\d+)?)[0-9]/ig
		, //_nonNumbersExp = /(?:([\-+](?!(\d|=)))|[^\d\-+=e]|(e(?![\-+][\d])))+/ig,
		_setRatio = function (v) {
			var pt = this._firstPT
				, min = 0.000001
				, val;
			while (pt) {
				val = !pt.blob ? pt.c * v + pt.s : v ? this.join("") : this.start;
				if (pt.r) {
					val = Math.round(val);
				} else if (val < min)
					if (val > -min) { //prevents issues with converting very small numbers to strings in the browser
						val = 0;
					}
				if (!pt.f) {
					pt.t[pt.p] = val;
				} else if (pt.fp) {
					pt.t[pt.p](pt.fp, val);
				} else {
					pt.t[pt.p](val);
				}
				pt = pt._next;
			}
		}
		, //compares two strings (start/end), finds the numbers that are different and spits back an array representing the whole value but with the changing values isolated as elements. For example, "rgb(0,0,0)" and "rgb(100,50,0)" would become ["rgb(", 0, ",", 50, ",0)"]. Notice it merges the parts that are identical (performance optimization). The array also has a linked list of PropTweens attached starting with _firstPT that contain the tweening data (t, p, s, c, f, etc.). It also stores the starting value as a "start" property so that we can revert to it if/when necessary, like when a tween rewinds fully. If the quantity of numbers differs between the start and end, it will always prioritize the end value(s). The pt parameter is optional - it's for a PropTween that will be appended to the end of the linked list and is typically for actually setting the value after all of the elements have been updated (with array.join("")).
		_blobDif = function (start, end, filter, pt) {
			var a = [start, end]
				, charIndex = 0
				, s = ""
				, color = 0
				, startNums, endNums, num, i, l, nonNumbers, currentNum;
			a.start = start;
			if (filter) {
				filter(a); //pass an array with the starting and ending values and let the filter do whatever it needs to the values.
				start = a[0];
				end = a[1];
			}
			a.length = 0;
			startNums = start.match(_numbersExp) || [];
			endNums = end.match(_numbersExp) || [];
			if (pt) {
				pt._next = null;
				pt.blob = 1;
				a._firstPT = pt; //apply last in the linked list (which means inserting it first)
			}
			l = endNums.length;
			for (i = 0; i < l; i++) {
				currentNum = endNums[i];
				nonNumbers = end.substr(charIndex, end.indexOf(currentNum, charIndex) - charIndex);
				s += (nonNumbers || !i) ? nonNumbers : ","; //note: SVG spec allows omission of comma/space when a negative sign is wedged between two numbers, like 2.5-5.3 instead of 2.5,-5.3 but when tweening, the negative value may switch to positive, so we insert the comma just in case.
				charIndex += nonNumbers.length;
				if (color) { //sense rgba() values and round them.
					color = (color + 1) % 5;
				} else if (nonNumbers.substr(-5) === "rgba(") {
					color = 1;
				}
				if (currentNum === startNums[i] || startNums.length <= i) {
					s += currentNum;
				} else {
					if (s) {
						a.push(s);
						s = "";
					}
					num = parseFloat(startNums[i]);
					a.push(num);
					a._firstPT = {
						_next: a._firstPT
						, t: a
						, p: a.length - 1
						, s: num
						, c: ((currentNum.charAt(1) === "=") ? parseInt(currentNum.charAt(0) + "1", 10) * parseFloat(currentNum.substr(2)) : (parseFloat(currentNum) - num)) || 0
						, f: 0
						, r: (color && color < 4)
					};
					//note: we don't set _prev because we'll never need to remove individual PropTweens from this list.
				}
				charIndex += currentNum.length;
			}
			s += end.substr(charIndex);
			if (s) {
				a.push(s);
			}
			a.setRatio = _setRatio;
			return a;
		}
		, //note: "funcParam" is only necessary for function-based getters/setters that require an extra parameter like getAttribute("width") and setAttribute("width", value). In this example, funcParam would be "width". Used by AttrPlugin for example.
		_addPropTween = function (target, prop, start, end, overwriteProp, round, funcParam, stringFilter) {
			var s = (start === "get") ? target[prop] : start
				, type = typeof (target[prop])
				, isRelative = (typeof (end) === "string" && end.charAt(1) === "=")
				, pt = {
				t: target
				, p: prop
				, s: s
				, f: (type === "function")
				, pg: 0
				, n: overwriteProp || prop
				, r: round
				, pr: 0
				, c: isRelative ? parseInt(end.charAt(0) + "1", 10) * parseFloat(end.substr(2)) : (parseFloat(end) - s) || 0
			}
				, blob, getterName;
			if (type !== "number") {
				if (type === "function" && start === "get") {
					getterName = ((prop.indexOf("set") || typeof (target["get" + prop.substr(3)]) !== "function") ? prop : "get" + prop.substr(3));
					pt.s = s = funcParam ? target[getterName](funcParam) : target[getterName]();
				}
				if (typeof (s) === "string" && (funcParam || isNaN(s))) {
					//a blob (string that has multiple numbers in it)
					pt.fp = funcParam;
					blob = _blobDif(s, end, stringFilter || TweenLite.defaultStringFilter, pt);
					pt = {
						t: blob
						, p: "setRatio"
						, s: 0
						, c: 1
						, f: 2
						, pg: 0
						, n: overwriteProp || prop
						, pr: 0
					}; //"2" indicates it's a Blob property tween. Needed for RoundPropsPlugin for example.
				} else if (!isRelative) {
					pt.s = parseFloat(s);
					pt.c = (parseFloat(end) - pt.s) || 0;
				}
			}
			if (pt.c) { //only add it to the linked list if there's a change.
				if ((pt._next = this._firstPT)) {
					pt._next._prev = pt;
				}
				this._firstPT = pt;
				return pt;
			}
		}
		, _internals = TweenLite._internals = {
			isArray: _isArray
			, isSelector: _isSelector
			, lazyTweens: _lazyTweens
			, blobDif: _blobDif
		}, //gives us a way to expose certain private values to other GreenSock classes without contaminating tha main TweenLite object.
		_plugins = TweenLite._plugins = {}
		, _tweenLookup = _internals.tweenLookup = {}
		, _tweenLookupNum = 0
		, _reservedProps = _internals.reservedProps = {
			ease: 1
			, delay: 1
			, overwrite: 1
			, onComplete: 1
			, onCompleteParams: 1
			, onCompleteScope: 1
			, useFrames: 1
			, runBackwards: 1
			, startAt: 1
			, onUpdate: 1
			, onUpdateParams: 1
			, onUpdateScope: 1
			, onStart: 1
			, onStartParams: 1
			, onStartScope: 1
			, onReverseComplete: 1
			, onReverseCompleteParams: 1
			, onReverseCompleteScope: 1
			, onRepeat: 1
			, onRepeatParams: 1
			, onRepeatScope: 1
			, easeParams: 1
			, yoyo: 1
			, immediateRender: 1
			, repeat: 1
			, repeatDelay: 1
			, data: 1
			, paused: 1
			, reversed: 1
			, autoCSS: 1
			, lazy: 1
			, onOverwrite: 1
			, callbackScope: 1
			, stringFilter: 1
		}
		, _overwriteLookup = {
			none: 0
			, all: 1
			, auto: 2
			, concurrent: 3
			, allOnStart: 4
			, preexisting: 5
			, "true": 1
			, "false": 0
		}
		, _rootFramesTimeline = Animation._rootFramesTimeline = new SimpleTimeline()
		, _rootTimeline = Animation._rootTimeline = new SimpleTimeline()
		, _nextGCFrame = 30
		, _lazyRender = _internals.lazyRender = function () {
			var i = _lazyTweens.length
				, tween;
			_lazyLookup = {};
			while (--i > -1) {
				tween = _lazyTweens[i];
				if (tween && tween._lazy !== false) {
					tween.render(tween._lazy[0], tween._lazy[1], true);
					tween._lazy = false;
				}
			}
			_lazyTweens.length = 0;
		};

	_rootTimeline._startTime = _ticker.time;
	_rootFramesTimeline._startTime = _ticker.frame;
	_rootTimeline._active = _rootFramesTimeline._active = true;
	setTimeout(_lazyRender, 1); //on some mobile devices, there isn't a "tick" before code runs which means any lazy renders wouldn't run before the next official "tick".

	Animation._updateRoot = TweenLite.render = function () {
		var i, a, p;
		if (_lazyTweens.length) { //if code is run outside of the requestAnimationFrame loop, there may be tweens queued AFTER the engine refreshed, so we need to ensure any pending renders occur before we refresh again.
			_lazyRender();
		}
		_rootTimeline.render((_ticker.time - _rootTimeline._startTime) * _rootTimeline._timeScale, false, false);
		_rootFramesTimeline.render((_ticker.frame - _rootFramesTimeline._startTime) * _rootFramesTimeline._timeScale, false, false);
		if (_lazyTweens.length) {
			_lazyRender();
		}
		if (_ticker.frame >= _nextGCFrame) { //dump garbage every 120 frames or whatever the user sets TweenLite.autoSleep to
			_nextGCFrame = _ticker.frame + (parseInt(TweenLite.autoSleep, 10) || 120);
			for (p in _tweenLookup) {
				a = _tweenLookup[p].tweens;
				i = a.length;
				while (--i > -1) {
					if (a[i]._gc) {
						a.splice(i, 1);
					}
				}
				if (a.length === 0) {
					delete _tweenLookup[p];
				}
			}
			//if there are no more tweens in the root timelines, or if they're all paused, make the _timer sleep to reduce load on the CPU slightly
			p = _rootTimeline._first;
			if (!p || p._paused)
				if (TweenLite.autoSleep && !_rootFramesTimeline._first && _ticker._listeners.tick.length === 1) {
					while (p && p._paused) {
						p = p._next;
					}
					if (!p) {
						_ticker.sleep();
					}
				}
		}
	};

	_ticker.addEventListener("tick", Animation._updateRoot);

	var _register = function (target, tween, scrub) {
		var id = target._gsTweenID
			, a, i;
		if (!_tweenLookup[id || (target._gsTweenID = id = "t" + (_tweenLookupNum++))]) {
			_tweenLookup[id] = {
				target: target
				, tweens: []
			};
		}
		if (tween) {
			a = _tweenLookup[id].tweens;
			a[(i = a.length)] = tween;
			if (scrub) {
				while (--i > -1) {
					if (a[i] === tween) {
						a.splice(i, 1);
					}
				}
			}
		}
		return _tweenLookup[id].tweens;
	}
		, _onOverwrite = function (overwrittenTween, overwritingTween, target, killedProps) {
		var func = overwrittenTween.vars.onOverwrite
			, r1, r2;
		if (func) {
			r1 = func(overwrittenTween, overwritingTween, target, killedProps);
		}
		func = TweenLite.onOverwrite;
		if (func) {
			r2 = func(overwrittenTween, overwritingTween, target, killedProps);
		}
		return (r1 !== false && r2 !== false);
	}
		, _applyOverwrite = function (target, tween, props, mode, siblings) {
		var i, changed, curTween, l;
		if (mode === 1 || mode >= 4) {
			l = siblings.length;
			for (i = 0; i < l; i++) {
				if ((curTween = siblings[i]) !== tween) {
					if (!curTween._gc) {
						if (curTween._kill(null, target, tween)) {
							changed = true;
						}
					}
				} else if (mode === 5) {
					break;
				}
			}
			return changed;
		}
		//NOTE: Add 0.0000000001 to overcome floating point errors that can cause the startTime to be VERY slightly off (when a tween's time() is set for example)
		var startTime = tween._startTime + _tinyNum
			, overlaps = []
			, oCount = 0
			, zeroDur = (tween._duration === 0)
			, globalStart;
		i = siblings.length;
		while (--i > -1) {
			if ((curTween = siblings[i]) === tween || curTween._gc || curTween._paused) {
				//ignore
			} else if (curTween._timeline !== tween._timeline) {
				globalStart = globalStart || _checkOverlap(tween, 0, zeroDur);
				if (_checkOverlap(curTween, globalStart, zeroDur) === 0) {
					overlaps[oCount++] = curTween;
				}
			} else if (curTween._startTime <= startTime)
				if (curTween._startTime + curTween.totalDuration() / curTween._timeScale > startTime)
					if (!((zeroDur || !curTween._initted) && startTime - curTween._startTime <= 0.0000000002)) {
						overlaps[oCount++] = curTween;
					}
		}

		i = oCount;
		while (--i > -1) {
			curTween = overlaps[i];
			if (mode === 2)
				if (curTween._kill(props, target, tween)) {
					changed = true;
				}
			if (mode !== 2 || (!curTween._firstPT && curTween._initted)) {
				if (mode !== 2 && !_onOverwrite(curTween, tween)) {
					continue;
				}
				if (curTween._enabled(false, false)) { //if all property tweens have been overwritten, kill the tween.
					changed = true;
				}
			}
		}
		return changed;
	}
		, _checkOverlap = function (tween, reference, zeroDur) {
		var tl = tween._timeline
			, ts = tl._timeScale
			, t = tween._startTime;
		while (tl._timeline) {
			t += tl._startTime;
			ts *= tl._timeScale;
			if (tl._paused) {
				return -100;
			}
			tl = tl._timeline;
		}
		t /= ts;
		return (t > reference) ? t - reference : ((zeroDur && t === reference) || (!tween._initted && t - reference < 2 * _tinyNum)) ? _tinyNum : ((t += tween.totalDuration() / tween._timeScale / ts) > reference + _tinyNum) ? 0 : t - reference - _tinyNum;
	};


	//---- TweenLite instance methods -----------------------------------------------------------------------------

	p._init = function () {
		var v = this.vars
			, op = this._overwrittenProps
			, dur = this._duration
			, immediate = !!v.immediateRender
			, ease = v.ease
			, i, initPlugins, pt, p, startVars;
		if (v.startAt) {
			if (this._startAt) {
				this._startAt.render(-1, true); //if we've run a startAt previously (when the tween instantiated), we should revert it so that the values re-instantiate correctly particularly for relative tweens. Without this, a TweenLite.fromTo(obj, 1, {x:"+=100"}, {x:"-=100"}), for example, would actually jump to +=200 because the startAt would run twice, doubling the relative change.
				this._startAt.kill();
			}
			startVars = {};
			for (p in v.startAt) { //copy the properties/values into a new object to avoid collisions, like var to = {x:0}, from = {x:500}; timeline.fromTo(e, 1, from, to).fromTo(e, 1, to, from);
				startVars[p] = v.startAt[p];
			}
			startVars.overwrite = false;
			startVars.immediateRender = true;
			startVars.lazy = (immediate && v.lazy !== false);
			startVars.startAt = startVars.delay = null; //no nesting of startAt objects allowed (otherwise it could cause an infinite loop).
			this._startAt = TweenLite.to(this.target, 0, startVars);
			if (immediate) {
				if (this._time > 0) {
					this._startAt = null; //tweens that render immediately (like most from() and fromTo() tweens) shouldn't revert when their parent timeline's playhead goes backward past the startTime because the initial render could have happened anytime and it shouldn't be directly correlated to this tween's startTime. Imagine setting up a complex animation where the beginning states of various objects are rendered immediately but the tween doesn't happen for quite some time - if we revert to the starting values as soon as the playhead goes backward past the tween's startTime, it will throw things off visually. Reversion should only happen in TimelineLite/Max instances where immediateRender was false (which is the default in the convenience methods like from()).
				} else if (dur !== 0) {
					return; //we skip initialization here so that overwriting doesn't occur until the tween actually begins. Otherwise, if you create several immediateRender:true tweens of the same target/properties to drop into a TimelineLite or TimelineMax, the last one created would overwrite the first ones because they didn't get placed into the timeline yet before the first render occurs and kicks in overwriting.
				}
			}
		} else if (v.runBackwards && dur !== 0) {
			//from() tweens must be handled uniquely: their beginning values must be rendered but we don't want overwriting to occur yet (when time is still 0). Wait until the tween actually begins before doing all the routines like overwriting. At that time, we should render at the END of the tween to ensure that things initialize correctly (remember, from() tweens go backwards)
			if (this._startAt) {
				this._startAt.render(-1, true);
				this._startAt.kill();
				this._startAt = null;
			} else {
				if (this._time !== 0) { //in rare cases (like if a from() tween runs and then is invalidate()-ed), immediateRender could be true but the initial forced-render gets skipped, so there's no need to force the render in this context when the _time is greater than 0
					immediate = false;
				}
				pt = {};
				for (p in v) { //copy props into a new object and skip any reserved props, otherwise onComplete or onUpdate or onStart could fire. We should, however, permit autoCSS to go through.
					if (!_reservedProps[p] || p === "autoCSS") {
						pt[p] = v[p];
					}
				}
				pt.overwrite = 0;
				pt.data = "isFromStart"; //we tag the tween with as "isFromStart" so that if [inside a plugin] we need to only do something at the very END of a tween, we have a way of identifying this tween as merely the one that's setting the beginning values for a "from()" tween. For example, clearProps in CSSPlugin should only get applied at the very END of a tween and without this tag, from(...{height:100, clearProps:"height", delay:1}) would wipe the height at the beginning of the tween and after 1 second, it'd kick back in.
				pt.lazy = (immediate && v.lazy !== false);
				pt.immediateRender = immediate; //zero-duration tweens render immediately by default, but if we're not specifically instructed to render this tween immediately, we should skip this and merely _init() to record the starting values (rendering them immediately would push them to completion which is wasteful in that case - we'd have to render(-1) immediately after)
				this._startAt = TweenLite.to(this.target, 0, pt);
				if (!immediate) {
					this._startAt._init(); //ensures that the initial values are recorded
					this._startAt._enabled(false); //no need to have the tween render on the next cycle. Disable it because we'll always manually control the renders of the _startAt tween.
					if (this.vars.immediateRender) {
						this._startAt = null;
					}
				} else if (this._time === 0) {
					return;
				}
			}
		}
		this._ease = ease = (!ease) ? TweenLite.defaultEase : (ease instanceof Ease) ? ease : (typeof (ease) === "function") ? new Ease(ease, v.easeParams) : _easeMap[ease] || TweenLite.defaultEase;
		if (v.easeParams instanceof Array && ease.config) {
			this._ease = ease.config.apply(ease, v.easeParams);
		}
		this._easeType = this._ease._type;
		this._easePower = this._ease._power;
		this._firstPT = null;

		if (this._targets) {
			i = this._targets.length;
			while (--i > -1) {
				if (this._initProps(this._targets[i], (this._propLookup[i] = {}), this._siblings[i], (op ? op[i] : null))) {
					initPlugins = true;
				}
			}
		} else {
			initPlugins = this._initProps(this.target, this._propLookup, this._siblings, op);
		}

		if (initPlugins) {
			TweenLite._onPluginEvent("_onInitAllProps", this); //reorders the array in order of priority. Uses a static TweenPlugin method in order to minimize file size in TweenLite
		}
		if (op)
			if (!this._firstPT)
				if (typeof (this.target) !== "function") { //if all tweening properties have been overwritten, kill the tween. If the target is a function, it's probably a delayedCall so let it live.
					this._enabled(false, false);
				}
		if (v.runBackwards) {
			pt = this._firstPT;
			while (pt) {
				pt.s += pt.c;
				pt.c = -pt.c;
				pt = pt._next;
			}
		}
		this._onUpdate = v.onUpdate;
		this._initted = true;
	};

	p._initProps = function (target, propLookup, siblings, overwrittenProps) {
		var p, i, initPlugins, plugin, pt, v;
		if (target == null) {
			return false;
		}

		if (_lazyLookup[target._gsTweenID]) {
			_lazyRender(); //if other tweens of the same target have recently initted but haven't rendered yet, we've got to force the render so that the starting values are correct (imagine populating a timeline with a bunch of sequential tweens and then jumping to the end)
		}

		if (!this.vars.css)
			if (target.style)
				if (target !== window && target.nodeType)
					if (_plugins.css)
						if (this.vars.autoCSS !== false) { //it's so common to use TweenLite/Max to animate the css of DOM elements, we assume that if the target is a DOM element, that's what is intended (a convenience so that users don't have to wrap things in css:{}, although we still recommend it for a slight performance boost and better specificity). Note: we cannot check "nodeType" on the window inside an iframe.
							_autoCSS(this.vars, target);
						}
		for (p in this.vars) {
			v = this.vars[p];
			if (_reservedProps[p]) {
				if (v)
					if ((v instanceof Array) || (v.push && _isArray(v)))
						if (v.join("").indexOf("{self}") !== -1) {
							this.vars[p] = v = this._swapSelfInParams(v, this);
						}

			} else if (_plugins[p] && (plugin = new _plugins[p]())._onInitTween(target, this.vars[p], this)) {

				//t - target 		[object]
				//p - property 		[string]
				//s - start			[number]
				//c - change		[number]
				//f - isFunction	[boolean]
				//n - name			[string]
				//pg - isPlugin 	[boolean]
				//pr - priority		[number]
				this._firstPT = pt = {
					_next: this._firstPT
					, t: plugin
					, p: "setRatio"
					, s: 0
					, c: 1
					, f: 1
					, n: p
					, pg: 1
					, pr: plugin._priority
				};
				i = plugin._overwriteProps.length;
				while (--i > -1) {
					propLookup[plugin._overwriteProps[i]] = this._firstPT;
				}
				if (plugin._priority || plugin._onInitAllProps) {
					initPlugins = true;
				}
				if (plugin._onDisable || plugin._onEnable) {
					this._notifyPluginsOfEnabled = true;
				}
				if (pt._next) {
					pt._next._prev = pt;
				}

			} else {
				propLookup[p] = _addPropTween.call(this, target, p, "get", v, p, 0, null, this.vars.stringFilter);
			}
		}

		if (overwrittenProps)
			if (this._kill(overwrittenProps, target)) { //another tween may have tried to overwrite properties of this tween before init() was called (like if two tweens start at the same time, the one created second will run first)
				return this._initProps(target, propLookup, siblings, overwrittenProps);
			}
		if (this._overwrite > 1)
			if (this._firstPT)
				if (siblings.length > 1)
					if (_applyOverwrite(target, this, propLookup, this._overwrite, siblings)) {
						this._kill(propLookup, target);
						return this._initProps(target, propLookup, siblings, overwrittenProps);
					}
		if (this._firstPT)
			if ((this.vars.lazy !== false && this._duration) || (this.vars.lazy && !this._duration)) { //zero duration tweens don't lazy render by default; everything else does.
				_lazyLookup[target._gsTweenID] = true;
			}
		return initPlugins;
	};

	p.render = function (time, suppressEvents, force) {
		var prevTime = this._time
			, duration = this._duration
			, prevRawPrevTime = this._rawPrevTime
			, isComplete, callback, pt, rawPrevTime;
		if (time >= duration - 0.0000001) { //to work around occasional floating point math artifacts.
			this._totalTime = this._time = duration;
			this.ratio = this._ease._calcEnd ? this._ease.getRatio(1) : 1;
			if (!this._reversed) {
				isComplete = true;
				callback = "onComplete";
				force = (force || this._timeline.autoRemoveChildren); //otherwise, if the animation is unpaused/activated after it's already finished, it doesn't get removed from the parent timeline.
			}
			if (duration === 0)
				if (this._initted || !this.vars.lazy || force) { //zero-duration tweens are tricky because we must discern the momentum/direction of time in order to determine whether the starting values should be rendered or the ending values. If the "playhead" of its timeline goes past the zero-duration tween in the forward direction or lands directly on it, the end values should be rendered, but if the timeline's "playhead" moves past it in the backward direction (from a postitive time to a negative time), the starting values must be rendered.
					if (this._startTime === this._timeline._duration) { //if a zero-duration tween is at the VERY end of a timeline and that timeline renders at its end, it will typically add a tiny bit of cushion to the render time to prevent rounding errors from getting in the way of tweens rendering their VERY end. If we then reverse() that timeline, the zero-duration tween will trigger its onReverseComplete even though technically the playhead didn't pass over it again. It's a very specific edge case we must accommodate.
						time = 0;
					}
					if (prevRawPrevTime < 0 || (time <= 0 && time >= -0.0000001) || (prevRawPrevTime === _tinyNum && this.data !== "isPause"))
						if (prevRawPrevTime !== time) { //note: when this.data is "isPause", it's a callback added by addPause() on a timeline that we should not be triggered when LEAVING its exact start time. In other words, tl.addPause(1).play(1) shouldn't pause.
							force = true;
							if (prevRawPrevTime > _tinyNum) {
								callback = "onReverseComplete";
							}
						}
					this._rawPrevTime = rawPrevTime = (!suppressEvents || time || prevRawPrevTime === time) ? time : _tinyNum; //when the playhead arrives at EXACTLY time 0 (right on top) of a zero-duration tween, we need to discern if events are suppressed so that when the playhead moves again (next time), it'll trigger the callback. If events are NOT suppressed, obviously the callback would be triggered in this render. Basically, the callback should fire either when the playhead ARRIVES or LEAVES this exact spot, not both. Imagine doing a timeline.seek(0) and there's a callback that sits at 0. Since events are suppressed on that seek() by default, nothing will fire, but when the playhead moves off of that position, the callback should fire. This behavior is what people intuitively expect. We set the _rawPrevTime to be a precise tiny number to indicate this scenario rather than using another property/variable which would increase memory usage. This technique is less readable, but more efficient.
				}

		} else if (time < 0.0000001) { //to work around occasional floating point math artifacts, round super small values to 0.
			this._totalTime = this._time = 0;
			this.ratio = this._ease._calcEnd ? this._ease.getRatio(0) : 0;
			if (prevTime !== 0 || (duration === 0 && prevRawPrevTime > 0)) {
				callback = "onReverseComplete";
				isComplete = this._reversed;
			}
			if (time < 0) {
				this._active = false;
				if (duration === 0)
					if (this._initted || !this.vars.lazy || force) { //zero-duration tweens are tricky because we must discern the momentum/direction of time in order to determine whether the starting values should be rendered or the ending values. If the "playhead" of its timeline goes past the zero-duration tween in the forward direction or lands directly on it, the end values should be rendered, but if the timeline's "playhead" moves past it in the backward direction (from a postitive time to a negative time), the starting values must be rendered.
						if (prevRawPrevTime >= 0 && !(prevRawPrevTime === _tinyNum && this.data === "isPause")) {
							force = true;
						}
						this._rawPrevTime = rawPrevTime = (!suppressEvents || time || prevRawPrevTime === time) ? time : _tinyNum; //when the playhead arrives at EXACTLY time 0 (right on top) of a zero-duration tween, we need to discern if events are suppressed so that when the playhead moves again (next time), it'll trigger the callback. If events are NOT suppressed, obviously the callback would be triggered in this render. Basically, the callback should fire either when the playhead ARRIVES or LEAVES this exact spot, not both. Imagine doing a timeline.seek(0) and there's a callback that sits at 0. Since events are suppressed on that seek() by default, nothing will fire, but when the playhead moves off of that position, the callback should fire. This behavior is what people intuitively expect. We set the _rawPrevTime to be a precise tiny number to indicate this scenario rather than using another property/variable which would increase memory usage. This technique is less readable, but more efficient.
					}
			}
			if (!this._initted) { //if we render the very beginning (time == 0) of a fromTo(), we must force the render (normal tweens wouldn't need to render at a time of 0 when the prevTime was also 0). This is also mandatory to make sure overwriting kicks in immediately.
				force = true;
			}
		} else {
			this._totalTime = this._time = time;

			if (this._easeType) {
				var r = time / duration
					, type = this._easeType
					, pow = this._easePower;
				if (type === 1 || (type === 3 && r >= 0.5)) {
					r = 1 - r;
				}
				if (type === 3) {
					r *= 2;
				}
				if (pow === 1) {
					r *= r;
				} else if (pow === 2) {
					r *= r * r;
				} else if (pow === 3) {
					r *= r * r * r;
				} else if (pow === 4) {
					r *= r * r * r * r;
				}

				if (type === 1) {
					this.ratio = 1 - r;
				} else if (type === 2) {
					this.ratio = r;
				} else if (time / duration < 0.5) {
					this.ratio = r / 2;
				} else {
					this.ratio = 1 - (r / 2);
				}

			} else {
				this.ratio = this._ease.getRatio(time / duration);
			}
		}

		if (this._time === prevTime && !force) {
			return;
		} else if (!this._initted) {
			this._init();
			if (!this._initted || this._gc) { //immediateRender tweens typically won't initialize until the playhead advances (_time is greater than 0) in order to ensure that overwriting occurs properly. Also, if all of the tweening properties have been overwritten (which would cause _gc to be true, as set in _init()), we shouldn't continue otherwise an onStart callback could be called for example.
				return;
			} else if (!force && this._firstPT && ((this.vars.lazy !== false && this._duration) || (this.vars.lazy && !this._duration))) {
				this._time = this._totalTime = prevTime;
				this._rawPrevTime = prevRawPrevTime;
				_lazyTweens.push(this);
				this._lazy = [time, suppressEvents];
				return;
			}
			//_ease is initially set to defaultEase, so now that init() has run, _ease is set properly and we need to recalculate the ratio. Overall this is faster than using conditional logic earlier in the method to avoid having to set ratio twice because we only init() once but renderTime() gets called VERY frequently.
			if (this._time && !isComplete) {
				this.ratio = this._ease.getRatio(this._time / duration);
			} else if (isComplete && this._ease._calcEnd) {
				this.ratio = this._ease.getRatio((this._time === 0) ? 0 : 1);
			}
		}
		if (this._lazy !== false) { //in case a lazy render is pending, we should flush it because the new render is occurring now (imagine a lazy tween instantiating and then immediately the user calls tween.seek(tween.duration()), skipping to the end - the end render would be forced, and then if we didn't flush the lazy render, it'd fire AFTER the seek(), rendering it at the wrong time.
			this._lazy = false;
		}
		if (!this._active)
			if (!this._paused && this._time !== prevTime && time >= 0) {
				this._active = true; //so that if the user renders a tween (as opposed to the timeline rendering it), the timeline is forced to re-render and align it with the proper time/frame on the next rendering cycle. Maybe the tween already finished but the user manually re-renders it as halfway done.
			}
		if (prevTime === 0) {
			if (this._startAt) {
				if (time >= 0) {
					this._startAt.render(time, suppressEvents, force);
				} else if (!callback) {
					callback = "_dummyGS"; //if no callback is defined, use a dummy value just so that the condition at the end evaluates as true because _startAt should render AFTER the normal render loop when the time is negative. We could handle this in a more intuitive way, of course, but the render loop is the MOST important thing to optimize, so this technique allows us to avoid adding extra conditional logic in a high-frequency area.
				}
			}
			if (this.vars.onStart)
				if (this._time !== 0 || duration === 0)
					if (!suppressEvents) {
						this._callback("onStart");
					}
		}
		pt = this._firstPT;
		while (pt) {
			if (pt.f) {
				pt.t[pt.p](pt.c * this.ratio + pt.s);
			} else {
				pt.t[pt.p] = pt.c * this.ratio + pt.s;
			}
			pt = pt._next;
		}

		if (this._onUpdate) {
			if (time < 0)
				if (this._startAt && time !== -0.0001) { //if the tween is positioned at the VERY beginning (_startTime 0) of its parent timeline, it's illegal for the playhead to go back further, so we should not render the recorded startAt values.
					this._startAt.render(time, suppressEvents, force); //note: for performance reasons, we tuck this conditional logic inside less traveled areas (most tweens don't have an onUpdate). We'd just have it at the end before the onComplete, but the values should be updated before any onUpdate is called, so we ALSO put it here and then if it's not called, we do so later near the onComplete.
				}
			if (!suppressEvents)
				if (this._time !== prevTime || isComplete || force) {
					this._callback("onUpdate");
				}
		}
		if (callback)
			if (!this._gc || force) { //check _gc because there's a chance that kill() could be called in an onUpdate
				if (time < 0 && this._startAt && !this._onUpdate && time !== -0.0001) { //-0.0001 is a special value that we use when looping back to the beginning of a repeated TimelineMax, in which case we shouldn't render the _startAt values.
					this._startAt.render(time, suppressEvents, force);
				}
				if (isComplete) {
					if (this._timeline.autoRemoveChildren) {
						this._enabled(false, false);
					}
					this._active = false;
				}
				if (!suppressEvents && this.vars[callback]) {
					this._callback(callback);
				}
				if (duration === 0 && this._rawPrevTime === _tinyNum && rawPrevTime !== _tinyNum) { //the onComplete or onReverseComplete could trigger movement of the playhead and for zero-duration tweens (which must discern direction) that land directly back on their start time, we don't want to fire again on the next render. Think of several addPause()'s in a timeline that forces the playhead to a certain spot, but what if it's already paused and another tween is tweening the "time" of the timeline? Each time it moves [forward] past that spot, it would move back, and since suppressEvents is true, it'd reset _rawPrevTime to _tinyNum so that when it begins again, the callback would fire (so ultimately it could bounce back and forth during that tween). Again, this is a very uncommon scenario, but possible nonetheless.
					this._rawPrevTime = 0;
				}
			}
	};

	p._kill = function (vars, target, overwritingTween) {
		if (vars === "all") {
			vars = null;
		}
		if (vars == null)
			if (target == null || target === this.target) {
				this._lazy = false;
				return this._enabled(false, false);
			}
		target = (typeof (target) !== "string") ? (target || this._targets || this.target) : TweenLite.selector(target) || target;
		var simultaneousOverwrite = (overwritingTween && this._time && overwritingTween._startTime === this._startTime && this._timeline === overwritingTween._timeline)
			, i, overwrittenProps, p, pt, propLookup, changed, killProps, record, killed;
		if ((_isArray(target) || _isSelector(target)) && typeof (target[0]) !== "number") {
			i = target.length;
			while (--i > -1) {
				if (this._kill(vars, target[i], overwritingTween)) {
					changed = true;
				}
			}
		} else {
			if (this._targets) {
				i = this._targets.length;
				while (--i > -1) {
					if (target === this._targets[i]) {
						propLookup = this._propLookup[i] || {};
						this._overwrittenProps = this._overwrittenProps || [];
						overwrittenProps = this._overwrittenProps[i] = vars ? this._overwrittenProps[i] || {} : "all";
						break;
					}
				}
			} else if (target !== this.target) {
				return false;
			} else {
				propLookup = this._propLookup;
				overwrittenProps = this._overwrittenProps = vars ? this._overwrittenProps || {} : "all";
			}

			if (propLookup) {
				killProps = vars || propLookup;
				record = (vars !== overwrittenProps && overwrittenProps !== "all" && vars !== propLookup && (typeof (vars) !== "object" || !vars._tempKill)); //_tempKill is a super-secret way to delete a particular tweening property but NOT have it remembered as an official overwritten property (like in BezierPlugin)
				if (overwritingTween && (TweenLite.onOverwrite || this.vars.onOverwrite)) {
					for (p in killProps) {
						if (propLookup[p]) {
							if (!killed) {
								killed = [];
							}
							killed.push(p);
						}
					}
					if ((killed || !vars) && !_onOverwrite(this, overwritingTween, target, killed)) { //if the onOverwrite returned false, that means the user wants to override the overwriting (cancel it).
						return false;
					}
				}

				for (p in killProps) {
					if ((pt = propLookup[p])) {
						if (simultaneousOverwrite) { //if another tween overwrites this one and they both start at exactly the same time, yet this tween has already rendered once (for example, at 0.001) because it's first in the queue, we should revert the values to where they were at 0 so that the starting values aren't contaminated on the overwriting tween.
							if (pt.f) {
								pt.t[pt.p](pt.s);
							} else {
								pt.t[pt.p] = pt.s;
							}
							changed = true;
						}
						if (pt.pg && pt.t._kill(killProps)) {
							changed = true; //some plugins need to be notified so they can perform cleanup tasks first
						}
						if (!pt.pg || pt.t._overwriteProps.length === 0) {
							if (pt._prev) {
								pt._prev._next = pt._next;
							} else if (pt === this._firstPT) {
								this._firstPT = pt._next;
							}
							if (pt._next) {
								pt._next._prev = pt._prev;
							}
							pt._next = pt._prev = null;
						}
						delete propLookup[p];
					}
					if (record) {
						overwrittenProps[p] = 1;
					}
				}
				if (!this._firstPT && this._initted) { //if all tweening properties are killed, kill the tween. Without this line, if there's a tween with multiple targets and then you killTweensOf() each target individually, the tween would technically still remain active and fire its onComplete even though there aren't any more properties tweening.
					this._enabled(false, false);
				}
			}
		}
		return changed;
	};

	p.invalidate = function () {
		if (this._notifyPluginsOfEnabled) {
			TweenLite._onPluginEvent("_onDisable", this);
		}
		this._firstPT = this._overwrittenProps = this._startAt = this._onUpdate = null;
		this._notifyPluginsOfEnabled = this._active = this._lazy = false;
		this._propLookup = (this._targets) ? {} : [];
		Animation.prototype.invalidate.call(this);
		if (this.vars.immediateRender) {
			this._time = -_tinyNum; //forces a render without having to set the render() "force" parameter to true because we want to allow lazying by default (using the "force" parameter always forces an immediate full render)
			this.render(Math.min(0, -this._delay)); //in case delay is negative.
		}
		return this;
	};

	p._enabled = function (enabled, ignoreTimeline) {
		if (!_tickerActive) {
			_ticker.wake();
		}
		if (enabled && this._gc) {
			var targets = this._targets
				, i;
			if (targets) {
				i = targets.length;
				while (--i > -1) {
					this._siblings[i] = _register(targets[i], this, true);
				}
			} else {
				this._siblings = _register(this.target, this, true);
			}
		}
		Animation.prototype._enabled.call(this, enabled, ignoreTimeline);
		if (this._notifyPluginsOfEnabled)
			if (this._firstPT) {
				return TweenLite._onPluginEvent((enabled ? "_onEnable" : "_onDisable"), this);
			}
		return false;
	};


	//----TweenLite static methods -----------------------------------------------------

	TweenLite.to = function (target, duration, vars) {
		return new TweenLite(target, duration, vars);
	};

	TweenLite.from = function (target, duration, vars) {
		vars.runBackwards = true;
		vars.immediateRender = (vars.immediateRender != false);
		return new TweenLite(target, duration, vars);
	};

	TweenLite.fromTo = function (target, duration, fromVars, toVars) {
		toVars.startAt = fromVars;
		toVars.immediateRender = (toVars.immediateRender != false && fromVars.immediateRender != false);
		return new TweenLite(target, duration, toVars);
	};

	TweenLite.delayedCall = function (delay, callback, params, scope, useFrames) {
		return new TweenLite(callback, 0, {
			delay: delay
			, onComplete: callback
			, onCompleteParams: params
			, callbackScope: scope
			, onReverseComplete: callback
			, onReverseCompleteParams: params
			, immediateRender: false
			, lazy: false
			, useFrames: useFrames
			, overwrite: 0
		});
	};

	TweenLite.set = function (target, vars) {
		return new TweenLite(target, 0, vars);
	};

	TweenLite.getTweensOf = function (target, onlyActive) {
		if (target == null) {
			return [];
		}
		target = (typeof (target) !== "string") ? target : TweenLite.selector(target) || target;
		var i, a, j, t;
		if ((_isArray(target) || _isSelector(target)) && typeof (target[0]) !== "number") {
			i = target.length;
			a = [];
			while (--i > -1) {
				a = a.concat(TweenLite.getTweensOf(target[i], onlyActive));
			}
			i = a.length;
			//now get rid of any duplicates (tweens of arrays of objects could cause duplicates)
			while (--i > -1) {
				t = a[i];
				j = i;
				while (--j > -1) {
					if (t === a[j]) {
						a.splice(i, 1);
					}
				}
			}
		} else {
			a = _register(target).concat();
			i = a.length;
			while (--i > -1) {
				if (a[i]._gc || (onlyActive && !a[i].isActive())) {
					a.splice(i, 1);
				}
			}
		}
		return a;
	};

	TweenLite.killTweensOf = TweenLite.killDelayedCallsTo = function (target, onlyActive, vars) {
		if (typeof (onlyActive) === "object") {
			vars = onlyActive; //for backwards compatibility (before "onlyActive" parameter was inserted)
			onlyActive = false;
		}
		var a = TweenLite.getTweensOf(target, onlyActive)
			, i = a.length;
		while (--i > -1) {
			a[i]._kill(vars, target);
		}
	};



	/*
	 * ----------------------------------------------------------------
	 * TweenPlugin   (could easily be split out as a separate file/class, but included for ease of use (so that people don't need to include another script call before loading plugins which is easy to forget)
	 * ----------------------------------------------------------------
	 */
	var TweenPlugin = _class("plugins.TweenPlugin", function (props, priority) {
		this._overwriteProps = (props || "").split(",");
		this._propName = this._overwriteProps[0];
		this._priority = priority || 0;
		this._super = TweenPlugin.prototype;
	}, true);

	p = TweenPlugin.prototype;
	TweenPlugin.version = "1.18.0";
	TweenPlugin.API = 2;
	p._firstPT = null;
	p._addTween = _addPropTween;
	p.setRatio = _setRatio;

	p._kill = function (lookup) {
		var a = this._overwriteProps
			, pt = this._firstPT
			, i;
		if (lookup[this._propName] != null) {
			this._overwriteProps = [];
		} else {
			i = a.length;
			while (--i > -1) {
				if (lookup[a[i]] != null) {
					a.splice(i, 1);
				}
			}
		}
		while (pt) {
			if (lookup[pt.n] != null) {
				if (pt._next) {
					pt._next._prev = pt._prev;
				}
				if (pt._prev) {
					pt._prev._next = pt._next;
					pt._prev = null;
				} else if (this._firstPT === pt) {
					this._firstPT = pt._next;
				}
			}
			pt = pt._next;
		}
		return false;
	};

	p._roundProps = function (lookup, value) {
		var pt = this._firstPT;
		while (pt) {
			if (lookup[this._propName] || (pt.n != null && lookup[pt.n.split(this._propName + "_").join("")])) { //some properties that are very plugin-specific add a prefix named after the _propName plus an underscore, so we need to ignore that extra stuff here.
				pt.r = value;
			}
			pt = pt._next;
		}
	};

	TweenLite._onPluginEvent = function (type, tween) {
		var pt = tween._firstPT
			, changed, pt2, first, last, next;
		if (type === "_onInitAllProps") {
			//sorts the PropTween linked list in order of priority because some plugins need to render earlier/later than others, like MotionBlurPlugin applies its effects after all x/y/alpha tweens have rendered on each frame.
			while (pt) {
				next = pt._next;
				pt2 = first;
				while (pt2 && pt2.pr > pt.pr) {
					pt2 = pt2._next;
				}
				if ((pt._prev = pt2 ? pt2._prev : last)) {
					pt._prev._next = pt;
				} else {
					first = pt;
				}
				if ((pt._next = pt2)) {
					pt2._prev = pt;
				} else {
					last = pt;
				}
				pt = next;
			}
			pt = tween._firstPT = first;
		}
		while (pt) {
			if (pt.pg)
				if (typeof (pt.t[type]) === "function")
					if (pt.t[type]()) {
						changed = true;
					}
			pt = pt._next;
		}
		return changed;
	};

	TweenPlugin.activate = function (plugins) {
		var i = plugins.length;
		while (--i > -1) {
			if (plugins[i].API === TweenPlugin.API) {
				_plugins[(new plugins[i]())._propName] = plugins[i];
			}
		}
		return true;
	};

	//provides a more concise way to define plugins that have no dependencies besides TweenPlugin and TweenLite, wrapping common boilerplate stuff into one function (added in 1.9.0). You don't NEED to use this to define a plugin - the old way still works and can be useful in certain (rare) situations.
	_gsDefine.plugin = function (config) {
		if (!config || !config.propName || !config.init || !config.API) {
			throw "illegal plugin definition.";
		}
		var propName = config.propName
			, priority = config.priority || 0
			, overwriteProps = config.overwriteProps
			, map = {
			init: "_onInitTween"
			, set: "setRatio"
			, kill: "_kill"
			, round: "_roundProps"
			, initAll: "_onInitAllProps"
		}
			, Plugin = _class("plugins." + propName.charAt(0).toUpperCase() + propName.substr(1) + "Plugin"
			, function () {
				TweenPlugin.call(this, propName, priority);
				this._overwriteProps = overwriteProps || [];
			}, (config.global === true))
			, p = Plugin.prototype = new TweenPlugin(propName)
			, prop;
		p.constructor = Plugin;
		Plugin.API = config.API;
		for (prop in map) {
			if (typeof (config[prop]) === "function") {
				p[map[prop]] = config[prop];
			}
		}
		Plugin.version = config.version;
		TweenPlugin.activate([Plugin]);
		return Plugin;
	};


	//now run through all the dependencies discovered and if any are missing, log that to the console as a warning. This is why it's best to have TweenLite load last - it can check all the dependencies for you.
	a = window._gsQueue;
	if (a) {
		for (i = 0; i < a.length; i++) {
			a[i]();
		}
		for (p in _defLookup) {
			if (!_defLookup[p].func) {
				window.console.log("GSAP encountered missing dependency: com.greensock." + p);
			}
		}
	}

	_tickerActive = false; //ensures that the first official animation forces a ticker.tick() to update the time when it is instantiated

})((typeof (module) !== "undefined" && module.exports && typeof (global) !== "undefined") ? global : this || window, "TweenMax");
/* main page script*/
var open_menu_flag = false;
//START SCROL OPACITY OBJECT
function setOpacity(className) {
	var topWindow = jQuery(window).scrollTop() * 1.5;
	var windowHeight = jQuery(window).height();
	var position = topWindow / windowHeight;
	position = 1 - position;

	jQuery(className).css('opacity', position);

}

$(document).ready(function () {

	//START SCROLL SHOW ARROW TOP
	function checkScroll() {
		if ($(window).scrollTop() > 500) {
			$('.scroll-top.fixed').fadeIn();

		} else {
			$('.scroll-top.fixed').fadeOut();
		}
	}

	//END SCROLL SHOW ARROW TOP

	//START SLICK SLIDER
	$('.center').slick({
		centerMode: true
		, centerPadding: '0'
		, slidesToShow: 5
		, slidesToScroll: 5
		, focusOnSelect: true
		, arrows: true
		, infinite: true
		, draggable: true
		, autoplay: false
		, prevArrow: '.prev-arrow'
		, nextArrow: '.next-arrow'
		, responsive: [
			{
				breakpoint: 1280
				, settings: {
				arrows: false
				, centerMode: true
				, slidesToShow: 3
			}
			}
			, {
				breakpoint: 690
				, settings: {
					arrows: false
					, centerMode: true
					, slidesToShow: 1
				}
			}
		]
	});

	$('.partners').slick({
		centerMode: true
		, centerPadding: '0px'
		, slidesToShow: 5
		, slidesToScroll: 5
		, focusOnSelect: true
		, arrows: true
		, infinite: true
		, draggable: true
		, autoplay: false
		, prevArrow: '.prev'
		, nextArrow: '.next'
		, responsive: [
			{
				breakpoint: 1280
				, settings: {
				arrows: false
				, centerMode: true
				, slidesToShow: 3
			}
			}
			, {
				breakpoint: 580
				, settings: {
					arrows: false
					, centerMode: true
					, slidesToShow: 2
				}
			}
			, {
				breakpoint: 380
				, settings: {
					arrows: false
					, centerMode: true
					, slidesToShow: 1
				}
			}

		]
	});


	$('.comment').slick({
		dots: true
		, infinite: true
		, speed: 500
		, fade: true
		, prevArrow: '.arrow-prev'
		, nextArrow: '.arrow-next'
		, cssEase: 'linear'
	});

	//END SLICK SLIDER

	//START GALLERY
	$(".gallery").mixItUp({
		animation: {
			duration: 700
			, effects: 'stagger(34ms) rotateZ(45deg) fade scale(0.41)'
			, easing: 'ease'
		}
	});

	$(".menu-gallery li").click(function () {
		$(".menu-gallery li").removeClass("active");
		$(this).addClass("active");
	});
	//END GALLERY


	//START ARROW DOWN
	$('.arrow-down a, .scroll-top').on('click', function () {
		var scrollAnchor = $(this).attr('data-scroll')
			, scrollPoint = $('.scroll[data-anchor="' + scrollAnchor + '"]').offset().top - 28
			, $root = $('body,html');
		//$root.css({
		//    overflowY: 'auto',
		//    overflowX: 'hidden'
		//});
		$root.animate({
			scrollTop: scrollPoint
		}, 1000);
		return false;
	});
	//END ARROW DOWN




	$(window).scroll(function () {
		if (!open_menu_flag) {
			checkScroll();
			setOpacity('.home-page  header .col-lg-12');
		}
	});

	//END SCROL OPACITY OBJECT


	//START SVG BUUTON
	var tmax_optionsGlobal = {
		repeat: 0
		, repeatDelay: 0.65
		, yoyo: false
	};

	CSSPlugin.useSVGTransformAttr = true;

	var tl = new TimelineMax(tmax_optionsGlobal)
		, path = 'svg *'
		, stagger_val = 0.0225
		, duration = 4;

	$.each($(path), function (i, el) {
		tl.set($(this), {
			x: '+=' + getRandom(-500, 500)
			, y: '+=' + getRandom(-500, 500)
			, rotation: '+=' + getRandom(-720, 720)
			, scale: 0
			, opacity: 0
		});
	});

	var stagger_opts_to = {
		x: 0
		, y: 0
		, opacity: 1
		, scale: 1
		, rotation: 0
		, ease: Power4.easeInOut
	};

	tl.staggerTo(path, duration, stagger_opts_to, stagger_val);

	var $svg = $('svg');

	function getRandom(min, max) {
		return Math.random() * (max - min) + min;
	}

	//END SVG BUUTON


	//START BLOCK HEIGHT

	function resiseW() {
		var videoHeight = $('#bg-video').outerHeight();
		$('.size').outerHeight(videoHeight);
	}

	resiseW();
	$(window).load(resiseW);
	$(window).resize(resiseW);
	//END BLOCK HEIGHT

	$('body,html').css({
		overflowY: 'hidden'
	});
	//START TIMEOUT
	setTimeout(function () {
		$('.home-page header .col-lg-12').animated("slideInDown");
		$(' .home-page .col-lg-12.size').show();
		$('body,html').css({
			overflowY: 'auto'
		});
		resiseW();
	}, 2400);

	//END TIMEOUT


	//START ADAPTIVE

	if ($(window).width() > 991) {
		setTimeout(function () {
			$('header .logo').show();
			$('.header ul.menu').css('display', ' inline-block');
		}, 2400);
		$('.header .logo-mobile, .header .burger-menu').hide();
	} else {
		$('.header .logo').hide();
		setTimeout(function () {
			$('.burger-menu, .logo-mobile').animated("slideInDown");
			$('.header ul.menu').hide();
			$('.burger-menu,.logo-mobile').show();
		}, 2400);
	}


	$(window).on('resize', function () {
		if ($(window).width() > 991) {
			$('header .logo').show();
			$('.header ul.menu').css('display', ' inline-block');
			$('.header .logo-mobile,.header .burger-menu').hide();
		} else {
			$('.header .logo-mobile,.header .burger-menu').show();
			$('.header .logo,.header ul.menu').hide();
		}

		if ($(window).width() > 579) {
			$('.header, .header .wrapper, .header .col-lg-12').addClass('size');
		} else {
			$('.header, .header .wrapper, .header .col-lg-12').removeClass('size');
			$('.header .col-lg-12').height($('.header').height());
		}
	});

	if ($(window).width() > 579) {
		$('.header, .header .wrapper, .header .col-lg-12').addClass('size');
	} else {
		$('.header, .header .wrapper, .header .col-lg-12').removeClass('size');
		$('.header .col-lg-12').height($('.header').height());
	}
	//END ADAPTIVE

	$(".title").animated("bounceInLeft");
	$(".mini-title, .leave-comments .col-lg-5").animated("bounceInRight");

	$(".first-reason:nth-child(3),.first-reason:nth-child(6)").animated("bounceInUp");

	$(".first-reason:nth-child(5)," +
		".first-reason:nth-child(2)," +
		" .prev-arrow, .arrow-prev," +
		" .leave-comments .col-lg-7," +
		".da-thumbs li:nth-child(4n-3)," +
		".da-thumbs li:nth-child(4n-2)").animated("bounceInLeft");

	$(".first-reason:nth-child(4), " +
		".next-arrow, " +
		".arrow-next," +
		".da-thumbs li:nth-child(4n)," +
		".first-reason:nth-child(7)," +
		".da-thumbs li:nth-child(4n-1)").animated("bounceInRight");

	$("#servise-desc, #map, #global-map h4, .leave-comments h2 ").animated("zoomInUp");
	$(".center .slick-slide.slick-active").animated("fadeInUp");
	$("section a.button").animated("rotateIn");

});


/*google*/


//window.google = window.google || {};
//google.maps = google.maps || {};
//(function() {
//
//    function getScript(src) {
//        document.write('<' + 'script src="' + src + '"><' + '/script>');
//    }
//
//    var modules = google.maps.modules = {};
//    google.maps.__gjsload__ = function(name, text) {
//        modules[name] = text;
//    };
//
//    google.maps.Load = function(apiLoad) {
//        delete google.maps.Load;
//        apiLoad([0.009999999776482582,[[["https://mts0.googleapis.com/maps/vt?lyrs=m@352000000\u0026src=api\u0026hl=en-US\u0026","https://mts1.googleapis.com/maps/vt?lyrs=m@352000000\u0026src=api\u0026hl=en-US\u0026"],null,null,null,null,"m@352000000",["https://mts0.google.com/maps/vt?lyrs=m@352000000\u0026src=api\u0026hl=en-US\u0026","https://mts1.google.com/maps/vt?lyrs=m@352000000\u0026src=api\u0026hl=en-US\u0026"]],[["https://khms0.googleapis.com/kh?v=691\u0026hl=en-US\u0026","https://khms1.googleapis.com/kh?v=691\u0026hl=en-US\u0026"],null,null,null,1,"691",["https://khms0.google.com/kh?v=691\u0026hl=en-US\u0026","https://khms1.google.com/kh?v=691\u0026hl=en-US\u0026"]],null,[["https://mts0.googleapis.com/maps/vt?lyrs=t@132,r@352000000\u0026src=api\u0026hl=en-US\u0026","https://mts1.googleapis.com/maps/vt?lyrs=t@132,r@352000000\u0026src=api\u0026hl=en-US\u0026"],null,null,null,null,"t@132,r@352000000",["https://mts0.google.com/maps/vt?lyrs=t@132,r@352000000\u0026src=api\u0026hl=en-US\u0026","https://mts1.google.com/maps/vt?lyrs=t@132,r@352000000\u0026src=api\u0026hl=en-US\u0026"]],null,null,[["https://cbks0.googleapis.com/cbk?","https://cbks1.googleapis.com/cbk?"]],[["https://khms0.googleapis.com/kh?v=97\u0026hl=en-US\u0026","https://khms1.googleapis.com/kh?v=97\u0026hl=en-US\u0026"],null,null,null,null,"97",["https://khms0.google.com/kh?v=97\u0026hl=en-US\u0026","https://khms1.google.com/kh?v=97\u0026hl=en-US\u0026"]],[["https://mts0.googleapis.com/mapslt?hl=en-US\u0026","https://mts1.googleapis.com/mapslt?hl=en-US\u0026"]],[["https://mts0.googleapis.com/mapslt/ft?hl=en-US\u0026","https://mts1.googleapis.com/mapslt/ft?hl=en-US\u0026"]],[["https://mts0.googleapis.com/maps/vt?hl=en-US\u0026","https://mts1.googleapis.com/maps/vt?hl=en-US\u0026"]],[["https://mts0.googleapis.com/mapslt/loom?hl=en-US\u0026","https://mts1.googleapis.com/mapslt/loom?hl=en-US\u0026"]],[["https://mts0.googleapis.com/mapslt?hl=en-US\u0026","https://mts1.googleapis.com/mapslt?hl=en-US\u0026"]],[["https://mts0.googleapis.com/mapslt/ft?hl=en-US\u0026","https://mts1.googleapis.com/mapslt/ft?hl=en-US\u0026"]],[["https://mts0.googleapis.com/mapslt/loom?hl=en-US\u0026","https://mts1.googleapis.com/mapslt/loom?hl=en-US\u0026"]]],["en-US","US",null,0,null,null,"https://maps.gstatic.com/mapfiles/","https://csi.gstatic.com","https://maps.googleapis.com","https://maps.googleapis.com",null,"https://maps.google.com","https://gg.google.com","https://maps.gstatic.com/maps-api-v3/api/images/","https://www.google.com/maps",0,"https://www.google.com"],["https://maps.googleapis.com/maps-api-v3/api/js/25/5","3.25.5"],[1190751973],1,null,null,null,null,null,"",null,null,1,"https://khms.googleapis.com/mz?v=691\u0026",null,"https://earthbuilder.googleapis.com","https://earthbuilder.googleapis.com",null,"https://mts.googleapis.com/maps/vt/icon",[["https://maps.googleapis.com/maps/vt"],["https://maps.googleapis.com/maps/vt"],null,null,null,null,null,null,null,null,null,null,["https://www.google.com/maps/vt"],"/maps/vt",352000000,132],2,500,[null,"https://g0.gstatic.com/landmark/tour","https://g0.gstatic.com/landmark/config",null,"https://www.google.com/maps/preview/log204","","https://static.panoramio.com.storage.googleapis.com/photos/",["https://geo0.ggpht.com/cbk","https://geo1.ggpht.com/cbk","https://geo2.ggpht.com/cbk","https://geo3.ggpht.com/cbk"],"https://maps.googleapis.com/maps/api/js/GeoPhotoService.GetMetadata","https://maps.googleapis.com/maps/api/js/GeoPhotoService.SingleImageSearch",["https://lh3.ggpht.com/","https://lh4.ggpht.com/","https://lh5.ggpht.com/","https://lh6.ggpht.com/"]],["https://www.google.com/maps/api/js/master?pb=!1m2!1u25!2s5!2sen-US!3sUS!4s25/5","https://www.google.com/maps/api/js/widget?pb=!1m2!1u25!2s5!2sen-US"],null,0,null,"/maps/api/js/ApplicationService.GetEntityDetails",0,null,null,[null,null,null,null,null,null,null,null,null,[0,0]],null,[],["25.5"]], loadScriptTime);
//    };
//    var loadScriptTime = (new Date).getTime();
//})();
//// inlined
//(function(_){'use strict';var Ea,Fa,Ra,Wa,$a,fb,gb,hb,ib,mb,nb,qb,tb,pb,ub,yb,Fb,Lb,Qb,Yb,Zb,bc,ec,fc,hc,jc,lc,gc,ic,nc,qc,rc,xc,Jc,Lc,Qc,Pc,Rc,Sc,Tc,Uc,Yc,dd,fd,hd,id,qd,sd,rd,ud,zd,Ad,Fd,Od,Pd,Qd,ce,de,ge,je,le,ke,me,te,ue,xe,Ae,Be,Ce,Ge,He,Ie,Je,Me,Oe,Pe,Qe,Re,Se,Te,af,bf,cf,df,ef,mf,nf,of,rf,uf,Af,Bf,Df,Gf,If,Tf,Uf,Vf,Wf,Xf,Yf,$f,ag,bg,cg,eg,jg,lg,ug,vg,Bg,zg,Cg,Dg,Hg,Kg,Lg,Rg,Sg,Vg,Wg,Xg,Yg,Zg,Ba,Ca;_.aa="ERROR";_.ba="INVALID_REQUEST";_.ca="MAX_DIMENSIONS_EXCEEDED";_.da="MAX_ELEMENTS_EXCEEDED";_.ea="MAX_WAYPOINTS_EXCEEDED";
//    _.fa="NOT_FOUND";_.ga="OK";_.ha="OVER_QUERY_LIMIT";_.ia="REQUEST_DENIED";_.ja="UNKNOWN_ERROR";_.ka="ZERO_RESULTS";_.la=function(){return function(a){return a}};_.k=function(){return function(){}};_.ma=function(a){return function(b){this[a]=b}};_.m=function(a){return function(){return this[a]}};_.oa=function(a){return function(){return a}};_.ra=function(a){return function(){return _.qa[a].apply(this,arguments)}};_.sa=function(a){return void 0!==a};_.ta=_.k();
//    _.ua=function(a){a.Jc=function(){return a.Mb?a.Mb:a.Mb=new a}};
//    _.va=function(a){var b=typeof a;if("object"==b)if(a){if(a instanceof Array)return"array";if(a instanceof Object)return b;var c=Object.prototype.toString.call(a);if("[object Window]"==c)return"object";if("[object Array]"==c||"number"==typeof a.length&&"undefined"!=typeof a.splice&&"undefined"!=typeof a.propertyIsEnumerable&&!a.propertyIsEnumerable("splice"))return"array";if("[object Function]"==c||"undefined"!=typeof a.call&&"undefined"!=typeof a.propertyIsEnumerable&&!a.propertyIsEnumerable("call"))return"function"}else return"null";
//    else if("function"==b&&"undefined"==typeof a.call)return"object";return b};_.wa=function(a){var b=_.va(a);return"array"==b||"object"==b&&"number"==typeof a.length};_.xa=function(a){return"string"==typeof a};_.ya=function(a){return"number"==typeof a};_.za=function(a){return"function"==_.va(a)};_.Aa=function(a){var b=typeof a;return"object"==b&&null!=a||"function"==b};_.Da=function(a){return a[Ba]||(a[Ba]=++Ca)};Ea=function(a,b,c){return a.call.apply(a.bind,arguments)};
//    Fa=function(a,b,c){if(!a)throw Error();if(2<arguments.length){var d=Array.prototype.slice.call(arguments,2);return function(){var c=Array.prototype.slice.call(arguments);Array.prototype.unshift.apply(c,d);return a.apply(b,c)}}return function(){return a.apply(b,arguments)}};_.u=function(a,b,c){_.u=Function.prototype.bind&&-1!=Function.prototype.bind.toString().indexOf("native code")?Ea:Fa;return _.u.apply(null,arguments)};_.Ga=function(){return+new Date};
//    _.v=function(a,b){function c(){}c.prototype=b.prototype;a.Vc=b.prototype;a.prototype=new c;a.prototype.constructor=a;a.Jr=function(a,c,f){for(var g=Array(arguments.length-2),h=2;h<arguments.length;h++)g[h-2]=arguments[h];return b.prototype[c].apply(a,g)}};_.x=function(a){return a?a.length:0};_.Ha=function(a,b){return function(c){return b(a(c))}};_.Ja=function(a,b){_.Ia(b,function(c){a[c]=b[c]})};_.Ka=function(a){for(var b in a)return!1;return!0};
//    _.La=function(a,b,c){null!=b&&(a=Math.max(a,b));null!=c&&(a=Math.min(a,c));return a};_.Ma=function(a,b,c){c-=b;return((a-b)%c+c)%c+b};_.Na=function(a,b,c){return Math.abs(a-b)<=(c||1E-9)};_.Oa=function(a,b){for(var c=[],d=_.x(a),e=0;e<d;++e)c.push(b(a[e],e));return c};_.Qa=function(a,b){for(var c=_.Pa(void 0,_.x(b)),d=_.Pa(void 0,0);d<c;++d)a.push(b[d])};Ra=function(a){return null==a};_.Sa=function(a){return"undefined"!=typeof a};_.A=function(a){return"number"==typeof a};
//    _.Ta=function(a){return"object"==typeof a};_.Pa=function(a,b){return null==a?b:a};_.Ua=function(a){return"string"==typeof a};_.Va=function(a){return a===!!a};_.B=function(a,b){for(var c=0,d=_.x(a);c<d;++c)b(a[c],c)};_.Ia=function(a,b){for(var c in a)b(c,a[c])};Wa=function(a,b,c){return Function.prototype.call.apply(Array.prototype.slice,arguments)};_.Xa=function(a){return null!=a&&"object"==typeof a&&"number"==typeof a.length};
//    _.Za=function(a){return function(){var b=this,c=arguments;_.Ya(function(){a.apply(b,c)})}};_.Ya=function(a){return window.setTimeout(a,0)};$a=function(a,b){if(Object.prototype.hasOwnProperty.call(a,b))return a[b]};_.ab=function(a){window.console&&window.console.error&&window.console.error(a)};_.db=function(a){a=a||window.event;_.bb(a);_.cb(a)};_.bb=function(a){a.cancelBubble=!0;a.stopPropagation&&a.stopPropagation()};
//    _.cb=function(a){a.preventDefault&&_.Sa(a.defaultPrevented)?a.preventDefault():a.returnValue=!1};_.eb=function(a){a.handled=!0;_.Sa(a.bubbles)||(a.returnValue="handled")};fb=function(a,b){a.__e3_||(a.__e3_={});a=a.__e3_;a[b]||(a[b]={});return a[b]};gb=function(a,b){a=a.__e3_||{};if(b)b=a[b]||{};else{b={};for(var c in a)_.Ja(b,a[c])}return b};hb=function(a,b){return function(c){return b.call(a,c,this)}};
//    ib=function(a,b,c){return function(d){var e=[b,a];_.Qa(e,arguments);_.C.trigger.apply(this,e);c&&_.eb.apply(null,arguments)}};mb=function(a,b,c,d){this.Mb=a;this.H=b;this.j=c;this.R=null;this.S=d;this.id=++jb;fb(a,b)[this.id]=this;kb&&"tagName"in a&&(lb[this.id]=this)};
//    nb=function(a){return a.R=function(b){b||(b=window.event);if(b&&!b.target)try{b.target=b.srcElement}catch(d){}var c;c=a.j.apply(a.Mb,[b]);return b&&"click"==b.type&&(b=b.srcElement)&&"A"==b.tagName&&"javascript:void(0)"==b.href?!1:c}};_.ob=function(a){return""+(_.Aa(a)?_.Da(a):a)};_.G=_.k();qb=function(a,b){var c=b+"_changed";if(a[c])a[c]();else a.changed(b);var c=pb(a,b),d;for(d in c){var e=c[d];qb(e.Od,e.dc)}_.C.trigger(a,b.toLowerCase()+"_changed")};
//    _.sb=function(a){return rb[a]||(rb[a]=a.substr(0,1).toUpperCase()+a.substr(1))};tb=function(a){a.gm_accessors_||(a.gm_accessors_={});return a.gm_accessors_};pb=function(a,b){a.gm_bindings_||(a.gm_bindings_={});a.gm_bindings_.hasOwnProperty(b)||(a.gm_bindings_[b]={});return a.gm_bindings_[b]};ub=function(a){this.message=a;this.name="InvalidValueError";this.stack=Error().stack};_.vb=function(a,b){var c="";if(null!=b){if(!(b instanceof ub))return b;c=": "+b.message}return new ub(a+c)};
//    _.wb=function(a){if(!(a instanceof ub))throw a;_.ab(a.name+": "+a.message)};_.xb=function(a,b){return function(c){if(!c||!_.Ta(c))throw _.vb("not an Object");var d={},e;for(e in c)if(d[e]=c[e],!b&&!a[e])throw _.vb("unknown property "+e);for(e in a)try{var f=a[e](d[e]);if(_.Sa(f)||Object.prototype.hasOwnProperty.call(c,e))d[e]=a[e](d[e])}catch(g){throw _.vb("in property "+e,g);}return d}};yb=function(a){try{return!!a.cloneNode}catch(b){return!1}};
//    _.zb=function(a,b,c){return c?function(c){if(c instanceof a)return c;try{return new a(c)}catch(e){throw _.vb("when calling new "+b,e);}}:function(c){if(c instanceof a)return c;throw _.vb("not an instance of "+b);}};_.Ab=function(a){return function(b){for(var c in a)if(a[c]==b)return b;throw _.vb(b);}};_.Bb=function(a){return function(b){if(!_.Xa(b))throw _.vb("not an Array");return _.Oa(b,function(b,d){try{return a(b)}catch(e){throw _.vb("at index "+d,e);}})}};
//    _.Cb=function(a,b){return function(c){if(a(c))return c;throw _.vb(b||""+c);}};_.Db=function(a){var b=arguments;return function(a){for(var d=[],e=0,f=b.length;e<f;++e){var g=b[e];try{(g.Qh||g)(a)}catch(h){if(!(h instanceof ub))throw h;d.push(h.message);continue}return(g.then||g)(a)}throw _.vb(d.join("; and "));}};_.Eb=function(a){return function(b){return null==b?b:a(b)}};Fb=function(a){return function(b){if(b&&null!=b[a])return b;throw _.vb("no "+a+" property");}};
//    _.Ib=function(a){return a.replace(/^[\s\xa0]+|[\s\xa0]+$/g,"")};_.Kb=function(){return-1!=_.Jb.toLowerCase().indexOf("webkit")};
//    _.Mb=function(a,b){var c=0;a=_.Ib(String(a)).split(".");b=_.Ib(String(b)).split(".");for(var d=Math.max(a.length,b.length),e=0;0==c&&e<d;e++){var f=a[e]||"",g=b[e]||"",h=/(\d*)(\D*)/g,l=/(\d*)(\D*)/g;do{var n=h.exec(f)||["","",""],p=l.exec(g)||["","",""];if(0==n[0].length&&0==p[0].length)break;c=Lb(0==n[1].length?0:(0,window.parseInt)(n[1],10),0==p[1].length?0:(0,window.parseInt)(p[1],10))||Lb(0==n[2].length,0==p[2].length)||Lb(n[2],p[2])}while(0==c)}return c};
//    Lb=function(a,b){return a<b?-1:a>b?1:0};_.Nb=function(a,b,c){c=null==c?0:0>c?Math.max(0,a.length+c):c;if(_.xa(a))return _.xa(b)&&1==b.length?a.indexOf(b,c):-1;for(;c<a.length;c++)if(c in a&&a[c]===b)return c;return-1};_.Pb=function(a,b,c){for(var d=a.length,e=_.xa(a)?a.split(""):a,f=0;f<d;f++)f in e&&b.call(c,e[f],f,a)};Qb=function(a,b){for(var c=a.length,d=_.xa(a)?a.split(""):a,e=0;e<c;e++)if(e in d&&b.call(void 0,d[e],e,a))return e;return-1};
//    _.Sb=function(a,b){b=_.Nb(a,b);var c;(c=0<=b)&&_.Rb(a,b);return c};_.Rb=function(a,b){Array.prototype.splice.call(a,b,1)};_.Tb=function(a){return a*Math.PI/180};_.Ub=function(a){return 180*a/Math.PI};_.H=function(a,b,c){if(a&&(void 0!==a.lat||void 0!==a.lng))try{Vb(a),b=a.lng,a=a.lat,c=!1}catch(d){_.wb(d)}a-=0;b-=0;c||(a=_.La(a,-90,90),180!=b&&(b=_.Ma(b,-180,180)));this.lat=function(){return a};this.lng=function(){return b}};_.Wb=function(a){return _.Tb(a.lat())};_.Xb=function(a){return _.Tb(a.lng())};
//    Yb=function(a,b){b=Math.pow(10,b);return Math.round(a*b)/b};Zb=_.k();_.$b=function(a){try{if(a instanceof _.H)return a;a=Vb(a);return new _.H(a.lat,a.lng)}catch(b){throw _.vb("not a LatLng or LatLngLiteral",b);}};_.ac=function(a){this.j=_.$b(a)};bc=function(a){if(a instanceof Zb)return a;try{return new _.ac(_.$b(a))}catch(b){}throw _.vb("not a Geometry or LatLng or LatLngLiteral object");};_.cc=function(a,b){if(a)return function(){--a||b()};b();return _.ta};
//    _.dc=function(a,b,c){var d=a.getElementsByTagName("head")[0];a=a.createElement("script");a.type="text/javascript";a.charset="UTF-8";a.src=b;c&&(a.onerror=c);d.appendChild(a);return a};ec=function(a){for(var b="",c=0,d=arguments.length;c<d;++c){var e=arguments[c];e.length&&"/"==e[0]?b=e:(b&&"/"!=b[b.length-1]&&(b+="/"),b+=e)}return b};fc=function(a){this.R=window.document;this.j={};this.H=a};hc=function(){this.S={};this.H={};this.T={};this.j={};this.R=new gc};
//    jc=function(a,b){a.S[b]||(a.S[b]=!0,ic(a.R,function(c){for(var d=c.qj[b],e=d?d.length:0,f=0;f<e;++f){var g=d[f];a.j[g]||jc(a,g)}c=c.Io;c.j[b]||_.dc(c.R,ec(c.H,b)+".js")}))};lc=function(a,b){var c=kc;this.Io=a;this.qj=c;a={};for(var d in c)for(var e=c[d],f=0,g=e.length;f<g;++f){var h=e[f];a[h]||(a[h]=[]);a[h].push(d)}this.cq=a;this.Xm=b};gc=function(){this.j=[]};ic=function(a,b){a.H?b(a.H):a.j.push(b)};_.J=function(a,b,c){var d=hc.Jc();a=""+a;d.j[a]?b(d.j[a]):((d.H[a]=d.H[a]||[]).push(b),c||jc(d,a))};
//    _.mc=function(a,b){hc.Jc().j[""+a]=b};nc=function(a,b,c){var d=[],e=_.cc(a.length,function(){b.apply(null,d)});_.Pb(a,function(a,b){_.J(a,function(a){d[b]=a;e()},c)})};_.oc=function(a){a=a||{};this.R=a.id;this.j=null;try{this.j=a.geometry?bc(a.geometry):null}catch(b){_.wb(b)}this.H=a.properties||{}};_.L=function(a,b){this.x=a;this.y=b};qc=function(a){if(a instanceof _.L)return a;try{_.xb({x:_.pc,y:_.pc},!0)(a)}catch(b){throw _.vb("not a Point",b);}return new _.L(a.x,a.y)};
//    _.M=function(a,b,c,d){this.width=a;this.height=b;this.R=c||"px";this.H=d||"px"};rc=function(a){if(a instanceof _.M)return a;try{_.xb({height:_.pc,width:_.pc},!0)(a)}catch(b){throw _.vb("not a Size",b);}return new _.M(a.width,a.height)};_.sc=function(a){return function(){return this.get(a)}};_.tc=function(a,b){return b?function(c){try{this.set(a,b(c))}catch(d){_.wb(_.vb("set"+_.sb(a),d))}}:function(b){this.set(a,b)}};
//    _.wc=function(a,b){_.Ia(b,function(b,d){var e=_.sc(b);a["get"+_.sb(b)]=e;d&&(d=_.tc(b,d),a["set"+_.sb(b)]=d)})};_.yc=function(a){this.j=a||[];xc(this)};xc=function(a){a.set("length",a.j.length)};_.zc=function(a){this.R=a||_.ob;this.H={}};_.Ac=function(a,b){var c=a.H,d=a.R(b);c[d]||(c[d]=b,_.C.trigger(a,"insert",b),a.j&&a.j(b))};_.Bc=_.ma("j");_.Cc=function(a,b,c){this.heading=a;this.pitch=_.La(b,-90,90);this.zoom=Math.max(0,c)};_.Dc=function(){this.__gm=new _.G;this.S=null};_.Ec=_.la();
//    _.Gc=function(a,b,c){for(var d in a)b.call(c,a[d],d,a)};_.Hc=function(a){return-1!=_.Jb.indexOf(a)};_.Ic=function(){return _.Hc("Trident")||_.Hc("MSIE")};Jc=function(){return(_.Hc("Chrome")||_.Hc("CriOS"))&&!_.Hc("Edge")};Lc=function(a){_.Kc.setTimeout(function(){throw a;},0)};Qc=function(){var a=_.Mc.H,a=Nc(a);!_.za(_.Kc.setImmediate)||_.Kc.Window&&_.Kc.Window.prototype&&!_.Hc("Edge")&&_.Kc.Window.prototype.setImmediate==_.Kc.setImmediate?(Oc||(Oc=Pc()),Oc(a)):_.Kc.setImmediate(a)};
//    Pc=function(){var a=_.Kc.MessageChannel;"undefined"===typeof a&&"undefined"!==typeof window&&window.postMessage&&window.addEventListener&&!_.Hc("Presto")&&(a=function(){var a=window.document.createElement("IFRAME");a.style.display="none";a.src="";window.document.documentElement.appendChild(a);var b=a.contentWindow,a=b.document;a.open();a.write("");a.close();var c="callImmediate"+Math.random(),d="file:"==b.location.protocol?"*":b.location.protocol+"//"+b.location.host,a=(0,_.u)(function(a){if(("*"==
//        d||a.origin==d)&&a.data==c)this.port1.onmessage()},this);b.addEventListener("message",a,!1);this.port1={};this.port2={postMessage:function(){b.postMessage(c,d)}}});if("undefined"!==typeof a&&!_.Ic()){var b=new a,c={},d=c;b.port1.onmessage=function(){if(_.sa(c.next)){c=c.next;var a=c.cb;c.cb=null;a()}};return function(a){d.next={cb:a};d=d.next;b.port2.postMessage(0)}}return"undefined"!==typeof window.document&&"onreadystatechange"in window.document.createElement("SCRIPT")?function(a){var b=window.document.createElement("SCRIPT");
//        b.onreadystatechange=function(){b.onreadystatechange=null;b.parentNode.removeChild(b);b=null;a();a=null};window.document.documentElement.appendChild(b)}:function(a){_.Kc.setTimeout(a,0)}};Rc=function(a,b,c){this.S=c;this.R=a;this.T=b;this.H=0;this.j=null};Sc=function(){this.H=this.j=null};Tc=function(){this.next=this.j=this.Kd=null};_.Mc=function(a,b){_.Mc.j||_.Mc.T();_.Mc.R||(_.Mc.j(),_.Mc.R=!0);_.Mc.S.add(a,b)};Uc=function(a,b){return function(c){return c.Kd==a&&c.context==(b||null)}};
//    Yc=function(a){this.Ea=[];this.j=a&&a.Ie||_.ta;this.H=a&&a.Ke||_.ta};_.$c=function(a,b,c,d){function e(){_.Pb(f,function(a){b.call(c||null,function(b){if(a.Le){if(a.Le.Bi)return;a.Le.Bi=!0;_.Sb(g.Ea,a);g.Ea.length||g.j()}a.Kd.call(a.context,b)})})}var f=a.Ea.slice(0),g=a;d&&d.zq?e():Zc(e)};_.ad=function(){this.Ea=new Yc({Ie:(0,_.u)(this.Ie,this),Ke:(0,_.u)(this.Ke,this)})};_.bd=function(){_.ad.call(this)};_.cd=function(a){_.ad.call(this);this.j=a};dd=_.k();
//    fd=function(a){var b=a;if(a instanceof Array)b=Array(a.length),_.ed(b,a);else if(a instanceof Object){var c=b={},d;for(d in a)a.hasOwnProperty(d)&&(c[d]=fd(a[d]))}return b};_.ed=function(a,b){for(var c=0;c<b.length;++c)b.hasOwnProperty(c)&&(a[c]=fd(b[c]))};_.N=function(a,b){a[b]||(a[b]=[]);return a[b]};_.gd=function(a,b){return a[b]?a[b].length:0};hd=function(a,b,c,d){this.type=a;this.label=b;this.en=c;this.Hd=d};
//    id=function(a){switch(a){case "d":case "f":case "i":case "j":case "u":case "v":case "x":case "y":case "g":case "h":case "n":case "o":case "e":return 0;case "s":case "z":case "B":return"";case "b":return!1;default:return null}};_.jd=function(a,b,c){return new hd(a,1,_.sa(b)?b:id(a),c)};_.kd=function(a,b,c){return new hd(a,2,_.sa(b)?b:id(a),c)};_.ld=function(a,b){return new hd(a,3,void 0,b)};_.md=function(a){return _.jd("i",a)};_.nd=function(a){return _.jd("v",a)};_.od=function(a){return _.jd("b",a)};
//    _.pd=function(a){return _.jd("e",a)};_.O=function(a,b){return _.jd("m",a,b)};qd=_.k();sd=function(a,b,c){for(var d=1;d<b.U.length;++d){var e=b.U[d],f=a[d+b.ka];if(e&&null!=f)if(3==e.label)for(var g=0;g<f.length;++g)rd(f[g],d,e,c);else rd(f,d,e,c)}};rd=function(a,b,c,d){if("m"==c.type){var e=d.length;sd(a,c.Hd,d);d.splice(e,0,[b,"m",d.length-e].join(""))}else"b"==c.type&&(a=a?"1":"0"),d.push([b,c.type,(0,window.encodeURIComponent)(a)].join(""))};
//    _.td=function(){return _.Hc("iPhone")&&!_.Hc("iPod")&&!_.Hc("iPad")};ud=function(){var a=_.Kc.document;return a?a.documentMode:void 0};_.xd=function(a){return vd[a]||(vd[a]=0<=_.Mb(_.wd,a))};_.yd=function(a,b){this.j=a||0;this.H=b||0};zd=_.k();Ad=function(a,b){-180==a&&180!=b&&(a=180);-180==b&&180!=a&&(b=180);this.j=a;this.H=b};_.Bd=function(a){return a.j>a.H};_.Dd=function(a,b){return 1E-9>=Math.abs(b.j-a.j)%360+Math.abs(_.Cd(b)-_.Cd(a))};_.Ed=function(a,b){var c=b-a;return 0<=c?c:b+180-(a-180)};
//    _.Cd=function(a){return a.isEmpty()?0:_.Bd(a)?360-(a.j-a.H):a.H-a.j};Fd=function(a,b){this.H=a;this.j=b};_.Gd=function(a){return a.isEmpty()?0:a.j-a.H};_.Hd=function(a,b){a=a&&_.$b(a);b=b&&_.$b(b);if(a){b=b||a;var c=_.La(a.lat(),-90,90),d=_.La(b.lat(),-90,90);this.H=new Fd(c,d);a=a.lng();b=b.lng();360<=b-a?this.j=new Ad(-180,180):(a=_.Ma(a,-180,180),b=_.Ma(b,-180,180),this.j=new Ad(a,b))}else this.H=new Fd(1,-1),this.j=new Ad(180,-180)};
//    _.Id=function(a,b,c,d){return new _.Hd(new _.H(a,b,!0),new _.H(c,d,!0))};_.Kd=function(a){if(a instanceof _.Hd)return a;try{return a=Jd(a),_.Id(a.south,a.west,a.north,a.east)}catch(b){throw _.vb("not a LatLngBounds or LatLngBoundsLiteral",b);}};_.Nd=_.ma("__gm");Od=function(){this.j={};this.R={};this.H={}};Pd=function(){this.j={}};Qd=function(a){this.j=new Pd;var b=this;_.C.addListenerOnce(a,"addfeature",function(){_.J("data",function(c){c.j(b,a,b.j)})})};_.Sd=function(a){this.j=[];try{this.j=Rd(a)}catch(b){_.wb(b)}};
//    _.Ud=function(a){this.j=(0,_.Td)(a)};_.Wd=function(a){this.j=Vd(a)};_.Xd=function(a){this.j=(0,_.Td)(a)};_.Yd=function(a){this.j=(0,_.Td)(a)};_.$d=function(a){this.j=Zd(a)};_.be=function(a){this.j=ae(a)};ce=function(a){a=a||{};a.clickable=_.Pa(a.clickable,!0);a.visible=_.Pa(a.visible,!0);this.setValues(a);_.J("marker",_.ta)};de=function(a){var b=_,c=hc.Jc().R;a=c.H=new lc(new fc(a),b);for(var b=0,d=c.j.length;b<d;++b)c.j[b](a);c.j.length=0};
//    _.fe=function(a){this.__gm={set:null,Kf:null};ce.call(this,a)};ge=function(a){a=a||{};a.visible=_.Pa(a.visible,!0);return a};_.he=function(a){return a&&a.radius||6378137};je=function(a){return a instanceof _.yc?ie(a):new _.yc((0,_.Td)(a))};le=function(a){var b;_.Xa(a)?0==_.x(a)?b=!0:(b=a instanceof _.yc?a.getAt(0):a[0],b=_.Xa(b)):b=!1;return b?a instanceof _.yc?ke(ie)(a):new _.yc(_.Bb(je)(a)):new _.yc([je(a)])};
//    ke=function(a){return function(b){if(!(b instanceof _.yc))throw _.vb("not an MVCArray");b.forEach(function(b,d){try{a(b)}catch(e){throw _.vb("at index "+d,e);}});return b}};me=function(a){this.set("latLngs",new _.yc([new _.yc]));this.setValues(ge(a));_.J("poly",_.ta)};_.pe=function(a){me.call(this,a)};_.qe=function(a){me.call(this,a)};
//    _.re=function(a,b,c){function d(a){if(!a)throw _.vb("not a Feature");if("Feature"!=a.type)throw _.vb('type != "Feature"');var b=a.geometry;try{b=null==b?null:e(b)}catch(d){throw _.vb('in property "geometry"',d);}var f=a.properties||{};if(!_.Ta(f))throw _.vb("properties is not an Object");var g=c.idPropertyName;a=g?f[g]:a.id;if(null!=a&&!_.A(a)&&!_.Ua(a))throw _.vb((g||"id")+" is not a string or number");return{id:a,geometry:b,properties:f}}function e(a){if(null==a)throw _.vb("is null");var b=(a.type+
//    "").toLowerCase(),c=a.coordinates;try{switch(b){case "point":return new _.ac(h(c));case "multipoint":return new _.Xd(n(c));case "linestring":return g(c);case "multilinestring":return new _.Wd(p(c));case "polygon":return f(c);case "multipolygon":return new _.be(t(c))}}catch(d){throw _.vb('in property "coordinates"',d);}if("geometrycollection"==b)try{return new _.Sd(y(a.geometries))}catch(d){throw _.vb('in property "geometries"',d);}throw _.vb("invalid type");}function f(a){return new _.$d(q(a))}function g(a){return new _.Ud(n(a))}
//        function h(a){a=l(a);return _.$b({lat:a[1],lng:a[0]})}if(!b)return[];c=c||{};var l=_.Bb(_.pc),n=_.Bb(h),p=_.Bb(g),q=_.Bb(function(a){a=n(a);if(!a.length)throw _.vb("contains no elements");if(!a[0].j(a[a.length-1]))throw _.vb("first and last positions are not equal");return new _.Yd(a.slice(0,-1))}),t=_.Bb(f),y=_.Bb(e),z=_.Bb(d);if("FeatureCollection"==b.type){b=b.features;try{return _.Oa(z(b),function(b){return a.add(b)})}catch(w){throw _.vb('in property "features"',w);}}if("Feature"==b.type)return[a.add(d(b))];
//        throw _.vb("not a Feature or FeatureCollection");};te=function(a){var b=this;this.setValues(a||{});this.j=new Od;_.C.forward(this.j,"addfeature",this);_.C.forward(this.j,"removefeature",this);_.C.forward(this.j,"setgeometry",this);_.C.forward(this.j,"setproperty",this);_.C.forward(this.j,"removeproperty",this);this.H=new Qd(this.j);this.H.bindTo("map",this);this.H.bindTo("style",this);_.B(_.se,function(a){_.C.forward(b.H,a,b)});this.R=!1};ue=function(a){a.R||(a.R=!0,_.J("drawing_impl",function(b){b.Zn(a)}))};
//    _.ve=function(a){this.j=a||[]};_.we=function(a){this.j=a||[]};xe=function(a){this.j=a||[]};_.ye=function(a){this.j=a||[]};_.ze=function(a){this.j=a||[]};Ae=function(a){if(!a)return null;var b;_.xa(a)?(b=window.document.createElement("div"),b.style.overflow="auto",b.innerHTML=a):a.nodeType==window.Node.TEXT_NODE?(b=window.document.createElement("div"),b.appendChild(a)):b=a;return b};
//    Be=function(a,b){this.j=a;this.Ae=b;a.addListener("map_changed",(0,_.u)(this.ep,this));this.bindTo("map",a);this.bindTo("disableAutoPan",a);this.bindTo("maxWidth",a);this.bindTo("position",a);this.bindTo("zIndex",a);this.bindTo("internalAnchor",a,"anchor");this.bindTo("internalContent",a,"content");this.bindTo("internalPixelOffset",a,"pixelOffset")};Ce=function(a,b,c,d){c?a.bindTo(b,c,d):(a.unbind(b),a.set(b,void 0))};
//    _.De=function(a){function b(){e||(e=!0,_.J("infowindow",function(a){a.Cm(d)}))}window.setTimeout(function(){_.J("infowindow",_.ta)},100);a=a||{};var c=!!a.Ae;delete a.Ae;var d=new Be(this,c),e=!1;_.C.addListenerOnce(this,"anchor_changed",b);_.C.addListenerOnce(this,"map_changed",b);this.setValues(a)};_.Fe=function(a){_.Ee&&a&&_.Ee.push(a)};Ge=function(a){this.setValues(a)};He=_.k();Ie=_.k();Je=_.k();_.Ke=function(){_.J("geocoder",_.ta)};
//    _.Le=function(a,b,c){this.ta=null;this.set("url",a);this.set("bounds",_.Eb(_.Kd)(b));this.setValues(c)};Me=function(a,b){_.Ua(a)?(this.set("url",a),this.setValues(b)):this.setValues(a)};_.Ne=function(){var a=this;_.J("layers",function(b){b.j(a)})};Oe=function(a){this.setValues(a);var b=this;_.J("layers",function(a){a.H(b)})};Pe=function(){var a=this;_.J("layers",function(b){b.R(a)})};Qe=function(a){this.j=a||[]};Re=function(a){this.j=a||[]};Se=function(a){this.j=a||[]};Te=function(a){this.j=a||[]};
//    _.Ue=function(a){this.j=a||[]};_.Ve=function(a){this.j=a||[]};_.We=function(a){this.j=a||[]};_.$e=function(a){this.j=a||[]};af=function(a){this.j=a||[]};bf=function(a){this.j=a||[]};cf=function(a){this.j=a||[]};df=function(a){this.j=a||[]};ef=function(a){this.j=a||[]};_.ff=function(a){this.j=a||[]};_.gf=function(a){this.j=a||[]};_.hf=function(a){a=a.j[0];return null!=a?a:""};_.jf=function(a){a=a.j[1];return null!=a?a:""};_.lf=function(){var a=_.kf(_.P).j[9];return null!=a?a:""};
//    mf=function(){var a=_.kf(_.P).j[7];return null!=a?a:""};nf=function(){var a=_.kf(_.P).j[12];return null!=a?a:""};of=function(a){a=a.j[0];return null!=a?a:""};_.pf=function(a){a=a.j[1];return null!=a?a:""};rf=function(){var a=_.P.j[4],a=(a?new cf(a):qf).j[0];return null!=a?a:0};_.sf=function(){var a=_.P.j[0];return null!=a?a:1};_.tf=function(a){a=a.j[6];return null!=a?a:""};uf=function(){var a=_.P.j[11];return null!=a?a:""};_.vf=function(){var a=_.P.j[16];return null!=a?a:""};
//    _.kf=function(a){return(a=a.j[2])?new af(a):wf};_.yf=function(){var a=_.P.j[3];return a?new bf(a):xf};Af=function(){var a=_.P.j[33];return a?new Qe(a):zf};Bf=function(a){return _.N(_.P.j,8)[a]};Df=function(){var a=_.P.j[36],a=(a?new ef(a):Cf).j[0];return null!=a?a:""};
//    Gf=function(a,b){_.Dc.call(this);_.Fe(a);this.__gm=new _.G;this.R=null;b&&b.client&&(this.R=_.Ef[b.client]||null);var c=this.controls=[];_.Ia(_.Ff,function(a,b){c[b]=new _.yc});this.T=!0;this.H=a;this.setPov(new _.Cc(0,0,1));b&&b.Bc&&!_.A(b.Bc.zoom)&&(b.Bc.zoom=_.A(b.zoom)?b.zoom:1);this.setValues(b);void 0==this.getVisible()&&this.setVisible(!0);this.__gm.Nd=b&&b.Nd||new _.zc;_.C.addListenerOnce(this,"pano_changed",_.Za(function(){_.J("marker",(0,_.u)(function(a){a.j(this.__gm.Nd,this)},this))}))};
//    _.Hf=function(){this.S=[];this.H=this.j=this.R=null};If=function(a,b,c){this.Ga=b;this.j=new _.cd(new _.Bc([]));this.W=new _.zc;this.ua=new _.yc;this.ma=new _.zc;this.qa=new _.zc;this.S=new _.zc;var d=this.Nd=new _.zc;d.j=function(){delete d.j;_.J("marker",_.Za(function(b){b.j(d,a)}))};this.R=new Gf(b,{visible:!1,enableCloseButton:!0,Nd:d});this.R.bindTo("reportErrorControl",a);this.R.T=!1;this.H=new _.Hf;this.Ia=c};_.Jf=function(){this.Ea=new Yc};
//    _.Kf=function(){this.j=new _.L(128,128);this.R=256/360;this.S=256/(2*Math.PI);this.H=!0};_.Lf=function(a){this.va=this.wa=window.Infinity;this.Ba=this.Aa=-window.Infinity;_.B(a,(0,_.u)(this.extend,this))};_.Nf=function(a,b,c,d){var e=new _.Lf;e.wa=a;e.va=b;e.Aa=c;e.Ba=d;return e};_.Of=function(a,b,c){if(a=a.fromLatLngToPoint(b))c=Math.pow(2,c),a.x*=c,a.y*=c;return a};
//    _.Pf=function(a,b){var c=a.lat()+_.Ub(b);90<c&&(c=90);var d=a.lat()-_.Ub(b);-90>d&&(d=-90);b=Math.sin(b);var e=Math.cos(_.Tb(a.lat()));if(90==c||-90==d||1E-6>e)return new _.Hd(new _.H(d,-180),new _.H(c,180));b=_.Ub(Math.asin(b/e));return new _.Hd(new _.H(d,a.lng()-b),new _.H(c,a.lng()+b))};_.Qf=function(a){this.xl=a||0;_.C.bind(this,"forceredraw",this,this.W)};_.Rf=function(a,b){a=a.style;a.width=b.width+b.R;a.height=b.height+b.H};_.Sf=function(a){return new _.M(a.offsetWidth,a.offsetHeight)};
//    Tf=function(a){this.j=a||[]};Uf=function(a){this.j=a||[]};Vf=function(a){this.j=a||[]};Wf=function(a){this.j=a||[]};Xf=function(a){this.j=a||[]};Yf=function(a,b,c,d){_.Qf.call(this);this.T=b;this.S=new _.Kf;this.$=c+"/maps/api/js/StaticMapService.GetMapImage";this.H=this.j=null;this.R=d;this.set("div",a);this.set("loading",!0)};$f=function(a){var b=a.get("tilt")||a.get("mapMaker")||_.x(a.get("styles"));a=a.get("mapTypeId");return b?null:Zf[a]};ag=function(a){a.parentNode&&a.parentNode.removeChild(a)};
//    bg=function(a,b){var c=a.H;c.onload=null;c.onerror=null;b&&(c.parentNode||a.j.appendChild(c),_.Rf(c,a.get("size")),_.C.trigger(a,"staticmaploaded"),a.R.set(_.Ga()));a.set("loading",!1)};cg=function(a,b){var c=a.H;b!=c.src?(ag(c),c.onload=function(){bg(a,!0)},c.onerror=function(){bg(a,!1)},c.src=b):!c.parentNode&&b&&a.j.appendChild(c)};
//    eg=function(a,b,c,d,e){var f=_.dg[15]?nf():mf();this.j=a;this.H=d;this.R=_.sa(e)?e:_.Ga();var g=f+"/csi?v=2&s=mapsapi3&v3v="+Df()+"&action="+a;_.Gc(c,function(a,b){g+="&"+(0,window.encodeURIComponent)(b)+"="+(0,window.encodeURIComponent)(a)});b&&(g+="&e="+b);this.S=g};_.gg=function(a,b){var c={};c[b]=void 0;_.fg(a,c)};
//    _.fg=function(a,b){var c="";_.Gc(b,function(a,b){var f=(null!=a?a:_.Ga())-this.R;c&&(c+=",");c+=b+"."+Math.round(f);null==a&&window.performance&&window.performance.mark&&window.performance.mark("mapsapi:"+this.j+":"+b)},a);b=a.S+"&rt="+c;a.H.createElement("img").src=b;(a=_.Kc.__gm_captureCSI)&&a(b)};
//    _.hg=function(a,b){b=b||{};var c=b.Fp||{},d=_.N(_.P.j,12).join(",");d&&(c.libraries=d);var d=_.tf(_.P),e=Af(),f=[];d&&f.push(d);_.Pb(e.V(),function(a,b){a&&_.Pb(a,function(a,c){null!=a&&f.push(b+1+"_"+(c+1)+"_"+a)})});b.vn&&(f=f.concat(b.vn));return new eg(a,f.join(","),c,b.document||window.document,b.startTime)};jg=function(){this.H=_.hg("apiboot2",{startTime:_.ig});_.gg(this.H,"main");this.j=!1};lg=function(){var a=kg;a.j||(a.j=!0,_.gg(a.H,"firstmap"))};_.mg=_.k();_.ng=function(){this.j=""};
//    _.og=function(a){var b=new _.ng;b.j=a;return b};_.qg=function(){this.dh="";this.Ol=_.pg;this.j=null};_.rg=function(a,b){var c=new _.qg;c.dh=a;c.j=b;return c};_.sg=function(a,b){b.parentNode&&b.parentNode.insertBefore(a,b.nextSibling)};_.tg=function(a){a&&a.parentNode&&a.parentNode.removeChild(a)};ug=function(a,b,c,d,e){this.j=!!b;this.node=null;this.H=0;this.R=!1;this.S=!c;a&&this.setPosition(a,d);this.depth=void 0!=e?e:this.H||0;this.j&&(this.depth*=-1)};
//    vg=function(a,b,c,d){ug.call(this,a,b,c,null,d)};_.xg=function(a){for(var b;b=a.firstChild;)_.wg(b),a.removeChild(b)};_.wg=function(a){a=new vg(a);try{for(;;)_.C.clearInstanceListeners(a.next())}catch(b){if(b!==_.yg)throw b;}};
//    Bg=function(a,b){var c=_.Ga();kg&&lg();var d=new _.Jf;_.Nd.call(this,new If(this,a,d));var e=b||{};_.Sa(e.mapTypeId)||(e.mapTypeId="roadmap");this.setValues(e);this.j=_.dg[15]&&e.noControlsOrLogging;this.mapTypes=new zd;this.features=new _.G;_.Fe(a);this.notify("streetView");b=_.Sf(a);e.noClear||_.xg(a);var f=null;_.P&&zg(e.useStaticMap,b)&&(f=new Yf(a,_.Ag,_.lf(),new _.cd(null)),_.C.forward(f,"staticmaploaded",this),f.set("size",b),f.bindTo("center",this),f.bindTo("zoom",this),f.bindTo("mapTypeId",
//        this),f.bindTo("styles",this),f.bindTo("mapMaker",this));this.overlayMapTypes=new _.yc;var g=this.controls=[];_.Ia(_.Ff,function(a,b){g[b]=new _.yc});var h=this,l=!0;_.J("map",function(a){a.H(h,e,f,l,c,d)});l=!1;this.data=new te({map:this})};zg=function(a,b){if(_.Sa(a))return!!a;a=b.width;b=b.height;return 384E3>=a*b&&800>=a&&800>=b};Cg=function(){_.J("maxzoom",_.ta)};Dg=function(a,b){!a||_.Ua(a)||_.A(a)?(this.set("tableId",a),this.setValues(b)):this.setValues(a)};_.Eg=_.k();
//    _.Fg=function(a){this.setValues(ge(a));_.J("poly",_.ta)};_.Gg=function(a){this.setValues(ge(a));_.J("poly",_.ta)};Hg=function(){this.j=null};_.Ig=function(){this.j=null};
//    _.Jg=function(a){this.tileSize=a.tileSize||new _.M(256,256);this.name=a.name;this.alt=a.alt;this.minZoom=a.minZoom;this.maxZoom=a.maxZoom;this.R=(0,_.u)(a.getTileUrl,a);this.j=new _.zc;this.H=null;this.set("opacity",a.opacity);_.Kc.window&&_.C.addDomListener(window,"online",(0,_.u)(this.Bp,this));var b=this;_.J("map",function(a){var d=b.H=a.j,e=b.tileSize||new _.M(256,256);b.j.forEach(function(a){var c=a.__gmimt,h=c.La,l=c.zoom,n=b.R(h,l);c.Cc=d(h,l,e,a,n,function(){_.C.trigger(a,"load")})})})};
//    Kg=function(a,b){null!=a.style.opacity?a.style.opacity=b:a.style.filter=b&&"alpha(opacity="+Math.round(100*b)+")"};Lg=function(a){a=a.get("opacity");return"number"==typeof a?a:1};_.Mg=_.k();_.Ng=function(a,b){this.set("styles",a);a=b||{};this.j=a.baseMapTypeId||"roadmap";this.minZoom=a.minZoom;this.maxZoom=a.maxZoom||20;this.name=a.name;this.alt=a.alt;this.projection=null;this.tileSize=new _.M(256,256)};
//    _.Og=function(a,b){_.Cb(yb,"container is not a Node")(a);this.setValues(b);_.J("controls",(0,_.u)(function(b){b.Tm(this,a)},this))};Rg=_.ma("j");Sg=function(a,b,c){for(var d=Array(b.length),e=0,f=b.length;e<f;++e)d[e]=b.charCodeAt(e);d.unshift(c);a=a.j;c=b=0;for(e=d.length;c<e;++c)b*=1729,b+=d[c],b%=a;return b};
//    Vg=function(){var a=rf(),b=new Rg(131071),c=(0,window.unescape)("%26%74%6F%6B%65%6E%3D");return function(d){d=d.replace(Tg,"%27");var e=d+c;Ug||(Ug=/(?:https?:\/\/[^/]+)?(.*)/);d=Ug.exec(d);return e+Sg(b,d&&d[1],a)}};Wg=function(){var a=new Rg(2147483647);return function(b){return Sg(a,b,0)}};Xg=function(a){for(var b=a.split("."),c=window,d=window,e=0;e<b.length;e++)if(d=c,c=c[b[e]],!c)throw _.vb(a+" is not a function");return function(){c.apply(d)}};
//    Yg=function(){for(var a in Object.prototype)window.console&&window.console.error("This site adds property <"+a+"> to Object.prototype. Extending Object.prototype breaks JavaScript for..in loops, which are used heavily in Google Maps API v3.")};Zg=function(a){(a="version"in a)&&window.console&&window.console.error("You have included the Google Maps API multiple times on this page. This may cause unexpected errors.");return a};_.qa=[];_.Kc=this;Ba="closure_uid_"+(1E9*Math.random()>>>0);Ca=0;var kb,lb;_.C={};kb="undefined"!=typeof window.navigator&&-1!=window.navigator.userAgent.toLowerCase().indexOf("msie");lb={};_.C.addListener=function(a,b,c){return new mb(a,b,c,0)};_.C.hasListeners=function(a,b){b=(a=a.__e3_)&&a[b];return!!b&&!_.Ka(b)};_.C.removeListener=function(a){a&&a.remove()};_.C.clearListeners=function(a,b){_.Ia(gb(a,b),function(a,b){b&&b.remove()})};_.C.clearInstanceListeners=function(a){_.Ia(gb(a),function(a,c){c&&c.remove()})};
//    _.C.trigger=function(a,b,c){if(_.C.hasListeners(a,b)){var d=Wa(arguments,2),e=gb(a,b),f;for(f in e){var g=e[f];g&&g.j.apply(g.Mb,d)}}};_.C.addDomListener=function(a,b,c,d){if(a.addEventListener){var e=d?4:1;a.addEventListener(b,c,d);c=new mb(a,b,c,e)}else a.attachEvent?(c=new mb(a,b,c,2),a.attachEvent("on"+b,nb(c))):(a["on"+b]=c,c=new mb(a,b,c,3));return c};_.C.addDomListenerOnce=function(a,b,c,d){var e=_.C.addDomListener(a,b,function(){e.remove();return c.apply(this,arguments)},d);return e};
//    _.C.Ha=function(a,b,c,d){return _.C.addDomListener(a,b,hb(c,d))};_.C.bind=function(a,b,c,d){return _.C.addListener(a,b,(0,_.u)(d,c))};_.C.addListenerOnce=function(a,b,c){var d=_.C.addListener(a,b,function(){d.remove();return c.apply(this,arguments)});return d};_.C.forward=function(a,b,c){return _.C.addListener(a,b,ib(b,c))};_.C.Cb=function(a,b,c,d){return _.C.addDomListener(a,b,ib(b,c,!d))};_.C.kk=function(){var a=lb,b;for(b in a)a[b].remove();lb={};(a=_.Kc.CollectGarbage)&&a()};
//    _.C.Up=function(){kb&&_.C.addDomListener(window,"unload",_.C.kk)};var jb=0;mb.prototype.remove=function(){if(this.Mb){switch(this.S){case 1:this.Mb.removeEventListener(this.H,this.j,!1);break;case 4:this.Mb.removeEventListener(this.H,this.j,!0);break;case 2:this.Mb.detachEvent("on"+this.H,this.R);break;case 3:this.Mb["on"+this.H]=null}delete fb(this.Mb,this.H)[this.id];this.R=this.j=this.Mb=null;delete lb[this.id]}};_.r=_.G.prototype;_.r.get=function(a){var b=tb(this);a+="";b=$a(b,a);if(_.Sa(b)){if(b){a=b.dc;var b=b.Od,c="get"+_.sb(a);return b[c]?b[c]():b.get(a)}return this[a]}};_.r.set=function(a,b){var c=tb(this);a+="";var d=$a(c,a);if(d)if(a=d.dc,d=d.Od,c="set"+_.sb(a),d[c])d[c](b);else d.set(a,b);else this[a]=b,c[a]=null,qb(this,a)};_.r.notify=function(a){var b=tb(this);a+="";(b=$a(b,a))?b.Od.notify(b.dc):qb(this,a)};
//    _.r.setValues=function(a){for(var b in a){var c=a[b],d="set"+_.sb(b);if(this[d])this[d](c);else this.set(b,c)}};_.r.setOptions=_.G.prototype.setValues;_.r.changed=_.k();var rb={};_.G.prototype.bindTo=function(a,b,c,d){a+="";c=(c||a)+"";this.unbind(a);var e={Od:this,dc:a},f={Od:b,dc:c,yi:e};tb(this)[a]=f;pb(b,c)[_.ob(e)]=e;d||qb(this,a)};_.G.prototype.unbind=function(a){var b=tb(this),c=b[a];c&&(c.yi&&delete pb(c.Od,c.dc)[_.ob(c.yi)],this[a]=this.get(a),b[a]=null)};
//    _.G.prototype.unbindAll=function(){var a=(0,_.u)(this.unbind,this),b=tb(this),c;for(c in b)a(c)};_.G.prototype.addListener=function(a,b){return _.C.addListener(this,a,b)};_.$g={ROADMAP:"roadmap",SATELLITE:"satellite",HYBRID:"hybrid",TERRAIN:"terrain"};_.Ff={TOP_LEFT:1,TOP_CENTER:2,TOP:2,TOP_RIGHT:3,LEFT_CENTER:4,LEFT_TOP:5,LEFT:5,LEFT_BOTTOM:6,RIGHT_TOP:7,RIGHT:7,RIGHT_CENTER:8,RIGHT_BOTTOM:9,BOTTOM_LEFT:10,BOTTOM_CENTER:11,BOTTOM:11,BOTTOM_RIGHT:12,CENTER:13};var ah={or:"Point",mr:"LineString",POLYGON:"Polygon"};_.v(ub,Error);var ch;_.pc=_.Cb(_.A,"not a number");_.bh=_.Cb(_.Ua,"not a string");ch=_.Cb(_.Va,"not a boolean");_.dh=_.Eb(_.pc);_.eh=_.Eb(_.bh);_.fh=_.Eb(ch);var Vb=_.xb({lat:_.pc,lng:_.pc},!0);_.H.prototype.toString=function(){return"("+this.lat()+", "+this.lng()+")"};_.H.prototype.toJSON=function(){return{lat:this.lat(),lng:this.lng()}};_.H.prototype.j=function(a){return a?_.Na(this.lat(),a.lat())&&_.Na(this.lng(),a.lng()):!1};_.H.prototype.equals=_.H.prototype.j;_.H.prototype.toUrlValue=function(a){a=_.Sa(a)?a:6;return Yb(this.lat(),a)+","+Yb(this.lng(),a)};_.Td=_.Bb(_.$b);_.v(_.ac,Zb);_.ac.prototype.getType=_.oa("Point");_.ac.prototype.forEachLatLng=function(a){a(this.j)};_.ac.prototype.get=_.m("j");var Rd=_.Bb(bc);_.ua(hc);hc.prototype.Pc=function(a,b){var c=this,d=c.T;ic(c.R,function(e){for(var f=e.qj[a]||[],g=e.cq[a]||[],h=d[a]=_.cc(f.length,function(){delete d[a];b(e.Xm);for(var f=c.H[a],h=f?f.length:0,l=0;l<h;++l)f[l](c.j[a]);delete c.H[a];l=0;for(f=g.length;l<f;++l)h=g[l],d[h]&&d[h]()}),l=0,n=f.length;l<n;++l)c.j[f[l]]&&h()})};_.r=_.oc.prototype;_.r.getId=_.m("R");_.r.getGeometry=_.m("j");_.r.setGeometry=function(a){var b=this.j;try{this.j=a?bc(a):null}catch(c){_.wb(c);return}_.C.trigger(this,"setgeometry",{feature:this,newGeometry:this.j,oldGeometry:b})};_.r.getProperty=function(a){return $a(this.H,a)};_.r.setProperty=function(a,b){if(void 0===b)this.removeProperty(a);else{var c=this.getProperty(a);this.H[a]=b;_.C.trigger(this,"setproperty",{feature:this,name:a,newValue:b,oldValue:c})}};
//    _.r.removeProperty=function(a){var b=this.getProperty(a);delete this.H[a];_.C.trigger(this,"removeproperty",{feature:this,name:a,oldValue:b})};_.r.forEachProperty=function(a){for(var b in this.H)a(this.getProperty(b),b)};_.r.toGeoJson=function(a){var b=this;_.J("data",function(c){c.H(b,a)})};_.gh=new _.L(0,0);_.L.prototype.toString=function(){return"("+this.x+", "+this.y+")"};_.L.prototype.j=function(a){return a?a.x==this.x&&a.y==this.y:!1};_.L.prototype.equals=_.L.prototype.j;_.L.prototype.round=function(){this.x=Math.round(this.x);this.y=Math.round(this.y)};_.L.prototype.Rf=_.ra(0);_.hh=new _.M(0,0);_.M.prototype.toString=function(){return"("+this.width+", "+this.height+")"};_.M.prototype.j=function(a){return a?a.width==this.width&&a.height==this.height:!1};_.M.prototype.equals=_.M.prototype.j;var ih={CIRCLE:0,FORWARD_CLOSED_ARROW:1,FORWARD_OPEN_ARROW:2,BACKWARD_CLOSED_ARROW:3,BACKWARD_OPEN_ARROW:4};_.v(_.yc,_.G);_.r=_.yc.prototype;_.r.getAt=function(a){return this.j[a]};_.r.indexOf=function(a){for(var b=0,c=this.j.length;b<c;++b)if(a===this.j[b])return b;return-1};_.r.forEach=function(a){for(var b=0,c=this.j.length;b<c;++b)a(this.j[b],b)};_.r.setAt=function(a,b){var c=this.j[a],d=this.j.length;if(a<d)this.j[a]=b,_.C.trigger(this,"set_at",a,c),this.S&&this.S(a,c);else{for(c=d;c<a;++c)this.insertAt(c,void 0);this.insertAt(a,b)}};
//    _.r.insertAt=function(a,b){this.j.splice(a,0,b);xc(this);_.C.trigger(this,"insert_at",a);this.H&&this.H(a)};_.r.removeAt=function(a){var b=this.j[a];this.j.splice(a,1);xc(this);_.C.trigger(this,"remove_at",a,b);this.R&&this.R(a,b);return b};_.r.push=function(a){this.insertAt(this.j.length,a);return this.j.length};_.r.pop=function(){return this.removeAt(this.j.length-1)};_.r.getArray=_.m("j");_.r.clear=function(){for(;this.get("length");)this.pop()};_.wc(_.yc.prototype,{length:null});_.zc.prototype.remove=function(a){var b=this.H,c=this.R(a);b[c]&&(delete b[c],_.C.trigger(this,"remove",a),this.onRemove&&this.onRemove(a))};_.zc.prototype.contains=function(a){return!!this.H[this.R(a)]};_.zc.prototype.forEach=function(a){var b=this.H,c;for(c in b)a.call(this,b[c])};_.Bc.prototype.Ub=_.ra(1);_.Bc.prototype.forEach=function(a,b){_.Pb(this.j,function(c,d){a.call(b,c,d)})};var jh=_.xb({zoom:_.dh,heading:_.pc,pitch:_.pc});_.v(_.Dc,_.G);var kh=function(a){return function(){return a}}(null);a:{var lh=_.Kc.navigator;if(lh){var mh=lh.userAgent;if(mh){_.Jb=mh;break a}}_.Jb=""};var Oc,Nc=_.Ec;Rc.prototype.get=function(){var a;0<this.H?(this.H--,a=this.j,this.j=a.next,a.next=null):a=this.R();return a};var nh=new Rc(function(){return new Tc},function(a){a.reset()},100);Sc.prototype.add=function(a,b){var c=nh.get();c.set(a,b);this.H?this.H.next=c:this.j=c;this.H=c};Sc.prototype.remove=function(){var a=null;this.j&&(a=this.j,this.j=this.j.next,this.j||(this.H=null),a.next=null);return a};Tc.prototype.set=function(a,b){this.Kd=a;this.j=b;this.next=null};Tc.prototype.reset=function(){this.next=this.j=this.Kd=null};_.Mc.T=function(){if(_.Kc.Promise&&_.Kc.Promise.resolve){var a=_.Kc.Promise.resolve(void 0);_.Mc.j=function(){a.then(_.Mc.H)}}else _.Mc.j=function(){Qc()}};_.Mc.W=function(a){_.Mc.j=function(){Qc();a&&a(_.Mc.H)}};_.Mc.R=!1;_.Mc.S=new Sc;_.Mc.H=function(){for(var a;a=_.Mc.S.remove();){try{a.Kd.call(a.j)}catch(c){Lc(c)}var b=nh;b.T(a);b.H<b.S&&(b.H++,a.next=b.j,b.j=a)}_.Mc.R=!1};Yc.prototype.addListener=function(a,b,c){c=c?{Bi:!1}:null;var d=!this.Ea.length,e;e=this.Ea;var f=Qb(e,Uc(a,b));(e=0>f?null:_.xa(e)?e.charAt(f):e[f])?e.Le=e.Le&&c:this.Ea.push({Kd:a,context:b||null,Le:c});d&&this.H();return a};Yc.prototype.addListenerOnce=function(a,b){this.addListener(a,b,!0);return a};Yc.prototype.removeListener=function(a,b){if(this.Ea.length){var c=this.Ea;a=Qb(c,Uc(a,b));0<=a&&_.Rb(c,a);this.Ea.length||this.j()}};var Zc=_.Mc;_.r=_.ad.prototype;_.r.Ke=_.k();_.r.Ie=_.k();_.r.addListener=function(a,b){return this.Ea.addListener(a,b)};_.r.addListenerOnce=function(a,b){return this.Ea.addListenerOnce(a,b)};_.r.removeListener=function(a,b){return this.Ea.removeListener(a,b)};_.r.notify=function(a){_.$c(this.Ea,function(a){a(this.get())},this,a)};_.v(_.bd,_.ad);_.bd.prototype.set=function(a){this.Tj(a);this.notify()};_.v(_.cd,_.bd);_.cd.prototype.get=_.m("j");_.cd.prototype.Tj=_.ma("j");_.v(dd,_.G);_.oh=_.jd("d",void 0);_.ph=_.ld("d");_.qh=_.jd("f",void 0);_.Q=_.md();_.rh=_.kd("i",void 0);_.sh=_.ld("i");_.th=_.ld("j");_.uh=_.jd("u",void 0);_.vh=_.kd("u",void 0);_.wh=_.ld("u");_.xh=_.nd();_.R=_.od();_.S=_.pd();_.yh=_.ld("e");_.T=_.jd("s",void 0);_.zh=_.kd("s",void 0);_.Ah=_.ld("s");_.Bh=_.jd("x",void 0);_.Ch=_.kd("x",void 0);_.Dh=_.ld("x");_.Eh=_.ld("y");var Gh;_.Fh=new qd;Gh=/'/g;qd.prototype.j=function(a,b){var c=[];sd(a,b,c);return c.join("&").replace(Gh,"%27")};var Th,vd,Xh;_.Hh=_.Hc("Opera");_.Ih=_.Ic();_.Jh=_.Hc("Edge");_.Kh=_.Hc("Gecko")&&!(_.Kb()&&!_.Hc("Edge"))&&!(_.Hc("Trident")||_.Hc("MSIE"))&&!_.Hc("Edge");_.Lh=_.Kb()&&!_.Hc("Edge");_.Mh=_.Hc("Macintosh");_.Nh=_.Hc("Windows");_.Oh=_.Hc("Linux")||_.Hc("CrOS");_.Ph=_.Hc("Android");_.Qh=_.td();_.Rh=_.Hc("iPad");_.Sh=_.Hc("iPod");
//    a:{var Uh="",Vh=function(){var a=_.Jb;if(_.Kh)return/rv\:([^\);]+)(\)|;)/.exec(a);if(_.Jh)return/Edge\/([\d\.]+)/.exec(a);if(_.Ih)return/\b(?:MSIE|rv)[: ]([^\);]+)(\)|;)/.exec(a);if(_.Lh)return/WebKit\/(\S+)/.exec(a);if(_.Hh)return/(?:Version)[ \/]?(\S+)/.exec(a)}();Vh&&(Uh=Vh?Vh[1]:"");if(_.Ih){var Wh=ud();if(null!=Wh&&Wh>(0,window.parseFloat)(Uh)){Th=String(Wh);break a}}Th=Uh}_.wd=Th;vd={};Xh=_.Kc.document;_.Yh=Xh&&_.Ih?ud()||("CSS1Compat"==Xh.compatMode?(0,window.parseInt)(_.wd,10):5):void 0;_.Zh=_.Hc("Firefox");_.$h=_.td()||_.Hc("iPod");_.ai=_.Hc("iPad");_.bi=_.Hc("Android")&&!(Jc()||_.Hc("Firefox")||_.Hc("Opera")||_.Hc("Silk"));_.ci=Jc();_.di=_.Hc("Safari")&&!(Jc()||_.Hc("Coast")||_.Hc("Opera")||_.Hc("Edge")||_.Hc("Silk")||_.Hc("Android"))&&!(_.td()||_.Hc("iPad")||_.Hc("iPod"));_.yd.prototype.heading=_.m("j");_.yd.prototype.Eb=_.ra(2);_.yd.prototype.toString=function(){return this.j+","+this.H};_.ei=new _.yd;_.v(zd,_.G);zd.prototype.set=function(a,b){if(null!=b&&!(b&&_.A(b.maxZoom)&&b.tileSize&&b.tileSize.width&&b.tileSize.height&&b.getTile&&b.getTile.apply))throw Error("Expected value implementing google.maps.MapType");return _.G.prototype.set.apply(this,arguments)};_.r=Ad.prototype;_.r.isEmpty=function(){return 360==this.j-this.H};_.r.intersects=function(a){var b=this.j,c=this.H;return this.isEmpty()||a.isEmpty()?!1:_.Bd(this)?_.Bd(a)||a.j<=this.H||a.H>=b:_.Bd(a)?a.j<=c||a.H>=b:a.j<=c&&a.H>=b};_.r.contains=function(a){-180==a&&(a=180);var b=this.j,c=this.H;return _.Bd(this)?(a>=b||a<=c)&&!this.isEmpty():a>=b&&a<=c};_.r.extend=function(a){this.contains(a)||(this.isEmpty()?this.j=this.H=a:_.Ed(a,this.j)<_.Ed(this.H,a)?this.j=a:this.H=a)};
//    _.r.Hc=function(){var a=(this.j+this.H)/2;_.Bd(this)&&(a=_.Ma(a+180,-180,180));return a};_.r=Fd.prototype;_.r.isEmpty=function(){return this.H>this.j};_.r.intersects=function(a){var b=this.H,c=this.j;return b<=a.H?a.H<=c&&a.H<=a.j:b<=a.j&&b<=c};_.r.contains=function(a){return a>=this.H&&a<=this.j};_.r.extend=function(a){this.isEmpty()?this.j=this.H=a:a<this.H?this.H=a:a>this.j&&(this.j=a)};_.r.Hc=function(){return(this.j+this.H)/2};_.r=_.Hd.prototype;_.r.getCenter=function(){return new _.H(this.H.Hc(),this.j.Hc())};_.r.toString=function(){return"("+this.getSouthWest()+", "+this.getNorthEast()+")"};_.r.toJSON=function(){return{south:this.H.H,west:this.j.j,north:this.H.j,east:this.j.H}};_.r.toUrlValue=function(a){var b=this.getSouthWest(),c=this.getNorthEast();return[b.toUrlValue(a),c.toUrlValue(a)].join()};
//    _.r.Yk=function(a){if(!a)return!1;a=_.Kd(a);var b=this.H,c=a.H;return(b.isEmpty()?c.isEmpty():1E-9>=Math.abs(c.H-b.H)+Math.abs(b.j-c.j))&&_.Dd(this.j,a.j)};_.Hd.prototype.equals=_.Hd.prototype.Yk;_.r=_.Hd.prototype;_.r.contains=function(a){return this.H.contains(a.lat())&&this.j.contains(a.lng())};_.r.intersects=function(a){a=_.Kd(a);return this.H.intersects(a.H)&&this.j.intersects(a.j)};_.r.extend=function(a){this.H.extend(a.lat());this.j.extend(a.lng());return this};
//    _.r.union=function(a){a=_.Kd(a);if(!a||a.isEmpty())return this;this.extend(a.getSouthWest());this.extend(a.getNorthEast());return this};_.r.getSouthWest=function(){return new _.H(this.H.H,this.j.j,!0)};_.r.getNorthEast=function(){return new _.H(this.H.j,this.j.H,!0)};_.r.toSpan=function(){return new _.H(_.Gd(this.H),_.Cd(this.j),!0)};_.r.isEmpty=function(){return this.H.isEmpty()||this.j.isEmpty()};var Jd=_.xb({south:_.pc,west:_.pc,north:_.pc,east:_.pc},!1);_.v(_.Nd,_.G);_.r=Od.prototype;_.r.contains=function(a){return this.j.hasOwnProperty(_.ob(a))};_.r.getFeatureById=function(a){return $a(this.H,a)};
//    _.r.add=function(a){a=a||{};a=a instanceof _.oc?a:new _.oc(a);if(!this.contains(a)){var b=a.getId();if(b){var c=this.getFeatureById(b);c&&this.remove(c)}c=_.ob(a);this.j[c]=a;b&&(this.H[b]=a);var d=_.C.forward(a,"setgeometry",this),e=_.C.forward(a,"setproperty",this),f=_.C.forward(a,"removeproperty",this);this.R[c]=function(){_.C.removeListener(d);_.C.removeListener(e);_.C.removeListener(f)};_.C.trigger(this,"addfeature",{feature:a})}return a};
//    _.r.remove=function(a){var b=_.ob(a),c=a.getId();if(this.j[b]){delete this.j[b];c&&delete this.H[c];if(c=this.R[b])delete this.R[b],c();_.C.trigger(this,"removefeature",{feature:a})}};_.r.forEach=function(a){for(var b in this.j)a(this.j[b])};Pd.prototype.get=function(a){return this.j[a]};Pd.prototype.set=function(a,b){var c=this.j;c[a]||(c[a]={});_.Ja(c[a],b);_.C.trigger(this,"changed",a)};Pd.prototype.reset=function(a){delete this.j[a];_.C.trigger(this,"changed",a)};Pd.prototype.forEach=function(a){_.Ia(this.j,a)};_.v(Qd,_.G);Qd.prototype.overrideStyle=function(a,b){this.j.set(_.ob(a),b)};Qd.prototype.revertStyle=function(a){a?this.j.reset(_.ob(a)):this.j.forEach((0,_.u)(this.j.reset,this.j))};_.v(_.Sd,Zb);_.r=_.Sd.prototype;_.r.getType=_.oa("GeometryCollection");_.r.getLength=function(){return this.j.length};_.r.getAt=function(a){return this.j[a]};_.r.getArray=function(){return this.j.slice()};_.r.forEachLatLng=function(a){this.j.forEach(function(b){b.forEachLatLng(a)})};_.v(_.Ud,Zb);_.r=_.Ud.prototype;_.r.getType=_.oa("LineString");_.r.getLength=function(){return this.j.length};_.r.getAt=function(a){return this.j[a]};_.r.getArray=function(){return this.j.slice()};_.r.forEachLatLng=function(a){this.j.forEach(a)};var Vd=_.Bb(_.zb(_.Ud,"google.maps.Data.LineString",!0));_.v(_.Wd,Zb);_.r=_.Wd.prototype;_.r.getType=_.oa("MultiLineString");_.r.getLength=function(){return this.j.length};_.r.getAt=function(a){return this.j[a]};_.r.getArray=function(){return this.j.slice()};_.r.forEachLatLng=function(a){this.j.forEach(function(b){b.forEachLatLng(a)})};_.v(_.Xd,Zb);_.r=_.Xd.prototype;_.r.getType=_.oa("MultiPoint");_.r.getLength=function(){return this.j.length};_.r.getAt=function(a){return this.j[a]};_.r.getArray=function(){return this.j.slice()};_.r.forEachLatLng=function(a){this.j.forEach(a)};_.v(_.Yd,Zb);_.r=_.Yd.prototype;_.r.getType=_.oa("LinearRing");_.r.getLength=function(){return this.j.length};_.r.getAt=function(a){return this.j[a]};_.r.getArray=function(){return this.j.slice()};_.r.forEachLatLng=function(a){this.j.forEach(a)};var Zd=_.Bb(_.zb(_.Yd,"google.maps.Data.LinearRing",!0));_.v(_.$d,Zb);_.r=_.$d.prototype;_.r.getType=_.oa("Polygon");_.r.getLength=function(){return this.j.length};_.r.getAt=function(a){return this.j[a]};_.r.getArray=function(){return this.j.slice()};_.r.forEachLatLng=function(a){this.j.forEach(function(b){b.forEachLatLng(a)})};var ae=_.Bb(_.zb(_.$d,"google.maps.Data.Polygon",!0));_.v(_.be,Zb);_.r=_.be.prototype;_.r.getType=_.oa("MultiPolygon");_.r.getLength=function(){return this.j.length};_.r.getAt=function(a){return this.j[a]};_.r.getArray=function(){return this.j.slice()};_.r.forEachLatLng=function(a){this.j.forEach(function(b){b.forEachLatLng(a)})};var fi=_.xb({source:_.bh,webUrl:_.eh,iosDeepLinkId:_.eh});var gi=_.Ha(_.xb({placeId:_.eh,query:_.eh,location:_.$b}),function(a){if(a.placeId&&a.query)throw _.vb("cannot set both placeId and query");if(!a.placeId&&!a.query)throw _.vb("must set one of placeId or query");return a});_.v(ce,_.G);
//    _.wc(ce.prototype,{position:_.Eb(_.$b),title:_.eh,icon:_.Eb(_.Db(_.bh,{Qh:Fb("url"),then:_.xb({url:_.bh,scaledSize:_.Eb(rc),size:_.Eb(rc),origin:_.Eb(qc),anchor:_.Eb(qc),labelOrigin:_.Eb(qc),path:_.Cb(Ra)},!0)},{Qh:Fb("path"),then:_.xb({path:_.Db(_.bh,_.Ab(ih)),anchor:_.Eb(qc),labelOrigin:_.Eb(qc),fillColor:_.eh,fillOpacity:_.dh,rotation:_.dh,scale:_.dh,strokeColor:_.eh,strokeOpacity:_.dh,strokeWeight:_.dh,url:_.Cb(Ra)},!0)})),label:_.Eb(_.Db(_.bh,{Qh:Fb("text"),then:_.xb({text:_.bh,fontSize:_.eh,fontWeight:_.eh,
//        fontFamily:_.eh},!0)})),shadow:_.Ec,shape:_.Ec,cursor:_.eh,clickable:_.fh,animation:_.Ec,draggable:_.fh,visible:_.fh,flat:_.Ec,zIndex:_.dh,opacity:_.dh,place:_.Eb(gi),attribution:_.Eb(fi)});var kc={main:[],common:["main"],util:["common"],adsense:["main"],controls:["util"],data:["util"],directions:["util","geometry"],distance_matrix:["util"],drawing:["main"],drawing_impl:["controls"],elevation:["util","geometry"],geocoder:["util"],geojson:["main"],imagery_viewer:["main"],geometry:["main"],infowindow:["util"],kml:["onion","util","map"],layers:["map"],map:["common"],marker:["util"],maxzoom:["util"],onion:["util","map"],overlay:["common"],panoramio:["main"],places:["main"],places_impl:["controls"],
//        poly:["util","map","geometry"],search:["main"],search_impl:["onion"],stats:["util"],streetview:["util","geometry"],usage:["util"],visualization:["main"],visualization_impl:["onion"],weather:["main"],zombie:["main"]};var hi=_.Kc.google.maps,ii=hc.Jc(),ji=(0,_.u)(ii.Pc,ii);hi.__gjsload__=ji;_.Ia(hi.modules,ji);delete hi.modules;_.ki=_.Eb(_.zb(_.Nd,"Map"));var li=_.Eb(_.zb(_.Dc,"StreetViewPanorama"));_.v(_.fe,ce);_.fe.prototype.map_changed=function(){this.__gm.set&&this.__gm.set.remove(this);var a=this.get("map");this.__gm.set=a&&a.__gm.Nd;this.__gm.set&&_.Ac(this.__gm.set,this)};_.fe.MAX_ZINDEX=1E6;_.wc(_.fe.prototype,{map:_.Db(_.ki,li)});var ie=ke(_.zb(_.H,"LatLng"));_.v(me,_.G);me.prototype.map_changed=me.prototype.visible_changed=function(){var a=this;_.J("poly",function(b){b.H(a)})};me.prototype.getPath=function(){return this.get("latLngs").getAt(0)};me.prototype.setPath=function(a){try{this.get("latLngs").setAt(0,je(a))}catch(b){_.wb(b)}};_.wc(me.prototype,{draggable:_.fh,editable:_.fh,map:_.ki,visible:_.fh});_.v(_.pe,me);_.pe.prototype.ub=!0;_.pe.prototype.getPaths=function(){return this.get("latLngs")};_.pe.prototype.setPaths=function(a){this.set("latLngs",le(a))};_.v(_.qe,me);_.qe.prototype.ub=!1;_.se="click dblclick mousedown mousemove mouseout mouseover mouseup rightclick".split(" ");_.v(te,_.G);_.r=te.prototype;_.r.contains=function(a){return this.j.contains(a)};_.r.getFeatureById=function(a){return this.j.getFeatureById(a)};_.r.add=function(a){return this.j.add(a)};_.r.remove=function(a){this.j.remove(a)};_.r.forEach=function(a){this.j.forEach(a)};_.r.addGeoJson=function(a,b){return _.re(this.j,a,b)};_.r.loadGeoJson=function(a,b,c){var d=this.j;_.J("data",function(e){e.xn(d,a,b,c)})};_.r.toGeoJson=function(a){var b=this.j;_.J("data",function(c){c.un(b,a)})};
//    _.r.overrideStyle=function(a,b){this.H.overrideStyle(a,b)};_.r.revertStyle=function(a){this.H.revertStyle(a)};_.r.controls_changed=function(){this.get("controls")&&ue(this)};_.r.drawingMode_changed=function(){this.get("drawingMode")&&ue(this)};_.wc(te.prototype,{map:_.ki,style:_.Ec,controls:_.Eb(_.Bb(_.Ab(ah))),controlPosition:_.Eb(_.Ab(_.Ff)),drawingMode:_.Eb(_.Ab(ah))});_.ve.prototype.V=_.m("j");_.we.prototype.V=_.m("j");_.mi=new _.ve;_.ni=new _.ve;xe.prototype.V=_.m("j");_.oi=new _.ye;_.ye.prototype.V=_.m("j");_.pi=new _.ve;_.qi=new xe;_.ze.prototype.V=_.m("j");_.ri=new _.we;_.si=new _.ze;_.ti={METRIC:0,IMPERIAL:1};_.ui={DRIVING:"DRIVING",WALKING:"WALKING",BICYCLING:"BICYCLING",TRANSIT:"TRANSIT"};_.vi={BEST_GUESS:"bestguess",OPTIMISTIC:"optimistic",PESSIMISTIC:"pessimistic"};_.wi={BUS:"BUS",RAIL:"RAIL",SUBWAY:"SUBWAY",TRAIN:"TRAIN",TRAM:"TRAM"};_.xi={LESS_WALKING:"LESS_WALKING",FEWER_TRANSFERS:"FEWER_TRANSFERS"};var yi=_.xb({routes:_.Bb(_.Cb(_.Ta))},!0);_.v(Be,_.G);_.r=Be.prototype;_.r.internalAnchor_changed=function(){var a=this.get("internalAnchor");Ce(this,"attribution",a);Ce(this,"place",a);Ce(this,"internalAnchorMap",a,"map");Ce(this,"internalAnchorPoint",a,"anchorPoint");a instanceof _.fe?Ce(this,"internalAnchorPosition",a,"internalPosition"):Ce(this,"internalAnchorPosition",a,"position")};
//    _.r.internalAnchorPoint_changed=Be.prototype.internalPixelOffset_changed=function(){var a=this.get("internalAnchorPoint")||_.gh,b=this.get("internalPixelOffset")||_.hh;this.set("pixelOffset",new _.M(b.width+Math.round(a.x),b.height+Math.round(a.y)))};_.r.internalAnchorPosition_changed=function(){var a=this.get("internalAnchorPosition");a&&this.set("position",a)};_.r.internalAnchorMap_changed=function(){this.get("internalAnchor")&&this.j.set("map",this.get("internalAnchorMap"))};
//    _.r.ep=function(){var a=this.get("internalAnchor");!this.j.get("map")&&a&&a.get("map")&&this.set("internalAnchor",null)};_.r.internalContent_changed=function(){this.set("content",Ae(this.get("internalContent")))};_.r.trigger=function(a){_.C.trigger(this.j,a)};_.r.close=function(){this.j.set("map",null)};_.v(_.De,_.G);_.wc(_.De.prototype,{content:_.Db(_.eh,_.Cb(yb)),position:_.Eb(_.$b),size:_.Eb(rc),map:_.Db(_.ki,li),anchor:_.Eb(_.zb(_.G,"MVCObject")),zIndex:_.dh});_.De.prototype.open=function(a,b){this.set("anchor",b);b?!this.get("map")&&a&&this.set("map",a):this.set("map",a)};_.De.prototype.close=function(){this.set("map",null)};_.Ee=[];_.v(Ge,_.G);Ge.prototype.changed=function(a){if("map"==a||"panel"==a){var b=this;_.J("directions",function(c){c.$n(b,a)})}"panel"==a&&_.Fe(this.getPanel())};_.wc(Ge.prototype,{directions:yi,map:_.ki,panel:_.Eb(_.Cb(yb)),routeIndex:_.dh});He.prototype.route=function(a,b){_.J("directions",function(c){c.Qj(a,b,!0)})};Ie.prototype.getDistanceMatrix=function(a,b){_.J("distance_matrix",function(c){c.j(a,b)})};Je.prototype.getElevationAlongPath=function(a,b){_.J("elevation",function(c){c.getElevationAlongPath(a,b)})};Je.prototype.getElevationForLocations=function(a,b){_.J("elevation",function(c){c.getElevationForLocations(a,b)})};_.zi=_.zb(_.Hd,"LatLngBounds");_.Ke.prototype.geocode=function(a,b){_.J("geocoder",function(c){c.geocode(a,b)})};_.v(_.Le,_.G);_.Le.prototype.map_changed=function(){var a=this;_.J("kml",function(b){b.j(a)})};_.wc(_.Le.prototype,{map:_.ki,url:null,bounds:null,opacity:_.dh});_.Bi={UNKNOWN:"UNKNOWN",OK:_.ga,INVALID_REQUEST:_.ba,DOCUMENT_NOT_FOUND:"DOCUMENT_NOT_FOUND",FETCH_ERROR:"FETCH_ERROR",INVALID_DOCUMENT:"INVALID_DOCUMENT",DOCUMENT_TOO_LARGE:"DOCUMENT_TOO_LARGE",LIMITS_EXCEEDED:"LIMITS_EXECEEDED",TIMED_OUT:"TIMED_OUT"};_.v(Me,_.G);_.r=Me.prototype;_.r.ef=function(){var a=this;_.J("kml",function(b){b.H(a)})};_.r.url_changed=Me.prototype.ef;_.r.driveFileId_changed=Me.prototype.ef;_.r.map_changed=Me.prototype.ef;_.r.zIndex_changed=Me.prototype.ef;_.wc(Me.prototype,{map:_.ki,defaultViewport:null,metadata:null,status:null,url:_.eh,screenOverlays:_.fh,zIndex:_.dh});_.v(_.Ne,_.G);_.wc(_.Ne.prototype,{map:_.ki});_.v(Oe,_.G);_.wc(Oe.prototype,{map:_.ki});_.v(Pe,_.G);_.wc(Pe.prototype,{map:_.ki});_.Ef={japan_prequake:20,japan_postquake2010:24};_.Ci={NEAREST:"nearest",BEST:"best"};_.Di={DEFAULT:"default",OUTDOOR:"outdoor"};var Ei,Fi,Gi,Hi;Qe.prototype.V=_.m("j");var Ii=new Re,Ji=new Se,Ki=new Te;Re.prototype.V=_.m("j");Se.prototype.V=_.m("j");Te.prototype.V=_.m("j");_.Ue.prototype.V=_.m("j");_.Li=new _.Ue;_.Mi=new _.Ue;var wf,xf,qf,zf,Cf;_.Ve.prototype.V=_.m("j");_.Ve.prototype.getUrl=function(a){return _.N(this.j,0)[a]};_.Ve.prototype.setUrl=function(a,b){_.N(this.j,0)[a]=b};_.We.prototype.V=_.m("j");_.$e.prototype.V=_.m("j");_.Ni=new _.Ve;_.Oi=new _.Ve;_.Pi=new _.Ve;_.Qi=new _.Ve;_.Ri=new _.Ve;af.prototype.V=_.m("j");bf.prototype.V=_.m("j");cf.prototype.V=_.m("j");df.prototype.V=_.m("j");_.Si=new _.$e;_.Ti=new _.We;wf=new af;xf=new bf;qf=new cf;_.Ui=new _.ff;_.bj=new _.gf;zf=new Qe;Cf=new ef;ef.prototype.V=_.m("j");
//    _.ff.prototype.V=_.m("j");_.gf.prototype.V=_.m("j");_.v(Gf,_.Dc);Gf.prototype.visible_changed=function(){var a=this;!a.W&&a.getVisible()&&(a.W=!0,_.J("streetview",function(b){var c;a.R&&(c=a.R);b.Cp(a,c)}))};_.wc(Gf.prototype,{visible:_.fh,pano:_.eh,position:_.Eb(_.$b),pov:_.Eb(jh),photographerPov:null,location:null,links:_.Bb(_.Cb(_.Ta)),status:null,zoom:_.dh,enableCloseButton:_.fh});Gf.prototype.registerPanoProvider=_.tc("panoProvider");_.r=_.Hf.prototype;_.r.qf=_.ra(3);_.r.qc=_.ra(4);_.r.Ve=_.ra(5);_.r.Ue=_.ra(6);_.r.Te=_.ra(7);_.v(If,dd);_.Jf.prototype.addListener=function(a,b){this.Ea.addListener(a,b)};_.Jf.prototype.addListenerOnce=function(a,b){this.Ea.addListenerOnce(a,b)};_.Jf.prototype.removeListener=function(a,b){this.Ea.removeListener(a,b)};_.Jf.prototype.j=_.ra(8);_.dg={};_.Kf.prototype.fromLatLngToPoint=function(a,b){b=b||new _.L(0,0);var c=this.j;b.x=c.x+a.lng()*this.R;a=_.La(Math.sin(_.Tb(a.lat())),-(1-1E-15),1-1E-15);b.y=c.y+.5*Math.log((1+a)/(1-a))*-this.S;return b};_.Kf.prototype.fromPointToLatLng=function(a,b){var c=this.j;return new _.H(_.Ub(2*Math.atan(Math.exp((a.y-c.y)/-this.S))-Math.PI/2),(a.x-c.x)/this.R,b)};_.Lf.prototype.isEmpty=function(){return!(this.wa<this.Aa&&this.va<this.Ba)};_.Lf.prototype.extend=function(a){a&&(this.wa=Math.min(this.wa,a.x),this.Aa=Math.max(this.Aa,a.x),this.va=Math.min(this.va,a.y),this.Ba=Math.max(this.Ba,a.y))};_.Lf.prototype.getCenter=function(){return new _.L((this.wa+this.Aa)/2,(this.va+this.Ba)/2)};_.cj=_.Nf(-window.Infinity,-window.Infinity,window.Infinity,window.Infinity);_.dj=_.Nf(0,0,0,0);_.v(_.Qf,_.G);_.Qf.prototype.ya=function(){var a=this;a.ma||(a.ma=window.setTimeout(function(){a.ma=void 0;a.Ka()},a.xl))};_.Qf.prototype.W=function(){this.ma&&window.clearTimeout(this.ma);this.ma=void 0;this.Ka()};var ej,fj;Tf.prototype.V=_.m("j");Uf.prototype.V=_.m("j");var gj=new Tf;var hj,ij;Vf.prototype.V=_.m("j");Wf.prototype.V=_.m("j");var jj;Xf.prototype.V=_.m("j");Xf.prototype.getZoom=function(){var a=this.j[2];return null!=a?a:0};Xf.prototype.setZoom=function(a){this.j[2]=a};var kj=new Vf,lj=new Wf,mj=new Uf,nj=new Qe;_.v(Yf,_.Qf);var Zf={roadmap:0,satellite:2,hybrid:3,terrain:4},oj={0:1,2:2,3:2,4:2};_.r=Yf.prototype;_.r.Ui=_.sc("center");_.r.ii=_.sc("zoom");_.r.changed=function(){var a=this.Ui(),b=this.ii(),c=$f(this);if(a&&!a.j(this.ra)||this.qa!=b||this.ua!=c)ag(this.H),this.ya(),this.qa=b,this.ua=c;this.ra=a};
//    _.r.Ka=function(){var a="",b=this.Ui(),c=this.ii(),d=$f(this),e=this.get("size");if(b&&(0,window.isFinite)(b.lat())&&(0,window.isFinite)(b.lng())&&1<c&&null!=d&&e&&e.width&&e.height&&this.j){_.Rf(this.j,e);var f;(b=_.Of(this.S,b,c))?(f=new _.Lf,f.wa=Math.round(b.x-e.width/2),f.Aa=f.wa+e.width,f.va=Math.round(b.y-e.height/2),f.Ba=f.va+e.height):f=null;b=oj[d];if(f){var a=new Xf,g;a.j[0]=a.j[0]||[];g=new Vf(a.j[0]);g.j[0]=f.wa;g.j[1]=f.va;a.j[1]=b;a.setZoom(c);a.j[3]=a.j[3]||[];c=new Wf(a.j[3]);c.j[0]=
//        f.Aa-f.wa;c.j[1]=f.Ba-f.va;a.j[4]=a.j[4]||[];c=new Uf(a.j[4]);c.j[0]=d;c.j[4]=_.hf(_.kf(_.P));c.j[5]=_.jf(_.kf(_.P)).toLowerCase();c.j[9]=!0;c.j[11]=!0;d=this.$+(0,window.unescape)("%3F");if(!jj){c=jj={ka:-1,U:[]};hj||(hj={ka:-1,U:[,_.Q,_.Q]});b=_.O(kj,hj);ij||(ij={ka:-1,U:[]},ij.U=[,_.uh,_.uh,_.pd(1)]);f=_.O(lj,ij);fj||(g=[],fj={ka:-1,U:g},g[1]=_.S,g[2]=_.R,g[3]=_.R,g[5]=_.T,g[6]=_.T,ej||(ej={ka:-1,U:[,_.yh,_.R]}),g[9]=_.O(gj,ej),g[10]=_.R,g[11]=_.R,g[12]=_.R,g[100]=_.R);g=_.O(mj,fj);if(!Ei){var h=
//        Ei={ka:-1,U:[]};Fi||(Fi={ka:-1,U:[,_.R]});var l=_.O(Ii,Fi);Hi||(Hi={ka:-1,U:[,_.R,_.R]});var n=_.O(Ki,Hi);Gi||(Gi={ka:-1,U:[,_.R]});h.U=[,l,,,,,,,,,n,,_.O(Ji,Gi)]}c.U=[,b,_.S,_.uh,f,g,_.O(nj,Ei)]}a=_.Fh.j(a.j,jj);a=this.T(d+a)}}this.H&&e&&(_.Rf(this.H,e),cg(this,a))};
//    _.r.div_changed=function(){var a=this.get("div"),b=this.j;if(a)if(b)a.appendChild(b);else{b=this.j=window.document.createElement("div");b.style.overflow="hidden";var c=this.H=window.document.createElement("img");_.C.addDomListener(b,"contextmenu",function(a){_.cb(a);_.eb(a)});c.ontouchstart=c.ontouchmove=c.ontouchend=c.ontouchcancel=function(a){_.db(a);_.eb(a)};_.Rf(c,_.hh);a.appendChild(b);this.Ka()}else b&&(ag(b),this.j=null)};var kg;_.yg="StopIteration"in _.Kc?_.Kc.StopIteration:{message:"StopIteration",stack:""};_.mg.prototype.next=function(){throw _.yg;};_.mg.prototype.Eg=function(){return this};_.ng.prototype.fh=!0;_.ng.prototype.Kc=_.ra(10);_.ng.prototype.hj=!0;_.ng.prototype.Gf=_.ra(12);_.og("about:blank");_.qg.prototype.hj=!0;_.qg.prototype.Gf=_.ra(11);_.qg.prototype.fh=!0;_.qg.prototype.Kc=_.ra(9);_.pg={};_.rg("<!DOCTYPE html>",0);_.rg("",0);_.rg("<br>",0);!_.Kh&&!_.Ih||_.Ih&&9<=Number(_.Yh)||_.Kh&&_.xd("1.9.1");_.Ih&&_.xd("9");_.v(ug,_.mg);ug.prototype.setPosition=function(a,b,c){if(this.node=a)this.H=_.ya(b)?b:1!=this.node.nodeType?0:this.j?-1:1;_.ya(c)&&(this.depth=c)};
//    ug.prototype.next=function(){var a;if(this.R){if(!this.node||this.S&&0==this.depth)throw _.yg;a=this.node;var b=this.j?-1:1;if(this.H==b){var c=this.j?a.lastChild:a.firstChild;c?this.setPosition(c):this.setPosition(a,-1*b)}else(c=this.j?a.previousSibling:a.nextSibling)?this.setPosition(c):this.setPosition(a.parentNode,-1*b);this.depth+=this.H*(this.j?-1:1)}else this.R=!0;a=this.node;if(!this.node)throw _.yg;return a};
//    ug.prototype.splice=function(a){var b=this.node,c=this.j?1:-1;this.H==c&&(this.H=-1*c,this.depth+=this.H*(this.j?-1:1));this.j=!this.j;ug.prototype.next.call(this);this.j=!this.j;for(var c=_.wa(arguments[0])?arguments[0]:arguments,d=c.length-1;0<=d;d--)_.sg(c[d],b);_.tg(b)};_.v(vg,ug);vg.prototype.next=function(){do vg.Vc.next.call(this);while(-1==this.H);return this.node};_.qj=_.Kc.document&&_.Kc.document.createElement("div");_.v(Bg,_.Nd);_.r=Bg.prototype;_.r.streetView_changed=function(){this.get("streetView")||this.set("streetView",this.__gm.R)};_.r.getDiv=function(){return this.__gm.Ga};_.r.panBy=function(a,b){var c=this.__gm;_.J("map",function(){_.C.trigger(c,"panby",a,b)})};_.r.panTo=function(a){var b=this.__gm;a=_.$b(a);_.J("map",function(){_.C.trigger(b,"panto",a)})};_.r.panToBounds=function(a){var b=this.__gm,c=_.Kd(a);_.J("map",function(){_.C.trigger(b,"pantolatlngbounds",c)})};
//    _.r.fitBounds=function(a){var b=this;a=_.Kd(a);_.J("map",function(c){c.fitBounds(b,a)})};_.wc(Bg.prototype,{bounds:null,streetView:li,center:_.Eb(_.$b),zoom:_.dh,mapTypeId:_.eh,projection:null,heading:_.dh,tilt:_.dh,clickableIcons:ch});Cg.prototype.getMaxZoomAtLatLng=function(a,b){_.J("maxzoom",function(c){c.getMaxZoomAtLatLng(a,b)})};_.v(Dg,_.G);Dg.prototype.changed=function(a){if("suppressInfoWindows"!=a&&"clickable"!=a){var b=this;_.J("onion",function(a){a.j(b)})}};_.wc(Dg.prototype,{map:_.ki,tableId:_.dh,query:_.Eb(_.Db(_.bh,_.Cb(_.Ta,"not an Object")))});_.v(_.Eg,_.G);_.Eg.prototype.map_changed=function(){var a=this;_.J("overlay",function(b){b.Em(a)})};_.wc(_.Eg.prototype,{panes:null,projection:null,map:_.Db(_.ki,li)});_.v(_.Fg,_.G);_.Fg.prototype.map_changed=_.Fg.prototype.visible_changed=function(){var a=this;_.J("poly",function(b){b.j(a)})};_.Fg.prototype.center_changed=function(){_.C.trigger(this,"bounds_changed")};_.Fg.prototype.radius_changed=_.Fg.prototype.center_changed;_.Fg.prototype.getBounds=function(){var a=this.get("radius"),b=this.get("center");if(b&&_.A(a)){var c=this.get("map"),c=c&&c.__gm.get("mapType");return _.Pf(b,a/_.he(c))}return null};
//    _.wc(_.Fg.prototype,{center:_.Eb(_.$b),draggable:_.fh,editable:_.fh,map:_.ki,radius:_.dh,visible:_.fh});_.v(_.Gg,_.G);_.Gg.prototype.map_changed=_.Gg.prototype.visible_changed=function(){var a=this;_.J("poly",function(b){b.R(a)})};_.wc(_.Gg.prototype,{draggable:_.fh,editable:_.fh,bounds:_.Eb(_.Kd),map:_.ki,visible:_.fh});_.v(Hg,_.G);Hg.prototype.map_changed=function(){var a=this;_.J("streetview",function(b){b.Dm(a)})};_.wc(Hg.prototype,{map:_.ki});_.Ig.prototype.getPanorama=function(a,b){var c=this.j||void 0;_.J("streetview",function(d){_.J("geometry",function(e){d.En(a,b,e.computeHeading,e.computeOffset,c)})})};_.Ig.prototype.getPanoramaByLocation=function(a,b,c){this.getPanorama({location:a,radius:b,preference:50>(b||0)?"best":"nearest"},c)};_.Ig.prototype.getPanoramaById=function(a,b){this.getPanorama({pano:a},b)};_.v(_.Jg,_.G);_.r=_.Jg.prototype;_.r.getTile=function(a,b,c){if(!a||!c)return null;var d=c.createElement("div");c={La:a,zoom:b,Cc:null};d.__gmimt=c;_.Ac(this.j,d);var e=Lg(this);1!=e&&Kg(d,e);if(this.H){var e=this.tileSize||new _.M(256,256),f=this.R(a,b);c.Cc=this.H(a,b,e,d,f,function(){_.C.trigger(d,"load")})}return d};_.r.releaseTile=function(a){a&&this.j.contains(a)&&(this.j.remove(a),(a=a.__gmimt.Cc)&&a.release())};_.r.Vg=_.ra(13);_.r.Bp=function(){this.H&&this.j.forEach(function(a){a.__gmimt.Cc.bc()})};
//    _.r.opacity_changed=function(){var a=Lg(this);this.j.forEach(function(b){Kg(b,a)})};_.r.$d=!0;_.wc(_.Jg.prototype,{opacity:_.dh});_.v(_.Mg,_.G);_.Mg.prototype.getTile=kh;_.Mg.prototype.tileSize=new _.M(256,256);_.Mg.prototype.$d=!0;_.v(_.Ng,_.Mg);_.v(_.Og,_.G);_.wc(_.Og.prototype,{attribution:_.Eb(fi),place:_.Eb(gi)});var rj={Animation:{BOUNCE:1,DROP:2,pr:3,nr:4},Circle:_.Fg,ControlPosition:_.Ff,Data:te,GroundOverlay:_.Le,ImageMapType:_.Jg,InfoWindow:_.De,LatLng:_.H,LatLngBounds:_.Hd,MVCArray:_.yc,MVCObject:_.G,Map:Bg,MapTypeControlStyle:{DEFAULT:0,HORIZONTAL_BAR:1,DROPDOWN_MENU:2,INSET:3,INSET_LARGE:4},MapTypeId:_.$g,MapTypeRegistry:zd,Marker:_.fe,MarkerImage:function(a,b,c,d,e){this.url=a;this.size=b||e;this.origin=c;this.anchor=d;this.scaledSize=e;this.labelOrigin=null},NavigationControlStyle:{DEFAULT:0,SMALL:1,
//        ANDROID:2,ZOOM_PAN:3,qr:4,lm:5},OverlayView:_.Eg,Point:_.L,Polygon:_.pe,Polyline:_.qe,Rectangle:_.Gg,ScaleControlStyle:{DEFAULT:0},Size:_.M,StreetViewPreference:_.Ci,StreetViewSource:_.Di,StrokePosition:{CENTER:0,INSIDE:1,OUTSIDE:2},SymbolPath:ih,ZoomControlStyle:{DEFAULT:0,SMALL:1,LARGE:2,lm:3},event:_.C};
//    _.Ja(rj,{BicyclingLayer:_.Ne,DirectionsRenderer:Ge,DirectionsService:He,DirectionsStatus:{OK:_.ga,UNKNOWN_ERROR:_.ja,OVER_QUERY_LIMIT:_.ha,REQUEST_DENIED:_.ia,INVALID_REQUEST:_.ba,ZERO_RESULTS:_.ka,MAX_WAYPOINTS_EXCEEDED:_.ea,NOT_FOUND:_.fa},DirectionsTravelMode:_.ui,DirectionsUnitSystem:_.ti,DistanceMatrixService:Ie,DistanceMatrixStatus:{OK:_.ga,INVALID_REQUEST:_.ba,OVER_QUERY_LIMIT:_.ha,REQUEST_DENIED:_.ia,UNKNOWN_ERROR:_.ja,MAX_ELEMENTS_EXCEEDED:_.da,MAX_DIMENSIONS_EXCEEDED:_.ca},DistanceMatrixElementStatus:{OK:_.ga,
//        NOT_FOUND:_.fa,ZERO_RESULTS:_.ka},ElevationService:Je,ElevationStatus:{OK:_.ga,UNKNOWN_ERROR:_.ja,OVER_QUERY_LIMIT:_.ha,REQUEST_DENIED:_.ia,INVALID_REQUEST:_.ba,kr:"DATA_NOT_AVAILABLE"},FusionTablesLayer:Dg,Geocoder:_.Ke,GeocoderLocationType:{ROOFTOP:"ROOFTOP",RANGE_INTERPOLATED:"RANGE_INTERPOLATED",GEOMETRIC_CENTER:"GEOMETRIC_CENTER",APPROXIMATE:"APPROXIMATE"},GeocoderStatus:{OK:_.ga,UNKNOWN_ERROR:_.ja,OVER_QUERY_LIMIT:_.ha,REQUEST_DENIED:_.ia,INVALID_REQUEST:_.ba,ZERO_RESULTS:_.ka,ERROR:_.aa},KmlLayer:Me,
//        KmlLayerStatus:_.Bi,MaxZoomService:Cg,MaxZoomStatus:{OK:_.ga,ERROR:_.aa},SaveWidget:_.Og,StreetViewCoverageLayer:Hg,StreetViewPanorama:Gf,StreetViewService:_.Ig,StreetViewStatus:{OK:_.ga,UNKNOWN_ERROR:_.ja,ZERO_RESULTS:_.ka},StyledMapType:_.Ng,TrafficLayer:Oe,TrafficModel:_.vi,TransitLayer:Pe,TransitMode:_.wi,TransitRoutePreference:_.xi,TravelMode:_.ui,UnitSystem:_.ti});_.Ja(te,{Feature:_.oc,Geometry:Zb,GeometryCollection:_.Sd,LineString:_.Ud,LinearRing:_.Yd,MultiLineString:_.Wd,MultiPoint:_.Xd,MultiPolygon:_.be,Point:_.ac,Polygon:_.$d});var Tg=/'/g,Ug;_.mc("main",{});window.google.maps.Load(function(a,b){var c=window.google.maps;Yg();var d=Zg(c);_.P=new df(a);_.sj=Math.random()<_.sf();_.tj=Math.round(1E15*Math.random()).toString(36);_.Ag=Vg();_.Ai=Wg();_.pj=new _.yc;_.ig=b;for(a=0;a<_.gd(_.P.j,8);++a)_.dg[Bf(a)]=!0;a=_.yf();de(of(a));_.Ia(rj,function(a,b){c[a]=b});c.version=_.pf(a);window.setTimeout(function(){nc(["util","stats"],function(a,b){a.H.j();a.R();d&&b.j.j({ev:"api_alreadyloaded",client:_.tf(_.P),key:_.vf()})})},5E3);_.C.Up();kg=new jg;(a=uf())&&nc(_.N(_.P.j,
//        12),Xg(a),!0)});}).call(this,{});
/**!
 * MixItUp v2.1.6
 *
 * @copyright Copyright 2014 KunkaLabs Limited.
 * @author    KunkaLabs Limited.
 * @link      https://mixitup.kunkalabs.com
 *
 * @license   Commercial use requires a commercial license.
 *            https://mixitup.kunkalabs.com/licenses/
 *
 *            Non-commercial use permitted under terms of CC-BY-NC license.
 *            http://creativecommons.org/licenses/by-nc/3.0/
 */
! function (a, b) {
	a.MixItUp = function () {
		var b = this;
		b._execAction("_constructor", 0), a.extend(b, {
			selectors: {
				target: ".mix"
				, filter: ".filter"
				, sort: ".sort"
			}
			, animation: {
				enable: !0
				, effects: "fade scale"
				, duration: 600
				, easing: "ease"
				, perspectiveDistance: "3000"
				, perspectiveOrigin: "50% 50%"
				, queue: !0
				, queueLimit: 1
				, animateChangeLayout: !1
				, animateResizeContainer: !0
				, animateResizeTargets: !1
				, staggerSequence: !1
				, reverseOut: !1
			}
			, callbacks: {
				onMixLoad: !1
				, onMixStart: !1
				, onMixBusy: !1
				, onMixEnd: !1
				, onMixFail: !1
				, _user: !1
			}
			, controls: {
				enable: !0
				, live: !1
				, toggleFilterButtons: !1
				, toggleLogic: "or"
				, activeClass: "active"
			}
			, layout: {
				display: "inline-block"
				, containerClass: ""
				, containerClassFail: "fail"
			}
			, load: {
				filter: "all"
				, sort: !1
			}
			, _$body: null
			, _$container: null
			, _$targets: null
			, _$parent: null
			, _$sortButtons: null
			, _$filterButtons: null
			, _suckMode: !1
			, _mixing: !1
			, _sorting: !1
			, _clicking: !1
			, _loading: !0
			, _changingLayout: !1
			, _changingClass: !1
			, _changingDisplay: !1
			, _origOrder: []
			, _startOrder: []
			, _newOrder: []
			, _activeFilter: null
			, _toggleArray: []
			, _toggleString: ""
			, _activeSort: "default:asc"
			, _newSort: null
			, _startHeight: null
			, _newHeight: null
			, _incPadding: !0
			, _newDisplay: null
			, _newClass: null
			, _targetsBound: 0
			, _targetsDone: 0
			, _queue: []
			, _$show: a()
			, _$hide: a()
		}), b._execAction("_constructor", 1)
	}, a.MixItUp.prototype = {
		constructor: a.MixItUp
		, _instances: {}
		, _handled: {
			_filter: {}
			, _sort: {}
		}
		, _bound: {
			_filter: {}
			, _sort: {}
		}
		, _actions: {}
		, _filters: {}
		, extend: function (b) {
			for (var c in b) a.MixItUp.prototype[c] = b[c]
		}
		, addAction: function (b, c, d, e) {
			a.MixItUp.prototype._addHook("_actions", b, c, d, e)
		}
		, addFilter: function (b, c, d, e) {
			a.MixItUp.prototype._addHook("_filters", b, c, d, e)
		}
		, _addHook: function (b, c, d, e, f) {
			var g = a.MixItUp.prototype[b]
				, h = {};
			f = 1 === f || "post" === f ? "post" : "pre", h[c] = {}, h[c][f] = {}, h[c][f][d] = e, a.extend(!0, g, h)
		}
		, _init: function (b, c) {
			var d = this;
			if (d._execAction("_init", 0, arguments), c && a.extend(!0, d, c), d._$body = a("body"), d._domNode = b, d._$container = a(b), d._$container.addClass(d.layout.containerClass), d._id = b.id, d._platformDetect(), d._brake = d._getPrefixedCSS("transition", "none"), d._refresh(!0), d._$parent = d._$targets.parent().length ? d._$targets.parent() : d._$container, d.load.sort && (d._newSort = d._parseSort(d.load.sort), d._newSortString = d.load.sort, d._activeSort = d.load.sort, d._sort(), d._printSort()), d._activeFilter = "all" === d.load.filter ? d.selectors.target : "none" === d.load.filter ? "" : d.load.filter, d.controls.enable && d._bindHandlers(), d.controls.toggleFilterButtons) {
				d._buildToggleArray();
				for (var e = 0; e < d._toggleArray.length; e++) d._updateControls({
					filter: d._toggleArray[e]
					, sort: d._activeSort
				}, !0)
			} else d.controls.enable && d._updateControls({
				filter: d._activeFilter
				, sort: d._activeSort
			});
			d._filter(), d._init = !0, d._$container.data("mixItUp", d), d._execAction("_init", 1, arguments), d._buildState(), d._$targets.css(d._brake), d._goMix(d.animation.enable)
		}
		, _platformDetect: function () {
			var a = this
				, c = ["Webkit", "Moz", "O", "ms"]
				, d = ["webkit", "moz"]
				, e = window.navigator.appVersion.match(/Chrome\/(\d+)\./) || !1
				, f = "undefined" != typeof InstallTrigger
				, g = function (a) {
				for (var b = 0; b < c.length; b++)
					if (c[b] + "Transition" in a.style) return {
						prefix: "-" + c[b].toLowerCase() + "-"
						, vendor: c[b]
					};
				return "transition" in a.style ? "" : !1
			}
				, h = g(a._domNode);
			a._execAction("_platformDetect", 0), a._chrome = e ? parseInt(e[1], 10) : !1, a._ff = f ? parseInt(window.navigator.userAgent.match(/rv:([^)]+)\)/)[1]) : !1, a._prefix = h.prefix, a._vendor = h.vendor, a._suckMode = window.atob && a._prefix ? !1 : !0, a._suckMode && (a.animation.enable = !1), a._ff && a._ff <= 4 && (a.animation.enable = !1);
			for (var i = 0; i < d.length && !window.requestAnimationFrame; i++) window.requestAnimationFrame = window[d[i] + "RequestAnimationFrame"];
			"function" != typeof Object.getPrototypeOf && (Object.getPrototypeOf = "object" == typeof "test".__proto__ ? function (a) {
				return a.__proto__
			} : function (a) {
				return a.constructor.prototype
			}), a._domNode.nextElementSibling === b && Object.defineProperty(Element.prototype, "nextElementSibling", {
				get: function () {
					for (var a = this.nextSibling; a;) {
						if (1 === a.nodeType) return a;
						a = a.nextSibling
					}
					return null
				}
			}), a._execAction("_platformDetect", 1)
		}
		, _refresh: function (a, c) {
			var d = this;
			d._execAction("_refresh", 0, arguments), d._$targets = d._$container.find(d.selectors.target);
			for (var e = 0; e < d._$targets.length; e++) {
				var f = d._$targets[e];
				if (f.dataset === b || c) {
					f.dataset = {};
					for (var g = 0; g < f.attributes.length; g++) {
						var h = f.attributes[g]
							, i = h.name
							, j = h.value;
						if (i.indexOf("data-") > -1) {
							var k = d._helpers._camelCase(i.substring(5, i.length));
							f.dataset[k] = j
						}
					}
				}
				f.mixParent === b && (f.mixParent = d._id)
			}
			if (d._$targets.length && a || !d._origOrder.length && d._$targets.length) {
				d._origOrder = [];
				for (var e = 0; e < d._$targets.length; e++) {
					var f = d._$targets[e];
					d._origOrder.push(f)
				}
			}
			d._execAction("_refresh", 1, arguments)
		}
		, _bindHandlers: function () {
			var c = this
				, d = a.MixItUp.prototype._bound._filter
				, e = a.MixItUp.prototype._bound._sort;
			c._execAction("_bindHandlers", 0), c.controls.live ? c._$body.on("click.mixItUp." + c._id, c.selectors.sort, function () {
				c._processClick(a(this), "sort")
			}).on("click.mixItUp." + c._id, c.selectors.filter, function () {
				c._processClick(a(this), "filter")
			}) : (c._$sortButtons = a(c.selectors.sort), c._$filterButtons = a(c.selectors.filter), c._$sortButtons.on("click.mixItUp." + c._id, function () {
				c._processClick(a(this), "sort")
			}), c._$filterButtons.on("click.mixItUp." + c._id, function () {
				c._processClick(a(this), "filter")
			})), d[c.selectors.filter] = d[c.selectors.filter] === b ? 1 : d[c.selectors.filter] + 1, e[c.selectors.sort] = e[c.selectors.sort] === b ? 1 : e[c.selectors.sort] + 1, c._execAction("_bindHandlers", 1)
		}
		, _processClick: function (c, d) {
			var e = this
				, f = function (c, d, f) {
				var g = a.MixItUp.prototype;
				g._handled["_" + d][e.selectors[d]] = g._handled["_" + d][e.selectors[d]] === b ? 1 : g._handled["_" + d][e.selectors[d]] + 1, g._handled["_" + d][e.selectors[d]] === g._bound["_" + d][e.selectors[d]] && (c[(f ? "remove" : "add") + "Class"](e.controls.activeClass), delete g._handled["_" + d][e.selectors[d]])
			};
			if (e._execAction("_processClick", 0, arguments), !e._mixing || e.animation.queue && e._queue.length < e.animation.queueLimit) {
				if (e._clicking = !0, "sort" === d) {
					var g = c.attr("data-sort");
					(!c.hasClass(e.controls.activeClass) || g.indexOf("random") > -1) && (a(e.selectors.sort).removeClass(e.controls.activeClass), f(c, d), e.sort(g))
				}
				if ("filter" === d) {
					var h, i = c.attr("data-filter")
						, j = "or" === e.controls.toggleLogic ? "," : "";
					e.controls.toggleFilterButtons ? (e._buildToggleArray(), c.hasClass(e.controls.activeClass) ? (f(c, d, !0), h = e._toggleArray.indexOf(i), e._toggleArray.splice(h, 1)) : (f(c, d), e._toggleArray.push(i)), e._toggleArray = a.grep(e._toggleArray, function (a) {
						return a
					}), e._toggleString = e._toggleArray.join(j), e.filter(e._toggleString)) : c.hasClass(e.controls.activeClass) || (a(e.selectors.filter).removeClass(e.controls.activeClass), f(c, d), e.filter(i))
				}
				e._execAction("_processClick", 1, arguments)
			} else "function" == typeof e.callbacks.onMixBusy && e.callbacks.onMixBusy.call(e._domNode, e._state, e), e._execAction("_processClickBusy", 1, arguments)
		}
		, _buildToggleArray: function () {
			var a = this
				, b = a._activeFilter.replace(/\s/g, "");
			if (a._execAction("_buildToggleArray", 0, arguments), "or" === a.controls.toggleLogic) a._toggleArray = b.split(",");
			else {
				a._toggleArray = b.split("."), !a._toggleArray[0] && a._toggleArray.shift();
				for (var c, d = 0; c = a._toggleArray[d]; d++) a._toggleArray[d] = "." + c
			}
			a._execAction("_buildToggleArray", 1, arguments)
		}
		, _updateControls: function (c, d) {
			var e = this
				, f = {
				filter: c.filter
				, sort: c.sort
			}
				, g = function (a, b) {
				d && "filter" == h && "none" !== f.filter && "" !== f.filter ? a.filter(b).addClass(e.controls.activeClass) : a.removeClass(e.controls.activeClass).filter(b).addClass(e.controls.activeClass)
			}
				, h = "filter"
				, i = null;
			e._execAction("_updateControls", 0, arguments), c.filter === b && (f.filter = e._activeFilter), c.sort === b && (f.sort = e._activeSort), f.filter === e.selectors.target && (f.filter = "all");
			for (var j = 0; 2 > j; j++) i = e.controls.live ? a(e.selectors[h]) : e["_$" + h + "Buttons"], i && g(i, "[data-" + h + '="' + f[h] + '"]'), h = "sort";
			e._execAction("_updateControls", 1, arguments)
		}
		, _filter: function () {
			var b = this;
			b._execAction("_filter", 0);
			for (var c = 0; c < b._$targets.length; c++) {
				var d = a(b._$targets[c]);
				d.is(b._activeFilter) ? b._$show = b._$show.add(d) : b._$hide = b._$hide.add(d)
			}
			b._execAction("_filter", 1)
		}
		, _sort: function () {
			var a = this
				, b = function (a) {
				for (var b = a.slice(), c = b.length, d = c; d--;) {
					var e = parseInt(Math.random() * c)
						, f = b[d];
					b[d] = b[e], b[e] = f
				}
				return b
			};
			a._execAction("_sort", 0), a._startOrder = [];
			for (var c = 0; c < a._$targets.length; c++) {
				var d = a._$targets[c];
				a._startOrder.push(d)
			}
			switch (a._newSort[0].sortBy) {
				case "default":
					a._newOrder = a._origOrder;
					break;
				case "random":
					a._newOrder = b(a._startOrder);
					break;
				case "custom":
					a._newOrder = a._newSort[0].order;
					break;
				default:
					a._newOrder = a._startOrder.concat().sort(function (b, c) {
						return a._compare(b, c)
					})
			}
			a._execAction("_sort", 1)
		}
		, _compare: function (a, b, c) {
			c = c ? c : 0;
			var d = this
				, e = d._newSort[c].order
				, f = function (a) {
				return a.dataset[d._newSort[c].sortBy] || 0
			}
				, g = isNaN(1 * f(a)) ? f(a).toLowerCase() : 1 * f(a)
				, h = isNaN(1 * f(b)) ? f(b).toLowerCase() : 1 * f(b);
			return h > g ? "asc" == e ? -1 : 1 : g > h ? "asc" == e ? 1 : -1 : g == h && d._newSort.length > c + 1 ? d._compare(a, b, c + 1) : 0
		}
		, _printSort: function (a) {
			var b = this
				, c = a ? b._startOrder : b._newOrder
				, d = b._$parent[0].querySelectorAll(b.selectors.target)
				, e = d[d.length - 1].nextElementSibling
				, f = document.createDocumentFragment();
			b._execAction("_printSort", 0, arguments);
			for (var g = 0; g < d.length; g++) {
				var h = d[g]
					, i = h.nextSibling;
				"absolute" !== h.style.position && (i && "#text" == i.nodeName && b._$parent[0].removeChild(i), b._$parent[0].removeChild(h))
			}
			for (var g = 0; g < c.length; g++) {
				var j = c[g];
				if ("default" != b._newSort[0].sortBy || "desc" != b._newSort[0].order || a) f.appendChild(j), f.appendChild(document.createTextNode(" "));
				else {
					var k = f.firstChild;
					f.insertBefore(j, k), f.insertBefore(document.createTextNode(" "), j)
				}
			}
			e ? b._$parent[0].insertBefore(f, e) : b._$parent[0].appendChild(f), b._execAction("_printSort", 1, arguments)
		}
		, _parseSort: function (a) {
			for (var b = this, c = "string" == typeof a ? a.split(" ") : [a], d = [], e = 0; e < c.length; e++) {
				var f = "string" == typeof a ? c[e].split(":") : ["custom", c[e]]
					, g = {
					sortBy: b._helpers._camelCase(f[0])
					, order: f[1] || "asc"
				};
				if (d.push(g), "default" == g.sortBy || "random" == g.sortBy) break
			}
			return b._execFilter("_parseSort", d, arguments)
		}
		, _parseEffects: function () {
			var a = this
				, b = {
				opacity: ""
				, transformIn: ""
				, transformOut: ""
				, filter: ""
			}
				, c = function (b, c) {
				if (a.animation.effects.indexOf(b) > -1) {
					if (c) {
						var d = a.animation.effects.indexOf(b + "(");
						if (d > -1) {
							var e = a.animation.effects.substring(d)
								, f = /\(([^)]+)\)/.exec(e)
								, g = f[1];
							return {
								val: g
							}
						}
					}
					return !0
				}
				return !1
			}
				, d = function (a, b) {
				return b ? "-" === a.charAt(0) ? a.substr(1, a.length) : "-" + a : a
			}
				, e = function (a, e) {
				for (var f = [["scale", ".01"], ["translateX", "20px"], ["translateY", "20px"], ["translateZ", "20px"], ["rotateX", "90deg"], ["rotateY", "90deg"], ["rotateZ", "180deg"]], g = 0; g < f.length; g++) {
					var h = f[g][0]
						, i = f[g][1]
						, j = e && "scale" !== h;
					b[a] += c(h) ? h + "(" + d(c(h, !0).val || i, j) + ") " : ""
				}
			};
			return b.opacity = c("fade") ? c("fade", !0).val || "0" : "1", e("transformIn"), a.animation.reverseOut ? e("transformOut", !0) : b.transformOut = b.transformIn, b.transition = {}, b.transition = a._getPrefixedCSS("transition", "all " + a.animation.duration + "ms " + a.animation.easing + ", opacity " + a.animation.duration + "ms linear"), a.animation.stagger = c("stagger") ? !0 : !1, a.animation.staggerDuration = parseInt(c("stagger") && c("stagger", !0).val ? c("stagger", !0).val : 100), a._execFilter("_parseEffects", b)
		}
		, _buildState: function (a) {
			var b = this
				, c = {};
			return b._execAction("_buildState", 0), c = {
				activeFilter: "" === b._activeFilter ? "none" : b._activeFilter
				, activeSort: a && b._newSortString ? b._newSortString : b._activeSort
				, fail: !b._$show.length && "" !== b._activeFilter
				, $targets: b._$targets
				, $show: b._$show
				, $hide: b._$hide
				, totalTargets: b._$targets.length
				, totalShow: b._$show.length
				, totalHide: b._$hide.length
				, display: a && b._newDisplay ? b._newDisplay : b.layout.display
			}, a ? b._execFilter("_buildState", c) : (b._state = c, void b._execAction("_buildState", 1))
		}
		, _goMix: function (a) {
			var b = this
				, c = function () {
				b._chrome && 31 === b._chrome && f(b._$parent[0]), b._setInter(), d()
			}
				, d = function () {
				{
					var a = window.pageYOffset
						, c = window.pageXOffset;
					document.documentElement.scrollHeight
				}
				b._getInterMixData(), b._setFinal(), b._getFinalMixData(), window.pageYOffset !== a && window.scrollTo(c, a), b._prepTargets(), window.requestAnimationFrame ? requestAnimationFrame(e) : setTimeout(function () {
					e()
				}, 20)
			}
				, e = function () {
				b._animateTargets(), 0 === b._targetsBound && b._cleanUp()
			}
				, f = function (a) {
				var b = a.parentElement
					, c = document.createElement("div")
					, d = document.createDocumentFragment();
				b.insertBefore(c, a), d.appendChild(a), b.replaceChild(a, c)
			}
				, g = b._buildState(!0);
			b._execAction("_goMix", 0, arguments), !b.animation.duration && (a = !1), b._mixing = !0, b._$container.removeClass(b.layout.containerClassFail), "function" == typeof b.callbacks.onMixStart && b.callbacks.onMixStart.call(b._domNode, b._state, g, b), b._$container.trigger("mixStart", [b._state, g, b]), b._getOrigMixData(), a && !b._suckMode ? window.requestAnimationFrame ? requestAnimationFrame(c) : c() : b._cleanUp(), b._execAction("_goMix", 1, arguments)
		}
		, _getTargetData: function (a, b) {
			var c, d = this;
			a.dataset[b + "PosX"] = a.offsetLeft, a.dataset[b + "PosY"] = a.offsetTop, d.animation.animateResizeTargets && (c = window.getComputedStyle(a), a.dataset[b + "MarginBottom"] = parseInt(c.marginBottom), a.dataset[b + "MarginRight"] = parseInt(c.marginRight), a.dataset[b + "Width"] = a.offsetWidth, a.dataset[b + "Height"] = a.offsetHeight)
		}
		, _getOrigMixData: function () {
			var a = this
				, b = a._suckMode ? {
				boxSizing: ""
			} : window.getComputedStyle(a._$parent[0])
				, c = b.boxSizing || b[a._vendor + "BoxSizing"];
			a._incPadding = "border-box" === c, a._execAction("_getOrigMixData", 0), !a._suckMode && (a.effects = a._parseEffects()), a._$toHide = a._$hide.filter(":visible"), a._$toShow = a._$show.filter(":hidden"), a._$pre = a._$targets.filter(":visible"), a._startHeight = a._incPadding ? a._$parent.outerHeight() : a._$parent.height();
			for (var d = 0; d < a._$pre.length; d++) {
				var e = a._$pre[d];
				a._getTargetData(e, "orig")
			}
			a._execAction("_getOrigMixData", 1)
		}
		, _setInter: function () {
			var a = this;
			a._execAction("_setInter", 0), a._changingLayout && a.animation.animateChangeLayout ? (a._$toShow.css("display", a._newDisplay), a._changingClass && a._$container.removeClass(a.layout.containerClass).addClass(a._newClass)) : a._$toShow.css("display", a.layout.display), a._execAction("_setInter", 1)
		}
		, _getInterMixData: function () {
			var a = this;
			a._execAction("_getInterMixData", 0);
			for (var b = 0; b < a._$toShow.length; b++) {
				var c = a._$toShow[b];
				a._getTargetData(c, "inter")
			}
			for (var b = 0; b < a._$pre.length; b++) {
				var c = a._$pre[b];
				a._getTargetData(c, "inter")
			}
			a._execAction("_getInterMixData", 1)
		}
		, _setFinal: function () {
			var a = this;
			a._execAction("_setFinal", 0), a._sorting && a._printSort(), a._$toHide.removeStyle("display"), a._changingLayout && a.animation.animateChangeLayout && a._$pre.css("display", a._newDisplay), a._execAction("_setFinal", 1)
		}
		, _getFinalMixData: function () {
			var a = this;
			a._execAction("_getFinalMixData", 0);
			for (var b = 0; b < a._$toShow.length; b++) {
				var c = a._$toShow[b];
				a._getTargetData(c, "final")
			}
			for (var b = 0; b < a._$pre.length; b++) {
				var c = a._$pre[b];
				a._getTargetData(c, "final")
			}
			a._newHeight = a._incPadding ? a._$parent.outerHeight() : a._$parent.height(), a._sorting && a._printSort(!0), a._$toShow.removeStyle("display"), a._$pre.css("display", a.layout.display), a._changingClass && a.animation.animateChangeLayout && a._$container.removeClass(a._newClass).addClass(a.layout.containerClass), a._execAction("_getFinalMixData", 1)
		}
		, _prepTargets: function () {
			var b = this
				, c = {
				_in: b._getPrefixedCSS("transform", b.effects.transformIn)
				, _out: b._getPrefixedCSS("transform", b.effects.transformOut)
			};
			b._execAction("_prepTargets", 0), b.animation.animateResizeContainer && b._$parent.css("height", b._startHeight + "px");
			for (var d = 0; d < b._$toShow.length; d++) {
				var e = b._$toShow[d]
					, f = a(e);
				e.style.opacity = b.effects.opacity, e.style.display = b._changingLayout && b.animation.animateChangeLayout ? b._newDisplay : b.layout.display, f.css(c._in), b.animation.animateResizeTargets && (e.style.width = e.dataset.finalWidth + "px", e.style.height = e.dataset.finalHeight + "px", e.style.marginRight = -(e.dataset.finalWidth - e.dataset.interWidth) + 1 * e.dataset.finalMarginRight + "px", e.style.marginBottom = -(e.dataset.finalHeight - e.dataset.interHeight) + 1 * e.dataset.finalMarginBottom + "px")
			}
			for (var d = 0; d < b._$pre.length; d++) {
				var e = b._$pre[d]
					, f = a(e)
					, g = {
					x: e.dataset.origPosX - e.dataset.interPosX
					, y: e.dataset.origPosY - e.dataset.interPosY
				}
					, c = b._getPrefixedCSS("transform", "translate(" + g.x + "px," + g.y + "px)");
				f.css(c), b.animation.animateResizeTargets && (e.style.width = e.dataset.origWidth + "px", e.style.height = e.dataset.origHeight + "px", e.dataset.origWidth - e.dataset.finalWidth && (e.style.marginRight = -(e.dataset.origWidth - e.dataset.interWidth) + 1 * e.dataset.origMarginRight + "px"), e.dataset.origHeight - e.dataset.finalHeight && (e.style.marginBottom = -(e.dataset.origHeight - e.dataset.interHeight) + 1 * e.dataset.origMarginBottom + "px"))
			}
			b._execAction("_prepTargets", 1)
		}
		, _animateTargets: function () {
			var b = this;
			b._execAction("_animateTargets", 0), b._targetsDone = 0, b._targetsBound = 0, b._$parent.css(b._getPrefixedCSS("perspective", b.animation.perspectiveDistance + "px")).css(b._getPrefixedCSS("perspective-origin", b.animation.perspectiveOrigin)), b.animation.animateResizeContainer && b._$parent.css(b._getPrefixedCSS("transition", "height " + b.animation.duration + "ms ease")).css("height", b._newHeight + "px");
			for (var c = 0; c < b._$toShow.length; c++) {
				var d = b._$toShow[c]
					, e = a(d)
					, f = {
					x: d.dataset.finalPosX - d.dataset.interPosX
					, y: d.dataset.finalPosY - d.dataset.interPosY
				}
					, g = b._getDelay(c)
					, h = {};
				d.style.opacity = "";
				for (var i = 0; 2 > i; i++) {
					var j = 0 === i ? j = b._prefix : "";
					b._ff && b._ff <= 20 && (h[j + "transition-property"] = "all", h[j + "transition-timing-function"] = b.animation.easing + "ms", h[j + "transition-duration"] = b.animation.duration + "ms"), h[j + "transition-delay"] = g + "ms", h[j + "transform"] = "translate(" + f.x + "px," + f.y + "px)"
				}(b.effects.transform || b.effects.opacity) && b._bindTargetDone(e), b._ff && b._ff <= 20 ? e.css(h) : e.css(b.effects.transition).css(h)
			}
			for (var c = 0; c < b._$pre.length; c++) {
				var d = b._$pre[c]
					, e = a(d)
					, f = {
					x: d.dataset.finalPosX - d.dataset.interPosX
					, y: d.dataset.finalPosY - d.dataset.interPosY
				}
					, g = b._getDelay(c);
				(d.dataset.finalPosX !== d.dataset.origPosX || d.dataset.finalPosY !== d.dataset.origPosY) && b._bindTargetDone(e), e.css(b._getPrefixedCSS("transition", "all " + b.animation.duration + "ms " + b.animation.easing + " " + g + "ms")), e.css(b._getPrefixedCSS("transform", "translate(" + f.x + "px," + f.y + "px)")), b.animation.animateResizeTargets && (d.dataset.origWidth - d.dataset.finalWidth && 1 * d.dataset.finalWidth && (d.style.width = d.dataset.finalWidth + "px", d.style.marginRight = -(d.dataset.finalWidth - d.dataset.interWidth) + 1 * d.dataset.finalMarginRight + "px"), d.dataset.origHeight - d.dataset.finalHeight && 1 * d.dataset.finalHeight && (d.style.height = d.dataset.finalHeight + "px", d.style.marginBottom = -(d.dataset.finalHeight - d.dataset.interHeight) + 1 * d.dataset.finalMarginBottom + "px"))
			}
			b._changingClass && b._$container.removeClass(b.layout.containerClass).addClass(b._newClass);
			for (var c = 0; c < b._$toHide.length; c++) {
				for (var d = b._$toHide[c], e = a(d), g = b._getDelay(c), k = {}, i = 0; 2 > i; i++) {
					var j = 0 === i ? j = b._prefix : "";
					k[j + "transition-delay"] = g + "ms", k[j + "transform"] = b.effects.transformOut, k.opacity = b.effects.opacity
				}
				e.css(b.effects.transition).css(k), (b.effects.transform || b.effects.opacity) && b._bindTargetDone(e)
			}
			b._execAction("_animateTargets", 1)
		}
		, _bindTargetDone: function (b) {
			var c = this
				, d = b[0];
			c._execAction("_bindTargetDone", 0, arguments), d.dataset.bound || (d.dataset.bound = !0, c._targetsBound++, b.on("webkitTransitionEnd.mixItUp transitionend.mixItUp", function (e) {
				(e.originalEvent.propertyName.indexOf("transform") > -1 || e.originalEvent.propertyName.indexOf("opacity") > -1) && a(e.originalEvent.target).is(c.selectors.target) && (b.off(".mixItUp"), delete d.dataset.bound, c._targetDone())
			})), c._execAction("_bindTargetDone", 1, arguments)
		}
		, _targetDone: function () {
			var a = this;
			a._execAction("_targetDone", 0), a._targetsDone++, a._targetsDone === a._targetsBound && a._cleanUp(), a._execAction("_targetDone", 1)
		}
		, _cleanUp: function () {
			var b = this
				, c = b.animation.animateResizeTargets ? "transform opacity width height margin-bottom margin-right" : "transform opacity";
			unBrake = function () {
				b._$targets.removeStyle("transition", b._prefix)
			}, b._execAction("_cleanUp", 0), b._changingLayout ? b._$show.css("display", b._newDisplay) : b._$show.css("display", b.layout.display), b._$targets.css(b._brake), b._$targets.removeStyle(c, b._prefix).removeAttr("data-inter-pos-x data-inter-pos-y data-final-pos-x data-final-pos-y data-orig-pos-x data-orig-pos-y data-orig-height data-orig-width data-final-height data-final-width data-inter-width data-inter-height data-orig-margin-right data-orig-margin-bottom data-inter-margin-right data-inter-margin-bottom data-final-margin-right data-final-margin-bottom"), b._$hide.removeStyle("display"), b._$parent.removeStyle("height transition perspective-distance perspective perspective-origin-x perspective-origin-y perspective-origin perspectiveOrigin", b._prefix), b._sorting && (b._printSort(), b._activeSort = b._newSortString, b._sorting = !1), b._changingLayout && (b._changingDisplay && (b.layout.display = b._newDisplay, b._changingDisplay = !1), b._changingClass && (b._$parent.removeClass(b.layout.containerClass).addClass(b._newClass), b.layout.containerClass = b._newClass, b._changingClass = !1), b._changingLayout = !1), b._refresh(), b._buildState(), b._state.fail && b._$container.addClass(b.layout.containerClassFail), b._$show = a(), b._$hide = a(), window.requestAnimationFrame && requestAnimationFrame(unBrake), b._mixing = !1, "function" == typeof b.callbacks._user && b.callbacks._user.call(b._domNode, b._state, b), "function" == typeof b.callbacks.onMixEnd && b.callbacks.onMixEnd.call(b._domNode, b._state, b), b._$container.trigger("mixEnd", [b._state, b]), b._state.fail && ("function" == typeof b.callbacks.onMixFail && b.callbacks.onMixFail.call(b._domNode, b._state, b), b._$container.trigger("mixFail", [b._state, b])), b._loading && ("function" == typeof b.callbacks.onMixLoad && b.callbacks.onMixLoad.call(b._domNode, b._state, b), b._$container.trigger("mixLoad", [b._state, b])), b._queue.length && (b._execAction("_queue", 0), b.multiMix(b._queue[0][0], b._queue[0][1], b._queue[0][2]), b._queue.splice(0, 1)), b._execAction("_cleanUp", 1), b._loading = !1
		}
		, _getPrefixedCSS: function (a, b, c) {
			var d = this
				, e = {};
			for (i = 0; 2 > i; i++) {
				var f = 0 === i ? d._prefix : "";
				e[f + a] = c ? f + b : b
			}
			return d._execFilter("_getPrefixedCSS", e, arguments)
		}
		, _getDelay: function (a) {
			var b = this
				, c = "function" == typeof b.animation.staggerSequence ? b.animation.staggerSequence.call(b._domNode, a, b._state) : a
				, d = b.animation.stagger ? c * b.animation.staggerDuration : 0;
			return b._execFilter("_getDelay", d, arguments)
		}
		, _parseMultiMixArgs: function (a) {
			for (var b = this, c = {
				command: null
				, animate: b.animation.enable
				, callback: null
			}, d = 0; d < a.length; d++) {
				var e = a[d];
				null !== e && ("object" == typeof e || "string" == typeof e ? c.command = e : "boolean" == typeof e ? c.animate = e : "function" == typeof e && (c.callback = e))
			}
			return b._execFilter("_parseMultiMixArgs", c, arguments)
		}
		, _parseInsertArgs: function (b) {
			for (var c = this, d = {
				index: 0
				, $object: a()
				, multiMix: {
					filter: c._state.activeFilter
				}
				, callback: null
			}, e = 0; e < b.length; e++) {
				var f = b[e];
				"number" == typeof f ? d.index = f : "object" == typeof f && f instanceof a ? d.$object = f : "object" == typeof f && c._helpers._isElement(f) ? d.$object = a(f) : "object" == typeof f && null !== f ? d.multiMix = f : "boolean" != typeof f || f ? "function" == typeof f && (d.callback = f) : d.multiMix = !1
			}
			return c._execFilter("_parseInsertArgs", d, arguments)
		}
		, _execAction: function (a, b, c) {
			var d = this
				, e = b ? "post" : "pre";
			if (!d._actions.isEmptyObject && d._actions.hasOwnProperty(a))
				for (var f in d._actions[a][e]) d._actions[a][e][f].call(d, c)
		}
		, _execFilter: function (a, b, c) {
			var d = this;
			if (d._filters.isEmptyObject || !d._filters.hasOwnProperty(a)) return b;
			for (var e in d._filters[a]) return d._filters[a][e].call(d, c)
		}
		, _helpers: {
			_camelCase: function (a) {
				return a.replace(/-([a-z])/g, function (a) {
					return a[1].toUpperCase()
				})
			}
			, _isElement: function (a) {
				return window.HTMLElement ? a instanceof HTMLElement : null !== a && 1 === a.nodeType && "string" === a.nodeName
			}
		}
		, isMixing: function () {
			var a = this;
			return a._execFilter("isMixing", a._mixing)
		}
		, filter: function () {
			var a = this
				, b = a._parseMultiMixArgs(arguments);
			a._clicking && (a._toggleString = ""), a.multiMix({
				filter: b.command
			}, b.animate, b.callback)
		}
		, sort: function () {
			var a = this
				, b = a._parseMultiMixArgs(arguments);
			a.multiMix({
				sort: b.command
			}, b.animate, b.callback)
		}
		, changeLayout: function () {
			var a = this
				, b = a._parseMultiMixArgs(arguments);
			a.multiMix({
				changeLayout: b.command
			}, b.animate, b.callback)
		}
		, multiMix: function () {
			var a = this
				, c = a._parseMultiMixArgs(arguments);
			if (a._execAction("multiMix", 0, arguments), a._mixing) a.animation.queue && a._queue.length < a.animation.queueLimit ? (a._queue.push(arguments), a.controls.enable && !a._clicking && a._updateControls(c.command), a._execAction("multiMixQueue", 1, arguments)) : ("function" == typeof a.callbacks.onMixBusy && a.callbacks.onMixBusy.call(a._domNode, a._state, a), a._$container.trigger("mixBusy", [a._state, a]), a._execAction("multiMixBusy", 1, arguments));
			else {
				a.controls.enable && !a._clicking && (a.controls.toggleFilterButtons && a._buildToggleArray(), a._updateControls(c.command, a.controls.toggleFilterButtons)), a._queue.length < 2 && (a._clicking = !1), delete a.callbacks._user, c.callback && (a.callbacks._user = c.callback);
				var d = c.command.sort
					, e = c.command.filter
					, f = c.command.changeLayout;
				a._refresh(), d && (a._newSort = a._parseSort(d), a._newSortString = d, a._sorting = !0, a._sort()), e !== b && (e = "all" === e ? a.selectors.target : e, a._activeFilter = e), a._filter(), f && (a._newDisplay = "string" == typeof f ? f : f.display || a.layout.display, a._newClass = f.containerClass || "", (a._newDisplay !== a.layout.display || a._newClass !== a.layout.containerClass) && (a._changingLayout = !0, a._changingClass = a._newClass !== a.layout.containerClass, a._changingDisplay = a._newDisplay !== a.layout.display)), a._$targets.css(a._brake), a._goMix(c.animate ^ a.animation.enable ? c.animate : a.animation.enable), a._execAction("multiMix", 1, arguments)
			}
		}
		, insert: function () {
			var a = this
				, b = a._parseInsertArgs(arguments)
				, c = "function" == typeof b.callback ? b.callback : null
				, d = document.createDocumentFragment()
				, e = function () {
				return a._refresh(), a._$targets.length ? b.index < a._$targets.length || !a._$targets.length ? a._$targets[b.index] : a._$targets[a._$targets.length - 1].nextElementSibling : a._$parent[0].children[0]
			}();
			if (a._execAction("insert", 0, arguments), b.$object) {
				for (var f = 0; f < b.$object.length; f++) {
					var g = b.$object[f];
					d.appendChild(g), d.appendChild(document.createTextNode(" "))
				}
				a._$parent[0].insertBefore(d, e)
			}
			a._execAction("insert", 1, arguments), "object" == typeof b.multiMix && a.multiMix(b.multiMix, c)
		}
		, prepend: function () {
			var a = this
				, b = a._parseInsertArgs(arguments);
			a.insert(0, b.$object, b.multiMix, b.callback)
		}
		, append: function () {
			var a = this
				, b = a._parseInsertArgs(arguments);
			a.insert(a._state.totalTargets, b.$object, b.multiMix, b.callback)
		}
		, getOption: function (a) {
			var c = this
				, d = function (a, c) {
				for (var d = c.split("."), e = d.pop(), f = d.length, g = 1, h = d[0] || c;
					 (a = a[h]) && f > g;) h = d[g], g++;
				return a !== b ? a[e] !== b ? a[e] : a : void 0
			};
			return a ? c._execFilter("getOption", d(c, a), arguments) : c
		}
		, setOptions: function (b) {
			var c = this;
			c._execAction("setOptions", 0, arguments), "object" == typeof b && a.extend(!0, c, b), c._execAction("setOptions", 1, arguments)
		}
		, getState: function () {
			var a = this;
			return a._execFilter("getState", a._state, a)
		}
		, forceRefresh: function () {
			var a = this;
			a._refresh(!1, !0)
		}
		, destroy: function (b) {
			var c = this;
			c._execAction("destroy", 0, arguments), c._$body.add(a(c.selectors.sort)).add(a(c.selectors.filter)).off(".mixItUp");
			for (var d = 0; d < c._$targets.length; d++) {
				var e = c._$targets[d];
				b && (e.style.display = ""), delete e.mixParent
			}
			c._execAction("destroy", 1, arguments), delete a.MixItUp.prototype._instances[c._id]
		}
	}, a.fn.mixItUp = function () {
		var c, d = arguments
			, e = []
			, f = function (b, c) {
			var d = new a.MixItUp
				, e = function () {
				return ("00000" + (16777216 * Math.random() << 0).toString(16)).substr(-6).toUpperCase()
			};
			d._execAction("_instantiate", 0, arguments), b.id = b.id ? b.id : "MixItUp" + e(), d._instances[b.id] || (d._instances[b.id] = d, d._init(b, c)), d._execAction("_instantiate", 1, arguments)
		};
		return c = this.each(function () {
			if (d && "string" == typeof d[0]) {
				var c = a.MixItUp.prototype._instances[this.id];
				if ("isLoaded" == d[0]) e.push(c ? !0 : !1);
				else {
					var g = c[d[0]](d[1], d[2], d[3]);
					g !== b && e.push(g)
				}
			} else f(this, d[0])
		}), e.length ? e.length > 1 ? e : e[0] : c
	}, a.fn.removeStyle = function (a, c) {
		return c = c ? c : "", this.each(function () {
			for (var d = this, e = a.split(" "), f = 0; f < e.length; f++)
				for (var g = 0; 2 > g; g++) {
					var h = g ? e[f] : c + e[f];
					if (d.style[h] !== b && "unknown" != typeof d.style[h] && d.style[h].length > 0 && (d.style[h] = ""), !c) break
				}
			d.attributes && d.attributes.style && d.attributes.style !== b && "" === d.attributes.style.value && d.attributes.removeNamedItem("style")
		})
	}
}(jQuery);


/*from footer*/
jQuery(document).ready(function ($) {

	google.maps.event.addDomListener(window, 'load', init);

	function init() {
		// Basic options for a simple Google Map
		// For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
		var mapOptions = {
			// How zoomed in you want the map to start at (always required)
			zoom: 3
			, // The latitude and longitude to center the map (always required)
			center: new google.maps.LatLng(15.772183, 24.855237)
			, mapTypeId: google.maps.MapTypeId.ROADMAP
			, scrollwheel: false,

			// How you would like to style the map.
			// This is where you would paste any style found on Snazzy Maps.
			styles: [{
				"featureType": "water"
				, "elementType": "geometry"
				, "stylers": [{
					"color": "#ffffff"
				}]
			}, {
				"featureType": "landscape"
				, "elementType": "geometry"
				, "stylers": [{
					"color": "#cccccc"
				}]
			}, {
				"featureType": "poi"
				, "elementType": "geometry"
				, "stylers": [{
					"color": "#cccccc"
				}]
			}, {
				"featureType": "road.highway"
				, "elementType": "geometry.fill"
				, "stylers": [{
					"color": "#e4e4e4"
				}, {
					"lightness": -10
				}]
			}, {
				"featureType": "road.highway"
				, "elementType": "geometry.stroke"
				, "stylers": [{
					"color": "#e4e4e4"
				}]
			}, {
				"featureType": "road.arterial"
				, "elementType": "geometry.fill"
				, "stylers": [{
					"color": "#cccccc"
				}]
			}, {
				"featureType": "road.arterial"
				, "elementType": "geometry.stroke"
				, "stylers": [{
					"color": "#e4e4e4"
				}]
			}, {
				"featureType": "road.local"
				, "elementType": "geometry.fill"
				, "stylers": [{
					"color": "#cccccc"
				}]
			}, {
				"elementType": "labels.text.fill"
				, "stylers": [{
					"color": "#000000"
				}]
			}, {
				"elementType": "labels.text.stroke"
				, "stylers": [{
					"color": "#ffffff"
				}, {
					"lightness": -8
				}]
			}, {
				"featureType": "transit"
				, "elementType": "geometry"
				, "stylers": [{
					"color": "#999999"
				}, {
					"visibility": "on"
				}]
			}, {
				"featureType": "administrative"
				, "elementType": "geometry"
				, "stylers": [{
					"color": "#ffffff"
				}, {
					"weight": 0.4
				}]
			}, {
				"featureType": "road.local"
				, "elementType": "geometry.stroke"
				, "stylers": [{
					"visibility": "off"
				}]
			}]
		};

		// Get the HTML DOM element that will contain your map
		// We are using a div with id="map" seen below in the <body>
		var mapElement = document.getElementById('map');

		// Create the Google Map using our element and options defined above
		var map = new google.maps.Map(mapElement, mapOptions);

		// Let's also add a marker while we're at it
		var marker_path  = "http://alscon-web.com/wp-content/themes/alscon/images/marker-min.png";
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(48.221738, 16.372220), //Austria
			map: map
			, title: 'Smart Viena, Alscon Team'
			, icon: marker_path
			, url: 'http://smartvienna.com/',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(53.333322, 83.5971963), //Barnaul
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(13.7248946, 100.4930265), //Bangkok
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(-29.1220341, 26.1015308), //Bloemfontein
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(-34.6156625, -58.5033599), //Buenos Aires
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(55.6713442, 12.4907996), //Copenhagen
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(48.4624412, 34.8602743), //Dnepropetrovsk
			map: map
			, title: 'Home Space Today, Alscon Team'
			, icon: marker_path
			, url: 'http://homespace.today/',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(55.4103099, 37.9002626), //Domodedovo
			map: map
			, title: 'STM, Alscon Team'
			, icon: marker_path
			, url: 'http://stm-m.ru/',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(25.1179593, 54.9750664), //Dubai
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(23.7807777, 90.3492859), //Dacca
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(52.2984716, 104.1971187), //Irkutsk
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(56.8140005, 60.5148523), //Ekaterinburg
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(40.1535005, 44.4185274), //Yerevan
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(46.2050836, 6.1090692), //Geneva
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(23.1259819, 112.9476595), //Guangzhou
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(56.026842, 92.7256527), //Krasnoyarsk
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(50.457789, 30.515689), //Kyiv
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(6.5485419, 3.2574157), //Lagos
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(51.5287718, -0.2416804), //London
			map: map
			, title: 'Lime Tree Design, Alscon Team'
			, icon: marker_path
			, url: 'http://limetree.innuodesign.co.uk/',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(-20.2005156, 56.5543536), //Mauritius
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(53.8839692, 27.5249347), //Minsk
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(42.641207, 19.122372), //Montenegro
			map: map
			, title: 'Prime Vision Group, Alscon Team'
			, icon: marker_path
			, url: 'http://primevisiongroup.com/',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(55.7498598, 37.3523218), //Moscow
			map: map
			, title: 'Laser Hair Removal Machine, Alscon Team'
			, icon: marker_path
			, url: 'http://www.innovatione.com/',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(48.155004, 11.4717963), //Munich
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(40.8537158, 14.1729672), //Naples
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(43.7032472, 7.2177977), //Nice
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(-36.860743, 174.779632), //New Zelend
			map: map
			, title: 'Life Coach Blog, Alscon Team'
			, icon: marker_path
			, url: 'http://yelenakostyugova.com',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(46.460123, 30.5717051), //Odessa
			map: map
			, title: 'ADVANCE LOGISTICS, Alscon Team'
			, icon: marker_path
			, url: 'http://advlog.pro/',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(59.8939529, 10.6450358), //Oslo
			map: map
			, title: 'Bara Eiendom, Alscon Team'
			, icon: marker_path
			, url: 'http://baraeiendom.no/',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(39.9390731, 116.1172781), //Peking
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(45.5425913, -122.7945045), //Portland
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#'
			, });
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(50.0598058, 14.3255419), //Prague
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(47.2611562, 39.4879177), //Rostov-on-Don
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(29.4816561, -98.6544884), //San Antonio
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(59.9174925, 30.0448875), //St. Petersburg
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(7.8784551, 79.5790936), //Sri Lanka
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(36.4085253, -122.7106231), //San Jose
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(45.0451233, 41.9260474), //Stavropol
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(59.326242, 17.8474646), //Stockholm
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(-33.7960362, 150.6422532), //Sydney
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(25.0855451, 121.4932092), //Taipei
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(32.0879585, 34.7622266), //Tel Aviv
			map: map
			, title: 'Real Estate Website, Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(48.3214409, 25.8638791), //Chernovtsy
			map: map
			, title: 'Real Estate, Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(54.8088967, 55.8807922), //Ufa
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(32.1459851, 34.8740364), //Hod Hasharon
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(52.3797505, 9.6914322), //Hanover
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(47.3775499, 8.4666755), //Zurich
			map: map
			, title: 'Alscon Team'
			, icon: marker_path
			, url: '#',

		});

		google.maps.event.addListener(marker, 'click', function () {
			window.location.href = this.url;
		});

	}

	if ($('.da-thumbs').length) {
		$('.da-thumbs li').each(function () {
			var _this = $(this);
			var bgHover = _this.find('div').attr('data-bg');
			var colorInner = _this.find('div').attr('data-colorInner');

			_this.find('div').css({
				backgroundColor: bgHover
			});

			_this.find('div > *').css({
				color: colorInner
			});
			_this.find('span').css({
				borderColor: colorInner
			})

		})
	}
	//MODAL
	$('.modal').hide();
	$('#contact_us_but').click(function () {
		$('.modal').show();
		$('body').css('overflow', 'hidden');
		$('.modal').css('background', 'rgba(0,0,0,0.9)');
	});

	$('.close').click(function () {
		$('.modal').hide();
		$('body').css('overflow', 'auto');
	});

	//HIDE MODAL CLICK BODY
	$(document).mouseup(function (e) {
		var folder = $(".modal ");

		if (!folder.is(e.target) && folder.has(e.target).length === 0) {
			folder.hide();
			$('body').css('overflow', 'auto');
		}
	});
	$(function () {
		$(' #da-thumbs > li ').each(function () {
			$(this).hoverdir();
		});
	});

	var burgerMenu = 1;
	$('.fa-bars.burger-menu').click(function () {
		if (burgerMenu) {
			$('.menu').show();
			$('html').addClass('active')
			$('.burger-menu').addClass('fa-times').removeClass('fa-bars');
			jQuery('.home-page  header .col-lg-12').css('opacity', 1);
			open_menu_flag = true;
			burgerMenu = 0;

			$('.menu li a').click(function () {
				$('.menu').hide();
				$('html').removeClass('active')
				$('.burger-menu').removeClass('fa-times').addClass('fa-bars');
				open_menu_flag = false;
				setOpacity('.home-page  header .col-lg-12');
			});
		} else {
			burgerMenu = 1;
			$('.menu').hide();
			$('html').removeClass('active')
			$('.burger-menu').removeClass('fa-times').addClass('fa-bars');
			open_menu_flag = false;
			setOpacity('.home-page  header .col-lg-12');

		}
	});

	$('.next-arrow, .arrow-next').on("click", function () {

	});


	$('.next-arrow, .arrow-next').trigger('click');

});