<?php
if ( ! function_exists( 'alscon_style' ) ) :
    function alscon_style(){
//        wp_enqueue_style( 'al_animate', get_template_directory_uri() .'/css/animate.min.css', true );
//        wp_enqueue_style( 'al_bootstrap', get_template_directory_uri() .'/css/bootstrap.min.css', true );
//        wp_enqueue_style( 'al_bootstrap_grid', get_template_directory_uri() .'/css/bootstrap-grid-3.3.1.min.css', true );
//        wp_enqueue_style( 'al_bootstrap_theme', get_template_directory_uri() .'/css/bootstrap-theme.min.css', true );
//        wp_enqueue_style( 'al_font_awesome', get_template_directory_uri() .'/css/font-awesome.min.css', true );
//        wp_enqueue_style( 'al_fonts', get_template_directory_uri() .'/css/fonts.css', true );
//        wp_enqueue_style( 'al_ico', get_template_directory_uri() .'/css/ico.css', true );
//        wp_enqueue_style( 'al_fancybox', get_template_directory_uri() .'/css/jquery.fancybox.css', true );
//        wp_enqueue_style( 'al_main', get_template_directory_uri() .'/css/main.css', true );
//        wp_enqueue_style( 'al_style', get_template_directory_uri() .'/css/style.css', true );
//        wp_enqueue_style( 'al_slick', get_template_directory_uri() .'/css/slick.css', true );
//        wp_enqueue_style( 'al_slick_theme', get_template_directory_uri() .'/css/slick-theme.css', true );
//        wp_enqueue_style( 'al_media', get_template_directory_uri() .'/css/media.css', true );
          wp_enqueue_style( 'all_css', get_template_directory_uri() .'/new_css/all.css', true );
//        wp_enqueue_style( 'al_googleapis', 'https://fonts.googleapis.com/css?family=Raleway:400,800', true );
    }
    add_action( 'wp_enqueue_scripts', 'alscon_style' );
endif;

if ( ! function_exists( 'alscon_scripts' ) ) :
    function alscon_scripts(){
        wp_deregister_script('jquery');
        wp_register_script('jquery', 'http://js.alscon-web.com/js/jquery-1-11-0-custom.js', array(), '', true);//was in header
        if (!is_admin()) {
            wp_enqueue_script('jquery');
        }
        if(is_single()){
            wp_enqueue_script( 'al_single', 'http://js.alscon-web.com/js/single-page.js',false, '', true);
        }
        if(is_page_template( 'template-portfolio.php') ){
            wp_enqueue_script( 'al_portfolio', 'http://js.alscon-web.com/js/portfolio-page.js',false, '', true);
        }
        if(is_page_template('template-main.php')){

//              wp_enqueue_script('al_main_page', 'http://js.alscon-web.com/js/main-page.js' , false, null, true);
            wp_enqueue_script('al_main_page', 'http://js.alscon-web.com/js/main-page.min.js' , false, null, true);

        }
        wp_enqueue_script( 'al_map', 'https://maps.googleapis.com/maps/api/js?sensor=false',false, '', true);
    }
    add_action( 'wp_enqueue_scripts', 'alscon_scripts' );
endif;
function als_stylesheet(){
    wp_enqueue_style("al_for_admin",get_template_directory_uri() ."/css/for-admin.css");
}
add_action('admin_head', 'als_stylesheet');