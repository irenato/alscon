<!DOCTYPE html>
<html <?php language_attributes(); ?> >
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="title" content="Alscon team ">

    <title><?php bloginfo('name'). wp_title('|')?></title>
    <?php wp_head(); ?>

</head>
<body>
<?php if( is_page_template( 'template-main.php' )){?>
<section class="mobil-block">
	<div class="wrapper">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class='video-play' onclick='document.getElementById("bg-vertical-video").play();'></div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php } ?>
<!--START HEADER-->
<div class="<?php
    if( is_page_template( 'template-main.php' )){echo "home-page";}
    if(is_page_template( 'template-portfolio.php' )){ echo'portfolio'; }
    if(is_single()){ echo'portfolio'; }?>">
   <?php if ($_SERVER['REMOTE_ADDR'] == '178.151.21.147') { ?>
    <header class="header size <?php if( is_page_template( 'template-main.php' )){echo 'scroll';} ?>" id="<?php if(is_page_template( 'template-main.php' )){echo 'scroll-top';} ?>" data-anchor="<?php if( is_page_template( 'template-main.php' )){echo 'scroll-top';} ?>">
        <?php if( is_page_template( 'template-main.php' ) ){
      echo '<video autoplay loop muted poster="" id="bg-video" >
            	<source src="'. esc_url(get_theme_mod("themeslug_video")) .'" type="video/mp4">
            </video>'
		  
           .'<video autoplay muted loop webkit-playsinline poster="" id="bg-vertical-video" >
            	<source src="'. esc_url(get_theme_mod("themeslug_mobile_vert")) .'">
            </video>'
           .'<video autoplay muted loop webkit-playsinline poster="" id="bg-gorizontal-video" >
            	<source src="'. esc_url(get_theme_mod("themeslug_mobile_goriz")) .'">
            </video>';
			
        } ?>
       
        <div class="wrapper size">

            <i class="fa fa-bars burger-menu" aria-hidden="true"></i>
            <?php if( is_page_template( 'template-main.php' ) ){ ?>
                <div class="logo-mobile">
                    <a href="<?= get_home_url() ?>"><img src="<?= esc_url(get_theme_mod('themeslug_logo')); ?>"alt="<?= esc_attr(get_bloginfo('name', 'display')); ?>"></a>
                </div>
            <?php }  ?>

            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 size">

                        <?php if( is_page_template( 'template-main.php' ) ){
                            if (get_theme_mod('themeslug_logo')) : ?>
                                <div class="logo">
                                    <a href="<?= get_home_url() ?>"><img src="<?= esc_url(get_theme_mod('themeslug_logo')); ?>"alt="<?= esc_attr(get_bloginfo('name', 'display')); ?>"></a>
                                </div>
                            <?php endif; ?>

                            <?php  echo main_site_menu('main_menu'); }
//                    if(is_page_template( 'template-portfolio.php' ))
                        else
                        {
                            if (get_theme_mod('themeslug_logo_color')) : ?>
                                <div class="logo">
                                    <a href="<?= get_home_url() ?>"><img src="<?= esc_url(get_theme_mod('themeslug_logo_color')); ?>"alt="<?= esc_attr(get_bloginfo('name', 'display')); ?>"></a>
                                </div>
                            <?php endif; ?>
                            <?php  echo main_site_menu('sub_menu'); ?>
                        <?php } // End header image check. ?>
                        <div class="write-side">
                            <div class="language">
                                <div class="language-menu">
                                    <?php icl_post_languages() ?>
                                </div>
                            </div>

                            <div class="contact-us">
                                <a id="contact_us_but" href="#"  class="button"><?php _e('Contact us','buttons')?></a>

                                <ul>
                                    <li><a href="<?= get_option("freelancer_link"); ?>"><img src="<?php if(is_page_template( 'template-main.php' )){ echo get_template_directory_uri() .'/images/freelancer.png';}else{ echo get_template_directory_uri().'/images/freelancer.com.png'; } ?>"
                                                                                             target="_blank" alt="#" ></a></li>
                                    <li><a href="<?= get_option("upwork_link"); ?>"><img src="<?php if(is_page_template( 'template-main.php' )){ echo get_template_directory_uri() .'/images/upwork.png'; } else { echo get_template_directory_uri() .'/images/upwork-2.png'; } ?>"
                                                                                         alt="#" target="_blank"></a></li>
                                </ul>

                            </div>
                        </div>
                        <?php if( is_page_template( 'template-main.php' )){ ?>
                            <div class="button-main">
                                <div class="slide-button">
                                    <!--                        <h1>-->
                                    <? //= get_field('site_slogan', get_the_ID())?><!--</h1>-->
                                    <?php if (ICL_LANGUAGE_CODE == 'en') { ?>
                                        <a href="<?= get_permalink(1746); ?>" class="button">
                                            <svg version="1.1" id="sloy" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 viewBox="-547.9 225.3 248.5 62.8" style="enable-background:new -547.9 225.3 248.5 62.8;" xml:space="preserve">
										<style type="text/css">
                                            .st0{fill:#fff;}
                                            .header .slide-button .button:hover .st0{fill:#d4574a; stroke: #d4574a;}
                                        </style>
                                                <g>
                                                    <g>
                                                        <path class="st0" d="M-494.7,247.2h-0.3v17.3h4.5v-3.1C-491.9,256.7-493.2,251.9-494.7,247.2z"/>
                                                        <path class="st0" d="M-490.5,258.7h4.2c3.9,0,6-2.6,6-5.7c0-3.1-2.1-5.8-6-5.8h-8.4c1.5,4.7,2.8,9.5,4.2,14.2V258.7z M-490.5,251.1h3.6c1.2,0,2.1,0.7,2.1,1.9c0,1.2-0.9,1.8-2.1,1.8h-3.6V251.1z"/>
                                                        <path class="st0" d="M-469.9,246.9c-0.6,0-1.3,0.1-1.9,0.2c0,1.4,0,2.8,0.1,4.1c0.5-0.2,1.1-0.3,1.8-0.3c2.9,0,4.7,2.2,4.7,5 c0,2.8-1.8,5-4.7,5c-0.6,0-1.1-0.1-1.6-0.3c0,1.4,0.1,2.7,0.1,4.1c0.5,0.1,1,0.1,1.5,0.1c5.3,0,9.3-3.7,9.3-9 C-460.6,250.6-464.6,246.9-469.9,246.9z"/>
                                                        <path class="st0" d="M-471.5,260.6c-1.9-0.7-3.1-2.5-3.1-4.7c0-2.2,1.1-4,2.9-4.7c0-1.4-0.1-2.8-0.1-4.1c-4.3,0.8-7.4,4.2-7.4,8.8 c0,4.8,3.2,8.2,7.8,8.9C-471.4,263.4-471.5,262-471.5,260.6z"/>
                                                        <path class="st0" d="M-453.9,254.8v-3.7h3.1c0.2-1.3,0.4-2.6,0.6-3.9h-8.2v17.3h4.5v-5.8h1.9c0.2-1.3,0.4-2.6,0.6-3.9H-453.9z"/>
                                                        <path class="st0" d="M-447.1,258.2c1.6-0.6,3.5-2.2,3.5-5.2c0-3.2-2.2-5.8-6-5.8h-0.5c-0.2,1.3-0.4,2.6-0.6,3.9h0.4 c1.2,0,2.2,0.7,2.2,1.8c0,1.2-1,1.9-2.2,1.9h-1c-0.2,1.3-0.4,2.6-0.6,3.9h0.4l2.9,5.8h5.1L-447.1,258.2z"/>
                                                        <path class="st0" d="M-432.9,251.1h4.8v-3.9h-5.5c0.2,1.5,0.5,2.9,0.7,4.4V251.1z"/>
                                                        <path class="st0" d="M-437.3,264.6h4.5v-12.9c-0.2-1.5-0.5-2.9-0.7-4.4h-8.7v3.9h4.9V264.6z"/>
                                                        <path class="st0" d="M-425.9,264.6h4.5v-6.8h8.1v-3.9h-7.2c-1.8,3.2-3.5,6.3-5.3,9.5V264.6z"/>
                                                        <path class="st0" d="M-425.9,247.2v16.1c1.8-3.1,3.5-6.3,5.3-9.5h-0.8v-2.7h2.3c0.7-1.3,1.4-2.6,2.2-3.9H-425.9z"/>
                                                        <path class="st0" d="M-419.1,251.1h5.9v-3.9h-3.8C-417.7,248.5-418.4,249.8-419.1,251.1z"/>
                                                        <path class="st0" d="M-402.4,246.9c-2.2,0-4.1,0.6-5.6,1.7c1,1,2,2,3,3c0.7-0.5,1.6-0.8,2.6-0.8c2.9,0,4.7,2.2,4.7,5 c0,0.9-0.2,1.7-0.5,2.4c1.1,1,2.2,2.1,3.3,3.1c1.2-1.5,1.8-3.4,1.8-5.6C-393.1,250.6-397.1,246.9-402.4,246.9z"/>
                                                        <path class="st0" d="M-402.4,260.9c-2.9,0-4.7-2.2-4.7-5c0-1.8,0.8-3.4,2.1-4.3c-1-1-2-2-3-3c-2.2,1.6-3.6,4.1-3.6,7.3 c0,5.3,4,9,9.3,9c3.1,0,5.8-1.3,7.4-3.4c-1.1-1-2.2-2.1-3.3-3.1C-398.9,259.9-400.4,260.9-402.4,260.9z"/>
                                                        <path class="st0" d="M-379.4,264.6v-3.5c-1.5,1.2-3,2.3-4.5,3.5H-379.4z"/>
                                                        <path class="st0" d="M-386.4,260.7v-13.4h-4.5v17.3h7c1.5-1.2,3-2.3,4.5-3.5v-0.4H-386.4z"/>
                                                        <path class="st0" d="M-377.1,264.6h4.5v-8.9c-1.5,1.2-3,2.4-4.5,3.6V264.6z"/>
                                                        <path class="st0" d="M-377.1,259.2c1.5-1.2,3-2.4,4.5-3.6v-8.4h-4.5V259.2z"/>
                                                        <path class="st0" d="M-370.1,253.6c2.7-2.2,5.4-4.4,8.1-6.7C-366.1,247.3-369.2,249.9-370.1,253.6z"/>
                                                        <path class="st0" d="M-361.1,246.9c-0.3,0-0.6,0-0.9,0c-2.7,2.2-5.4,4.5-8.1,6.7c-0.2,0.7-0.3,1.5-0.3,2.3c0,5.3,4,9,9.3,9 c5.3,0,9.3-3.7,9.3-9C-351.8,250.6-355.8,246.9-361.1,246.9z M-361.1,260.9c-2.9,0-4.7-2.2-4.7-5c0-2.8,1.8-5,4.7-5 c2.9,0,4.7,2.2,4.7,5C-356.4,258.7-358.2,260.9-361.1,260.9z"/>
                                                    </g>
                                                    <g>
                                                        <path class="st0" d="M-547.9,288.1h4V263c-1.3,0.9-2.7,1.8-4,2.7V288.1z"/>
                                                        <path class="st0" d="M-547.9,265.7c1.3-0.9,2.7-1.8,4-2.7v-17c-1.3-1.3-2.6-2.5-4-3.7V265.7z"/>
                                                        <path class="st0" d="M-547.9,227.7v14.6c1.4,1.2,2.7,2.5,4,3.7v-18.3H-547.9z"/>
                                                        <rect x="-303.4" y="227.7" class="st0" width="4" height="60.4"/>
                                                        <path class="st0" d="M-494.5,229.3c1.9-1.3,3.9-2.7,5.8-4h-14.3c0.6,1.3,1.2,2.7,1.8,4H-494.5z"/>
                                                        <path class="st0" d="M-503.1,225.3h-23.5c0.7,1.3,1.4,2.7,2.1,4h23.2C-501.9,228-502.4,226.7-503.1,225.3z"/>
                                                        <path class="st0" d="M-526.6,225.3h-21.4v4h23.5C-525.2,228-525.9,226.7-526.6,225.3z"/>
                                                        <path class="st0" d="M-488.8,225.3c-1.9,1.3-3.9,2.7-5.8,4h22.7c0-1.3,0-2.7,0.1-4H-488.8z"/>
                                                        <path class="st0" d="M-447.5,225.3h-24.3c0,1.3-0.1,2.7-0.1,4h23.9C-447.8,228-447.6,226.7-447.5,225.3z"/>
                                                        <path class="st0" d="M-299.4,225.3h-36.8c-1.6,1.3-3.2,2.7-4.7,4h41.5V225.3z"/>
                                                        <path class="st0" d="M-429.2,225.3c1.1,1.3,2.2,2.7,3.4,4h18.6c0.7-1.3,1.5-2.7,2.2-4H-429.2z"/>
                                                        <path class="st0" d="M-375,225.3h-30c-0.7,1.3-1.5,2.7-2.2,4h40.3C-369.6,228-372.3,226.6-375,225.3z"/>
                                                        <path class="st0" d="M-340.9,229.3c1.6-1.3,3.2-2.7,4.7-4H-375c2.7,1.3,5.5,2.6,8.2,4H-340.9z"/>
                                                        <path class="st0" d="M-437.2,225.3h-10.2c-0.1,1.3-0.3,2.7-0.4,4h11.4C-436.8,228-437,226.7-437.2,225.3z"/>
                                                        <path class="st0" d="M-425.8,229.3c-1.1-1.3-2.3-2.7-3.4-4h-8.1c0.2,1.3,0.5,2.7,0.7,4H-425.8z"/>
                                                        <path class="st0" d="M-547.9,284.1v4h34.2c-0.9-1.3-1.8-2.7-2.7-4H-547.9z"/>
                                                        <path class="st0" d="M-516.5,284.1c0.9,1.3,1.8,2.7,2.7,4h18.2c-0.6-1.3-1.2-2.7-1.9-4H-516.5z"/>
                                                        <path class="st0" d="M-497.4,284.1c0.6,1.3,1.2,2.7,1.9,4h12.6c-0.4-1.3-0.8-2.7-1.2-4H-497.4z"/>
                                                        <path class="st0" d="M-471.1,284.1h-13.1c0.4,1.3,0.8,2.7,1.2,4h11.9C-471.1,286.8-471.1,285.5-471.1,284.1z"/>
                                                        <path class="st0" d="M-456.1,284.1h-15c0,1.3,0,2.7,0,4h14.4C-456.6,286.8-456.4,285.4-456.1,284.1z"/>
                                                        <path class="st0" d="M-370.2,284.1h-40.3c-2,1.3-3.9,2.7-5.9,4h50.7C-367.2,286.8-368.7,285.5-370.2,284.1z"/>
                                                        <path class="st0" d="M-335.1,288.1h35.7v-4H-329C-331.1,285.4-333.1,286.8-335.1,288.1z"/>
                                                        <path class="st0" d="M-365.7,288.1h30.6c2-1.3,4.1-2.7,6.1-4h-41.2C-368.7,285.5-367.2,286.8-365.7,288.1z"/>
                                                        <path class="st0" d="M-456.1,284.1c-0.2,1.3-0.4,2.7-0.6,4h16.4c0.8-1.3,1.6-2.7,2.4-4H-456.1z"/>
                                                        <path class="st0" d="M-428.1,284.1h-9.8c-0.8,1.3-1.6,2.7-2.4,4h12.8C-427.8,286.8-428,285.5-428.1,284.1z"/>
                                                        <path class="st0" d="M-427.6,288.1h11.2c2-1.3,4-2.7,5.9-4h-17.7C-428,285.5-427.8,286.8-427.6,288.1z"/>
                                                    </g>
                                                </g>
									</svg>
                                        </a>
                                    <?php } else { ?>
                                        <a href="<?= get_permalink(1845); ?>" class="button ru">
                                            <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 viewBox="-278.5 233.2 248.5 62.8" style="enable-background:new -278.5 233.2 248.5 62.8;" xml:space="preserve">
										<style type="text/css">
                                            .st0{fill:#fff;}
                                            .header .slide-button .button:hover .st0{fill:#d4574a; stroke: #d4574a;}
                                        </style>
                                                <g>
                                                    <g>
                                                        <path class="st0" d="M-274.5,235.5h-4v22.6c1.3-0.9,2.7-1.7,4-2.6V235.5z"/>
                                                        <path class="st0" d="M-278.5,280.1v15.9h4v-13C-275.9,282-277.2,281-278.5,280.1z"/>
                                                        <path class="st0" d="M-274.5,255.5c-1.3,0.9-2.7,1.7-4,2.6v21.9c1.3,1,2.6,2,4,2.9V255.5z"/>
                                                        <path class="st0" d="M-34,264.6v31.4h4v-24.1C-31.3,269.5-32.7,267-34,264.6z"/>
                                                        <path class="st0" d="M-30,235.5h-4v29c1.3,2.4,2.7,4.9,4,7.3V235.5z"/>
                                                        <path class="st0" d="M-236.8,233.2h-41.7v4h34.4C-241.7,235.8-239.3,234.5-236.8,233.2z"/>
                                                        <path class="st0" d="M-222,233.2h-14.8c-2.4,1.3-4.9,2.6-7.3,4h21.4C-222.5,235.8-222.3,234.5-222,233.2z"/>
                                                        <path class="st0" d="M-181.7,233.2c0.6,1.3,1.3,2.7,1.9,4h7.7c0.2-1.3,0.4-2.7,0.6-4H-181.7z"/>
                                                        <path class="st0" d="M-150.8,233.2h-20.7c-0.2,1.3-0.4,2.7-0.6,4h15.1C-154.9,235.8-152.9,234.5-150.8,233.2z"/>
                                                        <path class="st0" d="M-207.2,233.2c-0.4,1.3-0.9,2.7-1.4,4h9.4c0.3-1.3,0.7-2.7,1-4H-207.2z"/>
                                                        <path class="st0" d="M-181.7,233.2h-16.5c-0.4,1.3-0.7,2.7-1,4h19.4C-180.4,235.8-181.1,234.5-181.7,233.2z"/>
                                                        <path class="st0" d="M-207.2,233.2H-222c-0.2,1.3-0.5,2.7-0.8,4h14.2C-208.1,235.8-207.7,234.5-207.2,233.2z"/>
                                                        <path class="st0" d="M-150.8,233.2c-2.1,1.3-4.1,2.7-6.1,4h17c-0.1-1.3-0.2-2.7-0.3-4H-150.8z"/>
                                                        <path class="st0" d="M-130.7,233.2h-9.6c0.1,1.3,0.2,2.7,0.3,4h10.3C-130,235.8-130.4,234.5-130.7,233.2z"/>
                                                        <path class="st0" d="M-108.8,237.2c-0.5-1.3-1.1-2.7-1.6-4h-8.8c0.4,1.3,0.8,2.7,1.1,4H-108.8z"/>
                                                        <path class="st0" d="M-119.3,233.2h-11.4c0.3,1.3,0.7,2.7,1,4h11.5C-118.5,235.8-118.9,234.5-119.3,233.2z"/>
                                                        <path class="st0" d="M-79.2,233.2h-31.2c0.5,1.3,1.1,2.7,1.6,4h16.3c1.8-1.1,3.6-2.2,5.5-3.2c-1.9,1-3.7,2.1-5.5,3.2h14.6 C-78.3,235.8-78.8,234.5-79.2,233.2z"/>
                                                        <path class="st0" d="M-62.2,233.2h-17c0.5,1.3,0.9,2.7,1.3,4h17.3c-0.2-0.4-0.3-0.8-0.4-1.2C-61.4,235.1-61.8,234.1-62.2,233.2z"/>
                                                        <path class="st0" d="M-30,237.2v-4h-22.3c0.9,1.3,1.8,2.7,2.7,4H-30z"/>
                                                        <path class="st0" d="M-52.3,233.2h-9.9c0.4,1,0.9,1.9,1.2,2.8c0.2,0.4,0.3,0.8,0.4,1.2h10.9C-50.5,235.8-51.4,234.5-52.3,233.2z"/>
                                                        <path class="st0" d="M-186.2,291.9c-3,1.3-6.1,2.7-9.2,4h15.6c0.1-1.3,0.2-2.7,0.4-4H-186.2z"/>
                                                        <path class="st0" d="M-154.2,291.9h-25.2c-0.1,1.3-0.2,2.7-0.4,4h26.7C-153.4,294.6-153.8,293.3-154.2,291.9z"/>
                                                        <path class="st0" d="M-186.2,291.9h-28.7c-0.2,1.3-0.3,2.7-0.3,4h19.9C-192.3,294.6-189.2,293.3-186.2,291.9z"/>
                                                        <path class="st0" d="M-230,291.9c-0.4,1.3-0.9,2.7-1.3,4h16.1c0-1.3,0.1-2.7,0.3-4H-230z"/>
                                                        <path class="st0" d="M-278.5,291.9v4h47.2c0.4-1.3,0.8-2.7,1.3-4H-278.5z"/>
                                                        <path class="st0" d="M-154.2,291.9c0.4,1.3,0.8,2.7,1.2,4h21.6c-0.1-1.3-0.3-2.7-0.5-4H-154.2z"/>
                                                        <path class="st0" d="M-131.9,291.9c0.3,1.3,0.4,2.7,0.5,4h21.3c-0.5-1.3-1-2.7-1.6-4H-131.9z"/>
                                                        <path class="st0" d="M-111.6,291.9c0.5,1.3,1,2.7,1.6,4h7.9c-0.4-1.3-0.8-2.7-1.2-4H-111.6z"/>
                                                        <path class="st0" d="M-90.2,291.9h-13.2c0.4,1.3,0.8,2.7,1.2,4h13.3C-89.3,294.6-89.8,293.3-90.2,291.9z"/>
                                                        <path class="st0" d="M-90.2,291.9c0.4,1.3,0.9,2.7,1.3,4h21.7c-0.1-1.3-0.3-2.7-0.4-4H-90.2z"/>
                                                        <path class="st0" d="M-67.6,291.9c0.1,1.3,0.3,2.7,0.4,4h22.1c-0.2-1.3-0.5-2.7-0.7-4H-67.6z"/>
                                                        <path class="st0" d="M-45.1,295.9H-30v-4h-15.8C-45.6,293.3-45.3,294.6-45.1,295.9z"/>
                                                    </g>
                                                    <path class="st0" d="M-234,259.1h5.9c0.4-1.3,0.7-2.6,1.1-3.9h-11.5v17.3h4.5V259.1z"/>
                                                    <path class="st0" d="M-222.3,271.3c-0.2,0.4-0.3,0.8-0.5,1.2h0.5V271.3z"/>
                                                    <path class="st0" d="M-222.3,255.2h-4.7c-0.4,1.3-0.7,2.6-1.1,3.9h1.3v13.4h4c0.2-0.4,0.3-0.8,0.5-1.2V255.2z"/>
                                                    <path class="st0" d="M-220.1,263.8c0,0.6,0.1,1.2,0.2,1.7c1.3-3.1,2.6-6.1,3.8-9.2C-218.5,257.9-220.1,260.6-220.1,263.8z"/>
                                                    <path class="st0" d="M-204.2,257.3c-0.6,2.4-1.2,4.7-1.9,7.1c-0.1,0.7-0.3,1.4-0.6,2c-0.6,2.1-1.2,4.1-1.8,6.2c4.1-0.9,7-4.2,7-8.7 C-201.5,261.2-202.5,258.9-204.2,257.3z"/>
                                                    <path class="st0" d="M-210.8,268.9c-2.9,0-4.7-2.2-4.7-5c0-2.8,1.8-5,4.7-5c2.9,0,4.7,2.2,4.7,5c0,0.2,0,0.4,0,0.6 c0.6-2.4,1.3-4.7,1.9-7.1c-1.6-1.5-3.9-2.4-6.5-2.4c-2,0-3.8,0.5-5.3,1.5c-1.3,3.1-2.6,6.2-3.8,9.2c0.8,4.3,4.4,7.3,9.1,7.3 c0.8,0,1.6-0.1,2.3-0.3c0.6-2.1,1.2-4.1,1.8-6.2C-207.4,267.9-208.9,268.9-210.8,268.9z"/>
                                                    <path class="st0" d="M-190.6,255.2h-8.7V267c1.9-1.4,3.8-2.8,5.7-4.2h-1.2v-3.7h3.6c0.7,0,1.3,0.3,1.7,0.7c1.2-0.9,2.4-1.8,3.6-2.6 C-186.9,255.9-188.5,255.2-190.6,255.2z"/>
                                                    <path class="st0" d="M-185.8,257.1c-1.2,0.9-2.4,1.7-3.6,2.6c0.2,0.3,0.4,0.7,0.4,1.2c0,1.2-0.9,1.8-2.1,1.8h-2.4 c-1.9,1.4-3.8,2.8-5.7,4.2v5.5h4.5v-5.8h4.2c3.9,0,6-2.6,6-5.7C-184.5,259.5-185,258.2-185.8,257.1z"/>
                                                    <path class="st0" d="M-183.5,255.2v0.3c0.1-0.1,0.3-0.2,0.4-0.3H-183.5z"/>
                                                    <path class="st0" d="M-177.2,272.5h3v-13.4h4.8v-3.9h-5.4C-175.6,260.9-176.4,266.7-177.2,272.5z"/>
                                                    <path class="st0" d="M-183.1,255.2c-0.1,0.1-0.3,0.2-0.4,0.3v3.6h4.9v13.4h1.5c0.7-5.8,1.5-11.6,2.4-17.3H-183.1z"/>
                                                    <path class="st0" d="M-155.1,257v-1.8h-4.5v1.8c-5.4,0.5-8.5,2.9-8.5,6.9c0,4.1,3.1,6.4,8.5,6.9v1.8h4.5v-1.8 c5.4-0.5,8.5-2.8,8.5-6.9C-146.6,259.8-149.8,257.4-155.1,257z M-159.6,266.8c-2.6-0.3-4-1.3-4-2.9c0-1.6,1.3-2.6,4-2.9V266.8z M-155.1,266.8v-5.8c2.6,0.3,4,1.3,4,2.9C-151.2,265.5-152.5,266.4-155.1,266.8z"/>
                                                    <path class="st0" d="M-126.7,263.8c0-5.3-4-9-9.3-9c-0.8,0-1.5,0.1-2.3,0.2c0.2,1.3,0.3,2.7,0.5,4c0.5-0.2,1.1-0.3,1.7-0.3 c2.9,0,4.7,2.2,4.7,5c0,2.8-1.8,5-4.7,5c-0.1,0-0.2,0-0.3,0c0.2,1.3,0.4,2.6,0.7,4C-130.5,272.7-126.7,269-126.7,263.8z"/>
                                                    <path class="st0" d="M-140.7,263.8c0-2.2,1.1-4,3-4.7c-0.2-1.3-0.4-2.7-0.5-4c-4.1,0.9-7,4.2-7,8.7c0,5.3,4,9,9.3,9 c0.1,0,0.2,0,0.3,0c-0.2-1.3-0.4-2.7-0.7-4C-139,268.6-140.7,266.5-140.7,263.8z"/>
                                                    <path class="st0" d="M-108.3,272.5v-17.3h-5c1.5,5.8,3,11.6,4.5,17.3H-108.3z"/>
                                                    <path class="st0" d="M-108.7,272.5c-1.5-5.8-3-11.6-4.5-17.3h-8l-1,8.8c-0.5,4-1.5,4.9-3.1,4.9v4c3.8,0,6.8-1.5,7.6-8.5l0.6-5.2 h4.4v13.4H-108.7z"/>
                                                    <path class="st0" d="M-100.7,255.2h-1.5c0.5,1.5,1,2.9,1.5,4.4V255.2z"/>
                                                    <path class="st0" d="M-93.7,255.2l-5.7,8.3c0.5,1.5,1,3.1,1.5,4.6l4.3-6.3v10.7h4.5v-17.3H-93.7z"/>
                                                    <path class="st0" d="M-97.8,268.1c-0.5-1.5-1-3.1-1.5-4.6l-1.3,1.9v-5.8c-0.5-1.5-1-2.9-1.5-4.4h-3v17.3h4.3L-97.8,268.1z"/>
                                                    <path class="st0" d="M-68.3,263.8c0-3.6-1.8-6.5-4.7-7.9c1,4.6,1.8,9.2,2.5,13.9C-69.1,268.3-68.3,266.2-68.3,263.8z"/>
                                                    <path class="st0" d="M-73,255.9c-1.3-0.7-2.9-1-4.5-1c-5.3,0-9.3,3.7-9.3,9s4,9,9.3,9c2.9,0,5.4-1.1,7.1-3 C-71.2,265.2-72,260.5-73,255.9z M-77.5,268.9c-2.9,0-4.7-2.2-4.7-5c0-2.8,1.8-5,4.7-5c2.9,0,4.7,2.2,4.7,5 C-72.8,266.6-74.7,268.9-77.5,268.9z"/>
                                                </g>
									</svg>
                                        </a>
                                    <?php } ?>
                                    <a id="contact_us_but" href="#"  class="mobile-contact-us">
                                        <svg version="1.1" id="sloy_3" xmlns:x="&ns_extend;" xmlns:i="&ns_ai;" xmlns:graph="&ns_graphs;"
                                             xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-808.2 427.2 249 62.8"
                                             style="enable-background:new -808.2 427.2 249 62.8;" xml:space="preserve">

										<switch>

                                            <g i:extraneous="self">
                                                <polygon class="st0" points="-774.2,427.2 -772.7,432.2 -749.7,432.2 -745.8,427.2 		"/>
                                                <polygon class="st0" points="-735.5,427.2 -730.5,432.2 -634.7,432.2 -640.2,427.2 		"/>
                                                <polygon class="st0" points="-698.8,485 -696.2,490 -643,490 -645.9,485 		"/>
                                                <polygon class="st0" points="-592.3,485 -630.2,485 -629,490 -592.4,490 		"/>
                                                <polygon class="st0" points="-655,485 -651.6,490 -620.1,490 -621.3,485 		"/>
                                                <polygon class="st0" points="-586.2,427.2 -664.3,427.2 -662.5,432.2 -586.3,432.2 		"/>
                                                <polygon class="st0" points="-705.8,427.2 -700.8,432.2 -617.4,432.2 -619.2,427.2 		"/>
                                                <polygon class="st0" points="-567.1,427.2 -577.1,427.2 -577.1,432.2 -563.8,432.2 		"/>
                                                <polygon class="st0" points="-618.4,427.2 -618.5,432.2 -566.7,432.2 -566.6,427.2 		"/>
                                                <polygon class="st0" points="-597.3,485 -597.3,490 -573,490 -573,485 		"/>
                                                <polygon class="st0" points="-559.2,446.9 -564.1,427.2 -564.1,461.6 -559.2,462.7 		"/>
                                                <polygon class="st0" points="-564.1,454.1 -564.1,485 -576.3,485 -576.3,490 -564.1,490 -559.8,490 -559.2,490 -559.2,454.7 		"/>
                                                <polygon class="st0" points="-559.2,427.2 -559.8,427.2 -564.1,427.2 -568.8,427.2 -566.3,432.2 -564.1,432.2 -564.1,436.8
																			 -559.2,446.8 		"/>
                                                <path class="st0" d="M-690.9,485h-53.8c0,1.7,0,3.3,0,5h56.3L-690.9,485z"/>
                                                <path class="st0" d="M-769.2,485l1.4,5h29.8c0-1.7,0-3.3,0-5H-769.2z"/>
                                                <path class="st0" d="M-761.5,427.2l-10.9,5h77.9c-0.1-1.7-0.2-3.3-0.2-5H-761.5z"/>
                                                <path class="st0" d="M-716.5,427.2h-21.8c0,1.7,0,3.3,0.1,5h24L-716.5,427.2z"/>
                                                <polygon class="st0" points="-803.4,432.2 -799.9,432.2 -798,427.2 -803.4,427.2 -808.2,427.2 -808.2,432.2 -808.2,433.6
																			 -803.4,434.3 		"/>
                                                <polygon class="st0" points="-808.2,427.2 -808.2,467.4 -803.4,469.4 -803.4,433.3 		"/>
                                                <polygon class="st0" points="-808.2,438.5 -808.2,454.1 -803.4,441.4 -803.4,438.7 		"/>
                                                <polygon class="st0" points="-745,427.2 -800.2,427.2 -804.7,432.2 -742.5,432.2 		"/>
                                                <polygon class="st0" points="-762.3,485 -791.4,485 -794.7,490 -760.9,490 		"/>
                                                <polygon class="st0" points="-803.4,485 -803.4,441.4 -808.2,454.1 -808.2,485 -808.2,490 -803.4,490 -787.9,490 -785.2,485 		"/>
                                                <path class="st0" d="M-762.2,457.7c0-4.9,3.8-8.3,8.8-8.3c4.2,0,6.3,2.4,7.3,4.7l-4.2,1.9c-0.4-1.2-1.7-2.3-3.1-2.3
																	 c-2.3,0-3.9,1.8-3.9,4c0,2.2,1.6,4,3.9,4c1.4,0,2.7-1.1,3.1-2.3l4.2,1.9c-0.9,2.1-3.1,4.7-7.3,4.7
																	 C-758.4,466-762.2,462.6-762.2,457.7z"/>
                                                <path class="st0" d="M-745.5,457.7c0-4.9,3.8-8.3,8.8-8.3s8.8,3.4,8.8,8.3c0,4.9-3.8,8.3-8.8,8.3S-745.5,462.6-745.5,457.7z
																	 M-732.9,457.7c0-2.2-1.5-4-3.8-4s-3.8,1.8-3.8,4c0,2.2,1.5,4,3.8,4S-732.9,459.9-732.9,457.7z"/>
                                                <path class="st0" d="M-715.5,465.7l-5.9-8.1v8.1h-4.8v-16h5l5.6,7.6v-7.6h4.8v16H-715.5z"/>
                                                <path class="st0" d="M-704.8,465.7v-11.8h-4.3v-4.2h13.4v4.2h-4.3v11.8H-704.8z"/>
                                                <path class="st0" d="M-683.3,465.7l-0.6-1.9h-5.8l-0.6,1.9h-5.5l5.9-16h6.1l5.9,16H-683.3z M-686.8,454.4l-1.6,5.2h3.2
																	 L-686.8,454.4z"/>
                                                <path class="st0" d="M-677.8,457.7c0-4.9,3.8-8.3,8.8-8.3c4.2,0,6.3,2.4,7.3,4.7l-4.2,1.9c-0.4-1.2-1.7-2.3-3.1-2.3
																	 c-2.3,0-3.9,1.8-3.9,4c0,2.2,1.6,4,3.9,4c1.4,0,2.7-1.1,3.1-2.3l4.2,1.9c-0.9,2.1-3.1,4.7-7.3,4.7
																	 C-674,466-677.8,462.6-677.8,457.7z"/>
                                                <path class="st0" d="M-656.8,465.7v-11.8h-4.3v-4.2h13.4v4.2h-4.3v11.8H-656.8z"/>
                                                <path class="st0" d="M-639.8,459.1v-9.4h4.9v9.3c0,1.5,1,2.8,3,2.8c2,0,3-1.2,3-2.8v-9.3h4.9v9.4c0,4-2.5,6.9-7.9,6.9
																	 S-639.8,463.1-639.8,459.1z"/>
                                                <path class="st0" d="M-622.6,463.4l2.5-3.6c1.2,1.2,3,2.1,5.2,2.1c0.9,0,1.8-0.2,1.8-0.9c0-1.5-8.9,0-8.9-6.3
																	 c0-2.7,2.3-5.3,6.6-5.3c2.6,0,5,0.7,6.8,2.2l-2.6,3.4c-1.4-1.1-3.2-1.6-4.7-1.6c-1,0-1.2,0.3-1.2,0.7c0,1.5,8.9,0.2,8.9,6.2
																	 c0,3.5-2.6,5.6-6.9,5.6C-618.6,466-620.9,465-622.6,463.4z"/>
                                            </g>
                                        </switch>
									</svg>
                                    </a>

                                </div>
                                <div class="arrow-down">
                                    <a href="#" data-scroll="scroll-bottom"><i class="fa fa-angle-double-down"></i></a>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

    </header>
   <?php  } else { ?>
	<header class="header size <?php if( is_page_template( 'template-main.php' )){echo 'scroll';} ?>" id="<?php if(is_page_template( 'template-main.php' )){echo 'scroll-top';} ?>" data-anchor="<?php if( is_page_template( 'template-main.php' )){echo 'scroll-top';} ?>">
    <?php if( is_page_template( 'template-main.php' ) ){
	echo '<video autoplay loop muted poster="" id="bg-video" >
            <source src="'. esc_url(get_theme_mod("themeslug_video")) .'" type="video/mp4">
            </video>'
        .'<video autoplay loop muted poster="" id="bg-vertical-video" >
            <source src="'. esc_url(get_theme_mod("themeslug_mobile_vert")) .'" type="video/webm">
            </video>'
        .'<video autoplay loop muted poster="" id="bg-gorizontal-video" >
            <source src="'. esc_url(get_theme_mod("themeslug_mobile_goriz")) .'" type="video/webm">
            </video>';
    } ?>
	<div class="wrapper size">

        <i class="fa fa-bars burger-menu" aria-hidden="true"></i>
        <?php if( is_page_template( 'template-main.php' ) ){ ?>
            <div class="logo-mobile">
               <a href="<?= get_home_url() ?>"><img src="<?= esc_url(get_theme_mod('themeslug_logo')); ?>"alt="<?= esc_attr(get_bloginfo('name', 'display')); ?>"></a>
            </div>
        <?php }  ?>

        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 size">

                    <?php if( is_page_template( 'template-main.php' ) ){
                        if (get_theme_mod('themeslug_logo')) : ?>
                            <div class="logo">
                                <a href="<?= get_home_url() ?>"><img src="<?= esc_url(get_theme_mod('themeslug_logo')); ?>"alt="<?= esc_attr(get_bloginfo('name', 'display')); ?>"></a>
                            </div>
                        <?php endif; ?>

                       <?php  echo main_site_menu('main_menu'); }
//                    if(is_page_template( 'template-portfolio.php' ))
                    else
                    {
                        if (get_theme_mod('themeslug_logo_color')) : ?>
                            <div class="logo">
                                <a href="<?= get_home_url() ?>"><img src="<?= esc_url(get_theme_mod('themeslug_logo_color')); ?>"alt="<?= esc_attr(get_bloginfo('name', 'display')); ?>"></a>
                            </div>
                        <?php endif; ?>
                        <?php  echo main_site_menu('sub_menu'); ?>
                    <?php } // End header image check. ?>
                    <div class="write-side">
                        <div class="language">
                            <div class="language-menu">
                                <?php icl_post_languages() ?>
                            </div>
                        </div>

                        <div class="contact-us">
                            <a id="contact_us_but" href="#"  class="button"><?php _e('Contact us','buttons')?></a>

                            <ul>
                                <li><a href="<?= get_option("freelancer_link"); ?>"><img src="<?php if(is_page_template( 'template-main.php' )){ echo get_template_directory_uri() .'/images/freelancer.png';}else{ echo get_template_directory_uri().'/images/freelancer.com.png'; } ?>"
                                                                                         target="_blank" alt="#" ></a></li>
                                <li><a href="<?= get_option("upwork_link"); ?>"><img src="<?php if(is_page_template( 'template-main.php' )){ echo get_template_directory_uri() .'/images/upwork.png'; } else { echo get_template_directory_uri() .'/images/upwork-2.png'; } ?>"
                                                     alt="#" target="_blank"></a></li>
                            </ul>

                        </div>
                    </div>
                    <?php if( is_page_template( 'template-main.php' )){ ?>
                        <div class="button-main">
                            <div class="slide-button">
                                <!--                        <h1>-->
                                <? //= get_field('site_slogan', get_the_ID())?><!--</h1>-->
                                <?php if (ICL_LANGUAGE_CODE == 'en') { ?>
                                <a href="<?= get_permalink(1746); ?>" class="button">
                                    <svg version="1.1" id="sloy" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="-547.9 225.3 248.5 62.8" style="enable-background:new -547.9 225.3 248.5 62.8;" xml:space="preserve">
										<style type="text/css">
											.st0{fill:#fff;}
											.header .slide-button .button:hover .st0{fill:#d4574a; stroke: #d4574a;}
										</style>
                                        <g>
                                            <g>
                                                <path class="st0" d="M-494.7,247.2h-0.3v17.3h4.5v-3.1C-491.9,256.7-493.2,251.9-494.7,247.2z"/>
                                                <path class="st0" d="M-490.5,258.7h4.2c3.9,0,6-2.6,6-5.7c0-3.1-2.1-5.8-6-5.8h-8.4c1.5,4.7,2.8,9.5,4.2,14.2V258.7z M-490.5,251.1h3.6c1.2,0,2.1,0.7,2.1,1.9c0,1.2-0.9,1.8-2.1,1.8h-3.6V251.1z"/>
                                                <path class="st0" d="M-469.9,246.9c-0.6,0-1.3,0.1-1.9,0.2c0,1.4,0,2.8,0.1,4.1c0.5-0.2,1.1-0.3,1.8-0.3c2.9,0,4.7,2.2,4.7,5 c0,2.8-1.8,5-4.7,5c-0.6,0-1.1-0.1-1.6-0.3c0,1.4,0.1,2.7,0.1,4.1c0.5,0.1,1,0.1,1.5,0.1c5.3,0,9.3-3.7,9.3-9 C-460.6,250.6-464.6,246.9-469.9,246.9z"/>
                                                <path class="st0" d="M-471.5,260.6c-1.9-0.7-3.1-2.5-3.1-4.7c0-2.2,1.1-4,2.9-4.7c0-1.4-0.1-2.8-0.1-4.1c-4.3,0.8-7.4,4.2-7.4,8.8 c0,4.8,3.2,8.2,7.8,8.9C-471.4,263.4-471.5,262-471.5,260.6z"/>
                                                <path class="st0" d="M-453.9,254.8v-3.7h3.1c0.2-1.3,0.4-2.6,0.6-3.9h-8.2v17.3h4.5v-5.8h1.9c0.2-1.3,0.4-2.6,0.6-3.9H-453.9z"/>
                                                <path class="st0" d="M-447.1,258.2c1.6-0.6,3.5-2.2,3.5-5.2c0-3.2-2.2-5.8-6-5.8h-0.5c-0.2,1.3-0.4,2.6-0.6,3.9h0.4 c1.2,0,2.2,0.7,2.2,1.8c0,1.2-1,1.9-2.2,1.9h-1c-0.2,1.3-0.4,2.6-0.6,3.9h0.4l2.9,5.8h5.1L-447.1,258.2z"/>
                                                <path class="st0" d="M-432.9,251.1h4.8v-3.9h-5.5c0.2,1.5,0.5,2.9,0.7,4.4V251.1z"/>
                                                <path class="st0" d="M-437.3,264.6h4.5v-12.9c-0.2-1.5-0.5-2.9-0.7-4.4h-8.7v3.9h4.9V264.6z"/>
                                                <path class="st0" d="M-425.9,264.6h4.5v-6.8h8.1v-3.9h-7.2c-1.8,3.2-3.5,6.3-5.3,9.5V264.6z"/>
                                                <path class="st0" d="M-425.9,247.2v16.1c1.8-3.1,3.5-6.3,5.3-9.5h-0.8v-2.7h2.3c0.7-1.3,1.4-2.6,2.2-3.9H-425.9z"/>
                                                <path class="st0" d="M-419.1,251.1h5.9v-3.9h-3.8C-417.7,248.5-418.4,249.8-419.1,251.1z"/>
                                                <path class="st0" d="M-402.4,246.9c-2.2,0-4.1,0.6-5.6,1.7c1,1,2,2,3,3c0.7-0.5,1.6-0.8,2.6-0.8c2.9,0,4.7,2.2,4.7,5 c0,0.9-0.2,1.7-0.5,2.4c1.1,1,2.2,2.1,3.3,3.1c1.2-1.5,1.8-3.4,1.8-5.6C-393.1,250.6-397.1,246.9-402.4,246.9z"/>
                                                <path class="st0" d="M-402.4,260.9c-2.9,0-4.7-2.2-4.7-5c0-1.8,0.8-3.4,2.1-4.3c-1-1-2-2-3-3c-2.2,1.6-3.6,4.1-3.6,7.3 c0,5.3,4,9,9.3,9c3.1,0,5.8-1.3,7.4-3.4c-1.1-1-2.2-2.1-3.3-3.1C-398.9,259.9-400.4,260.9-402.4,260.9z"/>
                                                <path class="st0" d="M-379.4,264.6v-3.5c-1.5,1.2-3,2.3-4.5,3.5H-379.4z"/>
                                                <path class="st0" d="M-386.4,260.7v-13.4h-4.5v17.3h7c1.5-1.2,3-2.3,4.5-3.5v-0.4H-386.4z"/>
                                                <path class="st0" d="M-377.1,264.6h4.5v-8.9c-1.5,1.2-3,2.4-4.5,3.6V264.6z"/>
                                                <path class="st0" d="M-377.1,259.2c1.5-1.2,3-2.4,4.5-3.6v-8.4h-4.5V259.2z"/>
                                                <path class="st0" d="M-370.1,253.6c2.7-2.2,5.4-4.4,8.1-6.7C-366.1,247.3-369.2,249.9-370.1,253.6z"/>
                                                <path class="st0" d="M-361.1,246.9c-0.3,0-0.6,0-0.9,0c-2.7,2.2-5.4,4.5-8.1,6.7c-0.2,0.7-0.3,1.5-0.3,2.3c0,5.3,4,9,9.3,9 c5.3,0,9.3-3.7,9.3-9C-351.8,250.6-355.8,246.9-361.1,246.9z M-361.1,260.9c-2.9,0-4.7-2.2-4.7-5c0-2.8,1.8-5,4.7-5 c2.9,0,4.7,2.2,4.7,5C-356.4,258.7-358.2,260.9-361.1,260.9z"/>
                                            </g>
                                            <g>
                                                <path class="st0" d="M-547.9,288.1h4V263c-1.3,0.9-2.7,1.8-4,2.7V288.1z"/>
                                                <path class="st0" d="M-547.9,265.7c1.3-0.9,2.7-1.8,4-2.7v-17c-1.3-1.3-2.6-2.5-4-3.7V265.7z"/>
                                                <path class="st0" d="M-547.9,227.7v14.6c1.4,1.2,2.7,2.5,4,3.7v-18.3H-547.9z"/>
                                                <rect x="-303.4" y="227.7" class="st0" width="4" height="60.4"/>
                                                <path class="st0" d="M-494.5,229.3c1.9-1.3,3.9-2.7,5.8-4h-14.3c0.6,1.3,1.2,2.7,1.8,4H-494.5z"/>
                                                <path class="st0" d="M-503.1,225.3h-23.5c0.7,1.3,1.4,2.7,2.1,4h23.2C-501.9,228-502.4,226.7-503.1,225.3z"/>
                                                <path class="st0" d="M-526.6,225.3h-21.4v4h23.5C-525.2,228-525.9,226.7-526.6,225.3z"/>
                                                <path class="st0" d="M-488.8,225.3c-1.9,1.3-3.9,2.7-5.8,4h22.7c0-1.3,0-2.7,0.1-4H-488.8z"/>
                                                <path class="st0" d="M-447.5,225.3h-24.3c0,1.3-0.1,2.7-0.1,4h23.9C-447.8,228-447.6,226.7-447.5,225.3z"/>
                                                <path class="st0" d="M-299.4,225.3h-36.8c-1.6,1.3-3.2,2.7-4.7,4h41.5V225.3z"/>
                                                <path class="st0" d="M-429.2,225.3c1.1,1.3,2.2,2.7,3.4,4h18.6c0.7-1.3,1.5-2.7,2.2-4H-429.2z"/>
                                                <path class="st0" d="M-375,225.3h-30c-0.7,1.3-1.5,2.7-2.2,4h40.3C-369.6,228-372.3,226.6-375,225.3z"/>
                                                <path class="st0" d="M-340.9,229.3c1.6-1.3,3.2-2.7,4.7-4H-375c2.7,1.3,5.5,2.6,8.2,4H-340.9z"/>
                                                <path class="st0" d="M-437.2,225.3h-10.2c-0.1,1.3-0.3,2.7-0.4,4h11.4C-436.8,228-437,226.7-437.2,225.3z"/>
                                                <path class="st0" d="M-425.8,229.3c-1.1-1.3-2.3-2.7-3.4-4h-8.1c0.2,1.3,0.5,2.7,0.7,4H-425.8z"/>
                                                <path class="st0" d="M-547.9,284.1v4h34.2c-0.9-1.3-1.8-2.7-2.7-4H-547.9z"/>
                                                <path class="st0" d="M-516.5,284.1c0.9,1.3,1.8,2.7,2.7,4h18.2c-0.6-1.3-1.2-2.7-1.9-4H-516.5z"/>
                                                <path class="st0" d="M-497.4,284.1c0.6,1.3,1.2,2.7,1.9,4h12.6c-0.4-1.3-0.8-2.7-1.2-4H-497.4z"/>
                                                <path class="st0" d="M-471.1,284.1h-13.1c0.4,1.3,0.8,2.7,1.2,4h11.9C-471.1,286.8-471.1,285.5-471.1,284.1z"/>
                                                <path class="st0" d="M-456.1,284.1h-15c0,1.3,0,2.7,0,4h14.4C-456.6,286.8-456.4,285.4-456.1,284.1z"/>
                                                <path class="st0" d="M-370.2,284.1h-40.3c-2,1.3-3.9,2.7-5.9,4h50.7C-367.2,286.8-368.7,285.5-370.2,284.1z"/>
                                                <path class="st0" d="M-335.1,288.1h35.7v-4H-329C-331.1,285.4-333.1,286.8-335.1,288.1z"/>
                                                <path class="st0" d="M-365.7,288.1h30.6c2-1.3,4.1-2.7,6.1-4h-41.2C-368.7,285.5-367.2,286.8-365.7,288.1z"/>
                                                <path class="st0" d="M-456.1,284.1c-0.2,1.3-0.4,2.7-0.6,4h16.4c0.8-1.3,1.6-2.7,2.4-4H-456.1z"/>
                                                <path class="st0" d="M-428.1,284.1h-9.8c-0.8,1.3-1.6,2.7-2.4,4h12.8C-427.8,286.8-428,285.5-428.1,284.1z"/>
                                                <path class="st0" d="M-427.6,288.1h11.2c2-1.3,4-2.7,5.9-4h-17.7C-428,285.5-427.8,286.8-427.6,288.1z"/>
                                            </g>
                                        </g>
									</svg>
                                </a>
                                <?php } else { ?>
                                <a href="<?= get_permalink(1845); ?>" class="button ru">
									<svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
										 viewBox="-278.5 233.2 248.5 62.8" style="enable-background:new -278.5 233.2 248.5 62.8;" xml:space="preserve">
										<style type="text/css">
											.st0{fill:#fff;}
											.header .slide-button .button:hover .st0{fill:#d4574a; stroke: #d4574a;}
										</style>
                                        <g>
                                            <g>
                                                <path class="st0" d="M-274.5,235.5h-4v22.6c1.3-0.9,2.7-1.7,4-2.6V235.5z"/>
                                                <path class="st0" d="M-278.5,280.1v15.9h4v-13C-275.9,282-277.2,281-278.5,280.1z"/>
                                                <path class="st0" d="M-274.5,255.5c-1.3,0.9-2.7,1.7-4,2.6v21.9c1.3,1,2.6,2,4,2.9V255.5z"/>
                                                <path class="st0" d="M-34,264.6v31.4h4v-24.1C-31.3,269.5-32.7,267-34,264.6z"/>
                                                <path class="st0" d="M-30,235.5h-4v29c1.3,2.4,2.7,4.9,4,7.3V235.5z"/>
                                                <path class="st0" d="M-236.8,233.2h-41.7v4h34.4C-241.7,235.8-239.3,234.5-236.8,233.2z"/>
                                                <path class="st0" d="M-222,233.2h-14.8c-2.4,1.3-4.9,2.6-7.3,4h21.4C-222.5,235.8-222.3,234.5-222,233.2z"/>
                                                <path class="st0" d="M-181.7,233.2c0.6,1.3,1.3,2.7,1.9,4h7.7c0.2-1.3,0.4-2.7,0.6-4H-181.7z"/>
                                                <path class="st0" d="M-150.8,233.2h-20.7c-0.2,1.3-0.4,2.7-0.6,4h15.1C-154.9,235.8-152.9,234.5-150.8,233.2z"/>
                                                <path class="st0" d="M-207.2,233.2c-0.4,1.3-0.9,2.7-1.4,4h9.4c0.3-1.3,0.7-2.7,1-4H-207.2z"/>
                                                <path class="st0" d="M-181.7,233.2h-16.5c-0.4,1.3-0.7,2.7-1,4h19.4C-180.4,235.8-181.1,234.5-181.7,233.2z"/>
                                                <path class="st0" d="M-207.2,233.2H-222c-0.2,1.3-0.5,2.7-0.8,4h14.2C-208.1,235.8-207.7,234.5-207.2,233.2z"/>
                                                <path class="st0" d="M-150.8,233.2c-2.1,1.3-4.1,2.7-6.1,4h17c-0.1-1.3-0.2-2.7-0.3-4H-150.8z"/>
                                                <path class="st0" d="M-130.7,233.2h-9.6c0.1,1.3,0.2,2.7,0.3,4h10.3C-130,235.8-130.4,234.5-130.7,233.2z"/>
                                                <path class="st0" d="M-108.8,237.2c-0.5-1.3-1.1-2.7-1.6-4h-8.8c0.4,1.3,0.8,2.7,1.1,4H-108.8z"/>
                                                <path class="st0" d="M-119.3,233.2h-11.4c0.3,1.3,0.7,2.7,1,4h11.5C-118.5,235.8-118.9,234.5-119.3,233.2z"/>
                                                <path class="st0" d="M-79.2,233.2h-31.2c0.5,1.3,1.1,2.7,1.6,4h16.3c1.8-1.1,3.6-2.2,5.5-3.2c-1.9,1-3.7,2.1-5.5,3.2h14.6 C-78.3,235.8-78.8,234.5-79.2,233.2z"/>
                                                <path class="st0" d="M-62.2,233.2h-17c0.5,1.3,0.9,2.7,1.3,4h17.3c-0.2-0.4-0.3-0.8-0.4-1.2C-61.4,235.1-61.8,234.1-62.2,233.2z"/>
                                                <path class="st0" d="M-30,237.2v-4h-22.3c0.9,1.3,1.8,2.7,2.7,4H-30z"/>
                                                <path class="st0" d="M-52.3,233.2h-9.9c0.4,1,0.9,1.9,1.2,2.8c0.2,0.4,0.3,0.8,0.4,1.2h10.9C-50.5,235.8-51.4,234.5-52.3,233.2z"/>
                                                <path class="st0" d="M-186.2,291.9c-3,1.3-6.1,2.7-9.2,4h15.6c0.1-1.3,0.2-2.7,0.4-4H-186.2z"/>
                                                <path class="st0" d="M-154.2,291.9h-25.2c-0.1,1.3-0.2,2.7-0.4,4h26.7C-153.4,294.6-153.8,293.3-154.2,291.9z"/>
                                                <path class="st0" d="M-186.2,291.9h-28.7c-0.2,1.3-0.3,2.7-0.3,4h19.9C-192.3,294.6-189.2,293.3-186.2,291.9z"/>
                                                <path class="st0" d="M-230,291.9c-0.4,1.3-0.9,2.7-1.3,4h16.1c0-1.3,0.1-2.7,0.3-4H-230z"/>
                                                <path class="st0" d="M-278.5,291.9v4h47.2c0.4-1.3,0.8-2.7,1.3-4H-278.5z"/>
                                                <path class="st0" d="M-154.2,291.9c0.4,1.3,0.8,2.7,1.2,4h21.6c-0.1-1.3-0.3-2.7-0.5-4H-154.2z"/>
                                                <path class="st0" d="M-131.9,291.9c0.3,1.3,0.4,2.7,0.5,4h21.3c-0.5-1.3-1-2.7-1.6-4H-131.9z"/>
                                                <path class="st0" d="M-111.6,291.9c0.5,1.3,1,2.7,1.6,4h7.9c-0.4-1.3-0.8-2.7-1.2-4H-111.6z"/>
                                                <path class="st0" d="M-90.2,291.9h-13.2c0.4,1.3,0.8,2.7,1.2,4h13.3C-89.3,294.6-89.8,293.3-90.2,291.9z"/>
                                                <path class="st0" d="M-90.2,291.9c0.4,1.3,0.9,2.7,1.3,4h21.7c-0.1-1.3-0.3-2.7-0.4-4H-90.2z"/>
                                                <path class="st0" d="M-67.6,291.9c0.1,1.3,0.3,2.7,0.4,4h22.1c-0.2-1.3-0.5-2.7-0.7-4H-67.6z"/>
                                                <path class="st0" d="M-45.1,295.9H-30v-4h-15.8C-45.6,293.3-45.3,294.6-45.1,295.9z"/>
										    </g>
                                            <path class="st0" d="M-234,259.1h5.9c0.4-1.3,0.7-2.6,1.1-3.9h-11.5v17.3h4.5V259.1z"/>
                                            <path class="st0" d="M-222.3,271.3c-0.2,0.4-0.3,0.8-0.5,1.2h0.5V271.3z"/>
                                            <path class="st0" d="M-222.3,255.2h-4.7c-0.4,1.3-0.7,2.6-1.1,3.9h1.3v13.4h4c0.2-0.4,0.3-0.8,0.5-1.2V255.2z"/>
                                            <path class="st0" d="M-220.1,263.8c0,0.6,0.1,1.2,0.2,1.7c1.3-3.1,2.6-6.1,3.8-9.2C-218.5,257.9-220.1,260.6-220.1,263.8z"/>
                                            <path class="st0" d="M-204.2,257.3c-0.6,2.4-1.2,4.7-1.9,7.1c-0.1,0.7-0.3,1.4-0.6,2c-0.6,2.1-1.2,4.1-1.8,6.2c4.1-0.9,7-4.2,7-8.7 C-201.5,261.2-202.5,258.9-204.2,257.3z"/>
                                            <path class="st0" d="M-210.8,268.9c-2.9,0-4.7-2.2-4.7-5c0-2.8,1.8-5,4.7-5c2.9,0,4.7,2.2,4.7,5c0,0.2,0,0.4,0,0.6 c0.6-2.4,1.3-4.7,1.9-7.1c-1.6-1.5-3.9-2.4-6.5-2.4c-2,0-3.8,0.5-5.3,1.5c-1.3,3.1-2.6,6.2-3.8,9.2c0.8,4.3,4.4,7.3,9.1,7.3 c0.8,0,1.6-0.1,2.3-0.3c0.6-2.1,1.2-4.1,1.8-6.2C-207.4,267.9-208.9,268.9-210.8,268.9z"/>
                                            <path class="st0" d="M-190.6,255.2h-8.7V267c1.9-1.4,3.8-2.8,5.7-4.2h-1.2v-3.7h3.6c0.7,0,1.3,0.3,1.7,0.7c1.2-0.9,2.4-1.8,3.6-2.6 C-186.9,255.9-188.5,255.2-190.6,255.2z"/>
                                            <path class="st0" d="M-185.8,257.1c-1.2,0.9-2.4,1.7-3.6,2.6c0.2,0.3,0.4,0.7,0.4,1.2c0,1.2-0.9,1.8-2.1,1.8h-2.4 c-1.9,1.4-3.8,2.8-5.7,4.2v5.5h4.5v-5.8h4.2c3.9,0,6-2.6,6-5.7C-184.5,259.5-185,258.2-185.8,257.1z"/>
                                            <path class="st0" d="M-183.5,255.2v0.3c0.1-0.1,0.3-0.2,0.4-0.3H-183.5z"/>
                                            <path class="st0" d="M-177.2,272.5h3v-13.4h4.8v-3.9h-5.4C-175.6,260.9-176.4,266.7-177.2,272.5z"/>
                                            <path class="st0" d="M-183.1,255.2c-0.1,0.1-0.3,0.2-0.4,0.3v3.6h4.9v13.4h1.5c0.7-5.8,1.5-11.6,2.4-17.3H-183.1z"/>
                                            <path class="st0" d="M-155.1,257v-1.8h-4.5v1.8c-5.4,0.5-8.5,2.9-8.5,6.9c0,4.1,3.1,6.4,8.5,6.9v1.8h4.5v-1.8 c5.4-0.5,8.5-2.8,8.5-6.9C-146.6,259.8-149.8,257.4-155.1,257z M-159.6,266.8c-2.6-0.3-4-1.3-4-2.9c0-1.6,1.3-2.6,4-2.9V266.8z M-155.1,266.8v-5.8c2.6,0.3,4,1.3,4,2.9C-151.2,265.5-152.5,266.4-155.1,266.8z"/>
                                            <path class="st0" d="M-126.7,263.8c0-5.3-4-9-9.3-9c-0.8,0-1.5,0.1-2.3,0.2c0.2,1.3,0.3,2.7,0.5,4c0.5-0.2,1.1-0.3,1.7-0.3 c2.9,0,4.7,2.2,4.7,5c0,2.8-1.8,5-4.7,5c-0.1,0-0.2,0-0.3,0c0.2,1.3,0.4,2.6,0.7,4C-130.5,272.7-126.7,269-126.7,263.8z"/>
                                            <path class="st0" d="M-140.7,263.8c0-2.2,1.1-4,3-4.7c-0.2-1.3-0.4-2.7-0.5-4c-4.1,0.9-7,4.2-7,8.7c0,5.3,4,9,9.3,9 c0.1,0,0.2,0,0.3,0c-0.2-1.3-0.4-2.7-0.7-4C-139,268.6-140.7,266.5-140.7,263.8z"/>
                                            <path class="st0" d="M-108.3,272.5v-17.3h-5c1.5,5.8,3,11.6,4.5,17.3H-108.3z"/>
                                            <path class="st0" d="M-108.7,272.5c-1.5-5.8-3-11.6-4.5-17.3h-8l-1,8.8c-0.5,4-1.5,4.9-3.1,4.9v4c3.8,0,6.8-1.5,7.6-8.5l0.6-5.2 h4.4v13.4H-108.7z"/>
                                            <path class="st0" d="M-100.7,255.2h-1.5c0.5,1.5,1,2.9,1.5,4.4V255.2z"/>
                                            <path class="st0" d="M-93.7,255.2l-5.7,8.3c0.5,1.5,1,3.1,1.5,4.6l4.3-6.3v10.7h4.5v-17.3H-93.7z"/>
                                            <path class="st0" d="M-97.8,268.1c-0.5-1.5-1-3.1-1.5-4.6l-1.3,1.9v-5.8c-0.5-1.5-1-2.9-1.5-4.4h-3v17.3h4.3L-97.8,268.1z"/>
                                            <path class="st0" d="M-68.3,263.8c0-3.6-1.8-6.5-4.7-7.9c1,4.6,1.8,9.2,2.5,13.9C-69.1,268.3-68.3,266.2-68.3,263.8z"/>
                                            <path class="st0" d="M-73,255.9c-1.3-0.7-2.9-1-4.5-1c-5.3,0-9.3,3.7-9.3,9s4,9,9.3,9c2.9,0,5.4-1.1,7.1-3 C-71.2,265.2-72,260.5-73,255.9z M-77.5,268.9c-2.9,0-4.7-2.2-4.7-5c0-2.8,1.8-5,4.7-5c2.9,0,4.7,2.2,4.7,5 C-72.8,266.6-74.7,268.9-77.5,268.9z"/>
									    </g>
									</svg>
                                </a>
                                <?php } ?>
                                <a id="contact_us_but" href="#"  class="mobile-contact-us">
									<svg version="1.1" id="sloy_3" xmlns:x="&ns_extend;" xmlns:i="&ns_ai;" xmlns:graph="&ns_graphs;"
										 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-808.2 427.2 249 62.8"
										 style="enable-background:new -808.2 427.2 249 62.8;" xml:space="preserve">
										
										<switch>
											
											<g i:extraneous="self">
												<polygon class="st0" points="-774.2,427.2 -772.7,432.2 -749.7,432.2 -745.8,427.2 		"/>
												<polygon class="st0" points="-735.5,427.2 -730.5,432.2 -634.7,432.2 -640.2,427.2 		"/>
												<polygon class="st0" points="-698.8,485 -696.2,490 -643,490 -645.9,485 		"/>
												<polygon class="st0" points="-592.3,485 -630.2,485 -629,490 -592.4,490 		"/>
												<polygon class="st0" points="-655,485 -651.6,490 -620.1,490 -621.3,485 		"/>
												<polygon class="st0" points="-586.2,427.2 -664.3,427.2 -662.5,432.2 -586.3,432.2 		"/>
												<polygon class="st0" points="-705.8,427.2 -700.8,432.2 -617.4,432.2 -619.2,427.2 		"/>
												<polygon class="st0" points="-567.1,427.2 -577.1,427.2 -577.1,432.2 -563.8,432.2 		"/>
												<polygon class="st0" points="-618.4,427.2 -618.5,432.2 -566.7,432.2 -566.6,427.2 		"/>
												<polygon class="st0" points="-597.3,485 -597.3,490 -573,490 -573,485 		"/>
												<polygon class="st0" points="-559.2,446.9 -564.1,427.2 -564.1,461.6 -559.2,462.7 		"/>
												<polygon class="st0" points="-564.1,454.1 -564.1,485 -576.3,485 -576.3,490 -564.1,490 -559.8,490 -559.2,490 -559.2,454.7 		"/>
												<polygon class="st0" points="-559.2,427.2 -559.8,427.2 -564.1,427.2 -568.8,427.2 -566.3,432.2 -564.1,432.2 -564.1,436.8 
																			 -559.2,446.8 		"/>
												<path class="st0" d="M-690.9,485h-53.8c0,1.7,0,3.3,0,5h56.3L-690.9,485z"/>
												<path class="st0" d="M-769.2,485l1.4,5h29.8c0-1.7,0-3.3,0-5H-769.2z"/>
												<path class="st0" d="M-761.5,427.2l-10.9,5h77.9c-0.1-1.7-0.2-3.3-0.2-5H-761.5z"/>
												<path class="st0" d="M-716.5,427.2h-21.8c0,1.7,0,3.3,0.1,5h24L-716.5,427.2z"/>
												<polygon class="st0" points="-803.4,432.2 -799.9,432.2 -798,427.2 -803.4,427.2 -808.2,427.2 -808.2,432.2 -808.2,433.6 
																			 -803.4,434.3 		"/>
												<polygon class="st0" points="-808.2,427.2 -808.2,467.4 -803.4,469.4 -803.4,433.3 		"/>
												<polygon class="st0" points="-808.2,438.5 -808.2,454.1 -803.4,441.4 -803.4,438.7 		"/>
												<polygon class="st0" points="-745,427.2 -800.2,427.2 -804.7,432.2 -742.5,432.2 		"/>
												<polygon class="st0" points="-762.3,485 -791.4,485 -794.7,490 -760.9,490 		"/>
												<polygon class="st0" points="-803.4,485 -803.4,441.4 -808.2,454.1 -808.2,485 -808.2,490 -803.4,490 -787.9,490 -785.2,485 		"/>
												<path class="st0" d="M-762.2,457.7c0-4.9,3.8-8.3,8.8-8.3c4.2,0,6.3,2.4,7.3,4.7l-4.2,1.9c-0.4-1.2-1.7-2.3-3.1-2.3
																	 c-2.3,0-3.9,1.8-3.9,4c0,2.2,1.6,4,3.9,4c1.4,0,2.7-1.1,3.1-2.3l4.2,1.9c-0.9,2.1-3.1,4.7-7.3,4.7
																	 C-758.4,466-762.2,462.6-762.2,457.7z"/>
												<path class="st0" d="M-745.5,457.7c0-4.9,3.8-8.3,8.8-8.3s8.8,3.4,8.8,8.3c0,4.9-3.8,8.3-8.8,8.3S-745.5,462.6-745.5,457.7z
																	 M-732.9,457.7c0-2.2-1.5-4-3.8-4s-3.8,1.8-3.8,4c0,2.2,1.5,4,3.8,4S-732.9,459.9-732.9,457.7z"/>
												<path class="st0" d="M-715.5,465.7l-5.9-8.1v8.1h-4.8v-16h5l5.6,7.6v-7.6h4.8v16H-715.5z"/>
												<path class="st0" d="M-704.8,465.7v-11.8h-4.3v-4.2h13.4v4.2h-4.3v11.8H-704.8z"/>
												<path class="st0" d="M-683.3,465.7l-0.6-1.9h-5.8l-0.6,1.9h-5.5l5.9-16h6.1l5.9,16H-683.3z M-686.8,454.4l-1.6,5.2h3.2
																	 L-686.8,454.4z"/>
												<path class="st0" d="M-677.8,457.7c0-4.9,3.8-8.3,8.8-8.3c4.2,0,6.3,2.4,7.3,4.7l-4.2,1.9c-0.4-1.2-1.7-2.3-3.1-2.3
																	 c-2.3,0-3.9,1.8-3.9,4c0,2.2,1.6,4,3.9,4c1.4,0,2.7-1.1,3.1-2.3l4.2,1.9c-0.9,2.1-3.1,4.7-7.3,4.7
																	 C-674,466-677.8,462.6-677.8,457.7z"/>
												<path class="st0" d="M-656.8,465.7v-11.8h-4.3v-4.2h13.4v4.2h-4.3v11.8H-656.8z"/>
												<path class="st0" d="M-639.8,459.1v-9.4h4.9v9.3c0,1.5,1,2.8,3,2.8c2,0,3-1.2,3-2.8v-9.3h4.9v9.4c0,4-2.5,6.9-7.9,6.9
																	 S-639.8,463.1-639.8,459.1z"/>
												<path class="st0" d="M-622.6,463.4l2.5-3.6c1.2,1.2,3,2.1,5.2,2.1c0.9,0,1.8-0.2,1.8-0.9c0-1.5-8.9,0-8.9-6.3
																	 c0-2.7,2.3-5.3,6.6-5.3c2.6,0,5,0.7,6.8,2.2l-2.6,3.4c-1.4-1.1-3.2-1.6-4.7-1.6c-1,0-1.2,0.3-1.2,0.7c0,1.5,8.9,0.2,8.9,6.2
																	 c0,3.5-2.6,5.6-6.9,5.6C-618.6,466-620.9,465-622.6,463.4z"/>
											</g>
										</switch>
									</svg>
                                </a>

                            </div>
                            <div class="arrow-down">
                                <a href="#" data-scroll="scroll-bottom"><i class="fa fa-angle-double-down"></i></a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>

</header>
    <?php } ?>
<!--END HEADER-->